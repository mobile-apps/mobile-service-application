-- Data for table organization
INSERT INTO organization ( organization_id, name, phone, created_date,location,domain, general_information, video_url,email, latitude, longitude)
	VALUES ( 1, 'Wake Tech', '919-839-1234', current_time(),'102 West Street, Raleigh, NC 27605','waketech.edu','Wake Tech Technical College is wonderful.','http://video','johndoe@waketech.edu',35.7814471,-78.6458671 );


INSERT INTO organization ( organization_id, name, phone, created_date,location,domain, general_information, video_url,email, latitude, longitude)
	VALUES ( 2, 'Christ Our King Community Church', '919-839-1234', current_time(),'102 West Street, Raleigh, NC 27605','waketech.edu','Wake Tech Technical College is wonderful.','http://video','johndoe@waketech.edu',35.7814471,-78.6458671 );


-- Data for table department
INSERT INTO department ( department_id,organization_id, name, phone, created_date,location, general_information, email, latitude, longitude)
	VALUES ( 1,1, 'Math','919-839-6667', current_time(),'102 West Street, Raleigh, NC 27605','Wake Tech math department.','johndoe@waketech.edu',35.7814471,-78.6458671 );

INSERT INTO department ( department_id,organization_id, name, phone, created_date,location, general_information, email, latitude, longitude)
	VALUES ( 2,1, 'English', '919-839-6668', current_time(),'102 West Street, Raleigh, NC 27605','Wake Tech math department.','johndoe@waketech.edu',35.7814471,-78.6458671 );

INSERT INTO department ( department_id,organization_id, name, phone, created_date,location, general_information, email, latitude, longitude)
	VALUES ( 3,2, 'Finance','919-839-6669' ,current_time(),'102 West Street, Raleigh, NC 27605','Wake Tech math department.','johndoe@waketech.edu',35.7814471,-78.6458671 );

INSERT INTO department ( department_id,organization_id, name, phone, created_date,location, general_information, email, latitude, longitude)
	VALUES ( 4,2, 'Administration', '919-839-6670', current_time(),'102 West Street, Raleigh, NC 27605','Wake Tech math department.','johndoe@waketech.edu', 35.7814471,-78.6458671 );



-- Data for table category
INSERT INTO category ( category_id, name, created_date)
	VALUES ( 1, 'Courses', current_time() );
INSERT INTO category ( category_id, name, created_date)
	VALUES ( 2, 'Events', current_time() );
INSERT INTO category ( category_id, name, created_date)
	VALUES ( 3, 'Studies', current_time() );
INSERT INTO category ( category_id, name, created_date)
	VALUES ( 4, 'Groups', current_time() );
INSERT INTO category ( category_id, name, created_date)
	VALUES ( 5, 'Video', current_time() );

-- Data for service table
INSERT INTO service (service_id, department_id, category_id, name, general_information, phone, created_date, start_date, end_date, location, allowed_participant, sunday_flag, monday_flag, tuesday_flag, wednesday_flag, thursday_flag, friday_flag, saturday_flag) 
VALUES (1, 1, 1, 'Algebra', '', '919-839-1234', current_time(), '2015-06-01', '2018-06-30', 'Cameron Village', 25, false, true, false, true, false, true, false);

INSERT INTO service (service_id, department_id, category_id, name, general_information, phone, created_date, start_date, end_date, location, allowed_participant, sunday_flag, monday_flag, tuesday_flag, wednesday_flag, thursday_flag, friday_flag, saturday_flag) 
VALUES (2, 2, 2, 'Archery', '', '919-839-1234', current_time(), '2015-06-01', '2018-06-30', 'Cameron Village', 25, false, true, false, true, false, true, false);

INSERT INTO service (service_id, department_id, category_id, name, general_information, phone, created_date, start_date, end_date, location, allowed_participant, sunday_flag, monday_flag, tuesday_flag, wednesday_flag, thursday_flag, friday_flag, saturday_flag) 
VALUES (3, 3, 3, 'Mobile Apps', '', '919-839-1234', current_time(), '2015-06-01', '2018-06-30', 'Cameron Village', 25, false, true, false, true, false, true, false);

