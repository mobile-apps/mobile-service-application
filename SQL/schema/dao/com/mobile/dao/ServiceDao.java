/*
 * This file was generated - do not edit it directly !!
 * Generated by AuDAO tool, a product of Spolecne s.r.o.
 * For more information please visit http://www.spoledge.com
 */
package com.mobile.dao;

import java.sql.Date;
import java.sql.Timestamp;

import com.spoledge.audao.db.dao.AbstractDao;
import com.spoledge.audao.db.dao.DaoException;

import com.mobile.dto.Service;


/**
 * This is the DAO.
 *
 * @author generated
 */
public interface ServiceDao extends AbstractDao {

    /**
     * Finds a record identified by its primary key.
     * @return the record found or null
     */
    public Service findByPrimaryKey( int serviceId );

    /**
     * Finds records ordered by name.
     */
    public Service[] findAll( );

    /**
     * Finds records.
     */
    public Service[] findAllByDepartmentID( Integer departmentId );

    /**
     * Deletes a record identified by its primary key.
     * @return true iff the record was really deleted (existed)
     */
    public boolean deleteByPrimaryKey( int serviceId ) throws DaoException;

    /**
     * Inserts a new record.
     * @return the generated primary key - serviceId
     */
    public int insert( Service dto ) throws DaoException;

    /**
     * Updates one record found by primary key.
     * @return true iff the record was really updated (=found and any change was really saved)
     */
    public boolean update( int serviceId, Service dto ) throws DaoException;

}
