/*
 * This file was generated - do not edit it directly !!
 * Generated by AuDAO tool, a product of Spolecne s.r.o.
 * For more information please visit http://www.spoledge.com
 */
package com.mobile.dao;

import java.sql.Connection;


/**
 * This is the main class for obtaining DAO objects.
 * It looks for the system property called "com.mobile.dao.DB_TYPE"
 * and its value is used as the DAO implementation subpackage name.
 * The default value is "mysql".
 * The name of the Factory class is assumed to be "DaoFactoryImpl"
 * and it must have the default constructor defined.
 *
 * @author generated
 */
public class DaoFactory {

    private static Factory factory;

    static {
        String pkgDao = DaoFactory.class.getPackage().getName();
        String pkgImpl = pkgDao + '.' + System.getProperty( pkgDao + ".DB_TYPE", "mysql" );
        try {
            Class<?> aclazz = Class.forName( pkgImpl + ".DaoFactoryImpl" );
            Class<? extends Factory> clazz = aclazz.asSubclass( Factory.class );
            factory = clazz.newInstance();
        }
        catch (Exception e) {
            throw new Error("A problem occurred when resolving DAO factory class", e);
        }
    }


    ////////////////////////////////////////////////////////////////////////////
    // Static methods
    ////////////////////////////////////////////////////////////////////////////

    public static CrashDao createCrashDao( Connection conn ) {
        return factory.createCrashDao( conn );
    }

    public static ProcessorDao createProcessorDao( Connection conn ) {
        return factory.createProcessorDao( conn );
    }

    public static NotificationDao createNotificationDao( Connection conn ) {
        return factory.createNotificationDao( conn );
    }

    public static CategoryDao createCategoryDao( Connection conn ) {
        return factory.createCategoryDao( conn );
    }

    public static OrganizationDao createOrganizationDao( Connection conn ) {
        return factory.createOrganizationDao( conn );
    }

    public static DepartmentDao createDepartmentDao( Connection conn ) {
        return factory.createDepartmentDao( conn );
    }

    public static ServiceDao createServiceDao( Connection conn ) {
        return factory.createServiceDao( conn );
    }

    public static DeviceDao createDeviceDao( Connection conn ) {
        return factory.createDeviceDao( conn );
    }

    public static UserCategoryDao createUserCategoryDao( Connection conn ) {
        return factory.createUserCategoryDao( conn );
    }

    public static RegisteredUserDao createRegisteredUserDao( Connection conn ) {
        return factory.createRegisteredUserDao( conn );
    }

    public static LogDao createLogDao( Connection conn ) {
        return factory.createLogDao( conn );
    }


    ////////////////////////////////////////////////////////////////////////////
    // Inner classes
    ////////////////////////////////////////////////////////////////////////////

    public static abstract class Factory {

        public abstract CrashDao createCrashDao( Connection conn );

        public abstract ProcessorDao createProcessorDao( Connection conn );

        public abstract NotificationDao createNotificationDao( Connection conn );

        public abstract CategoryDao createCategoryDao( Connection conn );

        public abstract OrganizationDao createOrganizationDao( Connection conn );

        public abstract DepartmentDao createDepartmentDao( Connection conn );

        public abstract ServiceDao createServiceDao( Connection conn );

        public abstract DeviceDao createDeviceDao( Connection conn );

        public abstract UserCategoryDao createUserCategoryDao( Connection conn );

        public abstract RegisteredUserDao createRegisteredUserDao( Connection conn );

        public abstract LogDao createLogDao( Connection conn );

    }
}
