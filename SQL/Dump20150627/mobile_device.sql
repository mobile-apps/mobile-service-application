-- MySQL dump 10.13  Distrib 5.6.24, for osx10.8 (x86_64)
--
-- Host: 127.0.0.1    Database: mobile
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `device`
--

DROP TABLE IF EXISTS `device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device` (
  `device_id` int(11) NOT NULL AUTO_INCREMENT,
  `registered_user_id` int(11) NOT NULL,
  `subscriber` varchar(250) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`device_id`),
  UNIQUE KEY `inx_subscriber` (`subscriber`),
  KEY `fk_device_registered_user_id` (`registered_user_id`),
  CONSTRAINT `fk_device_registered_user_id` FOREIGN KEY (`registered_user_id`) REFERENCES `registered_user` (`registered_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device`
--

LOCK TABLES `device` WRITE;
/*!40000 ALTER TABLE `device` DISABLE KEYS */;
INSERT INTO `device` VALUES (2,414,'APA91bGs74HoxMPlJ-muNz2p3he2Zl47uxvdo2olXtZekd-5K_O--1zkGQtKvH6GeQNLLB0SfVjl9am1vYL3AjFme7jWTHFohZhYdEytnb6Jt4kHrk1zlMOpHlmgZu_jX262EN9sW6aVybZcqi3E12FulHx2SvTr2g','2015-02-24 10:38:37'),(3,415,'APA91bFY930T8qnP3y6TOeeUNB0tYP9OIzoZj7eBbDWwIQWHv3POTLxWjEZBcGGsGseYOgkibh_SGK65TdMnY9XFpqSe9_AIMYA2qFx5J8utLduWmSt1KRskNuQ8_-DtYHJeaRqE5QSi97KomhLAFaK9LtcPAhzLRA','2015-02-24 15:25:33'),(4,416,'APA91bF1DkNImmfWCJvFgCWDZtMQPv0cxsNDLHJJWtiMeiF2boB6xtsYt_fd0Pxny0quV4uMFcwjfficS6uXVlpZbiWuU9K1Zutw6nw_j4a-N0MIK2gKPyEfFzeNWB_5MFx6ac0eJNyPL6apIYiri3_9YmTLg_6_JA','2015-02-24 15:25:33');
/*!40000 ALTER TABLE `device` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-27 20:49:27
