-- MySQL dump 10.13  Distrib 5.6.24, for osx10.8 (x86_64)
--
-- Host: 127.0.0.1    Database: mobile
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `created_date` datetime NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `location` varchar(250) NOT NULL,
  `allowed_participant` int(11) DEFAULT NULL,
  `department_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sunday_flag` tinyint(4) DEFAULT NULL,
  `monday_flag` tinyint(4) DEFAULT NULL,
  `tuesday_flag` tinyint(4) DEFAULT NULL,
  `wednesday_flag` tinyint(4) DEFAULT NULL,
  `thursday_flag` tinyint(4) DEFAULT NULL,
  `friday_flag` tinyint(4) DEFAULT NULL,
  `saturday_flag` tinyint(4) DEFAULT NULL,
  `general_Information` varchar(3000) NOT NULL,
  `phone` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`service_id`),
  KEY `fk_service_department_id` (`department_id`),
  KEY `fk_service_category_id` (`category_id`),
  CONSTRAINT `fk_service_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`),
  CONSTRAINT `fk_service_department_id` FOREIGN KEY (`department_id`) REFERENCES `department` (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'Sunday School','2014-08-04 18:19:10','2014-07-01 09:00:00','2016-04-03 19:00:00','Wake Tech Main Campus',10,4,1,1,0,0,0,0,0,0,'On Wednesday And Thursday, December 24th And 25th, Credit Union Branches Will Be Closed. During This Time, The Contact Center, Member Access, Voice Response And CashPoints ATMs Will Be Available. If You Need Assistance, Please Call The Contact Center At 888-732-8562.','8887575555'),(2,'Small Group','2014-08-20 21:17:32','2014-12-02 04:10:00','2015-05-18 03:00:00','1213 Somerset Road, Raleigh NC 27610',12,3,1,1,1,1,1,1,1,0,'On Wednesday and Thursday, December 24th and 25th, Credit Union branches will be closed. During this time, the Contact Center, Member Access, Voice Response and CashPoints ATMs will be available. If you need assistance, please call the Contact Center at 888-732-8562.','8887575555'),(3,'test','2014-11-14 17:52:08','2014-12-02 07:00:00','2015-01-27 05:00:00','1213 Somerset Road, Raleigh NC 27610',1,3,4,1,1,1,0,0,0,0,'On Wednesday and Thursday, December 24th and On Wednesday and Thursday, December 24th and ','8887575555'),(4,'today','2015-01-18 13:31:07','2015-01-18 15:00:00','2015-01-19 18:00:00','1213 Somerset Road, Raleigh nc 27610',23,2,2,1,1,0,0,0,0,0,'today','8887575555'),(5,'today','2015-01-19 10:58:47','2015-01-19 00:00:00','2015-01-20 00:00:00','1213 Somerset Road, Raleigh NC 27610',23,4,1,1,1,0,0,0,0,0,'today','8887575555'),(6,'Small Group','2015-02-08 07:53:43','2015-02-09 09:00:00','2015-02-10 04:00:00','1213 Somerset Road, Raleigh nc 27610',23,4,1,1,1,1,1,1,1,1,'Small Group','8887575555'),(7,'zxZXZxZ','2015-06-04 18:31:06','2015-06-04 00:00:00','2015-06-04 00:00:00','1213 Somerset Road, Raleigh nc 27610',23,4,1,1,0,0,0,0,0,0,'ZxZxZxZ','8887575555');
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-27 20:49:26
