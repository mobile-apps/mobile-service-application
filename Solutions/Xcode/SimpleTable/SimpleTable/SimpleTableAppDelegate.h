//
//  SimpleTableAppDelegate.h
//  SimpleTable
//
//  Created by Angela Wong on 7/20/14.
//  Copyright (c) 2014 Mobile Apps Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleTableAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
