//
//  main.m
//  SimpleTable
//
//  Created by Angela Wong on 7/20/14.
//  Copyright (c) 2014 Mobile Apps Team. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SimpleTableAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SimpleTableAppDelegate class]));
    }
}
