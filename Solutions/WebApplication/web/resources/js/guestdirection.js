/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 function findMyAddress() {
                        //var strconfirm = confirm("Are you sure you want to allow us to find your device current address?");
                        //if (strconfirm != true)
                        // {
                        //  return true;
                        //}


                        if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(geoLocation);

                            //alert("geo location support");
                        } else {
                            // alert("not supported");
                        }
                    }

                    function geoLocation(position) {

                        var loc = position.coords.latitude + "," + position.coords.longitude;
                        var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
                        url = url + loc;
                        var returnText = getJsonObjectPost(url);
                        var obj = JSON.parse(returnText);
                        var address = obj.results[0].formatted_address;
                        var field = document.getElementById("direction-form:startingaddress");
                        field.value = address;
                        //alert(address);
                        //alert(loc);
                    }

                    function createXMLHttpRequest() {
                        try {
                            return new ActiveXObject("Msxml2.XMLHTTP");
                        } catch (e) {
                        }

                        try {
                            return new ActiveXObject("Microsoft.XMLHTTP");
                        } catch (e) {
                        }

                        try {
                            return new XMLHttpRequest();
                        } catch (e) {
                        }

                        alert("XMLHttpRequest not supported");

                        return null;

                    } //createXMLHttpRequest


                    function getJsonObjectPost(url)
                    {

                        var xhReq = createXMLHttpRequest();

                        try {
                            xhReq.open("GET", url, false);
                            xhReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                            xhReq.send();
                        }
                        catch (e) {
                            return getJsonErrorMsg("Error communicating with server, exception encountered.");
                        }

                        if (xhReq.status == "200") {
                            return xhReq.responseText;
                        }
                        else if (xhReq.status == "500") {
                            return getJsonErrorMsg("Application error.");
                        }
                        else {
                            return getJsonErrorMsg("Error communicating with server.");
                        }

                    } //getJsonObjectPost


                    function getJsonErrorMsg(error) {

                        return "({'Error':true, 'Message':'" + error + "'})";

                    } //getJsonErrorMsg

