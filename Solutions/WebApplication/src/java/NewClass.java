
import java.util.Calendar;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author wtccuser
 */
public class NewClass {

    public static void computeCalendarDates(int month) {
        //first get current month
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        int currentMonth = calendar.get(Calendar.MONTH) + 1;
        Date startDate = null;
        Date endDate = null;
        System.out.println("month passed: " + month);
        if (month == currentMonth) {
            int firstDate = calendar.getActualMinimum(Calendar.DATE);
            calendar.set(Calendar.DATE, firstDate);
            startDate = calendar.getTime();
            System.out.println("first Date: " + startDate);
            int lastDate = calendar.getActualMaximum(Calendar.DATE);
            calendar.set(Calendar.DATE, lastDate);
            endDate = calendar.getTime();
            System.out.println("last Date: " + endDate);

        } else {
            if (month > currentMonth) {
                calendar.set(currentYear, month-1, 1);
                startDate = calendar.getTime();
                System.out.println("startdate: " + startDate);
                int lastDate = calendar.getActualMaximum(Calendar.DATE);
                calendar.set(Calendar.DATE, lastDate);
                endDate = calendar.getTime();
                System.out.println("enddate: " + endDate);

            } else {
                calendar.set(currentYear + 1, month-1, 1);
                startDate = calendar.getTime();
                System.out.println("startdate: " + startDate);
                int lastDate = calendar.getActualMaximum(Calendar.DATE);
                calendar.set(Calendar.DATE, lastDate);
                endDate = calendar.getTime();
                System.out.println("enddate: " + endDate);

            }
        }

    }

    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();

        int firstDate = calendar.getActualMinimum(Calendar.DATE);
        calendar.set(Calendar.DATE, firstDate);
        //System.out.println("first Date: " + calendar.getTime());
        int lastDate = calendar.getActualMaximum(Calendar.DATE);

        calendar.set(Calendar.DATE, lastDate);
        int lastDay = calendar.get(Calendar.DAY_OF_WEEK);

        //System.out.println("this Last Date: " + calendar.getTime());
        // System.out.println("Last Day : " + lastDay);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        //System.out.println("next Last Date: " + calendar.getTime());
        Date nextDate = calendar.getTime();
        computeCalendarDates(1);
        computeCalendarDates(2);
        computeCalendarDates(3);
        computeCalendarDates(4);
        computeCalendarDates(5);
        computeCalendarDates(6);
        computeCalendarDates(7);
        computeCalendarDates(8);
        computeCalendarDates(9);
        computeCalendarDates(10);
        computeCalendarDates(11);
        computeCalendarDates(12);
    }

}
