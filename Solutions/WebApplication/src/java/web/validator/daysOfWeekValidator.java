/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package web.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.List;

/**
 *
 * @author HopeBenziger
 */
@FacesValidator(value = "daysOfWeekValidator")
public class daysOfWeekValidator implements Validator{
    
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws  ValidatorException
    {
        List<String> selectedItemsCheckbox = (List<String>) value;
//        String[] selectedItemsCheckbox = (String[]) value;
        Boolean valid = false;
//        
        if(selectedItemsCheckbox.size() < 1)
        {
            valid = false;
        }
        
        //if()
        
        if(!valid)
        {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Required Days of Week", 
                                    "You must select a day of week.");
            throw new ValidatorException(message);
        }
    }        
    
}
