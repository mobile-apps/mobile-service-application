/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author wtccuser
 */
@FacesValidator("web.validator.PhoneMatchValidator")
public class PhoneMatchValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent component, Object value) throws ValidatorException {
        UIInput phoneComponent = (UIInput) fc.getViewRoot().findComponent("guestForm:phone");
        if (phoneComponent == null) {
            throw new IllegalArgumentException(String.format("Unable to find phone component."));
        }

//UIInput emailComponent = (UIInput) component.getAttributes().get("email");
        String phoneValue = (String)phoneComponent.getValue();
        if (phoneValue == null) {
            throw new IllegalArgumentException(String.format("Unable to find phone submitted."));
        }
        if (!value.toString().equals(phoneValue.toString())) {
            FacesMessage msg
                    = new FacesMessage("Phone match failed",
                            "Phone numbers do not match");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }

    }
}
