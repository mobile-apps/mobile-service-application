/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.validator;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author wtccuser
 */
@FacesValidator("web.validator.EmailMatchValidator")
public class EmailMatchValidator implements Validator {
private String email;
private String confirmEmail;
    @Override
    public void validate(FacesContext fc, UIComponent component, Object value) throws ValidatorException {
       //Object obj = fc.getExternalContext().getRequestParameterMap();
        UIInput emailComponent = (UIInput) fc.getViewRoot().findComponent("guestForm:email");
        if (emailComponent == null) {
            throw new IllegalArgumentException(String.format("Unable to find email component."));
        }

//UIInput emailComponent = (UIInput) component.getAttributes().get("email");
        String emailValue = (String)emailComponent.getValue();
        if (emailValue == null) {
            throw new IllegalArgumentException(String.format("Unable to find email submitted."));
        }
        if (!value.toString().equals(emailValue.toString())) {
            FacesMessage msg
                    = new FacesMessage("E-mail match failed",
                            "Emails do not match");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }

    }
}
