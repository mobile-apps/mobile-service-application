/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package web.service;

import data.helper.CategoryHelper;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import org.primefaces.json.JSONObject;
import web.Site;

/**
 * REST Web Service
 *
 * @author wtccuser
 */
@Path("mobile")
public class CategoryResource {
       private final static Logger LOGGER = Logger.getLogger(CategoryResource.class
            .getName());


    @Context
    private UriInfo context;

    /**
     * Creates a new instance of OganizationResource
     */
    public CategoryResource() {
    }

    /**
     * Retrieves representation of an instance of webservice.OganizationResource
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/getAllCategories")
    @Produces(MediaType.APPLICATION_JSON)
    public List<com.mobile.dto.Category> getAllCategories() {
        LOGGER.log(Level.OFF, "started getAllCategories");
        try {
           List<com.mobile.dto.Category> categories = CategoryHelper.findAll();
           
   
        return categories;
        } catch (Exception ex) {
            Logger.getLogger(CategoryResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.OFF, "end getAllCategories");
        return null;
    }
}
    
