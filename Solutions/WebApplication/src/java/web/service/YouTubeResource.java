/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import web.youtube.YouTube;
import web.youtube.YouTubeRSSParser;

/**
 * REST Web Service
 *
 * @author wtccuser
 */
@Path("mobile")
public class YouTubeResource {

    private final static Logger LOGGER = Logger.getLogger(YouTubeResource.class
            .getName());

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of OganizationResource
     */
    public YouTubeResource() {
    }

    /**
     * Retrieves representation of an instance of webservice.OganizationResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/getAllYouTubes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllYouTubes() {
        LOGGER.log(Level.OFF, "started getAllYouTubes");
        try {
            //  List<com.mobile.dto.Category> categories = CategoryHelper.findAll();
            List<YouTube> list = YouTubeRSSParser.getAll();

           
            JsonArrayBuilder builderArray = Json.createArrayBuilder();
            for (YouTube youTube : list) {
                
                JsonObjectBuilder jSONObject = Json.createObjectBuilder();
                jSONObject.add("height", Integer.parseInt(youTube.getHeight()));
                jSONObject.add("imageURL", youTube.getImageURL());
                jSONObject.add("published", youTube.getPublished());
                jSONObject.add("title", youTube.getTitle());
                jSONObject.add("width", Integer.parseInt(youTube.getWidth()));
                jSONObject.add("id", youTube.getYouTubeID());
                jSONObject.add("youTubeURL", youTube.getYouTubeURL());
       
                builderArray.add(jSONObject);

            }
            JsonObjectBuilder outterJSONObject = Json.createObjectBuilder();
            outterJSONObject.add("YouTube", builderArray);
              LOGGER.log(Level.OFF, "end getAllYouTubes");
        //Response.status(200).header("responseHeaderName", "responseHeaderValue").header("anotherResponseHeaderName", "foo").build();  
              //                                                                      Access-Control-Allow-Origin:   http://swagger.io           Access-Control-Allow-Headers:   Origin, X-Requested-With, Content-Type, Accept
return Response.ok(builderArray.build(), MediaType.APPLICATION_JSON).header("Access-Control-Allow-Origin", "*").header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept").build();
            
        } catch (Exception ex) {
            Logger.getLogger(YouTubeResource.class.getName()).log(Level.SEVERE, null, ex);
            JsonObjectBuilder errorJSONObject = Json.createObjectBuilder();
            errorJSONObject.add("error ", ex.getMessage());
            
            LOGGER.log(Level.OFF, "end getAllYouTubes");
            
             return Response.status(Response.Status.EXPECTATION_FAILED).entity(ex.getMessage()).build();
        }

    }
    @GET
    @Path("/getAllYouTubesA")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllYouTubesA() {
        LOGGER.log(Level.OFF, "started getAllYouTubes");
        String test = null;
        try {
            //  List<com.mobile.dto.Category> categories = CategoryHelper.findAll();
            List<YouTube> list = YouTubeRSSParser.getAll();

            JsonBuilderFactory factory = Json.createBuilderFactory(null);
            JsonArrayBuilder builderArray = factory.createArrayBuilder();
             //= Json.createArrayBuilder();
            for (YouTube youTube : list) {
                
             JsonObjectBuilder jSONObject = Json.createObjectBuilder();
                jSONObject.add("height", Integer.parseInt(youTube.getHeight()));
                jSONObject.add("imageURL", youTube.getImageURL());
                jSONObject.add("published", youTube.getPublished());
                jSONObject.add("title", youTube.getTitle());
                jSONObject.add("width", Integer.parseInt(youTube.getWidth()));
                jSONObject.add("id", youTube.getYouTubeID());
                jSONObject.add("youTubeURL", youTube.getYouTubeURL());
       
                builderArray.add(jSONObject);
                

            }
           // JsonObjectBuilder outterJSONObject = Json.createObjectBuilder();
            //outterJSONObject.add("YouTube", builderArray);
              LOGGER.log(Level.OFF, "end getAllYouTubes");
              
        //Response.status(200).header("responseHeaderName", "responseHeaderValue").header("anotherResponseHeaderName", "foo").build();  
              //                                                                      Access-Control-Allow-Origin:   http://swagger.io           Access-Control-Allow-Headers:   Origin, X-Requested-With, Content-Type, Accept
//return Response.ok(builderArray.build(), MediaType.APPLICATION_JSON).header("Access-Control-Allow-Origin", "*").header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept").build();
            return builderArray.build().toString();
              
        } catch (Exception ex) {
            Logger.getLogger(YouTubeResource.class.getName()).log(Level.SEVERE, null, ex);
            JsonObjectBuilder errorJSONObject = Json.createObjectBuilder();
            errorJSONObject.add("error ", ex.getMessage());
            
            LOGGER.log(Level.OFF, "end getAllYouTubes");
            return null;
             //return Response.status(Response.Status.EXPECTATION_FAILED).entity(ex.getMessage()).build();
        }

    }
}
