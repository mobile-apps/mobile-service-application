/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.service;

import com.mobile.dto.RegisteredUser;
import data.helper.CategoryHelper;
import data.helper.RegisterUserHelper;
import data.helper.EventHelper;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import org.primefaces.json.JSONObject;
import web.Event;
import web.Site;
import web.WebConstants;
import web.bean.PodcastBean;
import web.helper.DateHelper;
import web.helper.MapHelper;
import web.obj.GoogleMap;
import web.obj.Step;
import web.podcast.Episode;
import web.podcast.Podcast;
import web.podcast.PodcastXML;
import web.service.obj.Organization;

/**
 * REST Web Service
 *
 * @author wtccuser
 */
@Path("mobile")
public class OganizationResource {

    private final static Logger LOGGER = Logger.getLogger(OganizationResource.class
            .getName());

    @Context
    private ServletContext ctx;

    /**
     * Creates a new instance of OganizationResource
     */
    public OganizationResource() {
    }

    /**
     * http://localhost:8080/WebApplication/rest/mobile/getAllCategoriesEventsUser?userid="1"
     *
     * @param registerUserId
     * @return
     */
    @GET
    @Path("/getAllCategoriesEventsUser")
    @Produces(MediaType.TEXT_PLAIN)
    public String getAllCategoriesEventsUser(@QueryParam("userid") int registerUserId) {
        LOGGER.log(Level.OFF, "started getAllCategoriesEventsUser");
        Organization organization = new Organization();
        try {
            //find registerUser

            RegisteredUser registerUser = RegisterUserHelper.update(registerUserId);

            List<com.mobile.dto.Category> categories = CategoryHelper.findAll();
            List<Event> events = EventHelper.findAll(true);
            organization.setCategories(categories);
            organization.setEvents(events);

            LOGGER.log(Level.OFF, "end getAllCategories");

            organization.setRegisteredUser(registerUser);
            LOGGER.log(Level.OFF, "end getAllCategoriesEventsUser");

            return organization.toString();
        } catch (Exception ex) {
            Logger.getLogger(CategoryResource.class.getName()).log(Level.SEVERE, null, ex);
            JsonObjectBuilder errorJSONObject = Json.createObjectBuilder();
            errorJSONObject.add("error ", ex.getMessage());

            LOGGER.log(Level.OFF, "end getAllCategoriesEventsUser");

            return errorJSONObject.build().toString();

        }

    }

    @GET
    @Path("/getOrg")
    @Produces(MediaType.TEXT_PLAIN)
    public String getOrg() {
        LOGGER.log(Level.OFF, "started getOrg");
        List<com.mobile.dto.Organization> listOrg = data.helper.OrganizationHelper.findAll();

        //get first org
        com.mobile.dto.Organization firstOrg = listOrg.get(0);
        Organization organization = new Organization();
        try {
            JsonObjectBuilder orgJSONObject = Json.createObjectBuilder();
            if (firstOrg.getEmail() != null) {
                orgJSONObject.add("email", firstOrg.getEmail());
            }
            if (firstOrg.getGeneralInformation() != null) {
                orgJSONObject.add("generalinformation", firstOrg.getGeneralInformation());
            }
            if (firstOrg.getLocation() != null) {
                orgJSONObject.add("location", firstOrg.getLocation());
            }
            if (firstOrg.getLatitude() != null) {
                orgJSONObject.add("latitude", firstOrg.getLatitude());
            }
            if (firstOrg.getLongitude() != null) {
                orgJSONObject.add("longitude", firstOrg.getLongitude());
            }
            if (firstOrg.getPhone() != null) {
                orgJSONObject.add("phone", firstOrg.getPhone());
            }
            if (firstOrg.getIconstring() != null) {
                orgJSONObject.add("image", firstOrg.getIconstring());
            }
            if (firstOrg.getName() != null) {
                orgJSONObject.add("name", firstOrg.getName());
            }
            LOGGER.log(Level.OFF, "end getAllCategoriesAndEvents");
            return orgJSONObject.build().toString();
        } catch (Exception ex) {
            Logger.getLogger(CategoryResource.class.getName()).log(Level.SEVERE, null, ex);
            JsonObjectBuilder errorJSONObject = Json.createObjectBuilder();
            errorJSONObject.add("error ", ex.getMessage());
            LOGGER.log(Level.OFF, "end getAllCategoriesAndEvents");
            return errorJSONObject.build().toString();

        }

    }
    /*
     http://localhost:8080/WebApplication/rest/mobile/getAllCategoriesAndEvents
     */

    @GET
    @Path("/getAllCategoriesAndEvents")
    @Produces(MediaType.TEXT_PLAIN)
    public String getAllCategoriesAndEvents() {
        LOGGER.log(Level.OFF, "started getAllCategoriesAndEvents");
        Organization organization = new Organization();
        try {
            List<com.mobile.dto.Category> categories = CategoryHelper.findAll();
            List<Event> events = EventHelper.findAll(true);
            organization.setCategories(categories);
            organization.setEvents(events);
            LOGGER.log(Level.OFF, "end getAllCategoriesAndEvents");
            return organization.toString();
        } catch (Exception ex) {
            Logger.getLogger(CategoryResource.class.getName()).log(Level.SEVERE, null, ex);
            JsonObjectBuilder errorJSONObject = Json.createObjectBuilder();
            errorJSONObject.add("error ", ex.getMessage());
            LOGGER.log(Level.OFF, "end getAllCategoriesAndEvents");
            return errorJSONObject.build().toString();

        }

    }

    @GET
    @Path("/getGoogleMap")
    @Produces(MediaType.APPLICATION_JSON)
    public String getGoogleMap(@QueryParam("latitudeStart") double latitudeStart, @QueryParam("longitudeStart") double longitudeStart, @QueryParam("latitudeEnd") double latitudeEnd, @QueryParam("longitudeEnd") double longitudeEnd) {
        String googleURL = (String) ctx.getInitParameter("google.json.address");
        String startingAddress = latitudeStart + "," + longitudeStart;

        String endingAddress = latitudeEnd + "," + longitudeEnd;
        GoogleMap googleMap = MapHelper.getDrivingDirection(startingAddress, endingAddress, googleURL);
        JsonObjectBuilder mapJSONObject = Json.createObjectBuilder();
        mapJSONObject.add("startingAddress", googleMap.getStartingAddress());
        mapJSONObject.add("endingAddress", googleMap.getEndingAddress());
        JsonArrayBuilder stepsBuilder = Json.createArrayBuilder();
        List<Step> steps = googleMap.getSteps();
        for (Step step : steps) {
            JsonObjectBuilder stepJSONObject = Json.createObjectBuilder();
            stepJSONObject.add("distance", step.getDistance());
            stepJSONObject.add("duration", step.getDuration());
            stepJSONObject.add("instruction", step.getLeg());
            if (step.getManeuver() != null) {
                stepJSONObject.add("maneuver", step.getManeuver());
            }
            stepsBuilder.add(stepJSONObject);

        }//end of for
        mapJSONObject.add("steps", stepsBuilder);
        return mapJSONObject.build().toString();

    }

    /**
     * helper method for web and mobileApps
     *
     * @return
     */
    private JsonArrayBuilder getPodcastHelper() {
        LOGGER.log(Level.INFO, "started getAllPodcast");
        try {
            String podcastURL = (String) ctx.getInitParameter("podcast.address");
            LOGGER.log(Level.INFO, "podcastURL " + podcastURL);
            Podcast podcast = new PodcastXML(new URL("http://cokcc.podomatic.com/rss2.xml"));//
            //Podcast podcast = new PodcastXML(new URL(podcastURL));
            //System.out.println("Title - " + podcast.getTitle());

            JsonArrayBuilder episodesBuilder = Json.createArrayBuilder();
            List<Episode> episodes = podcast.getEpisodes();
            String t3 = podcast.getImageURL().getRef();
            //sort
            Collections.sort(episodes);
            if (episodes != null) {
                LOGGER.log(Level.INFO, "episodes size " + episodes.size());
            }
            int frameCounter = 0;
            for (Episode episode : episodes) {
                JsonObjectBuilder episodeJSONObject = Json.createObjectBuilder();
                try {
                    episodeJSONObject.add(WebConstants.PODCAST_TITLE, episode.getTitle());
                } catch (Exception ex) {
                    LOGGER.log(Level.SEVERE, "error " + ex.getMessage());
                    throw new java.lang.IllegalStateException(ex);
                }
                try {
                    episodeJSONObject.add(WebConstants.PODCAST_DESCRIPTION, episode.getSummary());
                } catch (Exception ex) {
                    LOGGER.log(Level.SEVERE, "error " + ex.getMessage());
                    throw new java.lang.IllegalStateException(ex);
                }

                try {
                    episodeJSONObject.add(WebConstants.PODCAST_IMAGE_URL, episode.getImageURL());
                } catch (Exception ex) {
                    LOGGER.log(Level.SEVERE, "error " + ex.getMessage());
                    throw new java.lang.IllegalStateException(ex);
                }

                try {
                    // String href = episode.getHref();
                    String href = episode.getGuid();
                    episodeJSONObject.add(WebConstants.PODCAST_URL, href);
                    // String publishDate = DateHelper.toString(episode.getPubDate().toString());v  
                    episodeJSONObject.add(WebConstants.PODCAST_PUBLISH_DATE, episode.getShortDate());
                } catch (Exception ex) {
                    LOGGER.log(Level.SEVERE, "error " + ex.getMessage());
                    throw new java.lang.IllegalStateException(ex);
                }

                try {
                    episodeJSONObject.add(WebConstants.PODCAST_MEDIA, episode.getMediaLocation().toString());
                } catch (Exception ex) {
                    LOGGER.log(Level.SEVERE, "error " + ex.getMessage());
                    throw new java.lang.IllegalStateException(ex);
                }
//episodeJSONObject.add("description", episode.getShortDescription());
                episodesBuilder.add(episodeJSONObject);
                frameCounter++;
            }

            LOGGER.log(Level.INFO, "ending getAllPodcast ");
            return episodesBuilder;
        } catch (Exception ex) {

            Logger.getLogger(OganizationResource.class.getName()).log(Level.SEVERE, null, ex);
            throw new java.lang.IllegalStateException(ex);

        }

    }

    @GET
    @Path("/getAllPodcastForMobileApps")
    @Produces(MediaType.APPLICATION_JSON)
    public String getPodcastForWeb() {
        LOGGER.log(Level.INFO, "started getPodcastForWeb");
        try {
            JsonArrayBuilder jsonArrayBuilder = getPodcastHelper();
            JsonObjectBuilder outterJSONObject = Json.createObjectBuilder();
            outterJSONObject.add("podcast", jsonArrayBuilder);
            LOGGER.log(Level.INFO, "ending getAllPodcast ");
            return outterJSONObject.build().toString();
        } catch (Exception ex) {
            JsonObjectBuilder errorJSONObject = Json.createObjectBuilder();
            errorJSONObject.add("error ", ex.getMessage());
            return errorJSONObject.build().toString();
        }

    }

    @GET
    @Path("/getAllPodcastForWeb")
    @Produces(MediaType.APPLICATION_JSON)
    public String getPodcastForMobileApps() {
        LOGGER.log(Level.INFO, "started getPodcastForWeb");

        JsonArrayBuilder jsonArrayBuilder = getPodcastHelper();
        LOGGER.log(Level.INFO, "ending getAllPodcast ");
        return jsonArrayBuilder.build().toString();

    }

    /**
     * Retrieves representation of an instance of webservice.OganizationResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/getAllOrganizations")
    @Produces(MediaType.TEXT_PLAIN)
    public String getSite() {
        LOGGER.log(Level.OFF, "started getSite");
        try {
            Site site = new Site();
            String json = site.toString();
            LOGGER.log(Level.OFF, " getSite " + json);

            return json;
        } catch (Exception ex) {
            Logger.getLogger(OganizationResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.OFF, "end getSite");
        return null;
    }

    /**
     * PUT method for updating or creating an instance of OganizationResource
     *
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/xml")
    public void putXml(String content) {
    }
}
