/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.service;

import data.helper.EventHelper;
import java.net.URI;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import web.Event;
import web.WebConstants;
import web.obj.DepartmentObj;
import web.obj.EventDatesObj;
import web.service.obj.Organization;

/**
 * REST Web Service
 *
 * @author wtccuser
 */
/**
 *
 * @author administrator
 */
@Path("mobile")
public class EventResource {

    private final static Logger LOGGER = Logger.getLogger(EventResource.class
            .getName());

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of OganizationResource
     */
    public EventResource() {
    }

    //@QueryParam("from") int from
    @GET
    @Path("/getEventsByMonth")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> getEventsByMonth(@QueryParam("monthId") int monthId) {
        LOGGER.log(Level.OFF, "started getEventsByMonth");
        Organization organization = new Organization();
        ArrayList<Event> myEvents = new ArrayList<Event>();
        try {
            EventDatesObj eventDatesObj = EventHelper.computeCalendarDates(monthId);
            List<Event> events = EventHelper.findAll(eventDatesObj.getStartDate(), eventDatesObj.getEndDate());

            return events;
        } catch (Exception ex) {
            Logger.getLogger(CategoryResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.OFF, "end getEventsByMonth");
        return null;
    }

    @GET
    @Path("/getAllEvents")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> getAllEvents() {
        LOGGER.log(Level.OFF, "started getAllEvents");
        Organization organization = new Organization();
        ArrayList<Event> myEvents = new ArrayList<Event>();
        try {

            List<Event> events = EventHelper.findAll(true);

            //add departments
            for (Event event : events) {
                DepartmentObj departmentObj = new DepartmentObj(event.getDepartment());
                /// event.setDepartmentObj(departmentObj);
                myEvents.add(event);
            }

            return events;
        } catch (Exception ex) {
            Logger.getLogger(CategoryResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.OFF, "end getAllEvents");
        return null;
    }

    @GET
    @Path("/getAllMobileYearEvents")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllMobileYear() {
        LOGGER.log(Level.OFF, "started getAllYearEvents");
        JsonArrayBuilder eventsBuilder = Json.createArrayBuilder();
        String url = "calendardetail.xhtml?id={0}&startDate={1}&endDate={2}";

        URI baseUri = context.getBaseUri();
        String path = baseUri.getScheme() + "://" + baseUri.getHost();
        if (baseUri.getPort() != -1) {
            path += ":" + baseUri.getPort();
        }
        URI reqUri = context.getRequestUri();
        String applicationBase = reqUri.getPath();
        String[] values = applicationBase.split("/");
        path = path + "/faces/mobile/";
        // path =  values[0];

        url = path + url;

//String url = "calendardetail.xhtml?id={0}";
        ArrayList<Event> myEvents = new ArrayList<Event>();
        try {

            List<Event> events = EventHelper.findAll(true);

            //add departments
            for (Event event : events) {
                JsonObjectBuilder eventJSONObject = Json.createObjectBuilder();
                eventJSONObject.add("title", event.getName());
                eventJSONObject.add("description", event.getGeneralInformation());
                eventJSONObject.add("location", event.getLocation());
                eventJSONObject.add("start", event.getStartDateLong());
                eventJSONObject.add("end", event.getEndDateLong());
                Object[] arguments = {
                    event.getServiceExtender().getServiceId(),
                    event.getStartDateLong(),
                    event.getEndDateLong(),
                    event.getEndDateUTC()
                };

                MessageFormat form = new MessageFormat(url);
                String result = form.format(arguments);
                eventJSONObject.add("url", result);
                /// event.setDepartmentObj(departmentObj);
                eventsBuilder.add(eventJSONObject);
            }

            return eventsBuilder.build().toString();
        } catch (Exception ex) {
            Logger.getLogger(EventResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.OFF, "end getAllEvents");
        return null;
    }

    @GET
    @Path("/getAllYearEvents")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllYear() {
        LOGGER.log(Level.OFF, "started getAllYearEvents");
        JsonArrayBuilder eventsBuilder = Json.createArrayBuilder();
        String url = "calendardetail.xhtml?id={0}&startDate={1}&endDate={2}";
        //String url = "calendardetail.xhtml?id={0}";
        ArrayList<Event> myEvents = new ArrayList<Event>();
        try {

            List<Event> events = EventHelper.findAll(true);

            //add departments
            for (Event event : events) {
                JsonObjectBuilder eventJSONObject = Json.createObjectBuilder();
                eventJSONObject.add("title", event.getName());
                eventJSONObject.add("start", event.getStartDateUTC());
                eventJSONObject.add("end", event.getEndDateUTC());
                Object[] arguments = {
                    event.getServiceExtender().getServiceId(),
                    event.getStartDateLong(),
                    event.getEndDateLong(),
                    event.getEndDateUTC()
                };
                MessageFormat form = new MessageFormat(url);
                String result = form.format(arguments);
                eventJSONObject.add("url", result);

                /// event.setDepartmentObj(departmentObj);
                eventsBuilder.add(eventJSONObject);
            }

            return eventsBuilder.build().toString();
        } catch (Exception ex) {
            Logger.getLogger(CategoryResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.OFF, "end getAllEvents");
        return null;
    }
}
