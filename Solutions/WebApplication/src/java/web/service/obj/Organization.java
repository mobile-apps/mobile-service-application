/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.service.obj;

import com.mobile.dto.Category;
import com.mobile.dto.Department;
import com.mobile.dto.RegisteredUser;
import data.helper.ApplicationEnum;
import java.math.BigDecimal;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import web.Event;

/**
 *
 * @author wtccuser
 */
public class Organization {

    private List<Event> events;
    private List<com.mobile.dto.Category> categories;

    public RegisteredUser getRegisteredUser() {
        return registeredUser;
    }

    public void setRegisteredUser(RegisteredUser registeredUser) {
        this.registeredUser = registeredUser;
    }
    private RegisteredUser registeredUser;

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String toString() {
        JsonArrayBuilder categoriesBuilder = Json.createArrayBuilder();
        // <editor-fold defaultstate="collapsed" desc="add categories">
        for (Category category : categories) {
            JsonObjectBuilder categoryJSONObject = Json.createObjectBuilder();
            categoryJSONObject.add("categoryId", category.getCategoryId());
            categoryJSONObject.add("name", category.getName());
            categoryJSONObject.add("createdDate", category.getCreatedDate().toString());
            categoriesBuilder.add(categoryJSONObject);

        }
        // </editor-fold>
        JsonArrayBuilder eventsBuilder = Json.createArrayBuilder();
        // <editor-fold defaultstate="collapsed" desc="add events">
        for (Event event : events) {
            JsonObjectBuilder eventJSONObject = Json.createObjectBuilder();
            eventJSONObject.add(ApplicationEnum.NAME.toString(), event.getName());
            eventJSONObject.add(ApplicationEnum.PHONE.toString(), event.getPhone());
            eventJSONObject.add(ApplicationEnum.STARTDATELONG.toString(), event.getStartDateLong());
            eventJSONObject.add(ApplicationEnum.ENDDATELONG.toString(), event.getEndDateLong());
            eventJSONObject.add(ApplicationEnum.GENERALINFORMATION.toString(), event.getGeneralInformation());
            
            // <editor-fold defaultstate="collapsed" desc="add department">
            JsonObjectBuilder departmentJSONObject = Json.createObjectBuilder();
            departmentJSONObject.add("name", event.getDepartment().getName());
            departmentJSONObject.add("phone", event.getDepartment().getPhone());
            departmentJSONObject.add("departmentId", event.getDepartment().getDepartmentId());
            departmentJSONObject.add("email", event.getDepartment().getEmail());
            departmentJSONObject.add("generalInformation", event.getDepartment().getGeneralInformation());
            if (event.getDepartment() != null && event.getDepartment().getIconstring() != null) {
                departmentJSONObject.add(ApplicationEnum.ICON.toString(), event.getDepartment().getIconstring());
            }
            if (event.getDepartment().getLatitude() == null) {
                throw new java.lang.IllegalStateException("Can not find latitude for " + event.getDepartment().getName());
            }
            if (event.getDepartment().getLongitude() == null) {
                throw new java.lang.IllegalStateException("Can not find longitude for " + event.getDepartment().getName());
            }
            departmentJSONObject.add("latitude", event.getDepartment().getLatitude());
            
            //if (event.getDepartment().getLatitude() != null) {
            departmentJSONObject.add("longitude", event.getDepartment().getLongitude());
            //}
            
            departmentJSONObject.add("location", event.getDepartment().getLocation());
            eventJSONObject.add("department", departmentJSONObject);
            
            // </editor-fold>
            eventsBuilder.add(eventJSONObject);

        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="add user">
        JsonObjectBuilder orgJSONObject = Json.createObjectBuilder();
        if (this.registeredUser != null) {
            JsonObjectBuilder userJSONObject = Json.createObjectBuilder();
            userJSONObject.add("registerUserId", registeredUser.getRegisteredUserId());
            String status = registeredUser.getStatus();
            if (status != null) {
                userJSONObject.add("status", status);
            }
            String email = registeredUser.getEmail();
            if (email != null) {
                userJSONObject.add("email", email);
            }
            String name = registeredUser.getUsername();
            if (name != null) {
                userJSONObject.add("name", name);
            }
            
            String phone = registeredUser.getPhone();
            if (phone != null) {
                userJSONObject.add("phone", phone);
            }

            orgJSONObject.add("registerUser", userJSONObject);
        }
        // </editor-fold>
        orgJSONObject.add("categories", categoriesBuilder);
        orgJSONObject.add("events", eventsBuilder);

        return orgJSONObject.build().toString();
    }

}
