/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.service;

import web.service.obj.Subscriber;

import com.mobile.dto.RegisteredUser;
import data.helper.CategoryHelper;
import data.helper.CrashHelper;
import data.helper.EventHelper;
import data.helper.RegisterUserHelper;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import web.Event;
import web.helper.EncryptHelper;
import web.service.obj.GenericResult;

/**
 * REST Web Service
 *
 * @author HSC314
 */
@Path("mobile")
public class CrashResource {
    
    private final static Logger LOGGER = Logger.getLogger(CrashResource.class
            .getName());
    @Context
    private UriInfo context;
    
    @Context
    ServletContext ctx;

    /**
     * Creates a new instance of UserResource
     */
    public CrashResource() {
    }
    
    @POST
    @Path("/createCrash")
    @Consumes(MediaType.APPLICATION_JSON)
    public GenericResult create(web.service.obj.Crash crash) {
        LOGGER.log(Level.OFF, "started find ");
        //find or add register users
        //http://localhost:8080/WebApplication/rest/mobile/createCrash
        /*
         {
         "id" : "444444",
         "errorMessage" : "none",
         "registerUserId" : 0
         }
         **/
        
        com.mobile.dto.Crash impCrash = new com.mobile.dto.Crash();
        if (crash.getStackTrace().length() > 989) {
        impCrash.setStackTrace(crash.getStackTrace().substring(0, 989));
        } else {
           impCrash.setStackTrace(crash.getStackTrace()); 
        }
        impCrash.setAndroidVersion(crash.getAndroidVersion());
        impCrash.setAppVersionCode(crash.getAppVersionCode());
        impCrash.setAppVersionName(crash.getAppVersionName());
        impCrash.setBrand(crash.getBrand());
        impCrash.setDeviceId(crash.getDeviceId());
        impCrash.setPackageName(crash.getPackageName());
        impCrash.setPhoneModel(crash.getPhoneModel());
        impCrash.setReportId(crash.getReportId());
        if (crash.getLogCat().length() > 989 ) {
        impCrash.setLogCat(crash.getLogCat().substring(0, 989));
        } else {
            impCrash.setLogCat(crash.getLogCat());
        }
        //add device
        //todo need to check for database error and do not perform the device add
        impCrash = CrashHelper.add(impCrash);
        GenericResult genericResult = new GenericResult();
        //did we encounter any application errors
        if (impCrash != null && impCrash.getCrashId() > 0) {
            
            LOGGER.log(Level.OFF, "end find " + impCrash.toString());
            genericResult.setMessage("good");
            return genericResult;
        } else {
            //error encounter
            String result = "database error";
            genericResult.setMessage("error");
            
            return genericResult;
        }
    }
    
}
