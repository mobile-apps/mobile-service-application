/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.service;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.mobile.dto.Device;
import com.mobile.dto.Processor;
import com.mobile.dto.RegisteredUser;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import data.helper.ApplicationEnum;
import data.helper.CategoryHelper;
import data.helper.DeviceHelper;
import data.helper.EventHelper;
import data.helper.JDBCLogHandler;
import data.helper.ProcesserHelper;
import data.helper.RegisterUserHelper;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.mail.internet.InternetAddress;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import org.primefaces.json.JSONObject;
import web.Event;
import web.Site;
import web.email.EmailHelper;
import web.email.EmailMessage;

/**
 * REST Web Service
 *
 * @author wtccuser
 */
@Path("mobile")
public class ProcessorResource {

    // set up the JDBCLogger handler
    JDBCLogHandler jdbcHandler
            = new JDBCLogHandler();

    private Logger LOGGER = Logger.getLogger(ProcessorResource.class
            .getName());

    @Context
    private ServletContext ctx;

    /**
     * Creates a new instance of OganizationResource
     */
    public ProcessorResource() {
        jdbcHandler.clear();
        LOGGER.addHandler(jdbcHandler);
    }

    /**
     * Loop thru users and generate messages
     *
     * @param processor
     * @return
     */
    private Processor generateMessages(Processor processor) {
        LOGGER.log(Level.INFO, "started generatedMessages");
        StringBuffer buffer = new StringBuffer();
        //update to  inprogress
        processor.setStatus(ApplicationEnum.INPROGRESS.toString());
        processor = ProcesserHelper.update(processor);
        // <editor-fold defaultstate="collapsed" desc="loop thru current events">
        //
        List<Event> events = EventHelper.findThisWeek();
        if (events == null || events.size() < 1) {
            //no events
            LOGGER.log(Level.INFO, "generatedMessages no events");

            processor.setComment(ApplicationEnum.NOEVENTS.toString());
            return processor;
        }
        LOGGER.log(Level.INFO, "number of events " + events.size());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="find register users">

        RegisteredUser[] registeredUsers = RegisterUserHelper.findAll();
        if (registeredUsers == null) {
            LOGGER.log(Level.INFO, " no users generatedMessages");

            processor.setStatus(ApplicationEnum.COMPLETED.toString());
            processor.setComment(ApplicationEnum.NOEVENTS.toString());
            return processor;
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="divide phone and email users">
        //
        List<RegisteredUser> emailUsers = new ArrayList<RegisteredUser>();
        List<RegisteredUser> googleUsers = new ArrayList<RegisteredUser>();

        List<InternetAddress> emailAddressList = new ArrayList<InternetAddress>();
        List<RegisteredUser> iPhoneUsers = new ArrayList<RegisteredUser>();

        List<Device> devices = DeviceHelper.findAllDevices();
        for (RegisteredUser user : registeredUsers) {
            String notificationMethod = user.getNotificationmethod();
            //get google user
            if (notificationMethod != null && notificationMethod.equalsIgnoreCase(ApplicationEnum.GOOGLEPUSH.toString())) {
                iPhoneUsers.add(user);
                continue;
            }
            //get google user
            if (notificationMethod != null && notificationMethod.equalsIgnoreCase(ApplicationEnum.IPHONEPUSH.toString())) {
                googleUsers.add(user);
                continue;
            }
            if (notificationMethod != null && notificationMethod.equalsIgnoreCase(ApplicationEnum.EMAIL.toString())) {
                emailUsers.add(user);
                if (user.getEmail() != null) {
                    try {
                        InternetAddress address = new InternetAddress(user.getEmail());
                        emailAddressList.add(address);
                    } catch (Exception ex) {
                        LOGGER.log(Level.INFO, "generatedMessages error " + ex.getMessage());

                    }
                }

            } else {
                iPhoneUsers.add(user);
            }

        }//end of for
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Send google events out">
        //get email information  from web.xml
        LOGGER.log(Level.INFO, "google users size " + googleUsers.size());
        if (googleUsers.size() > 0) {
            //loop thru and send push
            String googleApplicationKey
                    = ctx.getInitParameter("google.push.application.key");
            com.google.android.gcm.server.Sender sender = new Sender(googleApplicationKey);
            //get list of  devices

            List<String> googleRegistrationIds = new ArrayList<String>();
            for (RegisteredUser registeredUser : googleUsers) {
                for (Device device : devices) {
                    if (registeredUser.getRegisteredUserId().equals(device.getRegisteredUserId())) {
                        googleRegistrationIds.add(device.getSubscriber());
                        continue;
                    }

                }
            }

            LOGGER.log(Level.INFO, "google device size " + googleRegistrationIds.size());
            if (googleRegistrationIds.size() > 0) {
                for (Event event : events) {
                    Message.Builder messageBuilder = new Message.Builder();
                    messageBuilder.addData(ApplicationEnum.NAME.toString(), event.getName());
                    messageBuilder.addData("department", event.getDepartment().getName());
                    messageBuilder.addData("id", event.getServiceExtender().getServiceId().toString());
                    messageBuilder.addData(ApplicationEnum.GENERALINFORMATION.toString(), event.getGeneralInformation());
                    String endDateStr = Long.toString(event.getEndDateLong());
                    String startDateStr = Long.toString(event.getStartDateLong());
                    messageBuilder.addData(ApplicationEnum.ENDDATELONG.toString(), endDateStr);
                    messageBuilder.addData(ApplicationEnum.STARTDATELONG.toString(), startDateStr);
                    messageBuilder.addData(ApplicationEnum.PHONE.toString(), event.getPhone());
                    messageBuilder.addData(ApplicationEnum.DEPARTMENTID.toString(), event.getDepartment().getDepartmentId().toString());

                    Message notificationMessage = messageBuilder.build();
                    try {
                        //Result result = sender.send(notificationMessage, registrationIds, 3);
                        LOGGER.log(Level.INFO, "google try to send ");
                        MulticastResult results = sender.send(notificationMessage, googleRegistrationIds, 1);
                        if (results != null) {

                            buffer.append(" google successes ");
                            buffer.append(results.getSuccess());
                            buffer.append(" failures  ");
                            buffer.append(results.getFailure());
                            LOGGER.log(Level.INFO, " google message [" + notificationMessage + "]");
                            processor.setComment(buffer.toString());
                            LOGGER.log(Level.INFO, "google successes  " + results.getSuccess());
                            LOGGER.log(Level.INFO, "google failures  " + results.getFailure());
                            for (Result result : results.getResults()) {
                                String success = "";
                                if (result.getErrorCodeName() == null) {
                                    success = "success";
                                    LOGGER.log(Level.INFO, " [ google message id"
                                            + result.getMessageId() + "]");
                                } else {
                                    success = result.getErrorCodeName();
                                    LOGGER.log(Level.INFO, " [ google error "
                                            + success + "]");
                                }

                            }

                        }

                    } catch (IOException ex) {
                        processor.setStatus(ApplicationEnum.ERROR.toString());
                        buffer.append(ex.getMessage());
                        processor.setComment(buffer.toString());
                        Logger.getLogger(ProcessorResource.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
            //make sure you have some
        }
        // <editor-fold defaultstate="collapsed" desc="Send iphone events out">
        //  ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        try {
            String absolutePathToCert = ctx.getRealPath("/") + "resources/cert/CertificateChurch.p12";
            //InputStream cert = ctx.getResourceAsStream("/WEB-INF/resources/cert/CertificateChurch.p12");
            LOGGER.log(Level.INFO, "retrieved cert [" + absolutePathToCert + "]");

            ApnsService service
                    = APNS.newService()
                    .withCert(absolutePathToCert, "batman")
                    .withSandboxDestination()
                    .build();

            LOGGER.log(Level.INFO, "iPhone users size " + iPhoneUsers.size());
            if (iPhoneUsers.size() > 0) {
                List<String> iphoneRegistrationIds = new ArrayList<String>();
                for (RegisteredUser registeredUser : iPhoneUsers) {
                    for (Device device : devices) {
                        if (registeredUser.getRegisteredUserId().equals(device.getRegisteredUserId())) {
                            iphoneRegistrationIds.add(device.getSubscriber());
                            continue;
                        }

                    }
                }
                for (Event event : events) {

                    if (iphoneRegistrationIds.size() > 0) {
                        for (String subId : iphoneRegistrationIds) {
                            StringBuilder message = new StringBuilder();
                            message.append(event.getStartFormatTime());
                            message.append(" ");
                            message.append(event.getPartialInformation());
                            String payload = APNS.newPayload().alertBody(message.toString()).sound("default").build();
                            service.push(subId, payload);
                            LOGGER.log(Level.INFO, "sending push notification [" + subId + "]");
                        }
                    }

                }

            }//end of if (iPhoneUsers.size() > 0)

            

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, "error getting cert [" + ex.getMessage() + "]");
        }
//InputStream input = externalContext.getResourceAsStream("/resources/icons/foo.png");
        //ResourceBundle resourceBundle = ResourceBundle.getBundle("data.databaseproperties");
//resourceBundle.
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Send email events out">
        //loop thru events
        //get email information  from web.xml
        LOGGER.log(Level.INFO, "email addresses size " + emailAddressList.size());

        //FacesContext ctx = FacesContext.getCurrentInstance();
        // HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        //  String url = req.getRequestURL().toString();
        String urlSuffix
                = ctx.getInitParameter("add.email.url.suffix.address");
        LOGGER.log(Level.INFO, "add.email.url.suffix.address " + urlSuffix);
        //   String root = url.substring(0, url.length() - req.getRequestURI().length()) + req.getContextPath() + "/";
        // String fullURL = root + urlSuffix;
        String host
                = ctx.getInitParameter("email.host.server");
        LOGGER.log(Level.INFO, "email.host.server " + host);
        String hostPassword
                = ctx.getInitParameter("email.host.server.password");
        LOGGER.log(Level.INFO, "email.host.server.password " + hostPassword);
        String hostFrom
                = ctx.getInitParameter("email.from.address");
        LOGGER.log(Level.INFO, "email.from.address " + hostFrom);
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.setEmailServer(host);
        emailMessage.setFrom(hostFrom);
        emailMessage.setPassword(hostPassword);
        EmailHelper emailHelper = new EmailHelper(hostFrom, hostPassword);
        if (emailAddressList.size() > 0) {
            LOGGER.log(Level.INFO, "has email addresses generatedMessages");
            boolean hasError = false;
            for (Event event : events) {
                //get the events and 
                emailMessage.setBody(event.getDisplay());
                emailMessage.setSubject(event.getName());
                emailMessage.setMessage(event.getDisplay());
                try {
                    emailHelper.send(emailMessage, emailAddressList);
                } catch (Exception e) {
                    hasError = true;
                    LOGGER.log(Level.SEVERE, "Error sending stmp " + e.getMessage());
                    buffer.append(" Error sending stmp " + e.getMessage());
                    //break out
                    break;
                }

            }//end of for
            if (!hasError) {
                LOGGER.log(Level.INFO, "email sent successfully)");
                buffer.append(" email sent successfully ");

            }

        }
        // </editor-fold>
        processor.setComment(buffer.toString());
        //if there are email users 
        return processor;
    }

    /**
     * has the process execute today
     *
     * @param processor
     * @return
     */
    private boolean isToday(Processor processor) {
        java.util.Date todayDate = new java.util.Date();
        Calendar calendarToday = Calendar.getInstance();
        calendarToday.setTime(todayDate);

        // processor = generateMessages(processor);
        Calendar calendarProcessDay = Calendar.getInstance();
        calendarProcessDay.setTime(processor.getCreatedDate());
        int yearProcess = calendarProcessDay.get(Calendar.YEAR);
        int monthProcess = calendarProcessDay.get(Calendar.MONTH); // Jan = 0, dec = 11
        int dayOfMonthProcess = calendarProcessDay.get(Calendar.DAY_OF_MONTH);

        int yearCurrent = calendarToday.get(Calendar.YEAR);
        int monthCurrent = calendarToday.get(Calendar.MONTH); // Jan = 0, dec = 11
        int dayOfMonthCurrent = calendarToday.get(Calendar.DAY_OF_MONTH);

        if (yearProcess == yearCurrent
                && monthProcess == monthCurrent
                && dayOfMonthProcess == dayOfMonthCurrent) {
            return true;
        }

        return false;
    }

    /**
     * Retrieves representation of an instance of webservice.OganizationResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/processNotification")
    @Produces(MediaType.APPLICATION_JSON)
    public String processNotification() {
        LOGGER.log(Level.OFF, "started processNotification");

        try {
            //get running status
            Processor processor = ProcesserHelper.find();
            if (processor == null) {
                //first time running
                LOGGER.log(Level.INFO, "processNotification first time running");
                processor = new Processor();
                processor.setComment(ApplicationEnum.NEWPROGRESS.toString());
                processor.setStatus(ApplicationEnum.INPROGRESS.toString());
                processor = ProcesserHelper.add(processor);
                processor = generateMessages(processor);
                processor.setStatus(ApplicationEnum.COMPLETED.toString());
                //loop thru users and  send messagess
            } else {
                LOGGER.log(Level.INFO, "processNotification not first time running");

                if (processor.getStatus().equalsIgnoreCase(ApplicationEnum.COMPLETED.toString())) {
                    //make sure the processing has not all ready executed today
                    boolean hasProcessToday = isToday(processor);
                    LOGGER.log(Level.INFO, "processNotification has it executed today" + hasProcessToday);

                    if (!hasProcessToday) {
                        processor = generateMessages(processor);
                    } else {
                        StringBuilder buffer = new StringBuilder();
                        buffer.append(processor.getCreatedDate().toString());
                        buffer.append(" can not executed more than once on the same day ");
                        processor.setComment(buffer.toString());

                    }
                }
            }
            JsonObjectBuilder jSONObject = Json.createObjectBuilder();
            //save processor
            processor.setStatus(ApplicationEnum.COMPLETED.toString());
            ProcesserHelper.update(processor);
            jSONObject.add("status", processor.getStatus());
            jSONObject.add("comment", processor.getComment());
            LOGGER.log(Level.OFF, "end processNotification");

            return jSONObject.build().toString();
        } catch (Exception ex) {
            Logger.getLogger(ProcessorResource.class.getName()).log(Level.SEVERE, null, ex);
            JsonObjectBuilder errorJSONObject = Json.createObjectBuilder();
            errorJSONObject.add("error ", ex.getMessage());

            LOGGER.log(Level.OFF, "end getAllCategoriesEventsUser");

            return errorJSONObject.build().toString();
        }

    }
}
