/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.service;

import web.service.obj.Subscriber;
import com.mobile.dto.Device;
import com.mobile.dto.RegisteredUser;
import data.helper.CategoryHelper;
import data.helper.DeviceHelper;
import data.helper.EventHelper;
import data.helper.RegisterUserHelper;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import web.Event;
import web.helper.EncryptHelper;

/**
 * REST Web Service
 *
 * @author HSC314
 */
@Path("mobile")
public class UserResource {

    private final static Logger LOGGER = Logger.getLogger(UserResource.class
            .getName());
    @Context
    private UriInfo context;

    @Context
   ServletContext ctx;

    /**
     * Creates a new instance of UserResource
     */
    public UserResource() {
    }

    /**
     * Add registration
     * http://localhost:8080/WebApplication/webresources/mobile/getUser?email=andrew@ncsecu.org&subscriber=test
     *
     * @param email
     * @param subscriber
     * @return
     */
    @POST
    @Path("/updateSubscriber")
    @Produces(MediaType.APPLICATION_JSON)
    public Subscriber update(Subscriber subscriber) {
        LOGGER.log(Level.OFF, "started find ");
        //find or add register users
        //http://localhost:8080/WebApplication/rest/mobile/updateSubscriber
        /*
         {
         "id" : "xdfdfadfasdf",
         "userName" : "johndoe",
         "email" : "johndoe@aol.com",
         "errorMessage" : "none",
         "registerUserId" : 15
         }
         */
        //
        //add device
        //todo need to check for database error and do not perform the device add
        RegisteredUser registeredUser = RegisterUserHelper.update(subscriber);
        //did we encounter any application errors
        if (registeredUser != null) {
            LOGGER.log(Level.OFF, "end find ");

            return subscriber;
        } else {
            //error encounter
            String result = "database error";
            subscriber.setErrorMessage(result);
            return subscriber;
        }
    }
//@FormParam("id")("id")

    @POST
    @Path("/createSubscriber")
    @Consumes(MediaType.APPLICATION_JSON)
    public Subscriber create(Subscriber subscriber) {
        LOGGER.log(Level.OFF, "started find ");
        //find or add register users
        //http://localhost:8080/WebApplication/rest/mobile/createSubscriber
        /*
         {
         "id" : "444444",
         "errorMessage" : "none",
         "registerUserId" : 0
         }
         **/

        //determine if subscriber has already registered
        //DeviceHelper.
        RegisteredUser registeredUser = null;
        Device device = DeviceHelper.findSubscriber(subscriber.getId());
        if (device == null) {
            registeredUser = RegisterUserHelper.add(subscriber);

        //add device
            //todo need to check for database error and do not perform the device add
            device = DeviceHelper.add(subscriber.getId(), registeredUser);

        } else {
           registeredUser = RegisterUserHelper.find(device.getRegisteredUserId());

        }

        //did we encounter any application errors
        if (registeredUser != null && device != null) {
            subscriber.setRegisterUserId(registeredUser.getRegisteredUserId());

            LOGGER.log(Level.OFF, "end find ");
            String result = "User saved : " + registeredUser.getRegisteredUserId();
            subscriber.setStatus(registeredUser.getStatus());
            return subscriber;
        } else {
            //error encounter
            String result = "database error";

            subscriber.setErrorMessage(result);
            return subscriber;
        }
    }

}
