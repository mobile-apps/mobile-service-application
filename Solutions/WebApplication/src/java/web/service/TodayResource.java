/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.service;

import data.helper.EventHelper;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import web.Event;
import web.WebConstants;
import web.podcast.Episode;
import web.podcast.Podcast;
import web.podcast.PodcastXML;
import web.service.obj.Organization;
import web.youtube.YouTube;
import web.youtube.YouTubeRSSParser;

/**
 *
 * @author wtccuser
 */
@Path("mobile")
public class TodayResource {

    private final static Logger LOGGER = Logger.getLogger(TodayResource.class
            .getName());
    
    @Context
    private UriInfo context;

    @Context
    private ServletContext ctx;

    private JsonObjectBuilder getPodcastHelper() {
        LOGGER.log(Level.INFO, "started getAllPodcast");
        try {
            String podcastURL = (String) ctx.getInitParameter("podcast.address");
            LOGGER.log(Level.INFO, "podcastURL " + podcastURL);
            Podcast podcast = new PodcastXML(new URL("http://cokcc.podomatic.com/rss2.xml"));//
            //Podcast podcast = new PodcastXML(new URL(podcastURL));
            //System.out.println("Title - " + podcast.getTitle());

            List<Episode> episodes = podcast.getEpisodes();
            String t3 = podcast.getImageURL().getRef();
            //sort
            Collections.sort(episodes);
            if (episodes != null) {
                LOGGER.log(Level.INFO, "episodes size " + episodes.size());
            }
            int frameCounter = 0;
            Episode episode = episodes.get(0);

            JsonObjectBuilder episodeJSONObject = Json.createObjectBuilder();
            try {

                episodeJSONObject.add(WebConstants.PODCAST_TITLE, episode.getTitle());
                episodeJSONObject.add(WebConstants.PODCAST_DESCRIPTION, episode.getSummary());
                episodeJSONObject.add(WebConstants.PODCAST_IMAGE_URL, episode.getImageURL());
                //String href = episode.getHref();
                String href = episode.getGuid();
                episodeJSONObject.add(WebConstants.PODCAST_URL, href);
                //String publishDate = DateHelper.toString(episode.getPubDate().toString());
                episodeJSONObject.add(WebConstants.PODCAST_PUBLISH_DATE, episode.getShortDate());
                //episodeJSONObject.add(WebConstants.PODCAST_MEDIA, episode.getMediaLocation().toString());
            } catch (Exception ex) {
                LOGGER.log(Level.SEVERE, "error " + ex.getMessage());
                throw new java.lang.IllegalStateException(ex);
            }

//episodeJSONObject.add("description", episode.getShortDescription());
            //LOGGER.log(Level.INFO, episodeJSONObject.build().toString());
            LOGGER.log(Level.INFO, "ending getAllPodcast ");
            return episodeJSONObject;
        } catch (Exception ex) {

            Logger.getLogger(OganizationResource.class.getName()).log(Level.SEVERE, null, ex);
            throw new java.lang.IllegalStateException(ex);

        }

    }

    @GET
    @Path("/getToday")
    @Produces(MediaType.APPLICATION_JSON)
    public String getToday() {
        LOGGER.log(Level.OFF, "started getToday");
        try {
            // <editor-fold defaultstate="collapsed" desc="get first Event">

            ArrayList<Event> myEvents = new ArrayList<Event>();
            List<Event> events = EventHelper.findAll(true);
            JsonObjectBuilder eventJSONObject = Json.createObjectBuilder();
            if (events != null && events.size() > 0) {
                Event event = events.get(0);
                eventJSONObject.add("name", event.getName());
                eventJSONObject.add("phone", event.getPhone());
                eventJSONObject.add("endDateLong", event.getEndDateLong());
                eventJSONObject.add("latitude", event.getLatitude());
                eventJSONObject.add("longitude", event.getLongitude());
                eventJSONObject.add("location", event.getLocation());
                eventJSONObject.add("startDateLong", event.getStartDateLong());
                eventJSONObject.add("generalinformation", event.getGeneralInformation());
                eventJSONObject.add("image", event.getImage());
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="get first  YouTube">
            //  List<com.mobile.dto.Category> categories = CategoryHelper.findAll();
            List<YouTube> list = YouTubeRSSParser.getAll();

            JsonObjectBuilder youTubeJSONObject = Json.createObjectBuilder();
            if (list != null && list.size() > 0) {
                YouTube youTube = list.get(0);

                youTubeJSONObject.add("height", Integer.parseInt(youTube.getHeight()));
                youTubeJSONObject.add("imageURL", youTube.getImageURL());
                youTubeJSONObject.add("published", youTube.getPublished());
                youTubeJSONObject.add("title", youTube.getTitle());
                youTubeJSONObject.add("width", Integer.parseInt(youTube.getWidth()));
                youTubeJSONObject.add("id", youTube.getYouTubeID());
                youTubeJSONObject.add("youTubeURL", youTube.getYouTubeURL());

            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="get first Podcast">
            JsonObjectBuilder podcastObjectBuilder = getPodcastHelper();
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="get Org">
            List<com.mobile.dto.Organization> listOrg = data.helper.OrganizationHelper.findAll();

            //get first org
            com.mobile.dto.Organization firstOrg = listOrg.get(0);
            Organization organization = new Organization();
            JsonObjectBuilder orgJSONObject = Json.createObjectBuilder();
            if (firstOrg.getEmail() != null) {
                orgJSONObject.add("email", firstOrg.getEmail());
            }
            if (firstOrg.getGeneralInformation() != null) {
                orgJSONObject.add("generalinformation", firstOrg.getGeneralInformation());
            }
            if (firstOrg.getLocation() != null) {
                orgJSONObject.add("location", firstOrg.getLocation());
            }
            if (firstOrg.getLatitude() != null) {
                orgJSONObject.add("latitude", firstOrg.getLatitude());
            }
            if (firstOrg.getLongitude() != null) {
                orgJSONObject.add("longitude", firstOrg.getLongitude());
            }
            if (firstOrg.getPhone() != null) {
                orgJSONObject.add("phone", firstOrg.getPhone());
            }
            if (firstOrg.getIconstring() != null) {
                orgJSONObject.add("image", firstOrg.getIconstring());
            }
            if (firstOrg.getName() != null) {
                orgJSONObject.add("name", firstOrg.getName());
            }
            // </editor-fold>
            JsonObjectBuilder todayJSONObject = Json.createObjectBuilder();
            todayJSONObject.add("YouTube", youTubeJSONObject);
            todayJSONObject.add("Service", eventJSONObject);
            todayJSONObject.add("Podcast", podcastObjectBuilder);
            todayJSONObject.add("Org", orgJSONObject);
            String output = todayJSONObject.build().toString();
            LOGGER.log(Level.OFF, output);
            LOGGER.log(Level.OFF, "end getToday");
            //Response.status(200).header("responseHeaderName", "responseHeaderValue").header("anotherResponseHeaderName", "foo").build();  
            //                                                                      Access-Control-Allow-Origin:   http://swagger.io           Access-Control-Allow-Headers:   Origin, X-Requested-With, Content-Type, Accept
            return output;
//return Response.ok(todayJSONObject.build(), MediaType.APPLICATION_JSON).header("Access-Control-Allow-Origin", "*").header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept").build();

        } catch (Exception ex) {
            Logger.getLogger(YouTubeResource.class.getName()).log(Level.SEVERE, null, ex);
            JsonObjectBuilder errorJSONObject = Json.createObjectBuilder();
            errorJSONObject.add("error ", ex.getMessage());

            LOGGER.log(Level.OFF, "end gettoday");

            return errorJSONObject.build().toString();
        }

    }

}
