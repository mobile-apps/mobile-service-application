/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.service;

import com.mobile.dto.Department;
import data.helper.CategoryHelper;
import data.helper.DepartmentHelper;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import org.primefaces.json.JSONObject;
import web.Site;
import web.obj.DepartmentObj;

/**
 * REST Web Service
 *
 * @author wtccuser
 */
@Path("mobile")
public class DepartmentResource {

    private final static Logger LOGGER = Logger.getLogger(DepartmentResource.class
            .getName());

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of OganizationResource
     */
    public DepartmentResource() {
    }

    /**
     * Retrieves representation of an instance of webservice.OganizationResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/getAllDepartments")
    @Produces(MediaType.TEXT_PLAIN)
    public String getAllDepartments() {
        LOGGER.log(Level.OFF, "started getAllDepartments");
        try {
            //  List<com.mobile.dto.Category> categories = CategoryHelper.findAll();
            List<com.mobile.dto.Department> departments = DepartmentHelper.findAll();

            List<DepartmentObj> departmentObjs = new ArrayList<DepartmentObj>();
            JsonArrayBuilder departmentsBuilderArray = Json.createArrayBuilder();
            for (Department dep : departments) {
                DepartmentObj departmentObj = new DepartmentObj(dep);
                departmentObjs.add(departmentObj);
                JsonObjectBuilder depJSONObject = Json.createObjectBuilder();
                depJSONObject.add("email", dep.getEmail());
                depJSONObject.add("generalInformation", dep.getGeneralInformation());
                depJSONObject.add("departmentId", dep.getDepartmentId());
                depJSONObject.add("name", dep.getName());
                depJSONObject.add("phone", dep.getPhone());
                depJSONObject.add("icon", dep.getIconstring());
                depJSONObject.add("latitude", dep.getLatitude());
                depJSONObject.add("longitude", dep.getLongitude());
                depJSONObject.add("location", dep.getLocation());
                departmentsBuilderArray.add(depJSONObject);

            }
            JsonObjectBuilder departmentJSONObject = Json.createObjectBuilder();
            departmentJSONObject.add("departments", departmentsBuilderArray);
            
            LOGGER.log(Level.OFF, "end getAllDepartments");

            return departmentJSONObject.build().toString();
        } catch (Exception ex) {
            Logger.getLogger(DepartmentResource.class.getName()).log(Level.SEVERE, null, ex);
            JsonObjectBuilder errorJSONObject = Json.createObjectBuilder();
            errorJSONObject.add("error ", ex.getMessage());
            
            LOGGER.log(Level.OFF, "end getAllDepartments");
            
            return errorJSONObject.build().toString();
        }

    }
}
