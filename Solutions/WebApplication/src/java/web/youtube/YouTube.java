/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package web.youtube;

/**
 *
 * @author wtccuser
 */
public class YouTube {
    private String youTubeID;

    public String getYouTubeID() {
        int foundPosition = youTubeURL.indexOf("=");
        if (foundPosition > 0 ) {
            int endPosition = youTubeURL.trim().length();
            youTubeID = youTubeURL.substring(foundPosition + 1, endPosition);
        }
        return youTubeID;
    }

    public void setYouTubeID(String youTubeID) {
        this.youTubeID = youTubeID;
    }
    private String title;
    private String imageURL;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getYouTubeURL() {
        return youTubeURL;
    }

    public void setYouTubeURL(String youTubeURL) {
        this.youTubeURL = youTubeURL;
    }
    private String published;
    private String youTubeURL;

    public YouTube(String title, String imageURL, String published, String youTubeURL, String height, String width) {
        this.title = title;
        this.imageURL = imageURL;
        this.published = published;
        this.youTubeURL = youTubeURL;
        this.height = height;
        this.width = width;
    }
    private String height;

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }
    private String width;
    
    
}
