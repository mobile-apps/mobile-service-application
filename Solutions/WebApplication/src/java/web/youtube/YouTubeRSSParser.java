/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.youtube;

import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import web.youtube.YouTube;

/**
 *
 * @author wtccuser
 */
public class YouTubeRSSParser {
    public static List<YouTube> getAll() {
       List<YouTube> youTubelist = new ArrayList<YouTube>();
       String url = "https://www.youtube.com/feeds/videos.xml?channel_id=UC6YFdyS2cGDmjLe8y7CtGVA";
        try {
            DocumentBuilderFactory f
                    = DocumentBuilderFactory.newInstance();
            DocumentBuilder b = f.newDocumentBuilder();
            Document doc = b.parse(url);

            doc.getDocumentElement().normalize();
            System.out.println("Root element: "
                    + doc.getDocumentElement().getNodeName());

            // loop through each item
            NodeList items = doc.getElementsByTagName("entry");
            for (int i = 0; i < items.getLength(); i++) {
                Node n = items.item(i);
                if (n.getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                Element e = (Element) n;
                //get publish date
                NodeList publishList = e.getElementsByTagName("published");
                Element publishElem = (Element) publishList.item(0);
                Node publishNode = publishElem.getChildNodes().item(0);
                //get title
                NodeList titleList = e.getElementsByTagName("title");
                Element titleElem = (Element) titleList.item(0);
                Node titleNode = titleElem.getChildNodes().item(0);
                //get youtube url
                NodeList urlYoutubeList = e.getElementsByTagName("link");
                Element urlYouTubeElem = (Element) urlYoutubeList.item(0);
                //get image height width
                NodeList groupList = e.getElementsByTagName("media:group");
                Element groupListElem = (Element) groupList.item(0);
                NodeList imageList = groupListElem.getElementsByTagName("media:thumbnail");
                Element imageElem = (Element) imageList.item(0);
                // public YouTube(String title, String imageURL, String published, String youTubeURL, String height, String width)
                String title = titleNode.getNodeValue();
                String imageURL = imageElem.getAttribute("url");
                String published = publishNode.getNodeValue();
                String youTubeURL = urlYouTubeElem.getAttribute("href");
                String height = imageElem.getAttribute("height");
                String width = imageElem.getAttribute("width");
                YouTube youTube = new YouTube(title, imageURL, published, youTubeURL, height, width);
                youTubelist.add(youTube);
                System.out.println("image url " + imageElem.getAttribute("url"));
                System.out.println("image height " + imageElem.getAttribute("height"));
                 System.out.println("image width " + imageElem.getAttribute("width"));
             
                //Node publishNode = publishElem.getChildNodes().item(0);
                System.out.println("urlYoube " + urlYouTubeElem.getAttribute("href"));
                System.out.println("publish date " + publishNode.getNodeValue());
                System.out.println("title " + titleNode.getNodeValue());

                System.out.println(i);
                // get the "title elem" in this item (only one)
                //NodeList titleList = 
                //e.getElementsByTagName("title");
                //Element titleElem = (Element) titleList.item(0);

                // get the "text node" in the title (only one)
                //Node titleNode = titleElem.getChildNodes().item(0);
                //System.out.println(titleNode.getNodeValue());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
       
       return youTubelist;
    }
    public static void main(String[] args) {//http://news.google.com/?output=rss
      getAll();   
    }
}
