/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package web.mobel;

import com.mobile.dto.Department;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author wtccuser
 */
public class DepartmentDataModel extends ListDataModel<Department> implements SelectableDataModel<Department>{

    public DepartmentDataModel() {
    }

    public DepartmentDataModel(List<Department> data) {
        super(data);
    }
    @Override
    public Object getRowKey(Department department) {
        return department.getDepartmentId().toString();
    }

    @Override
    public Department getRowData(String rowKey) {
        List<Department> departments = (List<Department>) getWrappedData();
        for(Department department : departments) {
            if(department.getDepartmentId().toString().equals(rowKey))
                return department;
        }
        return null;
    }

    
    
}
