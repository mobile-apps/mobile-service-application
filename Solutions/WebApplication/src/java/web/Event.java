/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import com.mobile.dto.Department;
import data.extender.ServiceExtender;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Logger;

/**
 *
 * @author HopeBenziger
 */
public class Event implements Comparable<Event> {

    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());

    private String name;
    private String phone;
    private Date date;
    private Double latitude;
    private String location;

    public String getShortGeneralInformation() {
        String shortDisplay = name;
        if (this.generalInformation != null) {
            if (this.generalInformation.length() > 50) {
                shortDisplay = this.generalInformation.substring(0, 50);
                shortDisplay = shortDisplay + "......";

            } else {
                shortDisplay = this.generalInformation;
            }
        }
        return shortDisplay;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    private String time;
    private String image;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
    private int year;
    private int day;

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
    private String dayOfWeek;
    private String month;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
    private Double longitude;

    public ServiceExtender getServiceExtender() {
        return serviceExtender;
    }

    public void setServiceExtender(ServiceExtender serviceExtender) {
        this.serviceExtender = serviceExtender;
    }
    private String generalInformation;
    private Department department;
    private long startDateLong;
    private ServiceExtender serviceExtender;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getStartDayOfMonth() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public int getStartYear() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }

    public String getStartFormatTime() {
        TimeZone mtz = TimeZone.getTimeZone("EST");
        
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        sdf.setTimeZone(mtz);

        String time = sdf.format(date);

        return time;
    }

    public String getStartDateMonthName() {
        Calendar cal = Calendar.getInstance();
        String monthName = new SimpleDateFormat("MMM").format(date);
        return monthName;
    }

    TimeZone mtz = TimeZone.getTimeZone("EST");
    DateFormat mdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'",Locale.US);

    public String getStartDateUTC() {
        mdf.setTimeZone(mtz);
        String nowAsISO = mdf.format(date);
        return nowAsISO;
    }

    public String getEndDateUTC() {
        mdf.setTimeZone(mtz);
        String nowAsISO = mdf.format(endDate);
        
        return nowAsISO;
    }

    public long getStartDateLong() {
        startDateLong = date.getTime();
        return startDateLong;
    }

    public void setStartDateLong(long startDateLong) {
        this.startDateLong = startDateLong;
    }

    public long getEndDateLong() {
        endDateLong = endDate.getTime();
        return endDateLong;
    }

    public void setEndDateLong(long endDateLong) {
        this.endDateLong = endDateLong;
    }
    private long endDateLong;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    private Date endDate;

    public Department getDepartment() {
        return department;
    }

    public String getDisplay() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(getGeneralInformation());
        buffer.append("\n\r");
        buffer.append(department.getName());
        buffer.append("\n\r");
        buffer.append(department.getLocation());
        buffer.append("\n\r");
        buffer.append(this.getDate().toString());
        return buffer.toString();

    }

    public void setDeparment(Department deparment) {
        this.department = deparment;
    }

    public String getPartialInformation() {
        String partial = getGeneralInformation();
        if (partial.length() > 20) {
            partial = partial.substring(0, 19);
            partial = partial + "...";
        }
        return partial;
    }

    public String getWebPartialInformation() {
        String partial = getGeneralInformation();
        if (partial.length() > 40) {
            partial = partial.substring(0, 39);
            partial = partial + "...";
        }
        return partial;
    }

    public String getGeneralInformation() {
        if (generalInformation == null) {
            generalInformation = name;
        }
        return generalInformation;
    }

    public void setGeneralInformation(String generalInformation) {
        this.generalInformation = generalInformation;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        TimeZone tz = TimeZone.getTimeZone("EST");

        // set the time zone with the given time zone value 
        // and print it
        Calendar cal = Calendar.getInstance(tz);

        cal.setTime(date);
        return cal.getTime();
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int compareTo(Event o) {

        return getDate().compareTo(o.getDate());
    }

}
