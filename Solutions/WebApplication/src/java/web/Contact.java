/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package web;

/**
 *
 * @author wtccuser
 */
public class Contact {
    private String firstName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isPreferredMethodOfContact() {
        return preferredMethodOfContact;
    }

    public void setPreferredMethodOfContact(boolean preferredMethodOfContact) {
        this.preferredMethodOfContact = preferredMethodOfContact;
    }
    private String lastName;
    private String phone;
    private String emailAddress;
    private String comment;
    private boolean preferredMethodOfContact;
    
    
}
