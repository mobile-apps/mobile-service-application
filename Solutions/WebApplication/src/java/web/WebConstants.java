/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

/**
 *
 * @author wtccuser
 */
public class WebConstants {

    public static final String EVENT_SEARCH_RESULT_PAGE = "eventsearchresult?faces-redirect=true";
    public static final String USER = "user";
    public static final String CATEGORIES_PAGE = "categories";

    public static final String CATEGORY_DETAIL_PAGE = "categoryDetail?faces-redirect=true";
    public static final String SELECTED_CATEGORY = "selectedCategory";
    public static final String ORGANIZATIONS_PAGE = "organizations";
    public static final String ORGANIZATION_EDIT_PAGE = "editOrganization?faces-redirect=true";
    public static final String DEPARTMENTS_PAGE = "departments";
    public static final String DEPARTMENT_EDIT_PAGE = "editDepartment?faces-redirect=true";
    public static final String SELECTED_DEPARTMENT = "selectedDepartment";
    public static final String SERVICES_PAGE = "services";
    public static final String ORGANIZATION_ICONFILE = "organaization icon file";
    public static final String DEPARTMENT_ICONFILE = "department icon file";
    public static final String ORGANIZATIONBEAN_ORGLIST = "organizationbean organization list";
    public static final String ORGANIZATIONBEAN_ORGEXTLIST = "organizationbean organization extender list";
    public static final String DEPARTMENTBEAN_DEPTLIST = "departmentbean department list";
    public static final String DEPARTMENTBEAN_DEPTEXTLIST = "departmentbean department extender list";
    public static final String ORGANIZATIONBEAN_SELECTEDORG = "organizationbean_selected_organization";
    public static final String DEPARTMENTBEAN_SELECTEDDEPT = "departmentbean_selected_department";
    public static final String PODCAST_TITLE = "title";
    public static final String PODCAST_DESCRIPTION = "description";
    public static final String PODCAST_PUBLISH_DATE = "pubDate";
    public static final String PODCAST_LOCATION = "location";
    public static final String PODCAST_URL = "url";
    public static final String PODCAST_MEDIA = "mediaLocation";
    public static final String PODCAST_IMAGE_URL="imageURL";
    public static final String RTNEDITMSG = "rtneditmsg";
    public static final String ORGNODELETEMSG = "Cannot delete, organization still has associated departments.";
    public static final String DEPTNODELETEMSG = "Cannot delete, department still has associated services.";
    public static final String CRASHNODELETEMSG = "No records deleted, either report was empty or clearing failed.";
    public static final String CRASHYESDELETEMSG = "Crash report was successfully cleared.";
    public static final String LOFMSG = "No records deleted, either logs was empty.";
    public static final String LOGYESDELETEMSG = "Log was successfully cleared.";
}
