/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

/**
 *
 * @author wtccuser
 */
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RedirectServlet extends HttpServlet {
  
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        //2. Initialize the browser info variables
        String userAgent = request.getHeader("User-Agent");
        String httpAccept = request.getHeader("Accept");

        //3. Create the UAgentInfo object
        UAgentInfo detector = new UAgentInfo(userAgent, httpAccept);

        //4. Detect whether the visitor is using a mobile device.
        // For example, if it's an iPhone, redirect them to the 
        // iPhone-optimized version of your web site.
        boolean isMobile = detector.detectMobileQuick();
        String nextPage = "faces/guest/default.xhtml";
        if (isMobile) {
            nextPage = "javascript:top.location.href ='faces/mobile/default.xhtml'";
           
        }
       // Set response content type
      response.setContentType("text/html");
      
      response.setStatus(response.SC_MOVED_TEMPORARILY);
      response.setHeader("Location", nextPage);
        /*
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<h1>");
        out.println(isMobile);
         out.println("</h1>");
        
        out.println("</body>");
        out.println("</html>");
                */
    }
}
