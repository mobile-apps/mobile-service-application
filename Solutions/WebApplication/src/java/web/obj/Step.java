/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.obj;

/**
 *
 * @author wtccuser
 */
public class Step {

    private String leg;
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    private String maneuver;

    public Step(String leg) {
        this.leg = leg;
    }

    public void setImageSrc(boolean isFirstStep, boolean isLastStep) {
        if (isFirstStep) {
            image = "route_start.png";
            return;

        }
        if (isLastStep) {
            image = "route_end.png";
            return;
        }
        if (maneuver != null){
            String replaceText = maneuver.replace("-", "_");
            image = replaceText + ".png";
            return;
        } else {
            image = "route_end.png";
            return;
        }
   

    }

    public Step(String leg, String maneuver, String duration, String distance) {
        this.leg = leg;
        this.maneuver = maneuver;
        this.duration = duration;
        this.distance = distance;
    }

    public String getManeuver() {
        return maneuver;
    }

    public void setManeuver(String maneuver) {
        this.maneuver = maneuver;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
    private String duration;
    private String distance;

    public String getLeg() {
        return leg;
    }

    public void setLeg(String leg) {
        this.leg = leg;
    }
}
