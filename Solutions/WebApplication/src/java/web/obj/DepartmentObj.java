/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package web.obj;

import com.mobile.dto.Department;

/**
 *
 * @author wtccuser
 */
public class DepartmentObj {
    private String id;
    private String location;

    
    public DepartmentObj(Department department) {
        id = department.getDepartmentId().toString();
        location = department.getLocation();
        this.email = department.getEmail();
        this.name = department.getName();
        this.phone = department.getPhone();
        this.generalInformation = department.getGeneralInformation();
        
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    
    
    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGeneralInformation() {
        return generalInformation;
    }

    public void setGeneralInformation(String generalInformation) {
        this.generalInformation = generalInformation;
    }
    private String name; 
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    private String email;
    private String generalInformation;
    
    
}
