/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package web.obj;

import com.mobile.dto.Department;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author wtccuser
 */
public class CategoryMenuObj {
    private String name;

    public CategoryMenuObj(String name, List<Department> departments) {
        this.name = name;
        this.departments = departments;
    }
    private List<com.mobile.dto.Department> departments;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }
    
}
