/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.obj;

import java.util.List;

/**
 *
 * @author administrator
 */
public class GoogleMap {

    public String getStartingAddress() {
        return startingAddress;
    }

    public GoogleMap(String startingAddress, String endingAddress, List<Step> steps) {
        this.startingAddress = startingAddress;
        this.endingAddress = endingAddress;
        this.steps = steps;
    }

    public void setStartingAddress(String startingAddress) {
        this.startingAddress = startingAddress;
    }

    public String getEndingAddress() {
        return endingAddress;
    }

    public void setEndingAddress(String endingAddress) {
        this.endingAddress = endingAddress;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }
    private String startingAddress;
    private String endingAddress;
    private List<Step> steps;
    
}
