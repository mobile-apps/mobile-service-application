/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.email;

import data.helper.JDBCLogHandler;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.*;
import javax.mail.internet.*;

/**
 *
 * @author wtccuser
 */
public class EmailHelper {

    // use the classname for the logger, this way you can refactor
    private final static Logger mLog = Logger.getLogger(EmailHelper.class
            .getName());

    private String from;
    private String password;
    JDBCLogHandler jdbcHandler
            = new JDBCLogHandler();

    public EmailHelper(String from, String password) {
        mLog.log(Level.INFO, "EmailHelper(String from, String password)");
        this.from = from;
        this.password = password;
         //jdbcHandler.clear();
        mLog.addHandler(jdbcHandler);
    }

    public EmailHelper() {
        
        mLog.addHandler(jdbcHandler);
    }

    private class SMTPAuthenticator extends javax.mail.Authenticator {

        public PasswordAuthentication getPasswordAuthentication() {
            if (from != null && password != null) {
                mLog.log(Level.INFO, "password " + password + " from " + from);
                return new PasswordAuthentication(from, password);
            } else {
                mLog.log(Level.INFO, "support@cokcc.org ,batmanZZ00");

                return new PasswordAuthentication("support@cokcc.org", "batmanZZ00");
            }
        }
    }

    /**
     *
     * @param emailMessage
     * @param emailAddressList
     */
    public void send(EmailMessage emailMessage, List<InternetAddress> emailAddressList) {
        mLog.log(Level.INFO, "begin send ");
        String to = emailMessage.getTo();
        // Sender's email ID needs to be mentioned
        //String from = "support@cokcc.org";
        String from = emailMessage.getFrom();

        // Assuming you are sending email from localhost
        // String host = "mail.cokcc.org";
        String host = emailMessage.getEmailServer();

        // Get system properties
        Properties properties = System.getProperties();
        mLog.log(Level.INFO, "host " + host);
        // Setup mail server
        properties.setProperty("mail.smtp.host", host);
        properties.setProperty("mail.smtp.auth", "true");
        SecurityManager security = System.getSecurityManager();
        Authenticator auth = new SMTPAuthenticator();
        // Get the default Session object.
        Session session = Session.getDefaultInstance(properties, auth);

        try {
            mLog.log(Level.INFO, "Create a default MimeMessage object");
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);
            mLog.log(Level.INFO, " from " + from);
            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            for (InternetAddress internetAddress : emailAddressList) {
                message.addRecipient(Message.RecipientType.TO,
                        internetAddress);
                mLog.log(Level.INFO, " to [" + internetAddress.getAddress() + "]");
            }

            mLog.log(Level.INFO, " subject " + emailMessage.getSubject());
            // Set Subject: header field
            message.setSubject(emailMessage.getSubject());
            mLog.log(Level.INFO, " body " + emailMessage.getBody());
            // Now set the actual message
            message.setText(emailMessage.getBody());
            mLog.log(Level.INFO, "Try to send message");
            // Send message
            Transport.send(message);
            mLog.log(Level.INFO, "Sent message successfully....");
            System.out.println("Sent message successfully....");
        } catch (Exception mex) {

            String error = mex.getMessage();
            mLog.log(Level.SEVERE, "error " + error);
            mex.printStackTrace();
            throw new java.lang.IllegalStateException(mex.getMessage());
        }// TODO cod 
    }

    /**
     *
     * @param emailMessage
     */
    public void send(EmailMessage emailMessage) {
        mLog.log(Level.INFO, "begin send ");
        String to = emailMessage.getTo();
        // Sender's email ID needs to be mentioned
        //String from = "support@cokcc.org";
        String from = emailMessage.getFrom();

        // Assuming you are sending email from localhost
        // String host = "mail.cokcc.org";
        String host = emailMessage.getEmailServer();

        // Get system properties
        Properties properties = System.getProperties();
        mLog.log(Level.INFO, "host " + host);
        // Setup mail server
        properties.setProperty("mail.smtp.host", host);
        properties.setProperty("mail.smtp.auth", "true");
        SecurityManager security = System.getSecurityManager();
        Authenticator auth = new SMTPAuthenticator();
        // Get the default Session object.
        Session session = Session.getDefaultInstance(properties, auth);

        try {
            mLog.log(Level.INFO, "Create a default MimeMessage object");
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);
            mLog.log(Level.INFO, " from " + from);
            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));
            mLog.log(Level.INFO, " to [" + to + "]");
            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(to));
            mLog.log(Level.INFO, " subject " + emailMessage.getSubject());
            // Set Subject: header field
            message.setSubject(emailMessage.getSubject());
            mLog.log(Level.INFO, " body " + emailMessage.getBody());
            // Now set the actual message
            message.setText(emailMessage.getBody());
            mLog.log(Level.INFO, "Try to send message");
            // Send message
            Transport.send(message);
            mLog.log(Level.INFO, "Sent message successfully....");
            System.out.println("Sent message successfully....");
        } catch (Exception mex) {

            String error = mex.getMessage();
            mLog.log(Level.SEVERE, "error " + error);
            mex.printStackTrace();
            throw new java.lang.IllegalStateException(mex.getMessage());
        }// TODO cod
    }

    public void send() {
        // Recipient's email ID needs to be mentioned.

        String to = "java.university@nc.rr.com";
        // Sender's email ID needs to be mentioned
        String from = "support@cokcc.org";

        // Assuming you are sending email from localhost
        String host = "mail.cokcc.org";

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.setProperty("mail.smtp.host", host);
        properties.setProperty("mail.smtp.auth", "true");
        SecurityManager security = System.getSecurityManager();
        Authenticator auth = new SMTPAuthenticator();
        // Get the default Session object.
        Session session = Session.getDefaultInstance(properties, auth);

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(to));

            // Set Subject: header field
            message.setSubject("This is the Subject Line!");

            // Now set the actual message
            message.setText("This is actual message");

            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }// TODO code application logic here
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EmailHelper emailClient = new EmailHelper();
        emailClient.send();
    }

}
