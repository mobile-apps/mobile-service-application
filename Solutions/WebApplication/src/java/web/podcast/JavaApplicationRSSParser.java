/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.podcast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 *
 * @author administrator
 */
public class JavaApplicationRSSParser {

    /**
     * http://cokcc.podomatic.com/rss2.xml
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        Podcast podcast = new PodcastXML(new URL("http://cokcc.podomatic.com/rss2.xml"));
        System.out.println("Title - " + podcast.getTitle());
        List<Episode> episodes = podcast.getEpisodes();
        for (Episode episode : episodes) {
            System.out.println("Episode Title - " + episode.getTitle());
            System.out.println("Episode Guid - " + episode.getGuid());
            System.out.println("Episode media - " + episode.getMediaLocation().toString());
          //  System.out.println("Episode pub date - " + episode.pubDate);
            System.out.println("Episode comments - " + episode.getComments());
        }
    }

}
