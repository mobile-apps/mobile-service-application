/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.podcast;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Episode implements Comparable<Episode>{
	
	private String title;
	private URL link;
	private String description;
	private String encodedContent;
	private String author;
	private String[] categories;
	private URL comments;
	//Enclosure
	private URL mediaLocation;
	private long mediaLength;
	private String mediaType;
	private String imageURL;
        private String summary;

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
	private String guid;
	private Date pubDate;
	private String sourceName;
	private URL sourceLink;
        private String shortDate;
        
   public String getHref() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<a href='");
        buffer.append( getGuid());
        buffer.append("'>");
        buffer.append("Listen");
        buffer.append("</a>");
       return buffer.toString();
   }
   
   public String getShortDate() {
       SimpleDateFormat simpleFormat = new SimpleDateFormat("MM-dd-yy");
       String display = simpleFormat.format(pubDate);
       return display;
   }
   public void setShortDate(){};
   
     public String getPodcastHref() {
        StringBuffer buffer = new StringBuffer();
        //<a class="btn btn-default" href="#" data-featherlight="#fl3">iFrame</a>
        buffer.append("<a href='");
        buffer.append( getGuid());
        buffer.append("' class='btn btn-info' data-featherlight='");
        buffer.append("iframe");
        buffer.append("' >Listen</a>");
        return buffer.toString();
   }
   
 

   
   public String getPodcastText() {
      // java.text.MessageFormat
    String xml= "<a class='btn btn-default' href='{0}' data-featherlight='#fl1'>Listen</a>";
   
   
    
    java.text.MessageFormat messageFormat = new java.text.MessageFormat(xml);
    Object[] subs = new Object[] {getGuid()};
    String results = messageFormat.format(subs);
    return results;
   }
   
   
   
    public String getShortDescription() {
        String searchStr = "<br />";
        int initialPos = description.indexOf(searchStr);
        shortDescription = "unknown";
        if (initialPos > 0) {
            int adjStartPos = initialPos + searchStr.length();
            shortDescription = description.substring(adjStartPos, description.length());
        }
        
//description
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }
        private String shortDescription;
	
	public Episode() {
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public URL getLink() {
		return link;
	}
	
	public void setLink(URL link) {
		this.link = link;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getEncodedContent() {
		return encodedContent;
	}

	public void setEncodedContent(String encodedContent) {
		this.encodedContent = encodedContent;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public String[] getCategories() {
		return categories;
	}
	
	public void setCategories(String[] categories) {
		this.categories = categories;
	}
	
	public URL getComments() {
		return comments;
	}
	
	public void setComments(URL comments) {
		this.comments = comments;
	}
	
	public URL getMediaLocation() {
		return mediaLocation;
	}
	
	public void setMediaLocation(URL mediaLocation) {
		this.mediaLocation = mediaLocation;
	}
	
	public long getMediaLength() {
		return mediaLength;
	}
	
	public void setMediaLength(long mediaLength) {
		this.mediaLength = mediaLength;
	}
	
	public String getMediaType() {
		return mediaType;
	}
	
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
	
	public String getGuid() {
		return guid;
	}
	
	public void setGuid(String guid) {
		this.guid = guid;
	}
	
	public Date getPubDate() {
		return pubDate;
	}
	
	public void setPubDate(Date pubDate) {
		this.pubDate = pubDate;
	}
	
	public String getSourceName() {
		return sourceName;
	}
	
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	
	public URL getSourceLink() {
		return sourceLink;
	}
	
	public void setSourceLink(URL sourceLink) {
		this.sourceLink = sourceLink;
	}

    @Override
    public int compareTo(Episode o) {
        int result = o.pubDate.compareTo(pubDate);
        return result;
    }
	
}