/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.podcast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import web.helper.DateHelper;
import web.service.OganizationResource;

public class PodcastXML implements Podcast {

    private final static Logger LOGGER = Logger.getLogger(PodcastXML.class
            .getName());

    protected String xmlData;
    protected Document document;
    protected URL feedURL;
	//TODO Add support for HTTP basic-auth

    protected PodcastXML() {
    }

    private String removeLastChar(String str) {
        LOGGER.log(Level.INFO, "emoveLastChar starting");
        char test = str.charAt(str.length() - 1);
        String stringValueOf = String.valueOf(test);
        if (!stringValueOf.equalsIgnoreCase(">")) {
            return str.substring(0, str.length() - 1);
        } else {
            //	String tt = new String(test);
            return str;
        }
    }

    public PodcastXML(URL feed) {
        LOGGER.log(Level.INFO, "PodcastXML starting");
        this.feedURL = feed;
        URLConnection connection;
        BufferedReader reader = null;
        try {
            connection = feed.openConnection();
            connection.connect();
            LOGGER.log(Level.INFO, "PodcastXML connection open");
            this.xmlData = "";
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                this.xmlData += line + "\n";
            }
            //LOGGER.log(Level.INFO, "PodcastXML data read [" + this.xmlData + "]");
			//reader.close();
            //</rss>
            LOGGER.log(Level.INFO, "PodcastXML parse data");
            int foundPosition = this.xmlData.lastIndexOf("</rss>");
            int totalLength = "</rss>".length();
            String remove = this.xmlData.substring(0, foundPosition + totalLength);

            this.xmlData = remove;
            //this.xmlData = this.xmlData.replaceAll(remove, remove)
           // LOGGER.log(Level.INFO, "PodcastXML after remove last char [" + this.xmlData + "]");
            this.document = DocumentHelper.parseText(this.xmlData);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } finally {

		      // close the reader; this can throw an exception too, so
            // wrap it in another try/catch block.
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
    }

    public PodcastXML(String xml) {
        try {
            this.xmlData = xml;
            this.document = DocumentHelper.parseText(this.xmlData);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    public PodcastXML(String xml, URL feed) {
        try {
            this.xmlData = xml;
            this.feedURL = feed;
            this.document = DocumentHelper.parseText(this.xmlData);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getTitle() {
        Element rootElement = this.document.getRootElement();
        Element channel = rootElement.element("channel");
        return channel.elementText("title");
    }

    @Override
    public String getDescription() {
        Element rootElement = this.document.getRootElement();
        Element channel = rootElement.element("channel");
        return channel.elementText("description");
    }

    @Override
    public URL getLink() throws MalformedURLException {
        Element rootElement = this.document.getRootElement();
        Element channel = rootElement.element("channel");
        if ("atom".equalsIgnoreCase(channel.element("link").getNamespacePrefix())) {
            return new URL(channel.element("link").attributeValue("href"));
        }
        return new URL(channel.elementText("link"));
    }

	//Optional Params (Can return null)
    @Override
    public String getLanguage() {
        Element rootElement = this.document.getRootElement();
        Element channel = rootElement.element("channel");
        if (channel.element("language") != null) {
            return channel.elementText("language");
        }

        return null;
    }

    @Override
    public String getCopyright() {
        Element rootElement = this.document.getRootElement();
        Element channel = rootElement.element("channel");
        if (channel.element("copyright") != null) {
            return channel.elementText("copyright");
        }

        return null;
    }

    @Override
    public String getManagingEditor() {
        Element rootElement = this.document.getRootElement();
        Element channel = rootElement.element("channel");
        if (channel.element("managingEditor") != null) {
            return channel.elementText("managingEditor");
        }

        return null;
    }

    @Override
    public String getWebMaster() {
        Element rootElement = this.document.getRootElement();
        Element channel = rootElement.element("channel");
        if (channel.element("webMaster") != null) {
            return channel.elementText("webMaster");
        }

        return null;
    }

    @Override
    public Date getPubDate() {
        Element rootElement = this.document.getRootElement();
        Element channel = rootElement.element("channel");
        if (channel.element("pubDate") != null) {
            String dt = channel.elementText("pubDate").trim();
            return DateHelper.stringToDate(dt);
        }

        return null;
    }

    @Override
    public Date getLastBuildDate() {
        Element rootElement = this.document.getRootElement();
        Element channel = rootElement.element("channel");
        if (channel.element("lastBuildDate") != null) {
            String dt = channel.elementText("lastBuildDate").trim();
            return DateHelper.stringToDate(dt);
        }

        return null;
    }

    @Override
    public String[] getCategories() {
        List<String> categories = new ArrayList<String>();
        Element rootElement = this.document.getRootElement();
        Element channel = rootElement.element("channel");
        boolean hasiTunes = false;
        if (channel.element("category") != null) {
            for (Element child : (List<Element>) channel.elements("category")) {
                if (!"itunes".equalsIgnoreCase(child.getNamespacePrefix()) && !hasiTunes) {
                    categories.add(child.getText());
                } else if ("itunes".equalsIgnoreCase(child.getNamespacePrefix()) && !hasiTunes) {
                    hasiTunes = true;
                    //Clear Categories
                    categories.clear();
                    if (child.elements("category").size() == 0) {
                        if (child.attribute("text") != null) {
                            categories.add(child.attributeValue("text"));
                        } else {
                            categories.add(child.getText());
                        }
                    } else {
                        String finalCategory = "";
                        if (child.attribute("text") != null) {
                            finalCategory = child.attributeValue("text");
                        } else {
                            finalCategory = child.getText();
                        }

                        for (Element category : (List<Element>) child.elements("category")) {
                            if (category.attribute("text") != null) {
                                finalCategory += " > " + category.attributeValue("text");
                            } else {
                                finalCategory += " > " + category.getText();
                            }
                        }
                        categories.add(finalCategory);
                    }

                } else if (hasiTunes && "itunes".equalsIgnoreCase(child.getNamespacePrefix())) {
                    if (child.elements("category").size() == 0) {
                        if (child.attribute("text") != null) {
                            categories.add(child.attributeValue("text"));
                        } else {
                            categories.add(child.getText());
                        }
                    } else {
                        String finalCategory = "";
                        if (child.attribute("text") != null) {
                            finalCategory = child.attributeValue("text");
                        } else {
                            finalCategory = child.getText();
                        }

                        for (Element category : (List<Element>) child.elements("category")) {
                            if (category.attribute("text") != null) {
                                finalCategory += " > " + category.attributeValue("text");
                            } else {
                                finalCategory += " > " + category.getText();
                            }
                        }
                        categories.add(finalCategory);
                    }
                }
            }
        }

        if (categories.size() == 0) {
            return new String[0];
        }
        String[] output = new String[categories.size()];
        categories.toArray(output);
        return output;
    }

    @Override
    public String getGenerator() {
        Element rootElement = this.document.getRootElement();
        Element channel = rootElement.element("channel");
        if (channel.element("generator") != null) {
            return channel.elementText("generator");
        }

        return null;
    }

    @Override
    public URL getDocs() throws MalformedURLException {
        Element rootElement = this.document.getRootElement();
        Element channel = rootElement.element("channel");
        if (channel.element("docs") != null) {
            return new URL(channel.elementText("docs"));
        }

        return null;
    }

    @Override
    public Object getCloud() {
        //TODO
        return null;
    }

    @Override
    public int getTTL() {
        Element rootElement = this.document.getRootElement();
        Element channel = rootElement.element("channel");
        if (channel.element("ttl") != null) {
            return Integer.valueOf(channel.elementText("ttl"));
        }

        return -1;
    }

    @Override
    public URL getImageURL() throws MalformedURLException {
        Element rootElement = this.document.getRootElement();
        Element channel = rootElement.element("channel");
        if (channel.element("thumbnail") != null) {
            return new URL(channel.element("thumbnail").attributeValue("url"));
        }
        for (Element image : (List<Element>) channel.elements("image")) {
            if ("itunes".equalsIgnoreCase(image.getNamespacePrefix())) {
                return new URL(image.attributeValue("href"));
            } else if (image.element("url") != null) {
                return new URL(image.element("url").getText());
            }
        }

        return null;
    }

	//TODO Add getPICSRating()
	//IGNORED getTextInput()
	//IGNORED getSkipHours() / getSkipDays()
    @Override
    public String[] getKeywords() {
        List<String> keywords = new ArrayList<String>();
        Element rootElement = this.document.getRootElement();
        Element channel = rootElement.element("channel");
        boolean hasiTunes = false;
        if (channel.element("keywords") != null) {
            for (Element child : (List<Element>) channel.elements("keywords")) {
                if (!"itunes".equalsIgnoreCase(child.getNamespacePrefix()) && !hasiTunes) {
                    for (String kw : child.getText().split(",")) {
                        keywords.add(kw.trim());
                    }
                } else if ("itunes".equalsIgnoreCase(child.getNamespacePrefix()) && !hasiTunes) {
                    hasiTunes = true;
                    //Clear Categories
                    keywords.clear();
                    for (String kw : child.getText().split(",")) {
                        keywords.add(kw.trim());
                    }
                } else if (hasiTunes) {
                    for (String kw : child.getText().split(",")) {
                        keywords.add(kw.trim());
                    }
                }
            }
        }

        if (keywords.size() == 0) {
            return new String[0];
        }
        String[] output = new String[keywords.size()];
        keywords.toArray(output);
        return output;
    }

    //Some iTunes Specific Stuff
    @Override
    public boolean isExplicit() {
        LOGGER.log(Level.INFO, "isExplicit starting");
        Element rootElement = this.document.getRootElement();
        Element channel = rootElement.element("channel");
        if (channel == null) {
            return false;
        }
        if (channel.element("explicit") != null && "itunes".equalsIgnoreCase(channel.element("explicit").getNamespacePrefix())) {
            return "yes".equalsIgnoreCase(channel.elementText("explicit"));
        }

        return false;
    }

    //Episodes
    @Override
    public List<Episode> getEpisodes() throws MalformedURLException, MalformedFeedException {
        LOGGER.log(Level.INFO, "getEpisodes() starting");
        Element rootElement = this.document.getRootElement();
        Element channel = rootElement.element("channel");
        List<Episode> episodes = new ArrayList<Episode>();
        for (Element item : (List<Element>) channel.elements("item")) {
            Episode episode = new Episode();
            //Required Tags
            boolean hasOne = false;
            //Title
            if (item.element("title") != null) {
                episode.setTitle(item.elementText("title"));
                hasOne = true;
            }
            //Description
            if (item.element("description") != null) {
                //br />
                String desc = item.elementText("description");
                int position = desc.indexOf("br />");
                if (position > 0) {
                    position = position + "br />".length();
                    desc = desc.substring(position, desc.length());
                }
                episode.setDescription(desc);
                hasOne = true;
            }
                        //

            if (!hasOne) {
                throw new MalformedFeedException("Item contains neither title nor description", null);
            }

			//Optional Tags
            //Link
            if (item.element("link") != null) {
                URL link = null;
                if ("atom".equalsIgnoreCase(item.element("link").getNamespacePrefix())) {
                    link = new URL(item.element("link").attributeValue("href"));
                }
                link = new URL(item.elementText("link"));
                episode.setLink(link);
            }

            //Encoded Content
            if (item.element("encoded") != null && "content".equalsIgnoreCase(item.element("encoded").getNamespacePrefix())) {
                episode.setEncodedContent(item.elementText("encoded"));
            }

            //Author
            if (item.element("author") != null) {
                episode.setAuthor(item.elementText("author"));
            }

            //Categories
            List<String> categories = new ArrayList<String>();
            for (Element category : (List<Element>) item.elements("category")) {
                categories.add(category.getText());
            }

            if (categories.size() > 0) {
                String[] categoryArray = new String[categories.size()];
                categories.toArray(categoryArray);
                episode.setCategories(categoryArray);
            } else {
                episode.setCategories(new String[0]);
            }

			//TODO Comments
            //Enclosure
            if (item.element("enclosure") != null) {
                episode.setMediaLocation(new URL(item.element("enclosure").attributeValue("url")));
                episode.setMediaLength(Integer.valueOf(item.element("enclosure").attributeValue("length")));
                episode.setMediaType(item.element("enclosure").attributeValue("type"));
            }

            //GUID
            if (item.element("guid") != null) {
                episode.setGuid(item.elementText("guid"));
            }

            //PubDate
            if (item.element("pubDate") != null) {
                episode.setPubDate(DateHelper.stringToDate(item.elementText("pubDate")));
            }

            //Source
            if (item.element("source") != null) {
                episode.setSourceLink(new URL(item.element("source").attributeValue("url")));
                episode.setSourceName(item.element("source").attributeValue("name"));
            }
            if ("itunes".equalsIgnoreCase(item.element("image").getNamespacePrefix())) {
                if (item.element("image") != null) {
                    String imageURL = item.element("image").attributeValue("href");
                    episode.setImageURL(imageURL);
                }
            }
            if ("itunes".equalsIgnoreCase(item.element("summary").getNamespacePrefix())) {
                if (item.element("summary") != null) {
                    String summary = item.elementText("summary");
                        episode.setSummary(summary);
                }
            }

            episodes.add(episode);
        }

        return episodes;
    }

    @Override
    public String getXMLData() {
        return this.xmlData;
    }

    @Override
    public URL getFeedURL() {
        return feedURL;
    }

    public PodcastNative getNativeCopy() {
        return new PodcastNative(this);
    }
}
