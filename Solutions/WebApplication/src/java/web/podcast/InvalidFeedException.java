/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.podcast;

public class InvalidFeedException extends Exception {

	public InvalidFeedException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
