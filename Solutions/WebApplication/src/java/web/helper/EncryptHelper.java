/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.helper;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author wtccuser
 */
public class EncryptHelper {

    private static Logger mLog = Logger.getLogger(EncryptHelper.class.getName());

    /**
     * encrypt data
     *
     * @param data
     * @return
     */
    public static String encrypt(String data, String key) throws Exception {
        mLog.log(Level.INFO, "beginning encrypt");
        // Create key and cipher
        Key aesKey = new SecretKeySpec(key.getBytes(), "AES");

        Cipher cipher = Cipher.getInstance("AES");

        // encrypt the text
        cipher.init(Cipher.ENCRYPT_MODE, aesKey);
        byte[] encrypted = cipher.doFinal(data.getBytes());
        String encrpyt = new String(encrypted);
        mLog.log(Level.INFO, "ending encrypt");
        return encrpyt;
    }

    /**
     * decrypt data
     *
     * @param data
     * @return
     */
    public static String decrypt(String data, String key) throws Exception {
        mLog.log(Level.INFO, "beginning decrypt");
        // Create key and cipher
        Key aesKey = new SecretKeySpec(key.getBytes(), "AES");

        Cipher cipher = Cipher.getInstance("AES");

        // encrypt the text
        cipher.init(Cipher.DECRYPT_MODE, aesKey);
        byte[] encrypted = cipher.doFinal(data.getBytes());
        String decrypted = new String(cipher.doFinal(encrypted));
        mLog.log(Level.INFO, "ending decrypt");
        return decrypted;
    }

}
