/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.helper;

import com.mobile.dto.Organization;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import web.obj.GoogleMap;
import web.obj.Step;

/**
 *
 * @author administrator
 */
public class MapHelper {

    public static GoogleMap getDrivingDirection(String startingAddress, String endingAddress,String googleURL) {
        List<Organization> listOrg = data.helper.OrganizationHelper.findAll();

        //get first org
        Organization firstOrg = listOrg.get(0);

        //String endingAddress = firstOrg.getLatitude() + "," + firstOrg.getLongitude();
        Object[] args = {startingAddress, endingAddress};
        MessageFormat fmt = new MessageFormat("?origin={0}&destination={1}");
        String replaceSpace = fmt.format(args);
        replaceSpace = replaceSpace.replace(" ", "%20");
        //String decoded = java.net.URLEncoder.encode(fmt.format(args), "UTF-8");
        googleURL = googleURL + replaceSpace;
        List<Step> steps = new ArrayList<Step>();
        int counter = 0;
        try {
            URL url = new URL(googleURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            int code = conn.getResponseCode();
            String message = conn.getResponseMessage();

            if (code != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + message);
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            StringBuffer buffer = new StringBuffer();
            System.out.println("Output from Server .... \n");

            while ((output = br.readLine()) != null) {
                //System.out.println(output);
                buffer.append(output);
            }

            org.primefaces.json.JSONObject jsonObject = new org.primefaces.json.JSONObject(buffer.toString());
            org.primefaces.json.JSONArray routes = jsonObject.getJSONArray("routes");
            org.primefaces.json.JSONObject legs = routes.getJSONObject(0);
            org.primefaces.json.JSONArray legsArray = legs.getJSONArray("legs");
            org.primefaces.json.JSONObject oo = legsArray.getJSONObject(0);
            org.primefaces.json.JSONArray stepArray = oo.getJSONArray("steps");

            String googleStartingAddress = oo.getString("start_address");
            String googleEndingAddress = oo.getString("end_address");

            for (int x = 0; x < stepArray.length(); x++) {

                org.primefaces.json.JSONObject rec = stepArray.getJSONObject(x);
                String loc = rec.getString("html_instructions");
                String maneuver = null;
                try {
                    maneuver = rec.getString("maneuver");
                } catch (Exception ex) {
                    Logger.getLogger(MapHelper.class.getName()).log(Level.INFO, null, ex);
                }
                org.primefaces.json.JSONObject distanceObj = rec.getJSONObject("distance");
                String distance = distanceObj.getString("text");
                org.primefaces.json.JSONObject durationObj = rec.getJSONObject("duration");
                String duration = durationObj.getString("text");
                //(String leg, String maneuver, String duration, String distance)
                Step step = new Step(loc, maneuver, duration, distance);
                if (x == 0) {
                    step.setImageSrc(true, false);
                }
                if (x == stepArray.length() - 1) {
                    step.setImageSrc(false, true);
                }
                if (x > 0 && x < stepArray.length() - 1) {
                    step.setImageSrc(false, false);
                }
                steps.add(step);

            }
            GoogleMap googleMap = new GoogleMap(googleStartingAddress, googleEndingAddress, steps);
            conn.disconnect();
            return googleMap;

        } catch (Exception ex) {

            Logger.getLogger(MapHelper.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Failed : HTTP error code : " + ex.getMessage());
        }

    }

}
