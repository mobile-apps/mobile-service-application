/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import data.extender.OrganizationExtender;
import data.extender.DepartmentExtender;
import com.mobile.dto.Service;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author wtccuser
 */
public class Site {

    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());

    private List<OrganizationExtender> organizationExtenderList = new ArrayList<OrganizationExtender>();

    public List<OrganizationExtender> getOrganizationExtenderList() {
        return organizationExtenderList;
    }

    public void setOrganizationExtenderList(List<OrganizationExtender> organizationExtenderList) {
        this.organizationExtenderList = organizationExtenderList;
    }

    enum site {

        id, organizations, departments, services, name, latitude, longitude, location, email, startDate, endDate
    }

    /**
     * compose the json object
     *
     * @return
     */
    public String toString() {
        LOGGER.log(Level.OFF, "start toString()");
        // <editor-fold defaultstate="collapsed" desc="add organizations">
        JsonArrayBuilder orgsBuilder = Json.createArrayBuilder();
        for (OrganizationExtender org : organizationExtenderList) {
            // <editor-fold defaultstate="collapsed" desc="add departments">
            JsonArrayBuilder deptsBuilder = Json.createArrayBuilder();
            for (DepartmentExtender dept : org.getDepartments()) {
                // <editor-fold defaultstate="collapsed" desc="add services">
                JsonArrayBuilder servicesBuilder = Json.createArrayBuilder();
                for (Service service : dept.getServices()) {
                    servicesBuilder.add(Json.createObjectBuilder()
                            .add(site.id.toString(), service.getServiceId())
                            .add(site.name.toString(), service.getName())
                            .add(site.location.toString(), service.getLocation())
                            .add(site.endDate.toString(), service.getEndDate().toString())
                            .add(site.startDate.toString(), service.getStartDate().toString()));
                }
                // </editor-fold>
                // deptsBuilder.add(Json.createObjectBuilder()
                //.add(site.id.toString(), dept.getDepartmentId())
                // .add(site.name.toString(), dept.getName()
                //.add(site.latitude.toString(), dept.getLatitude())
                //.add(site.longitude.toString(), dept.getLongitude())
                //   .add(site.location.toString(), dept.getLocation())
                // .add(site.services.toString(), servicesBuilder));
            }
            // </editor-fold>
            JsonObjectBuilder orgItem = Json.createObjectBuilder();
            orgItem.add(site.id.toString(), org.getOrganizationId());

            orgItem.add(site.id.toString(), org.getOrganizationId());
            orgItem.add(site.name.toString(), org.getName());
            if (org.getLatitude() != null && org.getLongitude() != null) {
                orgItem.add(site.latitude.toString(), org.getLatitude());
                orgItem.add(site.longitude.toString(), org.getLongitude());
            }
            if (org.getEmail() != null && org.getLocation() != null) {
                orgItem.add(site.location.toString(), org.getLocation());
                orgItem.add(site.email.toString(), org.getEmail());
            }
            orgsBuilder.add(orgItem);
            //  .add(site.departments.toString(), deptsBuilder));
        }
        JsonObject siteObject = Json.createObjectBuilder()
                .add(site.name.toString(), "Site")
                .add(site.organizations.toString(), orgsBuilder)
                .build();
        // </editor-fold>
LOGGER.log(Level.OFF, "site " + siteObject.toString());
        LOGGER.log(Level.OFF, "end toString()");
        return siteObject.toString();
    }

    public Site() {
        LOGGER.log(Level.OFF, "start Site()");
        // <editor-fold defaultstate="collapsed" desc="build site data">
        organizationExtenderList = data.helper.OrganizationHelper.findAllExtender();
        if (organizationExtenderList == null) {
            organizationExtenderList = new ArrayList<OrganizationExtender>();
        }
        // </editor-fold>
        LOGGER.log(Level.OFF, "ending site");

    }

}
