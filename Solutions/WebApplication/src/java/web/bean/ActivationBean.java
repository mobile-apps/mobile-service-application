/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import com.mobile.dto.RegisteredUser;
import data.helper.RegisterUserHelper;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;


/**
 *
 * @author administrator
 */
@ManagedBean
public class ActivationBean {

    public RegisteredUser getRegisteredUser() {
        return registeredUser;
    }
  

    public void setRegisteredUser(RegisteredUser registeredUser) {
        this.registeredUser = registeredUser;
    }

    private String activationCode;
    private RegisteredUser registeredUser;

    /**
     *
     * @return
     */
    public String getActivationCode() {
        return activationCode;
    }

    /**
     * set activation code
     *
     * @param activationCode
     */
    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    /**
     * get next page
     *
     * @return
     */
    public String completeActivation() {
        registeredUser = RegisterUserHelper.updateActivation(this.activationCode);
        if (registeredUser == null) {
            return "invalidactivation";
        } else {
            return "thankyouphoneregistration";
        }
    }

}
