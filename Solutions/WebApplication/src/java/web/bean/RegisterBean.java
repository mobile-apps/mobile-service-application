/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import com.mobile.dto.RegisteredUser;
import data.helper.ApplicationEnum;
import data.helper.RegisterUserHelper;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpServletRequest;
import web.bean.ErrorHandlerBean;
import web.email.EmailHelper;
import web.email.EmailMessage;
import web.helper.RandomString;

/**
 *
 * @author wtccuser
 */
@ManagedBean
public class RegisterBean {

    @ManagedProperty(value = "#{errorHandlerBean}")
    private ErrorHandlerBean errorHandlerBean;

    private String activationCode;
    private String email;
    private String confirmEmail;
    private String confirmPhone;

    public void validatePhone(FacesContext fc, UIComponent component, Object value) {
        this.phone = (String) value;
    }

    public void validatePhoneComfirm(FacesContext fc, UIComponent component, Object value) throws ValidatorException {
        this.confirmPhone = (String) value;

//UIInput emailComponent = (UIInput) component.getAttributes().get("email");
        if (this.confirmPhone == null) {
            throw new IllegalArgumentException(String.format("Unable to find email submitted."));
        }
        if (!this.confirmPhone.equals(this.phone)) {
            FacesMessage msg
                    = new FacesMessage("Phone match failed",
                            "Phones do not match");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }

    }

    public void validateEmail(FacesContext fc, UIComponent component, Object value) {
        this.email = (String) value;
    }

    public void validateEmailComfirm(FacesContext fc, UIComponent component, Object value) throws ValidatorException {
        this.confirmEmail = (String) value;

//UIInput emailComponent = (UIInput) component.getAttributes().get("email");
        if (this.confirmEmail == null) {
            throw new IllegalArgumentException(String.format("Unable to find email submitted."));
        }
        if (!this.confirmEmail.equals(this.email)) {
            FacesMessage msg
                    = new FacesMessage("E-mail match failed",
                            "Emails do not match");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }

    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public ErrorHandlerBean getErrorHandlerBean() {
        return errorHandlerBean;
    }

    public void setErrorHandlerBean(ErrorHandlerBean errorHandlerBean) {
        this.errorHandlerBean = errorHandlerBean;
    }

    private String emailErrorMessage;

    public String getEmailErrorMessage() {
        return emailErrorMessage;
    }

    public void setEmailErrorMessage(String emailErrorMessage) {
        this.emailErrorMessage = emailErrorMessage;
    }

    public FacesContext getmCtx() {
        return mCtx;
    }

    public void setmCtx(FacesContext mCtx) {
        this.mCtx = mCtx;
    }
    FacesContext mCtx = FacesContext.getCurrentInstance();
    private final static Logger LOGGER = Logger.getLogger(RegisterBean.class
            .getName());
    private String emailOne;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    private String notificationMethod;

    public String getNotificationMethod() {
        return notificationMethod;
    }

    public void setNotificationMethod(String notificationMethod) {
        this.notificationMethod = notificationMethod;
    }

    private String phone;
    private String phoneCarrier;

    public String getPhoneCarrier() {
        return phoneCarrier;
    }

    public void setPhoneCarrier(String phoneCarrier) {
        this.phoneCarrier = phoneCarrier;
    }

    public String getEmailOne() {
        return emailOne;
    }

    public void setEmailOne(String emailOne) {
        this.emailOne = emailOne;
    }

    public String getEmailTwo() {
        return emailTwo;
    }

    public void setEmailTwo(String emailTwo) {
        this.emailTwo = emailTwo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    private String emailTwo;
    private String userName;

    public String completeActivation() {
        RegisterUserHelper.updateActivation(this.activationCode);
        return "thankyouregistration";
    }

    /**
     * Register user
     *
     * @return
     */
    public String sendRegistration() {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String url = req.getRequestURL().toString();
        String urlSuffix
                = mCtx.getExternalContext().getInitParameter("add.email.url.suffix.address");

        String root = url.substring(0, url.length() - req.getRequestURI().length()) + req.getContextPath() + "/";
        String fullURL = root + urlSuffix;
        String host
                = mCtx.getExternalContext().getInitParameter("email.host.server");
        String hostPassword
                = mCtx.getExternalContext().getInitParameter("email.host.server.password");
        String hostFrom
                = mCtx.getExternalContext().getInitParameter("email.from.address");
        String message = "";
        String subject = "";

        String emailAddress = this.emailOne.trim();
        String phoneAddress;
        com.mobile.dto.RegisteredUser registerUser = new com.mobile.dto.RegisteredUser();
        registerUser.setEmail(this.emailOne);
        registerUser.setUsername(this.userName);
        registerUser.setStatus("pending");
        phoneAddress = this.phone + this.phoneCarrier;
        phoneAddress = phoneAddress.replace("(", "");
        phoneAddress = phoneAddress.replace(")", "");
        phoneAddress = phoneAddress.replace("-", "");
        phoneAddress = phoneAddress.replace(" ", "");
        phoneAddress = phoneAddress.trim();
        registerUser.setPhone(phoneAddress);

        RandomString randomString = new RandomString(4);
        String newActivationCode = randomString.nextString();
        registerUser.setActivationCode(newActivationCode);
        EmailMessage emailMessage = new EmailMessage();
        String nextPage = "";
        if (this.notificationMethod.equals(ApplicationEnum.PHONE.toString())) {
            nextPage = "activation?faces-redirect=true";

            registerUser.setPhone(phoneAddress);
            registerUser.setNotificationmethod(ApplicationEnum.PHONE.toString());
            emailMessage.setTo(phoneAddress);
            this.emailOne = phoneAddress;
            subject
                    = mCtx.getExternalContext().getInitParameter("phone.registration.subject");
            message
                    = mCtx.getExternalContext().getInitParameter("phone.registration.message");
            message = message + " " + newActivationCode;
            //add to database
            registerUser = RegisterUserHelper.add(registerUser);
        } else {
            registerUser.setNotificationmethod(ApplicationEnum.EMAIL.toString());
            registerUser.setEmail(emailAddress);
            emailMessage.setTo(emailAddress);
            subject
                    = mCtx.getExternalContext().getInitParameter("email.registration.subject");

            message
                    = mCtx.getExternalContext().getInitParameter("email.registration.message");
            //add to database
            registerUser = RegisterUserHelper.add(registerUser);
            fullURL = fullURL + registerUser.getRegisteredUserId().toString();
            message = message + fullURL;
            nextPage = "thankyouregistration?faces-redirect=true";
        }

        emailMessage.setBody(message);
        emailMessage.setEmailServer(host);
        //emailMessage.setMessage(message);
        emailMessage.setFrom(hostFrom);
        emailMessage.setPassword(hostPassword);
        emailMessage.setSubject(subject);

        // EmailHelper emailHelper = new EmailHelper(host, hostPassword);
        EmailHelper emailHelper = new EmailHelper();

        try {
            emailHelper.send(emailMessage);

            return nextPage;
        } catch (Exception ex) {
            errorHandlerBean.setExternalException(ex);
            return "/error/errorEmail";
        }

    }

}
