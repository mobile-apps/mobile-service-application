/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package web.bean;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;   
import org.primefaces.context.RequestContext;

/**
 *
 * @author wtccuser
 */
@ManagedBean
public class LoginBean {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String passwrd) {
        this.password = passwrd;
    }
    
    
    public String validateCredentials() {
        
        boolean loggedIn = false;
         
        if(username != null && username.equals("admin") && password != null && password.equals("admin")) {
           return "/admin/organizations?faces-redirect=true";
           
        } else {
            return "loginerror";
           
        }
       
        
    }   
    
}
