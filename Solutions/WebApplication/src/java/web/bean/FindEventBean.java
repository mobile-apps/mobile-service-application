/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import com.mobile.dto.Department;
import data.helper.DepartmentHelper;
import data.helper.EventHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import web.Event;
import web.WebConstants;
import web.obj.DepartmentObj;

@ManagedBean
@SessionScoped
public class FindEventBean implements Serializable {

    private Date startDate;
    private Date endDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String goToResults() {
        return WebConstants.EVENT_SEARCH_RESULT_PAGE;
    }

    public void createEvents(ActionEvent actionEvent) {
        List<Event> events = new ArrayList<Event>();
        if (selectedDepartments != null && selectedDepartments.size() > 0) {
            //were date selected
            List<Event> allEventList = null;
            if (this.startDate != null && this.endDate != null) {
                allEventList = EventHelper.findAll(startDate, endDate);
            } else {
                allEventList = EventHelper.findAll(true);
            }

            for (Event event : allEventList) {
                for (DepartmentObj department : selectedDepartments) {
                    if (event.getDepartment().getDepartmentId().toString().equals(department.getId())) {
                        events.add(event);
                    }
                }
            }//end of for

        } //end if
        FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().put("events", events);
    }

    private List<DepartmentObj> departments;
    private DepartmentObj selectedDepartment;
    private List<DepartmentObj> selectedDepartments;

    public FindEventBean() {
        List<DepartmentObj> list = new ArrayList<DepartmentObj>();
        List<Department> departments = DepartmentHelper.findAll();
        for (Department department : departments) {
            DepartmentObj departmentObj = new DepartmentObj(department);
            list.add(departmentObj);
        }
        this.departments = list;
    }

    public List<DepartmentObj> getDepartments() {
        return departments;
    }

    public DepartmentObj getSelectedDepartment() {
        return selectedDepartment;
    }

    public void setSelectedDepartment(DepartmentObj selectedDepartment) {
        this.selectedDepartment = selectedDepartment;
    }

    public List<DepartmentObj> getSelectedDepartments() {

        return selectedDepartments;
    }

    public void setSelectedDepartments(List<DepartmentObj> selectedDepartments) {
        this.selectedDepartments = selectedDepartments;
    }

}
