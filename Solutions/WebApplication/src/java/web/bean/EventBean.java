/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import data.helper.EventHelper;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import web.Event;

/**
 *
 * @author HopeBenziger
 */
@ManagedBean
public class EventBean implements Serializable {

    private final static Logger LOGGER = Logger.getLogger(EventBean.class
            .getName());

    private String name;
    private String searchField;
    private Event selectedEvent;

    public Event getSelectedEvent() {
        return selectedEvent;
    }

    public void setSelectedEvent(Event selectedEvent) {
        this.selectedEvent = selectedEvent;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }
    private Date date;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    private List<Event> eventList;
    private List<Event> filterEventList;

    public List<Event> getFilterEventList() {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, 31);
        Date endDate = cal.getTime();
        Date todaydate = new Date();
        filterEventList = EventHelper.findAll(todaydate, endDate);
        return filterEventList;
    }

    /*
    Get only next two events
    */
    public List<Event> getNextEvents() {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, 31);
        Date endDate = cal.getTime();
        Date todaydate = new Date();
        filterEventList = EventHelper.findAll(todaydate, endDate, 2);
        return filterEventList;
    }

    public List<Event> getNextEventList() {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, 31);
        Date endDate = cal.getTime();
        Date todaydate = new Date();
        filterEventList = EventHelper.findAll(todaydate, endDate);
        return filterEventList;
    }

    public void setFilterEventList(List<Event> filterEventList) {
        this.filterEventList = filterEventList;
    }

    public List<Event> getEventList() {
        eventList = EventHelper.findAll();
        return eventList;
    }
}
