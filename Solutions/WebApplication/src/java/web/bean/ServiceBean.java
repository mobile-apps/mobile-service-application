/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import com.mobile.dto.Category;
import com.mobile.dto.Service;
import data.extender.ServiceExtender;
import data.helper.GeoLocation;
import data.helper.GeoLocationHelper;
import data.helper.ServiceHelper;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import web.WebConstants;

/**
 *
 * @author HSC314
 */
@ManagedBean
public class ServiceBean implements Serializable {
    
    private final static Logger LOGGER = Logger.getLogger(ServiceBean.class
            .getName());
    
    private String[] selectedDaysOfWeek;
    private List<String> listSelectedDaysOfWeek;
    
    private Date startDate;
    private Date selectedStartDate;
    
    public Date getSelectedStartDate() {
        selectedStartDate = new Date(this.selectedService.getStartDate().getTime());
        return selectedStartDate;
    }
    
    public void setSelectedStartDate(Date selectedStartDate) {
        this.selectedStartDate = selectedStartDate;
    }
    
    public Date getSelectedEndDate() {
        selectedEndDate = new Date(this.selectedService.getEndDate().getTime());
        return selectedEndDate;
    }
    
    public void setSelectedEndDate(Date selectedEndDate) {
        this.selectedEndDate = selectedEndDate;
    }
    private Date selectedEndDate;
    
    public Date getStartDate() {
        
        return startDate;
    }
    
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    public Date getEndDate() {
        
        return endDate;
    }
    
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    private Date endDate;
    
    private List<Service> serviceList;
    
    public List<Service> getServiceList() {
        serviceList = ServiceHelper.findAll();
        return serviceList;
    }
    
    public void setServiceList(List<Service> serviceList) {
        this.serviceList = serviceList;
    }
    
    private List<ServiceExtender> serviceExtenderList;
    
    public List<ServiceExtender> getServiceExtenderList() {
        serviceExtenderList = ServiceHelper.findAllExtender();
        return serviceExtenderList;
    }
    
    public void setServiceExtenderList(List<ServiceExtender> serviceList) {
        this.serviceExtenderList = serviceExtenderList;
    }
    
    private com.mobile.dto.Service service = new com.mobile.dto.Service();
    private com.mobile.dto.Service selectedService;
    
    public Service getSelectedService() {
        selectedService = (Service) FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get("service");
        return selectedService;
    }
    
    public void setSelectedService(Service selectedService) {
        
        this.selectedService = selectedService;
    }
    
    public Service getService() {
        return service;
    }
    
    public void setService(Service service) {
        this.service = service;
    }
    
    public String add() {
        LOGGER.log(Level.OFF, "start add");
        java.sql.Timestamp timestamp_StartDate = new java.sql.Timestamp(startDate.getTime());
        java.sql.Timestamp timestamp_EndDate = new java.sql.Timestamp(endDate.getTime());
        
        this.service.setStartDate(timestamp_StartDate);
        this.service.setEndDate(timestamp_EndDate);
        
        if ((this.service.getMondayFlag() == false)
                && (this.service.getTuesdayFlag() == false)
                && (this.service.getWednesdayFlag() == false)
                && (this.service.getThursdayFlag() == false)
                && (this.service.getFridayFlag() == false)
                && (this.service.getSaturdayFlag() == false)
                && (this.service.getSundayFlag() == false)) {
            //display an error message
            FacesContext.getCurrentInstance().addMessage("form:SundayCheckbox", new FacesMessage("Error: You must select a day."));
            LOGGER.log(Level.OFF, "end add ");
            return null;
        }
        GeoLocationHelper geoLocationHelper = new GeoLocationHelper();
       
        GeoLocation geoLocation = geoLocationHelper.getGeoLocation(this.service.getLocation());
        this.service.setLongitude(geoLocation.getLongitude());
        this.service.setLatitude(geoLocation.getLatitude());
        //set default values
        if (this.service.getImage() == null) {
            Category category = data.helper.CategoryHelper.find(this.service.getCategoryId().intValue());
            if (category != null && category.getImage() != null) {
                this.service.setImage(category.getImage());
            }
        }
        
        
        this.service = ServiceHelper.add(this.service);
        
        LOGGER.log(Level.OFF, "end add ");
        
        return WebConstants.SERVICES_PAGE;
    }

    /**
     * Navigate to detail page
     *
     * @param event
     * @return
     */
    public String update(Service service) {
        LOGGER.log(Level.OFF, "start update");
        
        java.sql.Timestamp timestamp_StartDate = new java.sql.Timestamp(this.selectedStartDate.getTime());
        java.sql.Timestamp timestamp_EndDate = new java.sql.Timestamp(this.selectedEndDate.getTime());
        
        service.setStartDate(timestamp_StartDate);
        service.setEndDate(timestamp_EndDate);
        
        if ((service.getMondayFlag() == false)
                && (service.getTuesdayFlag() == false)
                && (service.getWednesdayFlag() == false)
                && (service.getThursdayFlag() == false)
                && (service.getFridayFlag() == false)
                && (service.getSaturdayFlag() == false)
                && (service.getSundayFlag() == false)) {
            //display an error message
            FacesContext.getCurrentInstance().addMessage("form:SundayCheckbox", new FacesMessage("Error: You must select a day."));
            LOGGER.log(Level.OFF, "end update ");
            return null;
        }
                GeoLocationHelper geoLocationHelper = new GeoLocationHelper();
       
        GeoLocation geoLocation = geoLocationHelper.getGeoLocation(service.getLocation());
        service.setLongitude(geoLocation.getLongitude());
        service.setLatitude(geoLocation.getLatitude());
        
        //set default values
        if (service.getImage() == null || service.getImage().trim().equals("")) {
            Category category = data.helper.CategoryHelper.find(service.getCategoryId().intValue());
            if (category != null && category.getImage() != null) {
                service.setImage(category.getImage());
            }
        }
        
        ServiceHelper.update(service);
        
        LOGGER.log(Level.OFF, "end update ");
        
        return WebConstants.SERVICES_PAGE;
    }
    
    public String copy(Service service) {
        LOGGER.log(Level.OFF, "copy update");
        String newName = "copy of " + service.getName();
        service.setName(newName);
        ServiceHelper.add(service);
        
        LOGGER.log(Level.OFF, "end copy ");
        
        return WebConstants.SERVICES_PAGE;
    }
    
    public String delete(Service service) {
        LOGGER.log(Level.OFF, "start delete");
        ServiceHelper.delete(service);
        LOGGER.log(Level.OFF, "end delete ");
        
        return WebConstants.SERVICES_PAGE;
    }

//    public String findByOrganizationId(int id) {
//        LOGGER.log(Level.OFF, "start findByOrganizationId");
//        String org = DepartmentHelper.findByOrganizationId(id);
//        LOGGER.log(Level.OFF, "end findByOrganizationId ");
//        return org;
//    }
    /**
     * Navigate to detail page
     *
     * @param event
     * @return
     */
    public String onRowSelectNavigate(SelectEvent event) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put(WebConstants.SELECTED_CATEGORY, event.getObject());
        return WebConstants.CATEGORY_DETAIL_PAGE;
    }
    
    public void onRowSelect(SelectEvent event) {
        FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().put("service", selectedService);
        ConfigurableNavigationHandler configurableNavigationHandler
                = (ConfigurableNavigationHandler) FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        configurableNavigationHandler.performNavigation("editService?faces-redirect=true");
    }

    /**
     * @return the selectedDaysOfWeek
     */
    public String[] getSelectedDaysOfWeek() {
        return selectedDaysOfWeek;
    }

    /**
     * @param selectedDaysOfWeek the selectedDaysOfWeek to set
     */
    public void setSelectedDaysOfWeek(String[] selectedDaysOfWeek) {
        this.selectedDaysOfWeek = selectedDaysOfWeek;
    }

    /**
     * @return the listSelectedDaysOfWeek
     */
    public List<String> getListSelectedDaysOfWeek() {
        return listSelectedDaysOfWeek;
    }

    /**
     * @param listSelectedDaysOfWeek the listSelectedDaysOfWeek to set
     */
    public void setListSelectedDaysOfWeek(List<String> listSelectedDaysOfWeek) {
        this.listSelectedDaysOfWeek = listSelectedDaysOfWeek;
    }
    
}
