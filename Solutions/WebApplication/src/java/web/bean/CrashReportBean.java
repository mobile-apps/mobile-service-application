/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import com.mobile.dto.Crash;
import data.helper.CategoryHelper;
import data.helper.CrashHelper;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import web.WebConstants;

/**
 *
 * @author HSC314
 */
@ManagedBean
@SessionScoped
public class CrashReportBean {

    private List<com.mobile.dto.Crash> crashs;
    private final static Logger LOGGER = Logger.getLogger(CategoryHelper.class
            .getName());
    private Crash selectedCrash;

    public Crash getSelectedCrash() {
        return selectedCrash;
    }

    private String rtnEditMsg;

    public String getRtnEditMsg() {
        return rtnEditMsg;
    }

    public void setRtnEditMsg(String rtnEditMsg) {
        this.rtnEditMsg = rtnEditMsg;
    }

    public void setSelectedCrash(Crash selectedCrash) {
        this.selectedCrash = selectedCrash;
    }

    public List<com.mobile.dto.Crash> getCrashs() {
        LOGGER.log(Level.OFF, "start getRegisterUsers ");
        crashs = CrashHelper.findAll();

        Collections.sort(crashs, new Comparator<Crash>() {

            @Override
            public int compare(Crash o1, Crash o2) {
                int compareInt = o2.getCreatedDate().compareTo(o1.getCreatedDate());
                return compareInt;

            }
        });

        LOGGER.log(Level.OFF, "end getRegisterUsers ");
        return crashs;
    }

    public void deleteActionListener() {
        FacesContext fc = FacesContext.getCurrentInstance();
        if (CrashHelper.deleteAll() > 0) {
            fc.addMessage(null, new FacesMessage("Successful", WebConstants.CRASHYESDELETEMSG));
        } else {
            fc.addMessage(null, new FacesMessage("Unsuccessful", WebConstants.CRASHNODELETEMSG));
        }
    }

    public void setRegisterUsers(List<com.mobile.dto.Crash> crashs) {
        this.crashs = crashs;
    }
}
