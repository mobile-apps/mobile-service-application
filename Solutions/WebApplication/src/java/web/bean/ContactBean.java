/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package web.bean;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.FlowEvent;
import web.Contact;
import web.email.EmailHelper;
import web.email.EmailMessage;
/**
 *
 * @author wtccuser
 */
@ManagedBean
@ViewScoped
public class ContactBean implements Serializable{
    private Contact contact = new Contact();
    private boolean skip;

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }
    public void save() {   
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.setMessage(contact.getComment());
        emailMessage.setBody(contact.getComment());
        emailMessage.setSubject("Need your help");
        emailMessage.setEmailServer("mail.cokcc.org");
        emailMessage.setFrom("support@cokcc.org");
        emailMessage.setTo("javauniversity@nc.rr.com");
        emailMessage.setPassword(null);
        EmailHelper emailHelper = new EmailHelper();
        emailHelper.send(emailMessage);
        
        FacesMessage msg = new FacesMessage("Successful", "We will contact you shortly " + contact.getFirstName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    public String onFlowProcess(FlowEvent event) {
        if(skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        }
        else {
            return event.getNewStep();
        }
    }
    
}
