/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import web.podcast.Episode;
import web.podcast.Podcast;
import web.podcast.PodcastXML;

/**
 *
 * @author administrator
 */
@ManagedBean
@RequestScoped

public class PodcastBean {

    private int numberOfRows;
    private int currentItem = 0;
    private Episode episode;
    private List<Episode> episodes;
    private List<Episode> firstRowEpisodes = new ArrayList<Episode>();

    public List<Episode> getFirstRowEpisodes() {
        if (episodes == null) {
            episodes = this.getEpisodes();
        }
        for (int x = 0; x < this.episodes.size(); x++) {
            if (x < 3) {
                firstRowEpisodes.add(this.episodes.get(x));
            } else {
                break;
            }
        }
        return firstRowEpisodes;
    }

    public void setFirstRowEpisodes(List<Episode> firstRowEpisodes) {
        this.firstRowEpisodes = firstRowEpisodes;
    }

    public List<Episode> getSecondRowEpisodes() {
        if (episodes == null) {
            episodes = this.getEpisodes();
        }
        for (int x = 0; x < this.episodes.size(); x++) {
            if (x > 2 && x <= 5 ) {
                secondRowEpisodes.add(this.episodes.get(x));
            } else {
                continue;
            }
        }
        return secondRowEpisodes;
    }

    public void setSecondRowEpisodes(List<Episode> secondRowEpisodes) {
        this.secondRowEpisodes = secondRowEpisodes;
    }

    public List<Episode> getThirdRowEpisodes() {
        if (episodes == null) {
            episodes = this.getEpisodes();
        }
        for (int x = 0; x < this.episodes.size(); x++) {
            if (x > 5 && x <= 8 ) {
                thirdRowEpisodes.add(this.episodes.get(x));
            } else {
                continue;
            }
        }
        return thirdRowEpisodes;
    }

    public void setThirdRowEpisodes(List<Episode> thirdRowEpisodes) {
        this.thirdRowEpisodes = thirdRowEpisodes;
    }

    public List<Episode> getForthRowEpisodes() {
           if (episodes == null) {
            episodes = this.getEpisodes();
        }
        for (int x = 0; x < this.episodes.size(); x++) {
            if (x > 8 && x <= 11 ) {
                forthRowEpisodes.add(this.episodes.get(x));
            } else {
                continue;
            }
        }
        return forthRowEpisodes;
    }

    public void setForthRowEpisodes(List<Episode> forthRowEpisodes) {
        this.forthRowEpisodes = forthRowEpisodes;
    }

    public List<Episode> getFifthRowEpisodes() {
              if (episodes == null) {
            episodes = this.getEpisodes();
        }
        for (int x = 0; x < this.episodes.size(); x++) {
            if (x > 11 && x < 16 ) {
                fifthRowEpisodes.add(this.episodes.get(x));
            } else {
                continue;
            }
        }
        return fifthRowEpisodes;
    }

    public void setFifthRowEpisodes(List<Episode> fifthRowEpisodes) {
        this.fifthRowEpisodes = fifthRowEpisodes;
    }
    private List<Episode> secondRowEpisodes = new ArrayList<Episode>();
    private List<Episode> thirdRowEpisodes = new ArrayList<Episode>();
    private List<Episode> forthRowEpisodes = new ArrayList<Episode>();
    private List<Episode> fifthRowEpisodes = new ArrayList<Episode>();

    public Episode getEpisode() {
        if (episodes == null) {
            episodes = this.getEpisodes();
        }
        try {
            if (currentItem <= numberOfRows) {
                episode = episodes.get(currentItem);
                currentItem++;
            }

            //episodes.remove(0);
        } catch (Exception ex) {
            int a = 8;
        }
        return episode;
    }

    public void setEpisode(Episode episode) {
        this.episode = episode;
    }

    public int getNumberOfRows() {
        int totalPodcasts = getEpisodes().size();
        numberOfRows = totalPodcasts / 3;
        return numberOfRows;
    }

    public void setNumberOfRows(int numberOfRows) {
        this.numberOfRows = numberOfRows;
    }
    private Podcast podcast;

    public String testAtRow(int row, int col) {
        Episode rowColEpisode = getPodcastAtRow(row, col);

        /*
         <!-- 
         <a href="#{podcastBean.getPodcastAtRow(rowloop.index, colloop.index).guid}" data-featherlight="iframe" >
         <img src="#{podcastBean.getPodcastAtRow(rowloop.index, colloop.index).imageURL}" width="300px" style="height:200px; margin-left:-15px;" />
         </a>
         <p>#{podcastBean.getPodcastAtRow(rowloop.index, colloop.index).title}</p>

         <p>#{podcastBean.getPodcastAtRow(rowloop.index, colloop.index).summary}</p>
         <p>#{podcastBean.getPodcastAtRow(rowloop.index, colloop.index).author}</p>
         <p>#{podcastBean.getPodcastAtRow(rowloop.index, colloop.index).pubDate}</p>
         -->
         */
        Logger.getLogger(PodcastBean.class.getName()).log(Level.INFO, "row [" + row + "] col [" + col + "]");
        return "row [" + row + "] col [" + col + "]\n";
    }

    public Episode getPodcastAtRow(int row, int col) {
        if (episodes == null) {
            this.episodes = this.getEpisodes();
        }
        Logger.getLogger(PodcastBean.class.getName()).log(Level.INFO, "row [" + row + "] col [" + col + "]");
        Episode rowColEpisode = null;
        int offset = 2;
        switch (row) {
            case 1:
                rowColEpisode = this.episodes.get(col - 1);
                Logger.getLogger(PodcastBean.class.getName()).log(Level.INFO, "Row is collection  [" + (col - 1) + "]");
                Logger.getLogger(PodcastBean.class.getName()).log(Level.INFO, "Title  [" + rowColEpisode.getTitle() + "]");

                break;
            case 2:

                rowColEpisode = this.episodes.get(col + offset);
                Logger.getLogger(PodcastBean.class.getName()).log(Level.INFO, "Row is collection  [" + (col + offset) + "]");

                break;
            case 3:
                offset = 5;
                rowColEpisode = this.episodes.get(col + offset);
                break;
            case 4:
                offset = 8;
                rowColEpisode = this.episodes.get(col + offset);
                break;
            case 5:
                offset = 11;
                rowColEpisode = this.episodes.get(col + offset);
                break;

        }
        /*
         row 1  cols 0,1,2     0 =row 0
         row 2  cols 3,4,5  5 = row 1 + col 1  + 2     
         row 3  cols 6,7,8  6 = 2 
         row 4   col 9,10,11
         row 5 col 12,13,14
         */

        try {
            if (episodes == null) {
                episodes = this.getEpisodes();
            }
            rowColEpisode = this.episodes.get(row);
        } catch (Exception ex) {
            Logger.getLogger(PodcastBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rowColEpisode;
    }

    public Podcast getPodcast() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        Podcast podcast = null;
        try {
            //todo add to web xml
            String podcastURL = (String) ctx.getExternalContext().getInitParameter("podcast.address");
            Logger.getLogger(PodcastBean.class.getName()).log(Level.INFO, podcastURL);
            podcast = new PodcastXML(new URL(podcastURL));
            episodes = podcast.getEpisodes();
            Collections.sort(episodes);

            if (episodes != null) {
                Logger.getLogger(PodcastBean.class.getName()).log(Level.INFO, "episodes found");
            } else {
                Logger.getLogger(PodcastBean.class.getName()).log(Level.INFO, "episodes NOT found");
            }
        } catch (Exception ex) {
            Logger.getLogger(PodcastBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return podcast;
    }

    public void setPodcast(Podcast podcast) {
        this.podcast = podcast;
    }

    public List<Episode> getEpisodes() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        Podcast podcast = null;
        try {
            //todo add to web xml
            String podcastURL = (String) ctx.getExternalContext().getInitParameter("podcast.address");
            Logger.getLogger(PodcastBean.class.getName()).log(Level.INFO, podcastURL);
            podcast = new PodcastXML(new URL(podcastURL));
            episodes = podcast.getEpisodes();
            Collections.sort(episodes);

            if (episodes != null) {
                Logger.getLogger(PodcastBean.class.getName()).log(Level.INFO, "episodes found");
            } else {
                Logger.getLogger(PodcastBean.class.getName()).log(Level.INFO, "episodes NOT found");
            }
        } catch (Exception ex) {
            Logger.getLogger(PodcastBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return episodes;
    }

    public void setEpisodes(List<Episode> episodes) {
        this.episodes = episodes;
    }

}
