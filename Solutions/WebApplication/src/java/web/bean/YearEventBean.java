/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import data.helper.EventHelper;
import data.helper.EventHelper.EventType;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import web.Event;
import web.obj.EventDatesObj;

/**
 *
 * @author HopeBenziger
 */
@ManagedBean
public class YearEventBean implements Serializable {

    private final static Logger LOGGER = Logger.getLogger(YearEventBean.class
            .getName());

    private String name;
    private String searchField;
    private Event selectedEvent;
    private Event firstSpecialEvent;
    private boolean hasEvent;

    public boolean isHasEvent() {
        if (getFirstSpecialEvent() == null) {
            return false;
        } else {
            return true;
        }
    
    }

    public void setHasEvent(boolean hasEvent) {
        this.hasEvent = hasEvent;
    }

    public Event getFirstSpecialEvent() {
        firstSpecialEvent = EventHelper.findFirstSpecialEvent();
        return firstSpecialEvent;
    }

    public void setFirstSpecialEvent(Event firstSpecialEvent) {
        this.firstSpecialEvent = firstSpecialEvent;
    }
    private List<Event> monthEvent;
    private Date startDate = null;
    private Date endDate = null;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    

    public List<Event> getMonthEvent() {
        String idStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        int id = 0;
        if (idStr != null) {
            id = Integer.parseInt(idStr);

        }
        
        //get special
        String eventTypeQuery = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("eventType");
        boolean isSpecial = false;
        EventType eventType = EventType.All;
        if (eventTypeQuery != null) {
            if (eventTypeQuery.equalsIgnoreCase("special")) {
                eventType = EventType.Special;
            }
           if (eventTypeQuery.equalsIgnoreCase("general")) {
                eventType = EventType.General;
            }

        }
        
        Calendar cal = new GregorianCalendar();

        switch (id) {
            case 0:

                cal.add(Calendar.DATE, 31);
                endDate = cal.getTime();
                startDate = new Date();
                monthEvent = EventHelper.findAll(startDate, endDate,eventType);
                break;
            case 99:

                cal.add(Calendar.DATE, 365);
                endDate = cal.getTime();
                startDate = new Date();
                monthEvent = EventHelper.findAll(startDate, endDate,eventType);
                break;
            default:
               EventDatesObj eventDatesObj =  EventHelper.computeCalendarDates(id);
               endDate = eventDatesObj.getEndDate();
               startDate = eventDatesObj.getStartDate();
                monthEvent = EventHelper.findAll(startDate, endDate,eventType);
                break;
        }
        return monthEvent;
    }

    public void setMonthEvent(List<Event> monthEvent) {
        this.monthEvent = monthEvent;
    }

    public Event getSelectedEvent() {
        return selectedEvent;
    }

    public void setSelectedEvent(Event selectedEvent) {
        this.selectedEvent = selectedEvent;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }
    private Date date;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    private List<Event> eventList;
    private List<Event> filterEventList;

    public List<Event> getFilterEventList() {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, 31);
        Date endDate = cal.getTime();
        Date todaydate = new Date();
        filterEventList = EventHelper.findAll(todaydate, endDate);
        return filterEventList;
    }

    /*
     Get only next two events
     */
    public List<Event> getNextEvents() {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, 31);
        Date endDate = cal.getTime();
        Date todaydate = new Date();
        filterEventList = EventHelper.findAll(todaydate, endDate, 2);
        return filterEventList;
    }

    public List<Event> getNextEventList() {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, 31);
        Date endDate = cal.getTime();
        Date todaydate = new Date();
        filterEventList = EventHelper.findAll(todaydate, endDate);
        return filterEventList;
    }

    public void setFilterEventList(List<Event> filterEventList) {
        this.filterEventList = filterEventList;
    }

    public List<Event> getEventList() {
        eventList = EventHelper.findAll();
        return eventList;
    }
}
