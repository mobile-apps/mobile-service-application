/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import com.mobile.dto.Log;
import data.helper.LogHelper;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import web.WebConstants;

/**
 *
 * @author HSC314
 */
@ManagedBean
@SessionScoped
public class LogBean {

    private List<com.mobile.dto.Log> logs;
    private final static Logger LOGGER = Logger.getLogger(LogBean.class
            .getName());
    private Log selectedLog;

    public Log getSelectedLog() {
        return selectedLog;
    }

    private String rtnEditMsg;

    public String getRtnEditMsg() {
        return rtnEditMsg;
    }

    public void setRtnEditMsg(String rtnEditMsg) {
        this.rtnEditMsg = rtnEditMsg;
    }

    public void setSelectedLog(Log selectedLog) {
        this.selectedLog = selectedLog;
    }

    public List<com.mobile.dto.Log> getLogs() {
        LOGGER.log(Level.OFF, "start getRegisterUsers ");
        logs = LogHelper.findAll();
        if (logs == null) {
            return null;
        }
        Collections.sort(logs, new Comparator<Log>() {

            @Override
            public int compare(Log o1, Log o2) {
                int compareInt = 0;
                if (o1.getLogId() > o2.getLogId()) {
                    return 1;
                }
                return compareInt;

            }
        });

        LOGGER.log(Level.OFF, "end getRegisterUsers ");
        return logs;
    }

    public void deleteActionListener() {
        FacesContext fc = FacesContext.getCurrentInstance();
        if (LogHelper.deleteAll() > 0) {
            fc.addMessage(null, new FacesMessage("Successful", WebConstants.LOGYESDELETEMSG));
        } else {
            fc.addMessage(null, new FacesMessage("Unsuccessful", WebConstants.LOFMSG));
        }
    }

    public void setRegisterUsers(List<com.mobile.dto.Log> logs) {
        this.logs = logs;
    }
}
