/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import com.mobile.dto.Department;
import data.helper.DepartmentHelper;
import data.helper.EventHelper;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import org.primefaces.event.SelectEvent;
import web.Event;
import web.mobel.DepartmentDataModel;

/**
 *
 * @author HopeBenziger
 */
@ManagedBean
public class ScheduleBean implements Serializable {

    private List<Department> selectedDepartments;
    private DepartmentDataModel departmentDataModel;

    public ScheduleBean() {
        departmentList = DepartmentHelper.findAll();
        departmentDataModel = new DepartmentDataModel(departmentList);
    }
      public void check(SelectEvent event) {
            System.out.println("in check");
        }

    public DepartmentDataModel getDepartmentDataModel() {
        return departmentDataModel;
    }

    public void setDepartmentDataModel(DepartmentDataModel departmentDataModel) {
        this.departmentDataModel = departmentDataModel;
    }

    public List<Department> getSelectedDepartments() {

        return selectedDepartments;
    }

    public void setSelectedDepartments(List<Department> selectedDepartments) {
        this.selectedDepartments = selectedDepartments;
    }
    private final static Logger LOGGER = Logger.getLogger(ScheduleBean.class
            .getName());

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
    private Department department = new Department();

    private List<Event> eventList;

    public List<Event> getEventList() {

        return eventList;
    }

    public void setEventList(List<Event> eventList) {
        this.eventList = eventList;
    }

    public void generateEvents() {
        eventList = EventHelper.findAll(true);
    }

    public String showEvents() {
        if (selectedDepartments != null && selectedDepartments.size() > 0) {

            List<Event> allEventList = EventHelper.findAll(true);
            for (Event event : allEventList) {
                for (Department department : selectedDepartments) {
                    if (event.getDepartment().getDepartmentId() == department.getDepartmentId()) {
                        eventList.add(event);
                    }
                }
            }
        }
        return "schedules";
    }

    public List<Department> getDepartmentList() {

        return departmentList;
    }

    public void setDepartmentList(List<Department> departmentList) {
        this.departmentList = departmentList;
    }
    // List of departments 
    private List<Department> departmentList;

}
