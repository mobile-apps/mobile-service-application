    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package web.bean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

 
@ManagedBean(name = "carService")
@ApplicationScoped
public class CarService {
     
    
     
    public List<Car> createCars(int size) {
        List<Car> list = new ArrayList<Car>();
        Car car1 = new  Car("ford", 1920, "ford", "blue");
          list.add(car1);
           Car car2 = new  Car("mercedes", 1940, "mercedes", "black");
           list.add(car2);
        
        return list;
    }
     
   
}
