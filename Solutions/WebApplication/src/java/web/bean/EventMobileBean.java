/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import data.helper.EventHelper;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import web.Event;

/**
 *
 * @author HopeBenziger
 */
@ManagedBean
@ApplicationScoped
public class EventMobileBean implements Serializable {

    private final static Logger LOGGER = Logger.getLogger(EventMobileBean.class
            .getName());

    private Event selectedEvent;

    @PostConstruct
    public void init() {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, 31);
        Date endDate = cal.getTime();
        Date todaydate = new Date();
        filterEventList = EventHelper.findAll(todaydate, endDate);
    }

    public Event getSelectedEvent() {
        return selectedEvent;
    }

    public void setSelectedEvent(Event selectedEvent) {
        this.selectedEvent = selectedEvent;
    }

    private List<Event> filterEventList;

    public List<Event> getFilterEventList() {

        return filterEventList;
    }

    public void setFilterEventList(List<Event> filterEventList) {
        this.filterEventList = filterEventList;
    }

}
