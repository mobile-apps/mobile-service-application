/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import com.mobile.dto.Service;
import data.helper.ServiceHelper;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 *
 * @author wtccuser
 */
@ManagedBean
public class CalendarDetailBean {

    private String startDateFormatted;
    private String endDateFormatted;
    private com.mobile.dto.Service service;
    
    private void setValues() {
        
    }

    public Service getService() {
        String idStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        String startDateStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("startDate");
        String endDateStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("endDate");

        int id = 0;
        if (idStr != null) {
            id = Integer.parseInt(idStr);

        }
        service = ServiceHelper.findByService(id);
      
        long startDateLong = 0;
        if (startDateStr != null) {
            startDateLong = Long.parseLong(startDateStr.replaceAll(",", ""));

        }
        long endDateLong = 0;
        if (endDateStr != null) {
            endDateLong = Long.parseLong(endDateStr.replaceAll(",", ""));

        }
        Date startDate = new Date(startDateLong);
        Date endDate = new Date(endDateLong);

        TimeZone mtz = TimeZone.getTimeZone("EST");
        DateFormat mdf = new SimpleDateFormat("EEE MMM dd hh:mm a", Locale.US);
         DateFormat mdfEnd = new SimpleDateFormat("hh:mm a", Locale.US);


        mdf.setTimeZone(mtz);
        mdfEnd.setTimeZone(mtz);
        startDateFormatted = mdf.format(startDate);
        endDateFormatted = mdfEnd.format(endDate);

        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public String getStartDateFormatted() {
         String idStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        String startDateStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("startDate");
        String endDateStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("endDate");

        int id = 0;
        if (idStr != null) {
            id = Integer.parseInt(idStr);

        }
        
      
        long startDateLong = 0;
        if (startDateStr != null) {
            startDateLong = Long.parseLong(startDateStr.replaceAll(",", ""));

        }
        long endDateLong = 0;
        if (endDateStr != null) {
            endDateLong = Long.parseLong(endDateStr.replaceAll(",", ""));

        }
        Date startDate = new Date(startDateLong);
        Date endDate = new Date(endDateLong);

        TimeZone mtz = TimeZone.getTimeZone("EST");
        DateFormat mdf = new SimpleDateFormat("EEE MMM dd hh:mm a", Locale.US);
         DateFormat mdfEnd = new SimpleDateFormat("hh:mm a", Locale.US);


        mdf.setTimeZone(mtz);
        mdfEnd.setTimeZone(mtz);
        startDateFormatted = mdf.format(startDate);
        endDateFormatted = mdfEnd.format(endDate);
        return startDateFormatted;
    }

    public void setStartDateFormatted(String startDateFormatted) {
        this.startDateFormatted = startDateFormatted;
    }

    public String getEndDateFormatted() {
        
         String idStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        String startDateStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("startDate");
        String endDateStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("endDate");

        int id = 0;
        if (idStr != null) {
            id = Integer.parseInt(idStr);

        }
        
      
        long startDateLong = 0;
        if (startDateStr != null) {
            startDateLong = Long.parseLong(startDateStr.replaceAll(",", ""));

        }
        long endDateLong = 0;
        if (endDateStr != null) {
            endDateLong = Long.parseLong(endDateStr.replaceAll(",", ""));

        }
        Date startDate = new Date(startDateLong);
        Date endDate = new Date(endDateLong);

        TimeZone mtz = TimeZone.getTimeZone("EST");
        DateFormat mdf = new SimpleDateFormat("EEE MMM dd hh:mm a", Locale.US);
         DateFormat mdfEnd = new SimpleDateFormat("hh:mm a", Locale.US);


        mdf.setTimeZone(mtz);
        mdfEnd.setTimeZone(mtz);
        startDateFormatted = mdf.format(startDate);
        endDateFormatted = mdfEnd.format(endDate);
        return endDateFormatted;
    }

    public void setEndDateFormatted(String endDateFormatted) {
        this.endDateFormatted = endDateFormatted;
    }

}
