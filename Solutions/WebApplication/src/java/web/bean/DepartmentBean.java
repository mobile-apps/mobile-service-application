/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import com.mobile.dto.Department;
import data.extender.DepartmentExtender;
import data.helper.DepartmentHelper;
import data.helper.GeoLocation;
import data.helper.GeoLocationHelper;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFile;
import util.IconBase64;
import web.WebConstants;

/**
 *
 * @author HSC314
 */
@ManagedBean
public class DepartmentBean implements Serializable {


    private final static Logger LOGGER = Logger.getLogger(DepartmentBean.class
            .getName());

    // List of departments 
    private List<Department> departmentList;

    public List<Department> getDepartmentList() {
        FacesContext fc = FacesContext.getCurrentInstance();
        departmentList = DepartmentHelper.findAll();
        fc.getExternalContext().getSessionMap().put(WebConstants.DEPARTMENTBEAN_DEPTLIST, departmentList);
        return departmentList;
    }

    public void setDepartmentList(List<Department> departmentList) {
        this.departmentList = departmentList;
    }

    private List<DepartmentExtender> departmentExtenderList;

    public List<DepartmentExtender> getDepartmentExtenderList() {
        FacesContext fc = FacesContext.getCurrentInstance();
        departmentExtenderList = DepartmentHelper.findAllExtender();
        fc.getExternalContext().getSessionMap().put(WebConstants.DEPARTMENTBEAN_DEPTEXTLIST, departmentExtenderList);
        return departmentExtenderList;
    }

    public void setDepartmentExtenderList(List<DepartmentExtender> departmentExtenderList) {
        this.departmentExtenderList = departmentExtenderList;
    }

    // department used for adds
    private Department department = new Department();

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    // deparment used for selecting from list
    private DepartmentExtender selectedDepartment;

    public DepartmentExtender getSelectedDepartment() {
        FacesContext fc = FacesContext.getCurrentInstance();
        selectedDepartment = (DepartmentExtender) fc.getExternalContext().getSessionMap().get(WebConstants.DEPARTMENTBEAN_SELECTEDDEPT);
        return selectedDepartment;
    }

    public void setSelectedDepartment(DepartmentExtender selectedDepartment) {
        this.selectedDepartment = selectedDepartment;
    }

    // streamed content icons for displaying
//    public StreamedContent getIconForList() {
//        FacesContext fc = FacesContext.getCurrentInstance();
//        if (fc.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
//            return new DefaultStreamedContent();
//        } else {
//            String index = fc.getExternalContext().getRequestParameterMap().get("departmentIndex");
//            List<DepartmentExtender> deptextlist
//                    = (List<DepartmentExtender>) fc.getExternalContext().getSessionMap().get(WebConstants.DEPARTMENTBEAN_DEPTEXTLIST);
//            return deptextlist.get(Integer.valueOf(index)).getIconImage();
//        }
//    }
    public String getIconForAddString() {
        UploadedFile iconFile = getIconFile();
        if (iconFile != null) {
            return IconBase64.encodeToString(iconFile);
        } else {
            return "";
        }
    }

//    public StreamedContent getIconForAdd() {
//        FacesContext fc = FacesContext.getCurrentInstance();
//        if (fc.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
//            return new DefaultStreamedContent();
//        } else {
//            UploadedFile iconFile = getIconFile();
//            if (iconFile != null) {
//                try {
//                    return new DefaultStreamedContent(iconFile.getInputstream(), iconFile.getContentType());
//                } catch (IOException ex) {
//                    Logger.getLogger(DepartmentBean.class.getName()).log(Level.SEVERE, null, ex);
//                    return new DefaultStreamedContent();
//                }
//            } else {
//                return new DefaultStreamedContent();
//            }
//        }
//    }
    public String getIconForEditString() {
        UploadedFile iconFile = getIconFile();
        if (iconFile != null) {
            return IconBase64.encodeToString(iconFile);
        } else {
            String iconStr = "";
            if (selectedDepartment != null) {
                iconStr = selectedDepartment.getIconstring();
            }
            return iconStr;
        }

    }

//    public StreamedContent getIconForEdit() {
//        FacesContext fc = FacesContext.getCurrentInstance();
//        if (fc.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
//            return new DefaultStreamedContent();
//        } else {
//            UploadedFile iconFile = getIconFile();
//            if (iconFile != null) {
//                try {
//                    return new DefaultStreamedContent(iconFile.getInputstream(), iconFile.getContentType());
//                } catch (IOException ex) {
//                    Logger.getLogger(DepartmentBean.class.getName()).log(Level.SEVERE, null, ex);
//                    return new DefaultStreamedContent();
//                }
//            } else {
//                if (getSelectedDepartment() != null) {
//                    if (selectedDepartment.getIcon() != null) {
//                        return new DefaultStreamedContent(new ByteArrayInputStream(selectedDepartment.getIcon()), selectedDepartment.getIcontype());
//                    }
//                }
//                return new DefaultStreamedContent();
//
//            }
//        }
//    }
    // uploaded file used for icon
    public UploadedFile getIconFile() {
        FacesContext fc = FacesContext.getCurrentInstance();
        UploadedFile iconFile
                = (UploadedFile) fc.getExternalContext().getSessionMap().get(WebConstants.DEPARTMENT_ICONFILE);
        return iconFile;
    }

    public void setIconFile(UploadedFile iconFile) {
        FacesContext fc = FacesContext.getCurrentInstance();
        if (iconFile == null) {
            fc.getExternalContext().getSessionMap().remove(WebConstants.DEPARTMENT_ICONFILE);
        } else {
            fc.getExternalContext().getSessionMap().put(WebConstants.DEPARTMENT_ICONFILE, iconFile);
        }
    }

    // add function
    public String add() {
        LOGGER.log(Level.OFF, "start add");

        UploadedFile iconFile = getIconFile();
        if (iconFile != null) {
            LOGGER.log(Level.OFF, "  start add icon");

            this.department.setIconstring(IconBase64.encodeToString(iconFile));

            byte[] iconBytes = null;
            String iconType = "";

            try {
                iconBytes = IOUtils.toByteArray(iconFile.getInputstream());

            } catch (IOException ex) {
                Logger.getLogger(DepartmentBean.class
                        .getName()).log(Level.SEVERE, null, ex);
            }

            iconType = iconFile.getContentType();

            this.department.setIcon(iconBytes);
            this.department.setIcontype(iconType);

            setIconFile(null);  // clear stored iconfile once used

            LOGGER.log(Level.OFF, "  end add icon");

        } //if (iconFile != null)      

        GeoLocationHelper geoLocationHelper = new GeoLocationHelper();
        String location = null;
        if (selectedDepartment == null || selectedDepartment.getLocation() == null) {
            location = this.department.getLocation();
        }
        GeoLocation geoLocation = geoLocationHelper.getGeoLocation(location);

        this.department.setLatitude(geoLocation.latitude);
        this.department.setLongitude(geoLocation.longitude);

        this.department = DepartmentHelper.add(this.department);

        LOGGER.log(Level.OFF, "end add ");

        return WebConstants.DEPARTMENTS_PAGE;

    } //add

    // initializae values for the add department function
    public void addDepartmentInitializer(ActionEvent event) {
        setIconFile(null);  // clear stored iconfile once used        
    }

    // update function
    public String update() {
        LOGGER.log(Level.OFF, "start update");

        UploadedFile iconFile = getIconFile();
        if (iconFile != null) {
            LOGGER.log(Level.OFF, "  start update icon");

            this.selectedDepartment.setIconstring(IconBase64.encodeToString(iconFile));

            byte[] iconBytes = null;
            String iconType = "";

            try {
                iconBytes = IOUtils.toByteArray(iconFile.getInputstream());

            } catch (IOException ex) {
                Logger.getLogger(DepartmentBean.class
                        .getName()).log(Level.SEVERE, null, ex);
            }

            iconType = iconFile.getContentType();

            this.selectedDepartment.setIcon(iconBytes);
            this.selectedDepartment.setIcontype(iconType);

            setIconFile(null); // clear stored iconfile once used

            LOGGER.log(Level.OFF, "  end update icon");

        } //if (iconFile != null)      
        GeoLocationHelper geoLocationHelper = new GeoLocationHelper();
        GeoLocation geoLocation = geoLocationHelper.getGeoLocation(this.selectedDepartment.getLocation());

        this.selectedDepartment.setLatitude(geoLocation.latitude);
        this.selectedDepartment.setLongitude(geoLocation.longitude);
        DepartmentHelper.update(this.selectedDepartment);

        LOGGER.log(Level.OFF, "end update ");

        return WebConstants.DEPARTMENTS_PAGE;

    } //update

    public String a() {
        return WebConstants.DEPARTMENTS_PAGE;
    }

    // delete function
    public String delete(DepartmentExtender department) {
        LOGGER.log(Level.OFF, "start delete");

        String nextPage;
        if (department.getServices().isEmpty()) {
            DepartmentHelper.delete(department);
            setIconFile(null); // clear stored iconfile once used
            nextPage = WebConstants.DEPARTMENTS_PAGE;
        } else {
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, new FacesMessage("Unsuccessful", WebConstants.DEPTNODELETEMSG));
            nextPage = "";
        }

        LOGGER.log(Level.OFF, "end delete ");

        return nextPage;

    } //delete

    public String findByOrganizationId(int id) {
        LOGGER.log(Level.OFF, "start findByOrganizationId");

        String org = DepartmentHelper.findByOrganizationId(id);

        LOGGER.log(Level.OFF, "end findByOrganizationId ");

        return org;

    } //findByOrganizationId

    /**
     * Navigate to detail page
     *
     * @param event
     * @return
     */
    public String onRowSelectNavigate(SelectEvent event) {
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.getExternalContext().getFlash().put(WebConstants.SELECTED_CATEGORY, event.getObject());

        return WebConstants.CATEGORY_DETAIL_PAGE;

    } //onRowSelectNavigate

    public void onRowSelect(SelectEvent event) {
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.getExternalContext().getSessionMap().put(WebConstants.DEPARTMENTBEAN_SELECTEDDEPT, selectedDepartment);

        ConfigurableNavigationHandler configurableNavigationHandler
                = (ConfigurableNavigationHandler) fc.getApplication().getNavigationHandler();

        configurableNavigationHandler.performNavigation(WebConstants.DEPARTMENT_EDIT_PAGE);

    } //onRowSelect

    // upload file handler
    public void handleFileUploadDepartment(FileUploadEvent event) {
        LOGGER.log(Level.OFF, "start icon file upload");

        setIconFile(event.getFile());

        LOGGER.log(Level.OFF, "end icon file upload");

    } //handleFileUploadDepartment

} //class
