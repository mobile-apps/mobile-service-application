/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import com.mobile.dto.Category;
import com.mobile.dto.Department;
import data.extender.DepartmentExtender;
import data.helper.CategoryHelper;
import data.helper.DepartmentHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import web.WebConstants;
import web.obj.CategoryMenuObj;

/**
 *
 * @author HSC314
 */
@ManagedBean
public class CategoryBean implements Serializable {

    private final static Logger LOGGER = Logger.getLogger(CategoryHelper.class
            .getName());
    private List<CategoryMenuObj> categoryMenuList;

    public List<CategoryMenuObj> getCategoryMenuList() {
        
        categoryList = getCategoryList();
        if (categoryList == null) {
            return null;
        }
        categoryMenuList = new ArrayList<CategoryMenuObj>();
        List<Department> departmentList = null;
        for (Category category : categoryList) {
            departmentList = DepartmentHelper.findAllByCategoryId(category.getCategoryId());
            if (departmentList != null && departmentList.size() > 0) {
                CategoryMenuObj categoryMenuObj = new CategoryMenuObj(category.getName(),departmentList);
                categoryMenuList.add(categoryMenuObj);
            }
        }

        return categoryMenuList;
    }

    public void setCategoryMenuList(List<CategoryMenuObj> categoryMenuList) {
        this.categoryMenuList = categoryMenuList;
    }
    private List<Category> categoryList;

    public List<Category> getCategoryList() {
        categoryList = CategoryHelper.findAll();
        //add 
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }
    private com.mobile.dto.Category category = new com.mobile.dto.Category();
    private com.mobile.dto.Category selectedCategory;

    public Category getSelectedCategory() {
        FacesContext fc = FacesContext.getCurrentInstance();
        selectedCategory = (Category) fc.getExternalContext().getSessionMap().get(WebConstants.SELECTED_CATEGORY);

        return selectedCategory;
    }

    public void setSelectedCategory(Category selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String add() {
        LOGGER.log(Level.OFF, "start add");
        this.category = CategoryHelper.add(category.getName(), category.getImage());
        LOGGER.log(Level.OFF, "end add ");

        return WebConstants.CATEGORIES_PAGE;
    }

    public String update() {
        CategoryHelper.update(this.selectedCategory);
        LOGGER.log(Level.OFF, "end update ");

        return WebConstants.CATEGORIES_PAGE;
    }

    /**
     * Navigate to detail page
     *
     * @param event
     * @return
     */
    public void onRowSelectNavigate(SelectEvent event) {
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.getExternalContext().getSessionMap().put(WebConstants.SELECTED_CATEGORY, selectedCategory);

        ConfigurableNavigationHandler configurableNavigationHandler
                = (ConfigurableNavigationHandler) fc.getApplication().getNavigationHandler();

        configurableNavigationHandler.performNavigation(WebConstants.CATEGORY_DETAIL_PAGE);

    }

}
