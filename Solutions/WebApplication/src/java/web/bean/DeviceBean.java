/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package web.bean;

import com.mobile.dto.Device;
import data.helper.DeviceHelper;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import java.util.List;
import web.WebConstants;

/**
 *
 * @author HSC314
 */
@ManagedBean
public class DeviceBean implements Serializable {
    private final static Logger LOGGER = Logger.getLogger(DeviceBean.class
            .getName());

    public List<Device> getDeviceList() {
        deviceList = DeviceHelper.findAllDevices();
        return deviceList;
    }

    public void setDeviceList(List<Device> deviceList) {
        this.deviceList = deviceList;
    }
  
    private List<Device> deviceList;

  
   
   
    
   

   
   
}
