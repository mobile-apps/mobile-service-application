/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

/**
 *
 * @author wtccuser
 */
import com.mobile.dto.Department;
import com.mobile.dto.Organization;
import web.obj.Step;
import data.helper.GeoLocation;
import data.helper.GeoLocationHelper;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.Cookie;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import web.helper.MapHelper;
import web.obj.GoogleMap;

/**
 *
 * @author HSC314
 */
@ManagedBean

public class DrivingDirectionBean {

    private MapModel model = new DefaultMapModel();

    public MapModel getModel() {
        return model;
    }

    private boolean shouldRender;

    public boolean isShouldRender() {
        return shouldRender;
    }

    public void setShouldRender(boolean shouldRender) {
        this.shouldRender = shouldRender;
    }

    public DrivingDirectionBean() {
        List<Organization> listOrg = data.helper.OrganizationHelper.findAll();

        //get first org
        if (listOrg != null && listOrg.size() > 0) {
            Organization firstOrg = listOrg.get(0);
            setLatitude(firstOrg.getLatitude());
            setLongitude(firstOrg.getLongitude());
        }

        updateModel();
    }

    public void setModel(MapModel model) {
        this.model = model;
        updateModel();
    }
    // ST - added to update Map information everytime latitude and/or longitude is changed
    private void updateModel() {
        model.addOverlay(new Marker(new LatLng(latitude, longitude), "M1"));
    }
    
    public double latitude;
    public double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
        updateModel();
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
        updateModel();
    }
    private List<Step> steps = new ArrayList<Step>();
    FacesContext ctx = FacesContext.getCurrentInstance();
    private DefaultMapModel emptyModel;
    private Department department = new Department();

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    String contextAddressValue
            = ctx.getExternalContext().getInitParameter("google.json.address");

    private String startingAddress;

    public String getStartingAddress() {
        Map<String, Object> requestCookieMap = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestCookieMap();
        Cookie cookie = (Cookie) requestCookieMap.get("startingAddress");
        if (cookie != null) {
            startingAddress = cookie.getValue();
        }

        return startingAddress;

    } //getStartingAddress

    public void setStartingAddress(String startingAddress) {
        this.startingAddress = startingAddress;
    }

    public void mobileFindDirection() {

    }
    
    public String findStepImage(int rowPosition, Step step) {
        String path = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        return path;
    }

    public String findDirection() {
        String page = "directionresult";

        //write cookie
        FacesContext.getCurrentInstance()
                .getExternalContext()
                .addResponseCookie("startingAddress", startingAddress, null);
     //   String path = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
            List<com.mobile.dto.Organization> listOrg = data.helper.OrganizationHelper.findAll();

        //get first org
        com.mobile.dto.Organization firstOrg = listOrg.get(0);

        String endingAddress = firstOrg.getLatitude() + "," + firstOrg.getLongitude();

        //String endingAddress = firstOrg.getLatitude() + "," + firstOrg.getLongitude();
        GoogleMap googleMap = MapHelper.getDrivingDirection(startingAddress.trim(), endingAddress,this.contextAddressValue);
        this.shouldRender = true;
        steps = googleMap.getSteps();
        return page;

    } //findDirection

    public String findDrivingDirections() {
        try {

            //write cookie
            FacesContext.getCurrentInstance()
                    .getExternalContext()
                    .addResponseCookie("startingAddress", startingAddress, null);

            List<Organization> orgs = data.helper.OrganizationHelper.findAll();
            
            String destination = orgs.get(0).getLocation();

            Object[] args = {startingAddress.trim(), destination.trim()};
            MessageFormat fmt = new MessageFormat("?origin={0}&destination={1}");
            String replaceSpace = fmt.format(args);
            replaceSpace = replaceSpace.replace(" ", "%20");

            //String decoded = java.net.URLEncoder.encode(fmt.format(args), "UTF-8");
            this.contextAddressValue = this.contextAddressValue + replaceSpace;

            URL url = new URL(contextAddressValue);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            int code = conn.getResponseCode();
            String message = conn.getResponseMessage();

            if (code != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + message);
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            StringBuffer buffer = new StringBuffer();
            System.out.println("Output from Server .... \n");

            while ((output = br.readLine()) != null) {
                //System.out.println(output);
                buffer.append(output);
            }

            org.primefaces.json.JSONObject jsonObject = new org.primefaces.json.JSONObject(buffer.toString());
            org.primefaces.json.JSONArray routes = jsonObject.getJSONArray("routes");
            org.primefaces.json.JSONObject legs = routes.getJSONObject(0);
            org.primefaces.json.JSONArray legsArray = legs.getJSONArray("legs");
            org.primefaces.json.JSONObject oo = legsArray.getJSONObject(0);
            org.primefaces.json.JSONArray stepArray = oo.getJSONArray("steps");

            for (int x = 0; x < stepArray.length(); x++) {

                org.primefaces.json.JSONObject rec = stepArray.getJSONObject(x);
                String loc = rec.getString("html_instructions");
                Step step = new Step(loc);

                steps.add(step);
            }

            conn.disconnect();

        } catch (Exception ex) {
            Logger.getLogger(DrivingDirectionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "directionresult";

    } //findDrivingDirections

} //class
