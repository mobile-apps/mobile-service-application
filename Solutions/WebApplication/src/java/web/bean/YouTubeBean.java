/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import web.youtube.*;

/**
 *
 * @author wtccuser
 */
@ManagedBean
public class YouTubeBean {
    
    

    public List<YouTube> getFirstRow() {
        if (totalList == null) {
            totalList = YouTubeRSSParser.getAll();
        }
        if (totalList != null) {
            firstRow.add(totalList.get(0));
            firstRow.add(totalList.get(1));
            firstRow.add(totalList.get(2));
        }
        return firstRow;
    }

    public void setFirstRow(List<YouTube> firstRow) {
        this.firstRow = firstRow;
    }

    public List<YouTube> getSecondRow() {
         if (totalList == null) {
            totalList = YouTubeRSSParser.getAll();
        }
        if (totalList != null) {
            secondRow.add(totalList.get(3));
            secondRow.add(totalList.get(4));
            secondRow.add(totalList.get(5));
        }
        return secondRow;
    }

    public void setSecondRow(List<YouTube> secondRow) {
        this.secondRow = secondRow;
    }
    private List<YouTube> firstRow = new ArrayList<YouTube>();
    private List<YouTube> secondRow = new ArrayList<YouTube>();
    private List<YouTube> totalList;
    private String id;

    public String getId() {
        id =   FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
      
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    

    public List<YouTube> getTotalList() {
        totalList = YouTubeRSSParser.getAll();
        return totalList;
    }

    public void setTotalList(List<YouTube> totalList) {
        this.totalList = totalList;
    }

}
