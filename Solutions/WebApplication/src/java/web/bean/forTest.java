/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package web.bean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.jws.Oneway;

import org.primefaces.event.SelectEvent;

/**
 *
 * @author wtccuser
 */
 @ManagedBean(name="forTest")
    @SessionScoped
    public class forTest implements Serializable {
         private List<Car> carList = new ArrayList<Car>();
         private Car selectedCars[];

        public Car[] getSelectedCars() {
            return selectedCars;
        }

        public void setSelectedCars(Car selectedCars[]) {
            this.selectedCars = selectedCars;
        }

        public List<Car> getCarList() {
            if(carList.size()==0) {
                setCarList();
            }
            return carList;
        }

        public void setCarList() {
          Car car1 = new  Car("ford", 1920, "ford", "blue");
          carList.add(car1);
           Car car2 = new  Car("mercedes", 1940, "mercedes", "black");
           carList.add(car2);
        }
        public void check(SelectEvent event) {
            System.out.println("in check");
        }


    }
