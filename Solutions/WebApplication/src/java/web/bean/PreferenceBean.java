/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import com.mobile.dto.Category;
import com.mobile.dto.RegisteredUser;
import data.helper.CategoryHelper;
import data.helper.RegisterUserHelper;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import web.WebConstants;

/**
 *
 * @author administrator
 */
@ManagedBean
@RequestScoped
public class PreferenceBean {

    public List<Category> getSelectedCategories() {
        return selectedCategories;
    }

    public void setSelectedCategories(List<Category> selectedCategories) {
        this.selectedCategories = selectedCategories;
    }

    private List<com.mobile.dto.Category> selectedCategories;
    private List<com.mobile.dto.Category> categoryList;

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public RegisteredUser getRegisteredUser() {
        return registeredUser;
    }

    @PostConstruct
    public void initialize() {
        String token = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("token");
        //try to decrpyt
        if (token != null) {
            registeredUser = RegisterUserHelper.update(token);
            FacesContext context = FacesContext.getCurrentInstance();
            context.getExternalContext().getSessionMap().put(WebConstants.USER, registeredUser);
        }
        categoryList = CategoryHelper.findAll();
    }

    public void setRegisteredUser(RegisteredUser registeredUser) {
        this.registeredUser = registeredUser;
    }

    private String activationCode;
    private RegisteredUser registeredUser;

    /**
     *
     * @return
     */
    public String getActivationCode() {
        return activationCode;
    }

    /**
     * set activation code
     *
     * @param activationCode
     */
    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    /**
     * get next page
     *
     * @return
     */
    public String add() {
        String token = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("token");
        //try to decrpyt
        FacesContext context = FacesContext.getCurrentInstance();
        registeredUser = (RegisteredUser) context.getExternalContext().getSessionMap().get(WebConstants.USER);

        data.helper.UserCategoryHelper.delete(this.registeredUser);
        for (com.mobile.dto.Category cat : this.selectedCategories) {
            try {
                data.helper.UserCategoryHelper.add(this.registeredUser.getRegisteredUserId(), cat.getCategoryId());
            } catch (Exception ex) {

            }
        }
        return "thankyouphoneregistration";

    }

}
