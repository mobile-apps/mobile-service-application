/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import com.mobile.dto.Department;
import data.helper.DepartmentHelper;
import data.helper.EventHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import web.Event;
import web.obj.DepartmentObj;

@ManagedBean(name = "eventResultsBean")
public class EventResultsBean implements Serializable {

    public List<Event> getEvents() {
        events = (List<Event>) FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().get("events");
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    private List<Event> events;

}
