/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import com.mobile.dto.Organization;
import data.helper.GeoLocation;
import data.helper.GeoLocationHelper;
import data.helper.OrganizationHelper;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFile;
import util.IconBase64;
import data.extender.OrganizationExtender;
import javax.faces.application.FacesMessage;
import web.WebConstants;

/**
 *
 * @author HSC314
 */
@ManagedBean
public class OrganizationBean implements Serializable {

    private final static Logger LOGGER = Logger.getLogger(OrganizationBean.class.getName());

    // List of organizations
    private List<Organization> organizationList;
    private List<OrganizationExtender> organizationExtenderList;

    public List<Organization> getOrganizationList() {
        FacesContext fc = FacesContext.getCurrentInstance();
        organizationList = OrganizationHelper.findAll();
        fc.getExternalContext().getSessionMap().put(WebConstants.ORGANIZATIONBEAN_ORGLIST, organizationList);
        return organizationList;
    }

    public void setOrganizationList(List<Organization> organizationList) {
        this.organizationList = organizationList;
    }

    public List<OrganizationExtender> getOrganizationExtenderList() {
        FacesContext fc = FacesContext.getCurrentInstance();
        organizationExtenderList = OrganizationHelper.findAllExtender();
        fc.getExternalContext().getSessionMap().put(WebConstants.ORGANIZATIONBEAN_ORGEXTLIST, organizationExtenderList);
        return organizationExtenderList;
    }

    public void setOrganizationExtenderList(List<OrganizationExtender> organizationExtenderList) {
        this.organizationExtenderList = organizationExtenderList;
    }

    // organization used for adds
    private Organization organization = new Organization();

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    // orgnaization used for selection
    private OrganizationExtender selectedOrganization;

    public OrganizationExtender getSelectedOrganization() {
        FacesContext fc = FacesContext.getCurrentInstance();
        selectedOrganization = (OrganizationExtender) fc.getExternalContext().getSessionMap().get(WebConstants.ORGANIZATIONBEAN_SELECTEDORG);
        return selectedOrganization;
    }

    public void setSelectedOrganization(OrganizationExtender selectedOrganization) {
        this.selectedOrganization = selectedOrganization;
    }

    // streamed content icons for displaying
//    public StreamedContent getIconForList() {
//        FacesContext fc = FacesContext.getCurrentInstance();
//        if (fc.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
//            return new DefaultStreamedContent();
//        } else {
//            String index = fc.getExternalContext().getRequestParameterMap().get("organizationIndex");
//            List<OrganizationExtender> orgextlist
//                    = (List<OrganizationExtender>) fc.getExternalContext().getSessionMap().get(WebConstants.ORGANIZATIONBEAN_ORGEXTLIST);
//            return orgextlist.get(Integer.valueOf(index)).getIconImage();
//        }
//    }
    public String getIconForAddString() {
        UploadedFile iconFile = getIconFile();
        if (iconFile != null) {
            return IconBase64.encodeToString(iconFile);
        } else {
            return "";
        }
    }

//    public StreamedContent getIconForAdd() {
//        FacesContext fc = FacesContext.getCurrentInstance();
//        if (fc.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
//            return new DefaultStreamedContent();
//        } else {
//            UploadedFile iconFile = getIconFile();
//            if (iconFile != null) {
//                try {
//                    return new DefaultStreamedContent(iconFile.getInputstream(), iconFile.getContentType());
//                } catch (IOException ex) {
//                    Logger.getLogger(OrganizationBean.class.getName()).log(Level.SEVERE, null, ex);
//                    return new DefaultStreamedContent();
//                }
//            } else {
//                return new DefaultStreamedContent();
//            }
//        }
//    }
    public String getIconForEditString() {
        UploadedFile iconFile = getIconFile();
        if (iconFile != null) {
            return IconBase64.encodeToString(iconFile);
        } else {
            return selectedOrganization.getIconstring();
        }
    }

//    public StreamedContent getIconForEdit() {
//        FacesContext fc = FacesContext.getCurrentInstance();
//        if (fc.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
//            return new DefaultStreamedContent();
//        } else {
//            UploadedFile iconFile = getIconFile();
//            if (iconFile != null) {
//                try {
//                    return new DefaultStreamedContent(iconFile.getInputstream(), iconFile.getContentType());
//                } catch (IOException ex) {
//                    Logger.getLogger(OrganizationBean.class.getName()).log(Level.SEVERE, null, ex);
//                    return new DefaultStreamedContent();
//                }
//            } else {
//                if (getSelectedOrganization() != null) {
//                    if (selectedOrganization.getIcon() != null) {
//                        return new DefaultStreamedContent(new ByteArrayInputStream(selectedOrganization.getIcon()), selectedOrganization.getIcontype());
//                    }
//                }
//                return new DefaultStreamedContent();
//
//            }
//        }
//    }
    // uploaded file used for icon
    public UploadedFile getIconFile() {
        FacesContext fc = FacesContext.getCurrentInstance();
        UploadedFile iconImage
                = (UploadedFile) fc.getExternalContext().getSessionMap().get(WebConstants.ORGANIZATION_ICONFILE);
        return iconImage;
    }

    public void setIconFile(UploadedFile iconFile) {
        FacesContext fc = FacesContext.getCurrentInstance();
        if (iconFile == null) {
            fc.getExternalContext().getSessionMap().remove(WebConstants.ORGANIZATION_ICONFILE);
        } else {
            fc.getExternalContext().getSessionMap().put(WebConstants.ORGANIZATION_ICONFILE, iconFile);
        }
    }

    /**
     * Adds the organization to the Organization table
     *
     * @return
     */
    public String add() {
        LOGGER.log(Level.OFF, "start add");

        UploadedFile iconFile = getIconFile();
        if (iconFile != null) {
            LOGGER.log(Level.OFF, "  start add icon");

            this.organization.setIconstring(IconBase64.encodeToString(iconFile));

            byte[] iconBytes = null;
            String iconType = "";

            try {
                iconBytes = IOUtils.toByteArray(iconFile.getInputstream());
            } catch (IOException ex) {
                Logger.getLogger(OrganizationBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            iconType = iconFile.getContentType();

            this.organization.setIcon(iconBytes);
            this.organization.setIcontype(iconType);

            setIconFile(null);  // clear stored iconfile once used

            LOGGER.log(Level.OFF, "  end add icon");

        } //if (iconFile != null) {

        GeoLocationHelper geoLocationHelper = new GeoLocationHelper();
        GeoLocation geoLocation = geoLocationHelper.getGeoLocation(organization.getLocation());

        this.organization.setLatitude(geoLocation.latitude);
        this.organization.setLongitude(geoLocation.longitude);

        this.organization = OrganizationHelper.add(this.organization);

        LOGGER.log(Level.OFF, "end add ");

        return WebConstants.ORGANIZATIONS_PAGE;

    } //add

    // initializae values for the add orgranization function
    public void addOrganizationInitializer(ActionEvent event) {
        setIconFile(null);  // clear stored iconfile once used        
    }

    // upload file handler
    public void handleFileUploadOrganization(FileUploadEvent event) {
        LOGGER.log(Level.OFF, "start icon file upload");

        setIconFile(event.getFile());

        LOGGER.log(Level.OFF, "end icon file upload");

    } //handleFileUploadOrganization

    //  update function
    public String update(Organization organization) {
        LOGGER.log(Level.OFF, "start update");

        UploadedFile iconFile = getIconFile();
        if (iconFile != null) {
            LOGGER.log(Level.OFF, "  start update icon");

            organization.setIconstring(IconBase64.encodeToString(iconFile));

            byte[] iconBytes = null;
            String iconType = "";

            try {
                iconBytes = IOUtils.toByteArray(iconFile.getInputstream());
            } catch (IOException ex) {
                Logger.getLogger(OrganizationBean.class.getName()).log(Level.SEVERE, null, ex);
            }

            iconType = iconFile.getContentType();

            organization.setIcon(iconBytes);
            organization.setIcontype(iconType);

            setIconFile(null);

            LOGGER.log(Level.OFF, "  end update icon");
        }  // end if (iconFile != null)

        GeoLocationHelper geoLocationHelper = new GeoLocationHelper();
        GeoLocation geoLocation = geoLocationHelper.getGeoLocation(organization.getLocation());

        organization.setLatitude(geoLocation.latitude);
        organization.setLongitude(geoLocation.longitude);
        OrganizationHelper.update(organization);

        LOGGER.log(Level.OFF, "end update ");

        return WebConstants.ORGANIZATIONS_PAGE;
    }

    //  delete the organization
    public String delete(OrganizationExtender organization) {
        LOGGER.log(Level.OFF, "start delete");

        String nextPage;
        if (organization.getDepartments().isEmpty()) {
            OrganizationHelper.delete(organization);
            setIconFile(null); // clear stored iconfile once used
            nextPage = WebConstants.ORGANIZATIONS_PAGE;
        } else {
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, new FacesMessage("Unsuccessful", WebConstants.ORGNODELETEMSG));
            nextPage = "";
        }

        LOGGER.log(Level.OFF, "end delete ");

        return nextPage;

    } //delete

    /**
     * Navigate to detail page
     *
     * @param event
     * @return
     */
    public String onRowSelectNavigate(SelectEvent event) {
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.getExternalContext().getFlash().put(WebConstants.SELECTED_CATEGORY, event.getObject());

        return WebConstants.CATEGORY_DETAIL_PAGE;

    } //onRowSelectNavigate

    /**
     *
     * @param actionEvent
     */
    public void getDepartmentList(ActionEvent actionEvent) {
        int i = 1;

    } //getDepartmentList

    /**
     * Navigate to detail page
     *
     * @param event
     * @return
     */
    public void onRowSelect(SelectEvent event) {
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.getExternalContext().getSessionMap().put(WebConstants.ORGANIZATIONBEAN_SELECTEDORG, selectedOrganization);

        ConfigurableNavigationHandler configurableNavigationHandler
                = (ConfigurableNavigationHandler) fc.getApplication().getNavigationHandler();

        configurableNavigationHandler.performNavigation(WebConstants.ORGANIZATION_EDIT_PAGE);

    } //onRowSelect

} //class
