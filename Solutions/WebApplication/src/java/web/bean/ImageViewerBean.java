/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import web.Image;

/**
 *
 * @author wtccuser
 */
@ManagedBean
public class ImageViewerBean {

    private List<Image> images;
     //http://localhost:8080/WebApplication/faces/guest/RES_NOT_FOUND
    //http://www.primefaces.org/showcase/javax.faces.resource/demo/images/nature/nature5.jpg.xhtml

    @PostConstruct
    public void init() {
        images = new ArrayList<Image>();
        Image pastor = new Image("pastor.jpg", "Pastor", "James was a member of Cokcc from March, 2004 until September 1995 when he became a pastor.  Since July, 2002, he has served here as pastor for outreach & evangelism.  He is a native of Shelby County, Kentucky, and worked 28 1/2 years in two factories.  In 1994, he answered the call to be a vocational minister.");
        Image pastorWife = new Image("pastorwife.jpg", "Women Leader", "desc");
        Image family = new Image("family.JPG", "Family Night", "desc");
        Image children = new Image("children.jpg", "Children Church", "desc");
        Image men = new Image("men.JPG", "Children Church", "desc");
        images.add(pastor);
        images.add(pastorWife);
        images.add(family);
        images.add(children);
        images.add(men);
        // images = new ArrayList<String>();
        for (int i = 1; i <= 12; i++) {
            //images.add("nature" + i + ".jpg");
        }

    }

    public List<Image> getImages() {
        return images;
    }
}
