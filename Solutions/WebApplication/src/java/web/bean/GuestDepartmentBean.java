/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.bean;

import com.mobile.dto.Department;
import data.helper.DepartmentHelper;
import data.helper.EventHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import web.Event;

/**
 *
 * @author HSC314
 */
@ManagedBean
@SessionScoped
public class GuestDepartmentBean implements Serializable {

    public int id;
    public List<Event> eventList;

    public List<Event> getEventList() {
        List<Event> allYearEvent = EventHelper.findAllYear();
        eventList = new ArrayList<Event>();
        int counter = 0;
        for (Event event : allYearEvent) {
            String idStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
            String name = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("name");
           
            
            if (counter > 3) {
                break;
            }
            if (name != null) {
                department = DepartmentHelper.findByDepartment(name);
                if (event.getDepartment().getDepartmentId() == department.getDepartmentId()) {
                    counter = counter + 1;
                    eventList.add(event);
                 }
            } else {
            
                if (event.getDepartment().getDepartmentId() == Integer.parseInt(idStr)) {
                   counter = counter + 1;
                   eventList.add(event);
                }
            }
        }
        return eventList;
    }

    public void setEventList(List<Event> eventList) {
        this.eventList = eventList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private final static Logger LOGGER = Logger.getLogger(GuestDepartmentBean.class
            .getName());

    // List of departments 
    private List<Department> departmentList;

    public List<Department> getDepartmentList() {
        // FacesContext fc = FacesContext.getCurrentInstance();
        departmentList = DepartmentHelper.findAll(true);
        // fc.getExternalContext().getSessionMap().put(WebConstants.DEPARTMENTBEAN_DEPTLIST, departmentList);
        return departmentList;
    }

    public void setDepartmentList(List<Department> departmentList) {
        this.departmentList = departmentList;
    }

    // department used for adds
    private Department department = new Department();

    public Department getDepartment() {
        String idStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        String name = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("name");
        if (name != null) {
            department = DepartmentHelper.findByDepartment(name);
        } else {
            department = DepartmentHelper.findByDepartmentId(Integer.parseInt(idStr));
        }
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

} //class
