/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package data;

/**
 *
 * @author administrator
 */
public class RegisterUserCategoryAssignment {
    private int categoryId;
    private String name;

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHasAssigned() {
        return hasAssigned;
    }

    public void setHasAssigned(boolean hasAssigned) {
        this.hasAssigned = hasAssigned;
    }
    private boolean hasAssigned;
    
}
