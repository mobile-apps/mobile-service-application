/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.helper;

import com.mobile.dao.DaoFactory;
import com.mobile.dao.UserCategoryDao;
import java.util.logging.Logger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 *
 * @author HSC314
 */
public class UserCategoryHelper {
    // use the classname for the logger, this way you can refactor

    private final static Logger LOGGER = Logger.getLogger(UserCategoryHelper.class
            .getName());

    public static void delete(com.mobile.dto.RegisteredUser registerUser) {
        LOGGER.log(Level.OFF, "started delet ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (registerUser == null) {
                LOGGER.log(Level.OFF, "Null user");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Delete to database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            UserCategoryDao dao = DaoFactory.createUserCategoryDao(connection);
            dao.deleteByRegisteredUserId(registerUser.getRegisteredUserId());

            // </editor-fold>
            LOGGER.log(Level.OFF, "end delete ");

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
            throw new java.lang.IllegalStateException(ex.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");

    }

    public static void add(int registerUserId, int categoryId) {
        LOGGER.log(Level.OFF, "started add ");
        Connection connection = null;
        try {

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Add to database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            UserCategoryDao dao = DaoFactory.createUserCategoryDao(connection);
            //setting the properties
            java.util.Date date = new java.util.Date();
            com.mobile.dto.UserCategory userCategory = new com.mobile.dto.UserCategory();
            userCategory.setCategoryId(categoryId);
            userCategory.setCreatedDate(date);
            userCategory.setRegisteredUserId(registerUserId);
            int primaryKey = dao.insert(userCategory);
            
            if (primaryKey > 0) {
              LOGGER.log(Level.OFF, "added ");  
            } else {
                 LOGGER.log(Level.OFF, "failed ");
            }

            // </editor-fold>
            LOGGER.log(Level.OFF, "end add ");

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");

    }

    /**
     * find all users in database
     *
     * @return
     */
    public static com.mobile.dto.UserCategory[] findAll() {
        LOGGER.log(Level.OFF, "start findAll ");
        Connection connection = null;
        try {
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            UserCategoryDao dao = DaoFactory.createUserCategoryDao(connection);
            com.mobile.dto.UserCategory[] users = dao.findAll();
            LOGGER.log(Level.OFF, "end findAll ");
            return users;
        } catch (Exception ex) {
            Logger.getLogger(UserCategoryHelper.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "end findAll ");
        return null;
    }

}
