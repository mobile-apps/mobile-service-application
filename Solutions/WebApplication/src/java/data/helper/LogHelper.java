/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.helper;

import com.mobile.dao.LogDao;
import com.mobile.dao.DaoFactory;
import com.mobile.dao.LogDao;
import com.mobile.dto.Crash;
import com.spoledge.audao.db.dao.DaoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;

/**
 *
 * @author HSC314
 */
public class LogHelper {
    // use the classname for the logger, this way you can refactor

    private final static Logger LOGGER = Logger.getLogger(LogHelper.class
            .getName());



    /**
     * find all users in database
     *
     * @return
     */
    public static List<com.mobile.dto.Log> findAll() {
        List<com.mobile.dto.Log> list = null;
        LOGGER.log(Level.OFF, "start findAll ");
        try {
            //get a connection to the database
            Connection connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            LogDao dao = DaoFactory.createLogDao(connection);
            com.mobile.dto.Log[] logs = dao.findAll();
            list = Arrays.asList(logs);
            LOGGER.log(Level.OFF, "end findAll ");
            return list;
        } catch (Exception ex) {
            Logger.getLogger(LogHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.OFF, "end findAll ");
        return null;
    }

    public static int deleteAll() {
        LOGGER.log(Level.OFF, "started delete ");
        Connection connection = null;
        int deleteCount = 0;
        try {

            // <editor-fold defaultstate="collapsed" desc="Delete all crash reports">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            LogDao dao = DaoFactory.createLogDao(connection);
            //setting the properties

            //try update
            deleteCount = dao.deleteAll();
            LOGGER.log(Level.OFF, "deleted all creash reports");
            LOGGER.log(Level.OFF, "ended delete ");
            // </editor-fold>

        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended delete");
        return deleteCount;
    }

}
