/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.helper;

import com.mobile.dao.DaoFactory;
import com.mobile.dao.DepartmentDao;
import com.mobile.dao.OrganizationDao;
import com.mobile.dto.Department;
import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import data.extender.DepartmentExtender;

/**
 *
 * @author wtccuser
 */
public class DepartmentHelper {

    // use the classname for the logger, this way you can refactor
    private final static Logger LOGGER = Logger.getLogger(DepartmentHelper.class
            .getName());

    
    public static Department findByDepartment(String name) {
        LOGGER.log(Level.OFF, "started findByDepartment ");
        Connection connection = null;
       
        Department[] departments = null;
        try {
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            DepartmentDao dao = DaoFactory.createDepartmentDao(connection);
            departments = dao.findByName(name);
            if (departments == null || departments.length == 0) {
                return null;
            }
           

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);

        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended findByDepartment ");

        return departments[0];
    }

    
    
    
    public static Department findByDepartmentId(int departmentId) {
        LOGGER.log(Level.OFF, "started findByDepartmentId ");
        Connection connection = null;
        Department department = null;
        try {
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            DepartmentDao dao = DaoFactory.createDepartmentDao(connection);
            department = dao.findByPrimaryKey(departmentId);

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);

        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended findByDepartmentId ");

        return department;
    }

    /**
     * add user to database make sure no exception are thrown
     *
     * @param department
     * @return
     */
    public static Department add(Department department) {
        LOGGER.log(Level.OFF, "started add ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (department == null) {
                LOGGER.log(Level.OFF, "Null Department");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Add to database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            DepartmentDao dao = DaoFactory.createDepartmentDao(connection);
            //setting the properties

            Department existingDepartment = null;
            final String queryCheck = "SELECT * from department WHERE organization_id = ? and name = ?";
            final PreparedStatement ps = connection.prepareStatement(queryCheck);
            ps.setInt(1, department.getOrganizationId());
            ps.setString(2, department.getName());
            final ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                existingDepartment.setDepartmentId(resultSet.getInt(1));
                existingDepartment.setOrganizationId(resultSet.getInt(2));
                existingDepartment.setName(resultSet.getString(3));
                existingDepartment.setPhone(resultSet.getString(4));
                existingDepartment.setLocation(resultSet.getString(5));
                existingDepartment.setEmail(resultSet.getString(6));
                existingDepartment.setCreatedDate(resultSet.getDate(8));
            }

            //Department does not exist
            if (existingDepartment == null) {
                java.util.Date date = new java.util.Date();
                department.setCreatedDate(date);
                department.setOrganizationId(department.getOrganizationId());

                //perform an insert to the database
                Integer primaryKey = null;

                primaryKey = dao.insert(department);

                if (primaryKey > 0) {
                    department.setDepartmentId(primaryKey);
                    LOGGER.log(Level.OFF, "added to database key [" + primaryKey + "]");
                    return department;
                } else {
                    LOGGER.log(Level.SEVERE, "Cannot add to Department");
                    return null;
                }
            }

            //Department already exists
            // </editor-fold>
            LOGGER.log(Level.OFF, "end add ");
            return existingDepartment;
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");
        return null;
    }

    public static Department update(Department department) {
        LOGGER.log(Level.OFF, "started update ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (department == null) {
                LOGGER.log(Level.OFF, "Null Department");
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Update database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            DepartmentDao dao = DaoFactory.createDepartmentDao(connection);
            //setting the properties

            //try update
            if (dao.update(department.getDepartmentId(), department)) {
                LOGGER.log(Level.OFF, "updated database");
                LOGGER.log(Level.OFF, "ended update ");
                return department;
            } else {
                LOGGER.log(Level.SEVERE, "Cannot update Department");
                LOGGER.log(Level.OFF, "ended update ");
                return null;
            }
            // </editor-fold>

        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended update");
        return null;
        
    }

    public static Department delete(Department department) {
        LOGGER.log(Level.OFF, "started delete ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (department == null) {
                LOGGER.log(Level.OFF, "Null Department");
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Delete department">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            DepartmentDao dao = DaoFactory.createDepartmentDao(connection);
            //setting the properties

            //try update
            if (dao.deleteByPrimaryKey(department.getDepartmentId().intValue())) {
                LOGGER.log(Level.OFF, "deleted Department");
                LOGGER.log(Level.OFF, "ended delete ");
                return null;
            } else {
                LOGGER.log(Level.SEVERE, "Cannot delete Department");
                LOGGER.log(Level.OFF, "ended delete ");
                return department;
            }
            // </editor-fold>

        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended delete");
        return null;
        
    }
    public static List<com.mobile.dto.Department> findAll(boolean displayOnMenu) {
        LOGGER.log(Level.OFF, "start findAll ");
        Connection connection = null;
        try {
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();

            //generated class to manage the table
            DepartmentDao dao = DaoFactory.createDepartmentDao(connection);
            com.mobile.dto.Department[] departments = dao.findAllDisplayOnMenu(displayOnMenu);

            //Convert array to list and return the list
            List<com.mobile.dto.Department> list = Arrays.asList(departments);

            LOGGER.log(Level.OFF, "end findAll ");
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DepartmentHelper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "end findAll ");
        return null;
    }
    /**
     * find all departments in database
     *
     * @return
     */
    public static List<com.mobile.dto.Department> findAll() {
        LOGGER.log(Level.OFF, "start findAll ");
        Connection connection = null;
        try {
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();

            //generated class to manage the table
            DepartmentDao dao = DaoFactory.createDepartmentDao(connection);
            com.mobile.dto.Department[] departments = dao.findAll();

            //Convert array to list and return the list
            List<com.mobile.dto.Department> list = Arrays.asList(departments);

            LOGGER.log(Level.OFF, "end findAll ");
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DepartmentHelper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "end findAll ");
        return null;
    }

    /**
     * find all users in database
     *
     * @return
     */
    public static List<DepartmentExtender> findAllExtender() {
        List<com.mobile.dto.Department> list = findAll();
        if (list != null) {
            List<DepartmentExtender> extList = new ArrayList();
            for (com.mobile.dto.Department dept : list) {
                extList.add(new DepartmentExtender(dept));
            }
            return extList;
        } else {
            return null;
        }
    }

    /**
     * find all departments in database
     *
     * @param orgID
     * @return
     */
    public static List<com.mobile.dto.Department> findAllByOrganizationId(int orgID) {
        LOGGER.log(Level.OFF, "start findAllByOrganizationId");
        Connection connection = null;
        try {
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();

            //generated class to manage the table
            DepartmentDao dao = DaoFactory.createDepartmentDao(connection);
            com.mobile.dto.Department[] departments = dao.findAllByOrganaizationID(orgID);

            //Convert array to list and return the list
            List<com.mobile.dto.Department> list = Arrays.asList(departments);

            LOGGER.log(Level.OFF, "end findAllByOrganizationId");
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DepartmentHelper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "end findAllByOrganziationId");
        return null;
    }

    /**
     * find all departments in database
     *
     * @param orgID
     * @return
     */
    public static List<DepartmentExtender> findAllExtenderByOrganizationId(int orgID) {
        LOGGER.log(Level.OFF, "start findAllExtenderByOrganizationId");
        List<DepartmentExtender> listExtender = new ArrayList<DepartmentExtender>();
        List<com.mobile.dto.Department> list = findAllByOrganizationId(orgID);
        if (list != null) {
            for (com.mobile.dto.Department item : list) {
                listExtender.add(new DepartmentExtender(item));
            }
        }
        return listExtender;
    }

    public static String findByOrganizationId(int id) {
        Connection connection = null;
        try {
            connection = data.DatabaseManager.getConnection();
            OrganizationDao orgDao = DaoFactory.createOrganizationDao(connection);
            com.mobile.dto.Organization organization = orgDao.findByPrimaryKey(id);
            return organization.getName();
        } catch (Exception ex) {
            Logger.getLogger(DepartmentHelper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public static StreamedContent findIconImageByDepartmentId(int id) {
        Connection connection = null;
        try {
            // get connection for database access
            connection = data.DatabaseManager.getConnection();
            DepartmentDao deptDao = DaoFactory.createDepartmentDao(connection);

            // retrieve the department
            com.mobile.dto.Department department = deptDao.findByPrimaryKey(id);
            // set for returning a streamed content
            byte[] iconBytes = department.getIcon();
            if (iconBytes != null) {
                return new DefaultStreamedContent(new ByteArrayInputStream(iconBytes), department.getIcontype());
            } else {
                return null;
            }
        } catch (Exception ex) {
            Logger.getLogger(DepartmentHelper.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public static StreamedContent iconImageForDepartment(Department department) {
        byte[] iconBytes = department.getIcon();
        if (iconBytes != null) {
            return new DefaultStreamedContent(new ByteArrayInputStream(iconBytes), department.getIcontype());
        } else {
            return null;
        }
    }

    public static List<com.mobile.dto.Department> findAllByCategoryId(Integer orgID) {
        LOGGER.log(Level.OFF, "start findAllByCategoryId");
        Connection connection = null;
        try {
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();

            //generated class to manage the table
            DepartmentDao dao = DaoFactory.createDepartmentDao(connection);
            com.mobile.dto.Department[] departments = dao.findAllByCategory(orgID);

            //Convert array to list and return the list
            List<com.mobile.dto.Department> list = Arrays.asList(departments);

            LOGGER.log(Level.OFF, "end findAllByCategoryId");
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DepartmentHelper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "end findAllByOrganziationId");
        return null;
    }


}
