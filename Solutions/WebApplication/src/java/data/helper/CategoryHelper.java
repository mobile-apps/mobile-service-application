/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.helper;

import com.mobile.dao.CategoryDao;
import com.mobile.dao.DaoFactory;
import com.mobile.dao.OrganizationDao;
import com.mobile.dao.RegisteredUserDao;
import com.mobile.dto.Category;
import com.mobile.dto.Organization;
import com.spoledge.audao.db.dao.DaoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;

/**
 *
 * @author HSC314
 */
public class CategoryHelper {
    // use the classname for the logger, this way you can refactor

    private final static Logger LOGGER = Logger.getLogger(CategoryHelper.class
            .getName());
    
        public static Category update(Category category) {
        LOGGER.log(Level.OFF, "started update ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (category == null) {
                LOGGER.log(Level.OFF, "Null category");
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Update database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            CategoryDao dao = DaoFactory.createCategoryDao(connection);
            //setting the properties

            //try update
            if (dao.update(category.getCategoryId(), category)) {
                LOGGER.log(Level.OFF, "updated database");
                LOGGER.log(Level.OFF, "ended update ");
                return category;
            } else {
                LOGGER.log(Level.SEVERE, "Cannot update Organizaiton");
                LOGGER.log(Level.OFF, "ended update ");
                return null;
            }
            // </editor-fold>

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended update ");
        return null;
    }

    /**
     * add user to database make sure no exception are thrown
     *
     * @param user
     * @return
     */
    public static Category add(String name, String image) {
        LOGGER.log(Level.OFF, "started add ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (name == null) {
                LOGGER.log(Level.OFF, "Null user");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Add to database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            CategoryDao dao = DaoFactory.createCategoryDao(connection);
            //setting the properties
            Category category = dao.findByName(name);
            if (category == null) {
                category = new com.mobile.dto.Category();
                category.setName(name);
                category.setImage(image);
                java.util.Date date = new java.util.Date();
                category.setCreatedDate(date);
                //perform an insert to the database
                Integer primaryKey = null;

                primaryKey = dao.insert(category);

                if (primaryKey > 0) {
                    category.setCategoryId(primaryKey);
                    LOGGER.log(Level.OFF, "added to database key [" + primaryKey + "]");
                } else {
                    LOGGER.log(Level.SEVERE, "Can not add to user");
                }
            }
            // </editor-fold>
            LOGGER.log(Level.OFF, "end add ");
            return category;
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");
        return null;
    }
 public static Category find(String name) {
        LOGGER.log(Level.OFF, "start find");
        // <editor-fold defaultstate="collapsed" desc="Validate parameters">
        if (name == null) {
            LOGGER.log(Level.OFF, "Null email");
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="find in database">
        Connection connection = null;
        try {

            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            CategoryDao dao = DaoFactory.createCategoryDao(connection);
            //setting the properties
            
            Category category = dao.findByName(name);
            return category;

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended find ");
        return null;
        // </editor-fold>  
    }
    /**
     * find user by email
     *
     * @param email
     * @return
     */
    public static Category find(int categoryId) {
        LOGGER.log(Level.OFF, "start find");
        // <editor-fold defaultstate="collapsed" desc="Validate parameters">
        
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="find in database">
        Connection connection = null;
        try {

            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            CategoryDao dao = DaoFactory.createCategoryDao(connection);
            //setting the properties
            
            Category category = dao.findByPrimaryKey(categoryId);
            return category;

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended find ");
        return null;
        // </editor-fold>  
    }

    /**
     * find all users in database
     *
     * @return
     */
    public static List<com.mobile.dto.Category> findAll() {
        List<com.mobile.dto.Category> list = null;
        LOGGER.log(Level.OFF, "start findAll ");
        Connection connection = null;
        try {
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            CategoryDao dao = DaoFactory.createCategoryDao(connection);
            com.mobile.dto.Category[] categories = dao.findAll();
            list = Arrays.asList(categories);
            LOGGER.log(Level.OFF, "end findAll ");
            return list;
        } catch (Exception ex) {
            Logger.getLogger(CategoryHelper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }

        LOGGER.log(Level.OFF, "end findAll ");
        return null;
    }

}
