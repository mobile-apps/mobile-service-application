/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.helper;

import com.mobile.dao.DaoFactory;
import com.mobile.dao.ServiceDao;
import com.mobile.dto.Category;
import com.mobile.dto.Service;
import data.extender.ServiceExtender;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wtccuser
 */
public class ServiceHelper {

    // use the classname for the logger, this way you can refactor
    private final static Logger LOGGER = Logger.getLogger(ServiceHelper.class
            .getName());

    /**
     * add user to database make sure no exception are thrown
     *
     * @param service
     * @return
     */
    public static Service add(Service service) {
        LOGGER.log(Level.OFF, "started add ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (service == null) {
                LOGGER.log(Level.OFF, "Null Service");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Add to database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            ServiceDao dao = DaoFactory.createServiceDao(connection);
            //setting the properties

            //Department does not exist
            java.util.Date date = new java.util.Date();
            service.setCreatedDate(date);
            service.setDepartmentId(service.getDepartmentId());

            //perform an insert to the database
            Integer primaryKey = null;

            primaryKey = dao.insert(service);

            if (primaryKey > 0) {
                service.setServiceId(primaryKey);
                LOGGER.log(Level.OFF, "added to database key [" + primaryKey + "]");

                // end of send email
                return service;
            } else {
                LOGGER.log(Level.SEVERE, "Cannot add to Service");
                return null;
            }

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");
        return null;
    }

    public static Service update(Service service) {

        LOGGER.log(Level.OFF, "started update ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (service == null) {
                LOGGER.log(Level.OFF, "Null Service");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Update database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            ServiceDao dao = DaoFactory.createServiceDao(connection);
            //setting the properties

            //try update
            if (dao.update(service.getServiceId().intValue(), service)) {
                LOGGER.log(Level.OFF, "updated database");
                LOGGER.log(Level.OFF, "ended update ");
                return service;
            } else {
                LOGGER.log(Level.SEVERE, "Cannot update Service");
                LOGGER.log(Level.OFF, "ended update ");
                return null;
            }
            // </editor-fold>

        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended update");
        return null;

    }

    public static Service delete(Service service) {

        LOGGER.log(Level.OFF, "started delete ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (service == null) {
                LOGGER.log(Level.OFF, "Null Service");
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Delete service">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            ServiceDao dao = DaoFactory.createServiceDao(connection);
            //setting the properties

            //try delete
            if (dao.deleteByPrimaryKey(service.getServiceId().intValue())) {

                LOGGER.log(Level.OFF, "deleted service");
                LOGGER.log(Level.OFF, "ended delete ");
                return null;
            } else {
                LOGGER.log(Level.SEVERE, "Cannot delete Service");
                LOGGER.log(Level.OFF, "ended delete ");
                return service;
            }
            // </editor-fold>

        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended delete");
        return null;

    }

    public static List<ServiceExtender> findAllExtender() {
        List<com.mobile.dto.Service> list = findAll();
        if (list != null) {
            List<ServiceExtender> extList = new ArrayList();
            for (com.mobile.dto.Service service : list) {
                //compute image
                if (service.getImage() == null || service.getImage().trim().equals("")) {
                     LOGGER.log(Level.OFF, "no image");
                    Category category = data.helper.CategoryHelper.find(service.getCategoryId().intValue());
                    if (category != null ) {
                        if (category.getImage() != null && !category.getImage().trim().equals("")) {
                            service.setImage(category.getImage());
                             LOGGER.log(Level.OFF, service.getName() + " image assigned [" + category.getImage() + "]");
                        }
                    }
                }
                extList.add(new ServiceExtender(service));
            }
            return extList;
        } else {
            return null;
        }
    }

    /**
     * find all services in database
     *
     * @return
     */
    public static List<com.mobile.dto.Service> findAll() {
        LOGGER.log(Level.OFF, "start findAll ");
        Connection connection = null;
        try {
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();

            //generated class to manage the table
            ServiceDao dao = DaoFactory.createServiceDao(connection);
            com.mobile.dto.Service[] services = dao.findAll();

            //Convert array to list and return the list
            List<com.mobile.dto.Service> list = Arrays.asList(services);

            LOGGER.log(Level.OFF, "end findAll ");
            return list;
        } catch (Exception ex) {
            Logger.getLogger(ServiceHelper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "end findAll ");
        return null;
    }

    /**
     * find all services in database
     *
     * @param deptID
     * @return
     */
    public static List<com.mobile.dto.Service> findAllByDepartmentId(int deptID) {
        LOGGER.log(Level.OFF, "start findAllByDepartmentId");
        Connection connection = null;
        try {
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();

            //generated class to manage the table
            ServiceDao dao = DaoFactory.createServiceDao(connection);
            com.mobile.dto.Service[] services = dao.findAllByDepartmentID(deptID);

            //Convert array to list and return the list
            List<com.mobile.dto.Service> list = Arrays.asList(services);

            LOGGER.log(Level.OFF, "end findAllByDepartmentId");
            return list;
        } catch (Exception ex) {
            Logger.getLogger(ServiceHelper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "end findAllByDepartmentId");
        return null;
    }

    public static String findByServiceId(int id) {
        Connection connection = null;
        try {
            connection = data.DatabaseManager.getConnection();
            ServiceDao serviceDao = DaoFactory.createServiceDao(connection);
            com.mobile.dto.Service service = serviceDao.findByPrimaryKey(id);
            return service.getName();
        } catch (Exception ex) {
            Logger.getLogger(ServiceHelper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
    
    public static Service findByService(int id) {
        Connection connection = null;
        try {
            connection = data.DatabaseManager.getConnection();
            ServiceDao serviceDao = DaoFactory.createServiceDao(connection);
            com.mobile.dto.Service service = serviceDao.findByPrimaryKey(id);
            return service;
        } catch (Exception ex) {
            Logger.getLogger(ServiceHelper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

}
