/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package data.helper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import web.bean.DrivingDirectionBean;

/**
 *
 * @author Chitra
 */
public class GeoLocationHelper {
    
    private final static Logger LOGGER = Logger.getLogger(CategoryHelper.class.getName());
    
    FacesContext ctx = FacesContext.getCurrentInstance();
    String contextGeoCodeValue = 
            ctx.getExternalContext().getInitParameter("google.json.geocode");
    String contextGeoCodeSensorValue = 
            ctx.getExternalContext().getInitParameter("google.json.geocode.sensor");
   

    /**
     * Get the geolocation or geocode of the address
     * 
     * @param address 
     */
    public GeoLocation getGeoLocation(String address)
    {
        LOGGER.log(Level.OFF, "start getGeoLocation");
        
        GeoLocation geoLocation = new GeoLocation();
        
        try
        {
            //Write cookie
            FacesContext.getCurrentInstance()
                    .getExternalContext()
                    .addResponseCookie("address", address, null);
            
            Object[] args = {address.trim()};
            
            MessageFormat fmt = new MessageFormat("?address={0}");
            
            //Replace blank space in address with %20
            String replaceSpace = fmt.format(args);
            replaceSpace = replaceSpace.replace(" ", "%20");
            
            //String decoded = java.net.URLEncoder.encode(fmt.format(args), "UTF-8");
            this.contextGeoCodeValue = this.contextGeoCodeValue + replaceSpace + ";sensor=false";

            LOGGER.log(Level.OFF, "contextAddressValue: " + this.contextGeoCodeValue);
            
            URL url = new URL(contextGeoCodeValue);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            
            if (conn.getResponseCode() != 200)
            {
                throw new RuntimeException("Geocode/GeoLocation call failed: HTTP error code: "
                        + conn.getResponseCode() + "; Message: " + conn.getResponseMessage());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            String output;
            StringBuilder buffer = new StringBuilder();
            System.out.println("Output from Server .... \n");
            
            while ((output = br.readLine()) != null)
            {
                //System.out.println(output);
                buffer.append(output);
            }

            org.primefaces.json.JSONObject jsonObject = new org.primefaces.json.JSONObject(buffer.toString());
            org.primefaces.json.JSONArray results = jsonObject.getJSONArray("results");
            org.primefaces.json.JSONObject arrayAt0Position = results.getJSONObject(0);
            org.primefaces.json.JSONObject geometry = arrayAt0Position.getJSONObject("geometry");
            org.primefaces.json.JSONObject location = geometry.getJSONObject("location");
            
            geoLocation.latitude = Double.parseDouble(location.getString("lat"));
            geoLocation.longitude = Double.parseDouble(location.getString("lng"));
            
            conn.disconnect();
        }
        catch (Exception ex)
        {
            Logger.getLogger(DrivingDirectionBean.class.getName()).log(Level.SEVERE, null, ex);
            
        } //try
        
        LOGGER.log(Level.OFF, "end getGeoLocation");
        
        return geoLocation;
        
    } //getGeoLocation
    
} //class
