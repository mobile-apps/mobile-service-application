/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.helper;

import com.mobile.dao.CategoryDao;
import com.mobile.dao.DaoFactory;
import com.mobile.dao.DepartmentDao;
import com.mobile.dao.ProcessorDao;
import com.mobile.dao.RegisteredUserDao;
import com.mobile.dto.Category;
import com.mobile.dto.Department;
import com.mobile.dto.Processor;
import com.spoledge.audao.db.dao.DaoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HSC314
 */
public class ProcesserHelper {
    // use the classname for the logger, this way you can refactor

    private final static Logger LOGGER = Logger.getLogger(ProcesserHelper.class
            .getName());

    
        public static Processor update(Processor processor) {
        LOGGER.log(Level.OFF, "started update ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (processor == null) {
                LOGGER.log(Level.OFF, "Null processor");
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Update database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            ProcessorDao dao = DaoFactory.createProcessorDao(connection);
            //setting the properties

            //try update
            if (dao.update(processor.getProcessorId(), processor)) {
                LOGGER.log(Level.OFF, "updated database");
                LOGGER.log(Level.OFF, "ended update ");
                return processor;
            } else {
                LOGGER.log(Level.SEVERE, "Cannot update Department");
                LOGGER.log(Level.OFF, "ended update ");
                return null;
            }
            // </editor-fold>

        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended update");
        return null;
        
    }
    /**
     * add user to database make sure no exception are thrown
     *
     * @param user
     * @return
     */
    public static Processor add(Processor newProcessor) {
        LOGGER.log(Level.OFF, "started add ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (newProcessor == null) {
                LOGGER.log(Level.OFF, "Null user");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Add to database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            ProcessorDao dao = DaoFactory.createProcessorDao(connection);
            //setting the properties
            Processor processor = null;
            if (dao.findAll().length > 0) {
                processor = dao.findAll()[0];
            }
            //List[Processor] list = dao.findAll();
            if (processor == null) {
                processor = new com.mobile.dto.Processor();
                processor.setStatus(newProcessor.getStatus());
                processor.setComment(newProcessor.getComment());
                java.util.Date date = new java.util.Date();
                processor.setCreatedDate(date);
                //perform an insert to the database
                Integer primaryKey = null;

                primaryKey = dao.insert(processor);

                if (primaryKey > 0) {
                    processor.setProcessorId(primaryKey);
                    LOGGER.log(Level.OFF, "added to database key [" + primaryKey + "]");
                } else {
                    LOGGER.log(Level.SEVERE, "Can not add to user");
                }
            }
            // </editor-fold>
            LOGGER.log(Level.OFF, "end add ");
            return processor;
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");
        return null;
    }

    /**
     * find user by email
     *
     * @param email
     * @return
     */
    public static Processor find() {
        LOGGER.log(Level.OFF, "start find");
        // <editor-fold defaultstate="collapsed" desc="Validate parameters">
        

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="find in database">
        Connection connection = null;
        try {

            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            ProcessorDao dao = DaoFactory.createProcessorDao(connection);
            //setting the properties
           Processor[]processers = dao.findAll();
           if (processers != null && processers.length > 0) {
               return processers[0];
           } else {
               return null;
           }
         

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended find ");
        return null;
        // </editor-fold>  
    }

    /**
     * find all users in database
     *
     * @return
     */
    

}
