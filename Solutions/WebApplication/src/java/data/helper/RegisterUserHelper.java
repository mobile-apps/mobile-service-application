/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.helper;

import com.mobile.dao.DaoFactory;
import com.mobile.dao.RegisteredUserDao;
import java.util.logging.Logger;
import com.mobile.dto.RegisteredUser;
import com.spoledge.audao.db.dao.DaoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import web.NotificationEnumeration;
import web.RegistrationEnumeration;
import web.service.obj.Subscriber;

/**
 *
 * @author HSC314
 */
public class RegisterUserHelper {
    // use the classname for the logger, this way you can refactor

    private final static Logger LOGGER = Logger.getLogger(RegisterUserHelper.class
            .getName());

    public static RegisteredUser updateActivation(String activationCode) {
        LOGGER.log(Level.OFF, "started add ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (activationCode == null) {
                LOGGER.log(Level.OFF, "Null user");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Update database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
            //setting the properties
            String email = "";

            RegisteredUser[] user = dao.findAllByActivationCode(activationCode);
            //check for member not found
            if (user == null || user.length < 1) {
                return null;
            }
            if (user[0] != null) {
                user[0].setStatus("verified");
                boolean hasBeenUpdated = dao.update(user[0].getRegisteredUserId(), user[0]);
                //perform an insert to the database

                if (hasBeenUpdated) {

                    LOGGER.log(Level.OFF, "updated to database key [" + user[0].getRegisteredUserId() + "]");
                } else {
                    LOGGER.log(Level.SEVERE, "Can not update to user");
                }
            }
            // </editor-fold>
            LOGGER.log(Level.OFF, "end add ");
            return user[0];
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");
        return null;
    }

        public static RegisteredUser update(Subscriber subscriber) {
        LOGGER.log(Level.OFF, "started add ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (subscriber == null) {
                LOGGER.log(Level.OFF, "Null subscriber");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Update database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
            //setting the properties

            RegisteredUser user = dao.findByPrimaryKey(subscriber.getRegisterUserId());
            if (user != null) {
                user.setStatus(RegistrationEnumeration.VERIFIED.getStatus());
                user.setEmail(subscriber.getEmail());
                user.setUsername(subscriber.getUserName());
                user.setNotificationmethod(NotificationEnumeration.GOOGLEPUSH.getMethod());
                boolean hasBeenUpdated = dao.update(subscriber.getRegisterUserId(), user);
                //perform an insert to the database

                if (hasBeenUpdated) {

                    LOGGER.log(Level.OFF, "updated to database key [" + subscriber.getRegisterUserId() + "]");
                } else {
                    LOGGER.log(Level.SEVERE, "Can not update to user");
                }
            }
            // </editor-fold>
            LOGGER.log(Level.OFF, "end add ");
            return user;
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");
        return null;
    }

    public static void delete(com.mobile.dto.RegisteredUser registerUser) {
        LOGGER.log(Level.OFF, "started delet ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (registerUser == null) {
                LOGGER.log(Level.OFF, "Null user");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Delete to database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
            //setting the properties
            String email = "";
            dao.deleteByPrimaryKey(registerUser.getRegisteredUserId());

            // </editor-fold>
            LOGGER.log(Level.OFF, "end delete ");

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");

    }
    /**
     * add new user 
     * @param subscriber
     * @return 
     */
    public static RegisteredUser add(Subscriber subscriber) {
        LOGGER.log(Level.OFF, "started add subscriber");
        RegisteredUser registerUser = new RegisteredUser();
        registerUser.setEmail(subscriber.getEmail());
        if (subscriber.getPhoneType() != null && subscriber.getPhoneType().equals("IOS")) {
                   registerUser.setNotificationmethod(ApplicationEnum.IPHONEPUSH.toString());
 
        } else  {
                   registerUser.setNotificationmethod(ApplicationEnum.GOOGLEPUSH.toString());
 
        }
       // registerUser.setNotificationmethod(ApplicationEnum.GOOGLEPUSH.toString());
        registerUser.setUsername(subscriber.getUserName());
        registerUser.setStatus(ApplicationEnum.VERIFIED.toString());
        registerUser = add(registerUser);
        LOGGER.log(Level.OFF, "started add subscriber");
        return registerUser;
    }

    /**
     *
     * @param registerUser
     * @return
     */
    public static RegisteredUser add(com.mobile.dto.RegisteredUser registerUser) {
        LOGGER.log(Level.OFF, "started add ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (registerUser == null) {
                LOGGER.log(Level.OFF, "Null user");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Add to database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
            //setting the properties
            String email = "";

            if (registerUser.getNotificationmethod().equals("phone")) {
                email = registerUser.getPhone();
            } else {
                email = registerUser.getEmail();
            }

            RegisteredUser[] addUser = dao.findByEmail(email);
            if (addUser == null || addUser.length < 1) {

                java.util.Date date = new java.util.Date();
                registerUser.setCreatedDate(date);
                //perform an insert to the database
                Integer primaryKey = null;

                primaryKey = dao.insert(registerUser);

                if (primaryKey > 0) {
                    registerUser.setRegisteredUserId(primaryKey);
                    LOGGER.log(Level.OFF, "added to database key [" + primaryKey + "]");
                } else {
                    LOGGER.log(Level.SEVERE, "Can not add to user");
                }
            } else {
                registerUser = addUser[0];
            }
            // </editor-fold>
            LOGGER.log(Level.OFF, "end add ");
            return registerUser;
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");
        return null;
    }

    public static RegisteredUser add(String email) {
        LOGGER.log(Level.OFF, "started add ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (email == null) {
                LOGGER.log(Level.OFF, "Null user");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Add to database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
            //setting the properties
            RegisteredUser[] addUser = null;

            addUser = dao.findByEmail(email);

            if (addUser == null) {
                addUser[0] = new com.mobile.dto.RegisteredUser();
                addUser[0].setEmail(email);
                addUser[0].setNotificationmethod(ApplicationEnum.EMAIL.toString());
                java.util.Date date = new java.util.Date();
                addUser[0].setCreatedDate(date);
                //perform an insert to the database
                Integer primaryKey = null;

                primaryKey = dao.insert(addUser[0]);

                if (primaryKey > 0) {
                    addUser[0].setRegisteredUserId(primaryKey);
                    LOGGER.log(Level.OFF, "added to database key [" + primaryKey + "]");
                } else {
                    LOGGER.log(Level.SEVERE, "Can not add to user");
                }
            }
            // </editor-fold>
            LOGGER.log(Level.OFF, "end add ");
            return addUser[0];
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");
        return null;
    }

    public static RegisteredUser update(String id) {
        LOGGER.log(Level.OFF, "started add ");
        Connection connection = null;
        RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
        //setting the properties

        Integer idInt = Integer.parseInt(id);
        RegisteredUser user = dao.findByPrimaryKey(idInt);
        if (user != null) {
            // user.setStatus(RegistrationEnumeration.VERIFIED.getStatus());
            boolean hasBeenUpdated = false;
            try {
                hasBeenUpdated = dao.update(idInt, user);
            } catch (DaoException ex) {
                Logger.getLogger(RegisterUserHelper.class.getName()).log(Level.SEVERE, null, ex);
            }

            //perform an insert to the database
            if (hasBeenUpdated) {
                LOGGER.log(Level.OFF, "updated to database key [" + idInt + "]");
            } else {
                LOGGER.log(Level.SEVERE, "Can not update to user");
            }
        }
        return user;
    }

    public static RegisteredUser update(int id) {
        LOGGER.log(Level.OFF, "started add ");
        RegisteredUser user = null;
        try {
            Connection connection = data.DatabaseManager.getConnection();
            RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
        //setting the properties

            user = dao.findByPrimaryKey(id);
            if (user != null) {
                user.setStatus(RegistrationEnumeration.VERIFIED.getStatus());
                java.util.Date date = new java.util.Date();
                user.setLastLoginDate(date);
                boolean hasBeenUpdated = false;
                try {
                    hasBeenUpdated = dao.update(id, user);
                } catch (DaoException ex) {
                    Logger.getLogger(RegisterUserHelper.class.getName()).log(Level.SEVERE, null, ex);
                }

            //perform an insert to the database
                if (hasBeenUpdated) {
                    LOGGER.log(Level.OFF, "updated to database key [" + id + "]");
                } else {
                    LOGGER.log(Level.SEVERE, "Can not update to user");
                }
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error " + e.getMessage());
            throw new java.lang.IllegalStateException(e.getMessage());
        }
        return user;
    }

    /**
     * add user to database make sure no exception are thrown
     *
     * @param user
     * @return
     */
    public static RegisteredUser add() {
        LOGGER.log(Level.OFF, "started add ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Add to database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
            //setting the properties
            RegisteredUser addUser = null;

            addUser = new com.mobile.dto.RegisteredUser();

            java.util.Date date = new java.util.Date();
            addUser.setStatus(RegistrationEnumeration.PENDING.getStatus());
            addUser.setCreatedDate(date);
            //perform an insert to the database
            Integer primaryKey = null;

            primaryKey = dao.insert(addUser);

            if (primaryKey > 0) {
                addUser.setRegisteredUserId(primaryKey);
                LOGGER.log(Level.OFF, "added to database key [" + primaryKey + "]");
            } else {
                LOGGER.log(Level.SEVERE, "Can not add to user");
            }

            // </editor-fold>
            LOGGER.log(Level.OFF, "end add ");
            return addUser;
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");
        return null;
    }

    public static RegisteredUser find(String email) {
        LOGGER.log(Level.OFF, "start find");
        // <editor-fold defaultstate="collapsed" desc="Validate parameters">
        if (email == null) {
            LOGGER.log(Level.OFF, "Null email");
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="find in database">
        Connection connection = null;
        try {

            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
            //setting the properties
            RegisteredUser[] user = dao.findByEmail(email);
            return user[0];

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended find ");
        return null;
        // </editor-fold>  
    }

    /**
     * find user by email
     *
     * @param email
     * @return
     */
    public static RegisteredUser find(int registeredUserId) {
        LOGGER.log(Level.OFF, "start find");
        // <editor-fold defaultstate="collapsed" desc="Validate parameters">

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="find in database">
        Connection connection = null;
        try {

            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
            //setting the properties
            RegisteredUser user = dao.findByPrimaryKey(registeredUserId);
            return user;

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended find ");
        return null;
        // </editor-fold>  
    }

    /**
     * find all users in database
     *
     * @return
     */
    public static com.mobile.dto.RegisteredUser[] findAll() {
        LOGGER.log(Level.OFF, "start findAll ");
        try {
            //get a connection to the database
            Connection connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
            com.mobile.dto.RegisteredUser[] users = dao.findAll();
            LOGGER.log(Level.OFF, "end findAll ");
            return users;
        } catch (Exception ex) {
            Logger.getLogger(RegisterUserHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.OFF, "end findAll ");
        return null;
    }

}
