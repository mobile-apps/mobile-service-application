/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.helper;

import data.RegisterUserCategoryAssignment;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author administrator
 */
public class RegisterUserCategoryHelper {

    public static List<data.RegisterUserCategoryAssignment> getList() {
        List<RegisterUserCategoryAssignment> list = new ArrayList<RegisterUserCategoryAssignment>();
        List<com.mobile.dto.Category> categoryList = CategoryHelper.findAll();
        for (com.mobile.dto.Category category : categoryList) {
            RegisterUserCategoryAssignment registerUserCategoryAssignment = new RegisterUserCategoryAssignment();
            registerUserCategoryAssignment.setName(category.getName());
            registerUserCategoryAssignment.setCategoryId(category.getCategoryId());
            list.add(registerUserCategoryAssignment);
        }
        return list;
    }

}
