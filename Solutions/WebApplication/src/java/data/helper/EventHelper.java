/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.helper;

import com.mobile.dao.DaoFactory;
import com.mobile.dao.ServiceDao;
//import com.mobile.dao.DepartmentDao;
//import com.mobile.dao.CategoryDao;
//import com.mobile.dto.Department;
import com.mobile.dto.Service;
import data.extender.ServiceExtender;
//import static data.helper.OrganizationHelper.findAll;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import web.Event;
import web.obj.EventDatesObj;

/**
 *
 * @author HopeBenziger
 */
public class EventHelper {

    public enum EventType {

        All, General, Special
    }

    // use the classname for the logger, this way you can refactor
    private final static Logger LOGGER = Logger.getLogger(EventHelper.class
            .getName());

    public static EventDatesObj computeCalendarDates(int month) {
        //first get current month
        EventDatesObj eventDatesObj = new EventDatesObj();
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        int currentMonth = calendar.get(Calendar.MONTH) + 1;

        System.out.println("month passed: " + month);
        if (month == currentMonth) {
            //int firstDate = calendar.getActualMinimum(Calendar.DATE);
            //calendar.set(Calendar.DATE, firstDate);
            eventDatesObj.setStartDate(new Date());
            System.out.println("first Date: " + eventDatesObj.getStartDate());
            int lastDate = calendar.getActualMaximum(Calendar.DATE);
            calendar.set(Calendar.DATE, lastDate);
            eventDatesObj.setEndDate(calendar.getTime());
            System.out.println("last Date: " + eventDatesObj.getEndDate());

        } else {
            if (month > currentMonth) {
                calendar.set(currentYear, month - 1, 1);
                eventDatesObj.setStartDate(calendar.getTime());

                int lastDate = calendar.getActualMaximum(Calendar.DATE);
                calendar.set(Calendar.DATE, lastDate);
                eventDatesObj.setEndDate(calendar.getTime());

            } else {
                calendar.set(currentYear + 1, month - 1, 1);
                eventDatesObj.setStartDate(calendar.getTime());

                int lastDate = calendar.getActualMaximum(Calendar.DATE);
                calendar.set(Calendar.DATE, lastDate);
                eventDatesObj.setEndDate(calendar.getTime());

            }
        }
        return eventDatesObj;

    }

    /*
     Find 31 days of events  that starts from today
     */
    public static List<Event> findAll(boolean useDefault) {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, 31);
        Date endDate = cal.getTime();
        Date todaydate = new Date();
        List<Event> eventList = EventHelper.findAll(todaydate, endDate);
        return eventList;
    }

    public static List<Event> findAllYear() {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, 365);
        Date endDate = cal.getTime();
        Date todaydate = new Date();
        List<Event> eventList = EventHelper.findAll(todaydate, endDate);
        return eventList;
    }

    public static Event findFirstSpecialEvent() {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, 365);
        Date endDate = cal.getTime();
        Date todaydate = new Date();
        List<Event> eventList = EventHelper.findAll(todaydate, endDate, true);
        Event firstEvent = null;
        if (eventList != null && eventList.size() > 0) {
            firstEvent = eventList.get(0);
        }
        return firstEvent;
    }

    /**
     * find this week events
     *
     * @return
     */
    public static List<Event> findThisWeek() {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, 2);
        Date endDate = cal.getTime();
        Date todaydate = new Date();
        List<Event> eventList = EventHelper.findAll(todaydate, endDate, true);
        return eventList;
    }

    public static List<Event> findAll(java.util.Date startDate, java.util.Date endDate, int numberOfEvents) {

        List<Event> eventList = findAll();

        List<Event> filterList = new ArrayList();
        int loopCounter = 0;
        for (Event event : eventList) {
            java.util.Date eventEndDate = event.getEndDate();
            java.util.Date eventStartDate = event.getDate();
            if (loopCounter >= numberOfEvents) {
                break;
            }
            if (eventStartDate.after(startDate) && eventStartDate.before(endDate)) {
                filterList.add(event);
                loopCounter = loopCounter + 1;

            }

        }
        Collections.sort(filterList);
        return filterList;
    }

    /*
     find current events
     */
    public static List<Event> findAll(java.util.Date startDate, java.util.Date endDate) {

        List<Event> eventList = findAll();

        List<Event> filterList = new ArrayList();

        long HOUR = 3600 * 1000; // one hourin milli-seconds.
        long MIN = 60000;

        for (Event event : eventList) {

            Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
            calendar.setTime(event.getDate());   // assigns calendar to given date 
            int startHourOfDay = calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format

            int startMinuteDay = calendar.get(Calendar.MINUTE);
            calendar.setTime(event.getEndDate());
            int endHourOfDay = calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format

            int endMinuteDay = calendar.get(Calendar.MINUTE);

            Date newDate = null;
            if (endHourOfDay >= startHourOfDay) {
                int addHours = endHourOfDay - startHourOfDay;
                int addMin = 0;
                if (endMinuteDay >= startMinuteDay) {

                    addMin = endMinuteDay - startMinuteDay;

                } else {
                     addMin = startMinuteDay - endMinuteDay;

                }
                newDate = new Date(event.getDate().getTime() + (HOUR * addHours) + (MIN * addMin));

            }

            java.util.Date eventEndDate = event.getEndDate();
            java.util.Date eventStartDate = event.getDate();

            if (eventStartDate.after(startDate) && eventStartDate.before(endDate)) {
                if (newDate != null) {
                    event.setEndDate(newDate);
                }
                filterList.add(event);
            }

        }
        Collections.sort(filterList);
        return filterList;
    }

    public static List<Event> findAll(java.util.Date startDate, java.util.Date endDate, boolean special) {

        List<Event> eventList = findAll();

        List<Event> filterList = new ArrayList();

        for (Event event : eventList) {
            java.util.Date eventEndDate = event.getEndDate();
            java.util.Date eventStartDate = event.getDate();

            if (eventStartDate.after(startDate) && eventStartDate.before(endDate)) {

                if (event.getServiceExtender().getIsSpecial() && special) {
                    //special event
                    filterList.add(event);
                } else {
                    if (!special && !event.getServiceExtender().getIsSpecial()) {
                        //general event
                        filterList.add(event);
                    }
                }
            }

        }
        Collections.sort(filterList);
        return filterList;
    }

    public static List<Event> findAll(java.util.Date startDate, java.util.Date endDate, EventType eventType) {

        List<Event> eventList = findAll();

        List<Event> filterList = new ArrayList();

        for (Event event : eventList) {
            java.util.Date eventEndDate = event.getEndDate();
            java.util.Date eventStartDate = event.getDate();

            if (eventStartDate.after(startDate) && eventStartDate.before(endDate)) {
                switch (eventType) {
                    case General:

                        if (!event.getServiceExtender().getIsSpecial()) {
                            //general event
                            filterList.add(event);
                        }

                        break;
                    case All:

                        filterList.add(event);

                        break;
                    case Special:
                        if (event.getServiceExtender().getIsSpecial()) {
                            //special event
                            filterList.add(event);
                        }
                        break;

                }

            }

        }
        Collections.sort(filterList);
        return filterList;
    }

    public static List<Event> findAll() {

        LOGGER.log(Level.OFF, "start findAll ");

        List<Event> eventList = new ArrayList();

        try {

            //get list of all services
            List<ServiceExtender> serviceExtenderList = ServiceHelper.findAllExtender();

            for (ServiceExtender serviceExtender : serviceExtenderList) {

                //check start and end dates
                java.util.Date todaysDate = new java.util.Date();

//                if(service.getStartDate().before(todaysDate) && service.getEndDate().after(todaysDate))
//                {
                //get all events for service
                Calendar start = Calendar.getInstance(TimeZone.getTimeZone("EST"));
                Date startDate = serviceExtender.getStartDate();
                start.setTime(startDate);

                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.US);
                Locale locale = Locale.getDefault();
                String dayOfWeekDispaly = start.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, locale);
                // dayOfWeekDispaly =dayOfWeekDispaly.substring(0, 3);
                String month = start.getDisplayName(Calendar.MONTH, Calendar.LONG, locale);
                int year = start.get(Calendar.YEAR);
                String time = start.getDisplayName(Calendar.HOUR, Calendar.LONG, locale);
                String formatted = format1.format(start.getTime());
                Calendar end = Calendar.getInstance(TimeZone.getTimeZone("EST"));
                Date endDate = serviceExtender.getEndDate();
                end.setTime(endDate);

                //get service days of week
                String daysOfWeek = serviceExtender.getDaysOfWeek();
                ArrayList<Integer> intDaysOfWeekList = serviceExtender.getIntDaysOfWeek();
                //loops through days between start and end date
                while (!start.after(end)) {
                    Date targetDay = start.getTime();

                    int dayOfWeek = start.get(Calendar.DAY_OF_WEEK);

                    if (intDaysOfWeekList.contains(dayOfWeek)) {
                        Event event = new Event();
                        event.setDeparment(serviceExtender.getDepartment());
                        event.setName(serviceExtender.getName());
                        event.setPhone((serviceExtender.getPhone()));
                        event.setGeneralInformation(serviceExtender.getGeneralInformation());
                        event.setDate(targetDay);
                        event.setLatitude(serviceExtender.getLatitude());
                        event.setLongitude(serviceExtender.getLongitude());
                        event.setLocation(serviceExtender.getLocation());
                        event.setServiceExtender(serviceExtender);
                        event.setEndDate(serviceExtender.getEndDate());
                        event.setDayOfWeek(dayOfWeekDispaly);
                        event.setMonth(month);
                        event.setDay(dayOfWeek);
                        event.setYear(year);
                        event.setTime(formatted);
                        event.setImage(serviceExtender.getImage());
                        eventList.add(event);
                    }

                    start.add(Calendar.DATE, 1);
                }

//                }
            }
        } catch (Exception ex) {
            Logger.getLogger(EventHelper.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.OFF, "end findAll ");
        Collections.sort(eventList);
        return eventList;

    }

}
