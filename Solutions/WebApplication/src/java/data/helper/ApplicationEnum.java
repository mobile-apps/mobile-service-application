/*
 String inprogress = "inprogress";
 String completed = "completed";
 String error = "error";
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.helper;

/**
 *
 * @author wtccuser
 */
public enum ApplicationEnum {

    PHONE {
                public String toString() {
                    return "phone";
                }
            },
    EMAIL {
                public String toString() {
                    return "email";
                }
            },
    ENDDATELONG {
                public String toString() {
                    return "endDateLong";
                }
            },
    ERROR {
                public String toString() {
                    return "error";
                }
            },
    COMPLETED {
                public String toString() {
                    return "completed";
                }
            },
    DEPARTMENTID {
                public String toString() {
                    return "departmentId";
                }
            },
    GENERALINFORMATION {
                public String toString() {
                    return "generalInformation";
                }
            },
    GOOGLEPUSH {
                public String toString() {
                    return "googlePush";
                }
            },
    IPHONEPUSH {
                public String toString() {
                    return "iphonePush";
                }
            },
    ICON {
                public String toString() {
                    return "icon";
                }
            },
    INPROGRESS {
                public String toString() {
                    return "inprogress";
                }
            },
    NAME {
                public String toString() {
                    return "name";
                }
            },
    NEWPROGRESS {
                public String toString() {
                    return "new progress";
                }
            },
    NOEVENTS {
                public String toString() {
                    return "no events";
                }
            },
    STARTDATELONG {
                public String toString() {
                    return "startDateLong";
                }
            },
    VERIFIED {
                public String toString() {
                    return "verified";
                }
            }
}
