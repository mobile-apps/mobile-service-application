/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.helper;

import com.mobile.dao.CategoryDao;
import com.mobile.dao.DaoFactory;
import com.mobile.dao.CrashDao;
import com.mobile.dto.Crash;
import com.spoledge.audao.db.dao.DaoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;

/**
 *
 * @author HSC314
 */
public class CrashHelper {
    // use the classname for the logger, this way you can refactor

    private final static Logger LOGGER = Logger.getLogger(CrashHelper.class
            .getName());

    /**
     * add user to database make sure no exception are thrown
     *
     * @param user
     * @return
     */
    public static Crash add(Crash crash) {
        LOGGER.log(Level.OFF, "started add ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (crash == null) {
                LOGGER.log(Level.OFF, "Null user");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Add to database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            CrashDao dao = DaoFactory.createCrashDao(connection);
            //setting the properties

            java.util.Date date = new java.util.Date();
            crash.setCreatedDate(date);
            //perform an insert to the database
            Integer primaryKey = null;

            primaryKey = dao.insert(crash);

            if (primaryKey > 0) {
                crash.setCrashId(primaryKey);
                LOGGER.log(Level.OFF, "added to database key [" + primaryKey + "]");
            } else {
                LOGGER.log(Level.SEVERE, "Can not add to user");
            }

            // </editor-fold>
            LOGGER.log(Level.OFF, "end add ");
            return crash;
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");
        return null;
    }

    /**
     * find all users in database
     *
     * @return
     */
    public static List<com.mobile.dto.Crash> findAll() {
        List<com.mobile.dto.Crash> list = null;
        LOGGER.log(Level.OFF, "start findAll ");
        try {
            //get a connection to the database
            Connection connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            CrashDao dao = DaoFactory.createCrashDao(connection);
            com.mobile.dto.Crash[] crashes = dao.findAll();
            list = Arrays.asList(crashes);
            LOGGER.log(Level.OFF, "end findAll ");
            return list;
        } catch (Exception ex) {
            Logger.getLogger(CrashHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.OFF, "end findAll ");
        return null;
    }

    public static int deleteAll() {
        LOGGER.log(Level.OFF, "started delete ");
        Connection connection = null;
        int deleteCount = 0;
        try {

            // <editor-fold defaultstate="collapsed" desc="Delete all crash reports">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            CrashDao dao = DaoFactory.createCrashDao(connection);
            //setting the properties

            //try update
            deleteCount = dao.deleteAll();
            LOGGER.log(Level.OFF, "deleted all creash reports");
            LOGGER.log(Level.OFF, "ended delete ");
            // </editor-fold>

        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended delete");
        return deleteCount;
    }

}
