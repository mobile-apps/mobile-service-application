/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.helper;

import com.mobile.dao.DaoFactory;
import com.mobile.dao.OrganizationDao;
import com.mobile.dto.Organization;
import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import data.extender.OrganizationExtender;
import java.sql.PreparedStatement;

/**
 *
 * @author wtccuser
 */
public class OrganizationHelper {

    // use the classname for the logger, this way you can refactor
    private final static Logger LOGGER = Logger.getLogger(OrganizationHelper.class
            .getName());

    /**
     * add user to database make sure no exception are thrown
     *
     * @param user
     * @return
     */
    public static Organization add(Organization organization) {
        LOGGER.log(Level.OFF, "started add ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (organization == null) {
                LOGGER.log(Level.OFF, "Null Organization");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Add to database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            OrganizationDao dao = DaoFactory.createOrganizationDao(connection);
            //setting the properties

            Organization existingOrganization = dao.findByName(organization.getName());

            //Organization does not exist
            if (existingOrganization == null) {
                java.util.Date date = new java.util.Date();
                organization.setCreatedDate(date);

                //perform an insert to the database
                Integer primaryKey = null;

                primaryKey = dao.insert(organization);

                if (primaryKey > 0) {
                    organization.setOrganizationId(primaryKey);
                    LOGGER.log(Level.OFF, "added to database key [" + primaryKey + "]");
                    return organization;
                } else {
                    LOGGER.log(Level.SEVERE, "Cannot add to Organizaiton");
                    return null;
                }
            }

            //Organization already exists
            // </editor-fold>
            LOGGER.log(Level.OFF, "end add ");
            return existingOrganization;
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");
        return null;
    }

    /**
     * find all users in database
     *
     * @return
     */
    public static List<OrganizationExtender> findAllExtender() {
        List<com.mobile.dto.Organization> list = findAll();
        if (list != null) {
            List<OrganizationExtender> extList = new ArrayList();
            for (com.mobile.dto.Organization org : list) {
                extList.add(new OrganizationExtender(org));
            }
            return extList;
        } else {
            return null;
        }
    }

    /**
     * find all
     *
     * @return
     */
    public static List<com.mobile.dto.Organization> findAll() {
        List<com.mobile.dto.Organization> list = null;
        LOGGER.log(Level.OFF, "start findAll ");
        Connection connection = null;
        try {
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();

            //generated class to manage the table
            OrganizationDao dao = DaoFactory.createOrganizationDao(connection);
            com.mobile.dto.Organization[] organizations = dao.findAll();

            //Convert array to list and return the list
            list = Arrays.asList(organizations);

            LOGGER.log(Level.OFF, "end findAll ");
            return list;
        } catch (Exception ex) {
            Logger.getLogger(OrganizationHelper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "end findAll ");
        return null;
    }

    public static StreamedContent findIconImageByOrganizationId(int id) {
        Connection connection = null;
        try {
            connection = data.DatabaseManager.getConnection();
            OrganizationDao orgDao = DaoFactory.createOrganizationDao(connection);
            com.mobile.dto.Organization organization = orgDao.findByPrimaryKey(id);
            byte[] iconBytes = organization.getIcon();
            if (iconBytes != null) {
                return new DefaultStreamedContent(new ByteArrayInputStream(iconBytes), organization.getIcontype());
            } else {
                return null;
            }
        } catch (Exception ex) {
            Logger.getLogger(OrganizationHelper.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public static StreamedContent iconImageForOrganization(Organization organization) {
        byte[] iconBytes = organization.getIcon();
        if (iconBytes != null) {
            return new DefaultStreamedContent(new ByteArrayInputStream(iconBytes), organization.getIcontype());
        } else {
            return null;
        }
    }

    public static Organization update(Organization organization) {
        LOGGER.log(Level.OFF, "started update ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (organization == null) {
                LOGGER.log(Level.OFF, "Null Organization");
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Update database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            OrganizationDao dao = DaoFactory.createOrganizationDao(connection);
            //setting the properties

            //try update
            if (dao.update(organization.getOrganizationId(), organization)) {
                LOGGER.log(Level.OFF, "updated database");
                LOGGER.log(Level.OFF, "ended update ");
                return organization;
            } else {
                LOGGER.log(Level.SEVERE, "Cannot update Organizaiton");
                LOGGER.log(Level.OFF, "ended update ");
                return null;
            }
            // </editor-fold>

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended update ");
        return null;
    }

    public static Organization delete(Organization organization) {
        LOGGER.log(Level.OFF, "started delete ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (organization == null) {
                LOGGER.log(Level.OFF, "Null Organization");
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Delete organization">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            OrganizationDao dao = DaoFactory.createOrganizationDao(connection);
            //setting the properties

            //try delete
            if (dao.deleteByPrimaryKey(organization.getOrganizationId().intValue())) {
                LOGGER.log(Level.OFF, "deleted organization");
                LOGGER.log(Level.OFF, "ended delete ");
                return null;
            } else {
                LOGGER.log(Level.SEVERE, "Cannot delete Organizaiton");
                LOGGER.log(Level.OFF, "ended delete ");
                return organization;
            }
            // </editor-fold>

        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended delete");
        return null;

    }

}
