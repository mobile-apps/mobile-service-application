/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.extender;

import java.io.ByteArrayInputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.apache.commons.beanutils.BeanUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author wtccuser
 */
public class OrganizationExtender extends com.mobile.dto.Organization {

    private List<DepartmentExtender> departments;

    public OrganizationExtender(com.mobile.dto.Organization org) {
        try {
            BeanUtils.copyProperties(this, org);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(OrganizationExtender.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(OrganizationExtender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<DepartmentExtender> getDepartments() {
        if (departments == null){
            departments = data.helper.DepartmentHelper.findAllExtenderByOrganizationId(this.getOrganizationId());
        }
        return departments;
    }

    public void setDepartments(List<DepartmentExtender> departments) {
        this.departments = departments;
    }

    public StreamedContent getIconImage() {
        FacesContext fc = FacesContext.getCurrentInstance();
        if (fc.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {
            return new DefaultStreamedContent(new ByteArrayInputStream(this.getIcon()), this.getIcontype());
        }
    }

}
