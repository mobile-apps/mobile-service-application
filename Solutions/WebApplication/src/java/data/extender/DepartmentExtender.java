package data.extender;

import com.mobile.dto.Service;
import data.helper.DepartmentHelper;
import java.io.ByteArrayInputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.apache.commons.beanutils.BeanUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author wtccuser
 */
public class DepartmentExtender extends com.mobile.dto.Department {

    private String organizationName;
    private List<com.mobile.dto.Service> services;

    public DepartmentExtender(com.mobile.dto.Department dept) {
        try {
            BeanUtils.copyProperties(this, dept);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(DepartmentExtender.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(DepartmentExtender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Service> getServices() {
        if (services == null) {
            services = data.helper.ServiceHelper.findAllByDepartmentId(this.getDepartmentId());
        }

        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public String getOrganizationName() {
        if (organizationName == null) {
            organizationName = DepartmentHelper.findByOrganizationId(this.getOrganizationId());
        }
        return organizationName;
    }

    public StreamedContent getIconImage() {
        FacesContext fc = FacesContext.getCurrentInstance();
        if (fc.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {
            return new DefaultStreamedContent(new ByteArrayInputStream(this.getIcon()), this.getIcontype());
        }
    }

}
