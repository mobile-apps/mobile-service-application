package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseManager {

    // jbossewsalex
    private String databaseURL = "jdbc:mysql://localhost/classroom";// address
    // to the
    // database
    private static String driverName = "com.mysql.jdbc.Driver";

    public static Connection getConnection() throws Exception {
        java.lang.Class.forName(driverName);
        StringBuilder builder = new StringBuilder();
        Connection connection = null;
        if (DatabaseBundle.useLocalDatabase()) {
            builder.append(DatabaseBundle.getURL());
            connection = DriverManager.getConnection(builder.toString(),
                    DatabaseBundle.getUSER(), DatabaseBundle.getPASSWORD());
        } else {
            builder.append("jdbc:mysql://");
            builder.append(DatabaseSetting.MYSQL_DATABASE_HOST + ":"
                    + DatabaseSetting.MYSQL_DATABASE_PORT + "/");
            builder.append("jbossewsalex");
            connection = DriverManager.getConnection(builder.toString(),
                    DatabaseSetting.MYSQL_USERNAME, DatabaseSetting.MYSQL_PASSWORD);
        }

        return connection;
    }

    public static void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                if (!connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
