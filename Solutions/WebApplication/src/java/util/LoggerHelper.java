/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.LogManager;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author HSC314
 */
public class LoggerHelper {

    static private FileHandler fileTxt;
    static private SimpleFormatter formatterTxt;

    static private FileHandler fileHTML;
    static private Formatter formatterHTML;

    static public void setup() throws IOException {
        InputStream is = LoggerHelper.class.getResourceAsStream("logging.properties");
        LogManager.getLogManager().readConfiguration(is);
        /*
         // get the global logger to configure it
         Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

         logger.setLevel(Level.INFO);
         fileTxt = new FileHandler("Logging.txt");
         fileHTML = new FileHandler("Logging.html");

         // create a TXT formatter
         formatterTxt = new SimpleFormatter();
         fileTxt.setFormatter(formatterTxt);
         logger.addHandler(fileTxt);

         // create an HTML formatter
         formatterHTML = new MyHtmlFormatter();
         fileHTML.setFormatter(formatterHTML);
         logger.addHandler(fileHTML);
         */
    }
}
