/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import org.primefaces.model.UploadedFile;
import org.primefaces.util.Base64;

/**
 *
 * @author deweiss
 */
public class IconBase64 {

    public static String encodeToString(UploadedFile icon) {
        try {
            return "data:" + icon.getContentType() + ";base64,"
                    + Base64.encodeToString(IOUtils.toByteArray(icon.getInputstream()), true);
        } catch (IOException ex) {
            Logger.getLogger(IconBase64.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
