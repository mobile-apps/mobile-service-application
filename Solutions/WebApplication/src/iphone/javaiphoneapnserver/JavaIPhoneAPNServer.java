/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaiphoneapnserver;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wtccuser
 */
public class JavaIPhoneAPNServer {

    private static final Logger LOGGER = Logger.getLogger(JavaIPhoneAPNServer.class.getName());

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        ApnsService service
                = APNS.newService()
                .withCert("/Users/wtccuser/Documents/tonight/JavaIPhoneAPNServer/certs/CertificateChurch.p12", "batman")
                .withSandboxDestination()
                .build();
        
        String payload = APNS.newPayload().alertBody("Can't be simpler than this!").sound("default").build();
        //String token = "a69b437eda4e1c66dbf626e096e8d5d39528b4cd466faa0c6feef3cb47181f13";
        //String token = "73e7dfd35f099fbce9427bbef89d4a8aee5bf08889fcc862e0123565355e044d";
        String token = "f3ba614a2af48000d98f4f218779f258985d2d97d63baef395bc6c3414766bde";
        String token2 = "6b1f1fb69f0e7dfdc653caf369cdf4c75d1dac94c75de03c793fa409d7302528";

//token = "erer";
        String token1 = "0959583245597666cd5fd84f06d6288364f390dfe5fb915432655d6a78130d54";
        String badge = APNS.newPayload().badge(8).build();
        String sound = APNS.newPayload().sound("Chord.mp3").build();
        // service.push(token2, sound);

        service.push(token2, payload);
       // service.push(token2, payload);
        feedBack(service);
        // TODO code application logic here
    }

    public static void feedBack(ApnsService service) {
        Map<String, Date> inactiveDevices = service.getInactiveDevices();
         LOGGER.info("starting feedBack");
        for (String deviceToken : inactiveDevices.keySet()) {
            Date inactiveAsOf = inactiveDevices.get(deviceToken);
            String message = inactiveAsOf.toString() + " [" + deviceToken + "]";
LOGGER.info(" got one " + message);
        }
         LOGGER.info("ending feedBack");
        
    }

}
