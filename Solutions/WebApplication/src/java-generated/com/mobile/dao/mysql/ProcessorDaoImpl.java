/*
 * This file was generated - do not edit it directly !!
 * Generated by AuDAO tool, a product of Spolecne s.r.o.
 * For more information please visit http://www.spoledge.com
 */
package com.mobile.dao.mysql;


import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import java.util.ArrayList;

import com.spoledge.audao.db.dao.AbstractDaoImpl;
import com.spoledge.audao.db.dao.DBException;
import com.spoledge.audao.db.dao.DaoException;


import com.mobile.dao.ProcessorDao;
import com.mobile.dto.Processor;


/**
 * This is the DAO imlementation class.
 *
 * @author generated
 */
public class ProcessorDaoImpl extends AbstractDaoImpl<Processor> implements ProcessorDao {

    private static final String TABLE_NAME = "processor";

    protected static final String SELECT_COLUMNS = "processor_id, comment, status, created_date";

    protected static final String PK_CONDITION = "processor_id=?";

    private static final String SQL_INSERT = "INSERT INTO processor (comment,status,created_date) VALUES (?,?,?)";

    public ProcessorDaoImpl( Connection conn ) {
        super( conn );
    }

    /**
     * Finds a record identified by its primary key.
     * @return the record found or null
     */
    public Processor findByPrimaryKey( int processorId ) {
        return findOne( PK_CONDITION, processorId);
    }

    /**
     * Finds records ordered by status.
     */
    public Processor[] findAll( ) {
        return findManyArray( "1=1 ORDER BY status", 0, -1);
    }

    /**
     * Inserts a new record.
     * @return the generated primary key - processorId
     */
    public int insert( Processor dto ) throws DaoException {
        PreparedStatement stmt = null;
        ResultSet rs = null;

        debugSql( SQL_INSERT, dto );

        try {
            stmt = conn.prepareStatement( SQL_INSERT, PreparedStatement.RETURN_GENERATED_KEYS );

            if ( dto.getComment() == null ) {
                throw new DaoException("Value of column 'comment' cannot be null");
            }
            checkMaxLength( "comment", dto.getComment(), 250 );
            stmt.setString( 1, dto.getComment() );

            if ( dto.getStatus() == null ) {
                throw new DaoException("Value of column 'status' cannot be null");
            }
            checkMaxLength( "status", dto.getStatus(), 250 );
            stmt.setString( 2, dto.getStatus() );

            if ( dto.getCreatedDate() == null ) {
                dto.setCreatedDate( new Timestamp( System.currentTimeMillis()));
            }
            stmt.setTimestamp( 3, dto.getCreatedDate() );

            int n = stmt.executeUpdate();

            rs = stmt.getGeneratedKeys();
            rs.next();

            dto.setProcessorId( rs.getInt( 1 ));

            return dto.getProcessorId();
        }
        catch (SQLException e) {
            errorSql( e, SQL_INSERT, dto );
            handleException( e );
            throw new DBException( e );
        }
        finally {
            if (rs != null) try { rs.close(); } catch (SQLException e) {}
            if (stmt != null) try { stmt.close(); } catch (SQLException e) {}
        }
    }

    /**
     * Updates one record found by primary key.
     * @return true iff the record was really updated (=found and any change was really saved)
     */
    public boolean update( int processorId, Processor dto ) throws DaoException {
        StringBuffer sb = new StringBuffer();
        ArrayList<Object> params = new ArrayList<Object>();

        if ( dto.getComment() != null ) {
            checkMaxLength( "comment", dto.getComment(), 250 );
            sb.append( "comment=?" );
            params.add( dto.getComment());
        }

        if ( dto.getStatus() != null ) {
            if (sb.length() > 0) {
                sb.append( ", " );
            }

            checkMaxLength( "status", dto.getStatus(), 250 );
            sb.append( "status=?" );
            params.add( dto.getStatus());
        }

        if (sb.length() == 0) {
            return false;
        }

        params.add( processorId );

        Object[] oparams = new Object[ params.size() ];

        return updateOne( sb.toString(), PK_CONDITION, params.toArray( oparams ));
    }

    /**
     * Returns the table name.
     */
    public String getTableName() {
        return TABLE_NAME;
    }

    protected String getSelectColumns() {
        return SELECT_COLUMNS;
    }

    protected Processor fetch( ResultSet rs ) throws SQLException {
        Processor dto = new Processor();
        dto.setProcessorId( rs.getInt( 1 ));
        dto.setComment( rs.getString( 2 ));
        dto.setStatus( rs.getString( 3 ));
        dto.setCreatedDate( rs.getTimestamp( 4 ));

        return dto;
    }

    protected Processor[] toArray(ArrayList<Processor> list ) {
        Processor[] ret = new Processor[ list.size() ];
        return list.toArray( ret );
    }

}
