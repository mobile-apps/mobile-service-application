/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import junit.framework.Assert;
import org.junit.Test;
import java.io.OutputStreamWriter;
import java.net.URLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.json.JsonObjectBuilder;
import javax.json.Json;

public class RestfulJUnitTest {

    @Test
    public void createCrash() throws Exception {

        System.out.println("\nStarting test crash");

        String url = "http://localhost:8080/WebApplication/rest/mobile/createCrash";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        JsonObjectBuilder jsonObject = Json.createObjectBuilder();
        String appVersionCode = "appversionCode";//
        String reportId = "reportid";//
        String appVersionName = "appVersionName";//
        String phoneModel = "phoneModel";//
        String brand = "brand";//
        String deviceId = "deviceId";
        String packageName = "packageName";
        String logCat = "logCat";//
        String androidVersion = "androidVersion";//
        String stackTrace = "stackTrace";//

        jsonObject.add("appVersionCode", appVersionCode);
        jsonObject.add("reportId", reportId);
        jsonObject.add("appVersionName", appVersionName);
        jsonObject.add("phoneModel", phoneModel);
        jsonObject.add("brand", brand);
        jsonObject.add("deviceId", deviceId);
        jsonObject.add("packageName", packageName);
        jsonObject.add("logCat", logCat);
        jsonObject.add("androidVersion", androidVersion);
        jsonObject.add("stackTrace", stackTrace);
        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        String jsonText = jsonObject.build().toString();

        System.out.println("json sent " + jsonText);

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(jsonText);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);

        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());

    }

    @Test
    public void updateSubscriber() throws Exception {
        System.out.println("\nStarting test updateSubscriber");
        String url = "http://localhost:8084/WebApplication/rest/mobile/updateSubscriber";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        JsonObjectBuilder jsonObject = Json.createObjectBuilder();
        String id = "1";
        int registerUserId = 1;
        String email = "test@aol.com";
        String userName = "test user name";
        jsonObject.add("email", email);
        jsonObject.add("userName", userName);
        jsonObject.add("id", id);
        jsonObject.add("registerUserId", registerUserId);
        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        String jsonText = jsonObject.build().toString();
        System.out.println(jsonText);

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(jsonText);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);

        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());

    }

    @Test
    public void createSubscriber() throws Exception {
        System.out.println("\nStarting test createSubscriber");
        String url = "http://localhost:8084/WebApplication/rest/mobile/createSubscriber";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        JsonObjectBuilder jsonObject = Json.createObjectBuilder();
        String id = "1";
        String email = "test@aol.com";
        String userName = "test user name";
        jsonObject.add("email", email);
        jsonObject.add("userName", userName);
        jsonObject.add("id", id);
        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        String jsonText = jsonObject.build().toString();
        System.out.println(jsonText);

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(jsonText);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);

        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());

    }

    @Test
    public void getAllEventsAndCategories() throws Exception {
        System.out.println("\nStarting test getAllEventsAndCategories");
        URL url = new URL("http://localhost:8084/WebApplication/rest/mobile/getAllCategoriesAndEvents");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setDoOutput(true);
        connection.setRequestMethod("GET");
        //connection.setRequestProperty("Content-Type", "application/json");
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);

        int responseCode = connection.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());

        Assert.assertTrue(true);

    }

}
