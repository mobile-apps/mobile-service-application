/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ResourceBundle;
import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author wtccuser
 */
public class ResourceBundleJUnitTest {

    public ResourceBundleJUnitTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void getBundle() {
        ResourceBundle res = data.DatabaseBundle.getResourceBundle();
        if (res == null) {
            Assert.assertTrue(false);
        } else {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void getDatabaseProperties() {
        boolean validParameters = true;
        if (data.DatabaseBundle.getDATABASE() == null) {
            validParameters = false;
        }
        if (data.DatabaseBundle.getPASSWORD() == null) {
            validParameters = false;
        }
        if (data.DatabaseBundle.getPORT() == null) {
            validParameters = false;
        }
        if (data.DatabaseBundle.getURL() == null) {
            validParameters = false;
        }
        if (data.DatabaseBundle.getUSER() == null) {
            validParameters = false;
        }
        Assert.assertTrue(validParameters);
    }
}
