/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mobile.dao.CategoryDao;
import com.mobile.dao.DaoFactory;
import com.mobile.dao.DeviceDao;
import com.mobile.dao.DepartmentDao;
import com.mobile.dao.OrganizationDao;
import com.mobile.dao.RegisteredUserDao;
import java.sql.Connection;
import java.util.logging.Logger;
import java.util.Date;
import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author wtccuser
 */
public class DatabaseJUnitTest {
// assumes the current class is called logger

    private final static Logger LOGGER = Logger.getLogger(DatabaseJUnitTest.class.getName());

    public DatabaseJUnitTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void getDatabaseConnection() throws Exception {

        Connection connection = data.DatabaseManager.getConnection();
        if (connection == null) {
            Assert.assertTrue(false);
        } else {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void findAllUsers() throws Exception {
        //get a connection to the database
        Connection connection = data.DatabaseManager.getConnection();
        //generated class to manage the table
        RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
        com.mobile.dto.RegisteredUser[] users = dao.findAll();
        if (users == null) {
            Assert.assertTrue(false);
        } else {
            Assert.assertTrue(true);
        }

    }

    @Test
    public void addUser() throws Exception {
        String email = "john@aol.com";
        //generated code
        com.mobile.dto.RegisteredUser user;
        //get a connection to the database
        Connection connection = data.DatabaseManager.getConnection();
        //generated class to manage the table
        RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
        //setting the properties
        user = null;
        if (user == null) {
            user = new com.mobile.dto.RegisteredUser();
            user.setEmail(email);
            java.util.Date date = new java.util.Date();
            user.setCreatedDate(date);
            //perform an insert to the database
            int primaryKey = dao.insert(user);
            if (primaryKey > 0) {
                Assert.assertTrue(true);
            } else {
                Assert.assertTrue(false);
            }
        }

    }

    @Test
    public void addDevice() throws Exception {
        com.mobile.dto.Device device;
        Connection connection = data.DatabaseManager.getConnection();
        DeviceDao dao = DaoFactory.createDeviceDao(connection);

        // check for subscriber 
        String subscriber = "John Cell Phone";
        device = dao.findBySubscriber(subscriber);
        if (device == null) {
            // get registeredUserId
            RegisteredUserDao userDao = DaoFactory.createRegisteredUserDao(connection);
            com.mobile.dto.RegisteredUser user;
            user = userDao.findByPrimaryKey(1);
            // setup device to add
            device = new com.mobile.dto.Device();
            device.setRegisteredUserId(user.getRegisteredUserId());
            device.setSubscriber(subscriber);
            Date date = new Date();
            // add device
            device.setCreatedDate(date);
            int primaryKey = dao.insert(device);
            // check if added
            if (primaryKey > 0) {
                Assert.assertTrue(true);
            } else {
                Assert.assertTrue(false);
            }
        }
    }

    @Test
    public void addCategory() throws Exception {
        com.mobile.dto.Category category;
        Connection connection = data.DatabaseManager.getConnection();
        CategoryDao dao = DaoFactory.createCategoryDao(connection);
        //determine if the row exists before we add the row
        String name = "men group";
        category = dao.findByName(name);
        if (category == null) {
            //did not find row, perform add
            category = new com.mobile.dto.Category();
            category.setName(name);
            java.util.Date date = new java.util.Date();
            category.setCreatedDate(date);
            int primaryKey = dao.insert(category);
            if (primaryKey > 0) {
                Assert.assertTrue(true);
            } else {
                Assert.assertTrue(false);
            }
        }

    }

    @Test
    public void addDepartment() throws Exception {
        String organizationName = "wakeTech";
        com.mobile.dto.Organization organization = new com.mobile.dto.Organization();
        Connection organizationConnection = data.DatabaseManager.getConnection();
        OrganizationDao organizationDao = DaoFactory.createOrganizationDao(organizationConnection);
        java.util.Date date = new java.util.Date();

        organization = organizationDao.findByName(organizationName);

        String name = "Math";
        com.mobile.dto.Department department = new com.mobile.dto.Department();
        Connection connection = data.DatabaseManager.getConnection();
        DepartmentDao dao = DaoFactory.createDepartmentDao(connection);

        //department = dao.findByOrganizationId(organization.getOrganizationId());
        //if (department == null) {
        //    department = new com.mobile.dto.Department();
        //    department.setCreatedDate(date);
        //    department.setEmail("waketech.org");
//            department.setIcon(null);
        //    department.setLocation("Raleigh,NC");
        //    department.setName(name);
        //    department.setOrganizationId(organization.getOrganizationId());

        //    int primaryKey = dao.insert(department);
        //    if (primaryKey > 0) {
        //        Assert.assertTrue(true);
        //    } else {
        //        Assert.assertTrue(false);
        //    }
        //}
    }

    @Test
    public void addOrganization() throws Exception {
        String name = "wakeTech";
        com.mobile.dto.Organization organization = new com.mobile.dto.Organization();
        Connection connection = data.DatabaseManager.getConnection();
        OrganizationDao dao = DaoFactory.createOrganizationDao(connection);
        java.util.Date date = new java.util.Date();

        organization = dao.findByName(name);
        if (organization == null) {
            organization = new com.mobile.dto.Organization();
            organization.setCreatedDate(date);
            organization.setEmail("waketech.org");
//            organization.setIcon(null);
            organization.setLocation("Raleigh,NC");
            organization.setName("WakeTech");

            int primaryKey = dao.insert(organization);
            if (primaryKey > 0) {
                Assert.assertTrue(true);
            } else {
                Assert.assertTrue(false);
            }
        }
    }
    
    @Test
    public void findAllOrganizations() throws Exception {
        //get a connection to the database
        Connection connection = data.DatabaseManager.getConnection();
        //generated class to manage the table
        OrganizationDao dao = DaoFactory.createOrganizationDao(connection);
        com.mobile.dto.Organization[] organizations = dao.findAll();
        if (organizations == null) {
            Assert.assertTrue(false);
        } else {
            Assert.assertTrue(true);
        }

    }
}
