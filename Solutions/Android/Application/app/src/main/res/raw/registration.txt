<h1>Welcome to  Cokcc.org application</h1>
<hr>
<br/>
This application will allow you to view UpComing Events. You will be able to submit
 questions and comments.
<br/>
<p>
The application service will require you to have a smart active mobile device. Your mobile
device will be validated during registration. During online registration, we will capture your phone number, email and user name
<br/><br/>
If you experience any problems, please contact support@cokcc.org
<br/><br/>
For additional mobile service information, goto http://cokcc.org
<br/><br/>
You can call (919) 838-4848 for assistance
