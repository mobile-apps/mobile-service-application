package mobile.com.listeners;

import android.view.View;

import mobile.com.activities.MainActivity;
import mobile.com.fragments.EventDetailFragment;
import mobile.com.web.Event;

/**
 * Created by HopeBenziger on 7/2/15.
 */
public class EventDetailFragmentPhoneListener implements View.OnClickListener {
    private EventDetailFragment mEventDetailFragment;
    public EventDetailFragmentPhoneListener(EventDetailFragment eventDetailFragment){
        mEventDetailFragment = eventDetailFragment;
    }

    @Override
    public void onClick(View view) {
        if (mEventDetailFragment != null) {
            Event event = mEventDetailFragment.getEvent();
            if (event != null) {
                MainActivity mainActivity = (MainActivity)mEventDetailFragment.getActivity();
                mainActivity.launchPhone(event.getPhone());
            }
        }

    }

}

