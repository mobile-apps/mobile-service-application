package mobile.com.security;

import mobile.com.application.LoggingTag;
import mobile.com.util.LoggerHelper;
import mobile.com.util.PreferenceManagerHelper;

/**
 * Created by administrator on 8/13/14.
 */
public class SecurityManager {
    private final static String REGISTERED = "REGISTERED";
    private final static String GOOGLE_SUBSCRIBER_ID = "GOOGLE_SUBSCRIBER_ID";
    private final static String REGISTER_USER_ID    = "REGISTER_USER_ID";
    public final static String SKIP_SLIDE    = "SKIP_SLIDE";
    /*
	 * has the member registered
	 */
    public static boolean isRegistered(android.content.Context context) {
        //todo need to add logic for registration
        String regUserId = PreferenceManagerHelper.getStringFromSP(REGISTER_USER_ID,context);
        if (regUserId != null && !regUserId.isEmpty()) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean skipSlides(android.content.Context context) {
        //todo need to add logic for registration
        boolean skip = PreferenceManagerHelper.getBooleanFromSP(SKIP_SLIDE,context);
       return skip;

    }

    /**
     * get stored registration
     * @param context
     * @return
     */
    public static String getRegistrationId(android.content.Context context) {
        //todo need to add logic for registration
        LoggerHelper.d(
                LoggingTag.REGISTRATION.name(),
                "starting getRegistrationId ", SecurityManager.class.getSimpleName());
        String regUserId = PreferenceManagerHelper.getStringFromSP(REGISTER_USER_ID,context);
        LoggerHelper.d(
                LoggingTag.REGISTRATION.name(),
                "regUserId =[ " + regUserId + "]", SecurityManager.class.getSimpleName());
        return regUserId;

    }

    public static void saveGoogleRegistration(android.content.Context context, String Id) {
       PreferenceManagerHelper.saveStringInSP(context,GOOGLE_SUBSCRIBER_ID,Id);
    }
    public static void saveSkipSlide(android.content.Context context, boolean value) {
        PreferenceManagerHelper.saveBooleanInSP(context,SKIP_SLIDE,value);
    }
    public static void saveRegisteredId(android.content.Context context, String registrationId) {
        PreferenceManagerHelper.saveStringInSP(context,REGISTER_USER_ID,registrationId);
    }
}

