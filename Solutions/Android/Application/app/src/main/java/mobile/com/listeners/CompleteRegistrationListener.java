package mobile.com.listeners;

/**
 * Created by administrator on 2/13/15.
 */
import android.content.DialogInterface;
import android.content.Context;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;

import mobile.com.services.CompleteRegistrationService;

public class CompleteRegistrationListener implements OnClickListener {
    private Context mContext;

    public CompleteRegistrationListener(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * start main activity after registration and update registration
     * @param dialogInterface
     * @param i
     */
    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        //complete registration
        //start an async web service
        Intent serviceIntent = new Intent(mContext, CompleteRegistrationService.class);
        // Starts the IntentService
        mContext.startService(serviceIntent);
        Intent intent = new Intent(mContext, mobile.com.activities.MainActivity.class);


        mContext.startActivity(intent);

    }
}
