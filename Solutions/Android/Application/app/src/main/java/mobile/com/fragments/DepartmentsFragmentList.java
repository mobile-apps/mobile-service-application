package mobile.com.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import mobile.com.activities.MainActivity;
import mobile.com.activities.R;
import mobile.com.adapter.DepartmentAdapter;
import mobile.com.application.LoggingTag;
import mobile.com.listeners.DepartmentDetailListener;
import mobile.com.web.Department;
import mobile.com.web.service.clients.DepartmentClient;
import mobile.com.util.LoggerHelper;

public class DepartmentsFragmentList extends Fragment {
    private ListView mListView;
    private MainActivity mMainActivity;
    public DepartmentsFragmentList() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mMainActivity = (MainActivity)activity;

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_department_list, container, false);
        mListView = (ListView)rootView.findViewById(R.id.listView);
        DepartmentDetailListener departmentDetailListener = new DepartmentDetailListener(mMainActivity);
        mListView.setOnItemClickListener(departmentDetailListener);
        return rootView;
    }
    /**
     * fill podcast list
     */
    public void fillList(List<Department> departments) {
        //get the events from memory
        DepartmentAdapter departmentAdapter = new DepartmentAdapter(this.getActivity(),departments);
       if (mListView != null) {
           mListView.setAdapter(departmentAdapter);
       }


    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new PrefetchData().execute();

    }
    /*
     * Async Task to make http callå
     */
    private class PrefetchData extends AsyncTask<Void, Void, Void> {
        List<Department> departments;
        /** progress dialog to show user that the backup is processing. */
        private ProgressDialog dialog;
        /*

        /*
* Will make http call here This call will
* download required data
* before launching the app
*/
        @Override
        protected Void doInBackground(Void... arg0) {
            LoggerHelper.d(
                    LoggingTag.SPLASH.name(),
                    "starting doInBackground ", this.getClass().getSimpleName());
            departments = DepartmentClient.findAll();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
            DepartmentsFragmentList.this.fillList(departments);


        }


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            dialog = new ProgressDialog(mMainActivity);
            // dialog.setIndeterminateDrawable(R.drawable.splash);
            dialog.setMessage("Loading, please wait.");
            dialog.show();


        }
    }

}
