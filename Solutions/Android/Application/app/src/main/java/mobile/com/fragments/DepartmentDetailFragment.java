package mobile.com.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import mobile.com.activities.MainActivity;
import mobile.com.activities.R;
import mobile.com.application.LoggingTag;
import mobile.com.listeners.DepartmentFragmentDrivingDirectionListener;
import mobile.com.listeners.DepartmentFragmentEmailListener;
import mobile.com.listeners.DepartmentFragmentPhoneListener;
import mobile.com.web.Department;
import mobile.com.util.LoggerHelper;

public class DepartmentDetailFragment extends Fragment  {
    public Department getDepartment() {
        return mDepartment;
    }

    public void setDepartment(Department department) {
        this.mDepartment = department;
    }
    private MainActivity mMainActivity;
    private Department mDepartment;



    public DepartmentDetailFragment() {

    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mMainActivity = (MainActivity)activity;

    }
    //this method is only called once
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }


    public void drivingFragmentClicked(View v) {
       //mMainActivity.launchNavigator(mDepartment.getDepartment());

        //Toast.makeText(getActivity(), "ImageView clicked for the row = " + mEvent.getDepartment().getName(), Toast.LENGTH_SHORT).show();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_department_detail, container, false);
        final TextView textViewMessage = (TextView) rootView.findViewById(R.id.textViewMessage);
        final TextView textViewName = (TextView) rootView.findViewById(R.id.textViewName);
        final TextView textViewPhone = (TextView) rootView.findViewById(R.id.textPhone);
       //textPhone

        final TextView textViewLocation = (TextView) rootView.findViewById(R.id.textViewLocation);
        ImageView imageView = (ImageView) rootView.findViewById(R.id.ivDepartmentIcon);
       //ivPhoneIcon
        final ImageView imageViewPhoneIcon = (ImageView) rootView.findViewById(R.id.ivPhoneIcon);
        DepartmentFragmentPhoneListener departmentFragmentPhoneListener = new DepartmentFragmentPhoneListener(this);
        imageViewPhoneIcon.setOnClickListener(departmentFragmentPhoneListener);
        final ImageView imageViewDrivingDirectionIcon = (ImageView) rootView.findViewById(R.id.ivDrivingDirectionIcon);


        DepartmentFragmentDrivingDirectionListener departmentFragmentDrivingDirectionListener = new DepartmentFragmentDrivingDirectionListener(this);
        imageViewDrivingDirectionIcon.setOnClickListener(departmentFragmentDrivingDirectionListener);
        final ImageView imageViewimageViewEmailIcon = (ImageView) rootView.findViewById(R.id.imageViewEmailIcon);
        DepartmentFragmentEmailListener departmentFragmentEmailListener = new DepartmentFragmentEmailListener(this);
        imageViewimageViewEmailIcon.setOnClickListener(departmentFragmentEmailListener);
        final TextView textEmail = (TextView) rootView.findViewById(R.id.textEmail);
        if (mDepartment != null) {
            if (textViewName != null) {
                textViewName.setText(mDepartment.getName());
                textEmail.setText(mDepartment.getEmail());
            }
            if (textViewPhone != null) {
                if (mDepartment.getPhone() != null) {
                    textViewPhone.setText(mDepartment.getPhone());
                }
            }
            if (textViewMessage != null) {
                String message = mDepartment.getGeneralInformation();
               textViewMessage.setText(mDepartment.getGeneralInformation());
            }
            if (mDepartment != null) {
                if (textViewLocation != null) {

                    textViewLocation.setText( mDepartment.getLocation());
                }

                if (imageView != null) {
                    String icon =  mDepartment.getIcon();
                    if (icon != null) {
                        String strBase64 = icon;
                        byte[] decodedString = null;
                        try {
                            decodedString = Base64.decode(strBase64, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            //todo we need a bigger image imageView.setImageBitmap(decodedByte);
                            imageView.setContentDescription(mDepartment.getName());
                        } catch (Exception ex) {
                            LoggerHelper.d(
                                    LoggingTag.APPLICATION.name(),
                                    "Error  " + ex.getMessage(), this.getClass().getSimpleName());
                        }
                    }
                }

            }

        } else {
            throw new IllegalStateException("Event should not be null");
        }
       // return InputFragmentView;
        return rootView;
    }
}
