package mobile.com.web.service.clients;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mobile.com.application.LoggingTag;
import mobile.com.web.Podcast;
import mobile.com.web.Request;
import mobile.com.web.RequestType;
import mobile.com.web.URLHelper;
import mobile.com.util.LoggerHelper;

/**
 * Created by administrator on 4/20/15.
 */
public class PodcastClient {
    /**
     * get all of the 
     * @return
     */
    public static List<Podcast> findPodcasts() {
        LoggerHelper.d(
                LoggingTag.PODCAST.name(),
                "starting findPodcasts ", PodcastClient.class.getSimpleName());
        List<Podcast> podcasts = new ArrayList<Podcast>();
        Request request = new Request();
        String fullURL = URLHelper.PODCAST.getFullURL();

        //SplashScreenActivity.this.getApplicationContext().getString(R.string.getAllCategoriesAndEvents_url);
        request.setURL(fullURL);
        request.setRequestType(RequestType.OBJECT);
        try {
            JSONObject jsonOrgObject = (JSONObject) mobile.com.web.WebServiceHelper.getHTTPGetJSonRestful(request);

            JSONArray jsonArray = jsonOrgObject.getJSONArray("podcast");


            if (jsonOrgObject == null) {
                throw new IllegalStateException("Did not find json");
            }

            if (jsonArray != null && jsonArray.length() > 0) {

                for (int x = 0; x < jsonArray.length(); x++) {
                    JSONObject jsonStepObj = jsonArray.getJSONObject(x);
                    String description = jsonStepObj.getString(Podcast.PROPERTY_DESCRIPTION);
                    String title = jsonStepObj.getString(Podcast.PROPERTY_TITLE);
                    String publishDate = jsonStepObj.getString(Podcast.PROPERTY_PUBLISH_DATE);
                    String mediaLocation = jsonStepObj.getString(Podcast.PROPERTY_MEDIA_LOCATION);
                   Podcast podcast = new Podcast(title, description,mediaLocation,publishDate);
                    podcasts.add(podcast);


                    }//end of for
                LoggerHelper.d(
                        LoggingTag.PODCAST.name(),
                        "ending findPodcasts ", PodcastClient.class.getSimpleName());
                return podcasts;
            }
        } catch(Exception ex) {
            LoggerHelper.d(
                    LoggingTag.PODCAST.name(),
                    "Error  " + ex.getMessage(), PodcastClient.class.getSimpleName());
             return null;
        }

        return podcasts;
    }
}
