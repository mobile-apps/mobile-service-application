package mobile.com.fragments;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ListView;
import android.widget.TextView;

import mobile.com.activities.R;
import mobile.com.adapter.DirectionAdapter;
import mobile.com.application.LoggingTag;
import mobile.com.web.Department;
import mobile.com.web.Direction;
import mobile.com.web.URLHelper;
import mobile.com.web.service.clients.DrivingDirectionClient;
import mobile.com.util.LoggerHelper;

public class DrivingDirectionFragment extends Fragment {

    private Direction mDirection;
    private Department mDepartment;
    private WebView mWebView;

    public DrivingDirectionFragment() {
    }
    public void setDepartment(Department department) {
        mDepartment = department;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LoggerHelper.d(
                LoggingTag.DRIVING.name(),
                "starting onCreateView ", this.getClass().getSimpleName());
        View rootView = inflater.inflate(R.layout.fragment_community, container, false);
        mWebView= (WebView)rootView.findViewById(R.id.webPage);
        LoggerHelper.d(
                LoggingTag.DRIVING.name(),
                "ending onCreateView ", this.getClass().getSimpleName());
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LoggerHelper.d(
                LoggingTag.DRIVING.name(),
                "starting onActivityCreated ", this.getClass().getSimpleName());
        /*
         * Showing splashscreen while making network calls to download necessary
		 * data before launching the app Will use AsyncTask to make http call
		 */
        try {

            new PrefetchData().execute();
        } catch (Exception ex) {
            LoggerHelper.e(
                    LoggingTag.DRIVING.name(),
                    "error[" + ex.getMessage() + "]", this.getClass().getSimpleName());
            //try web view
            String url = URLHelper.MOBILE_MAP.getFullURL();
            mWebView.loadUrl(url);
        }
    }

    /**
     * populate direction on listview
     */
    public void populateDirections()
    {
        LoggerHelper.d(
                LoggingTag.DRIVING.name(),
                "starting populateDirections ", this.getClass().getSimpleName());
        TextView textView_Starting_Address = (TextView) this.getView().findViewById(R.id.textView_Start);
        textView_Starting_Address.setText(mDirection.getStartingAddress());

        DirectionAdapter directionAdapter = new DirectionAdapter(this.getActivity(), mDirection.getSteps());
        ListView listview = (ListView) this.getView().findViewById(R.id.listView_Directions);
        listview.setAdapter(directionAdapter);

        TextView textView_Ending_Address = (TextView) this.getView().findViewById(R.id.textView_End);
        textView_Ending_Address.setText(mDirection.getEndingAddress());
        LoggerHelper.d(
                LoggingTag.DRIVING.name(),
                "ending populateDirections ", this.getClass().getSimpleName());

    }

    /*
     * Async Task to make http call
     */
    private class PrefetchData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // before making http calls
            LoggerHelper.d(
                    LoggingTag.SPLASH.name(),
                    "starting onPreExecute ", this.getClass().getSimpleName());


        }

        /*
        * Will make http call here This call will
        * download required data
        * before launching the activity
        */
        @Override
        protected Void doInBackground(Void... arg0) {
            LoggerHelper.d(
                    LoggingTag.SPLASH.name(),
                    "starting doInBackground ", this.getClass().getSimpleName());

            //call to find driving directions
            mDirection = DrivingDirectionClient.findDrivingDirections(mDepartment);
            // mobile.com.web.services.Initializer.initial(SplashScreenActivity.this);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            DrivingDirectionFragment.this.populateDirections();
        }

    }
}
