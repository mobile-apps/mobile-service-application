package mobile.com.web.service.clients;

import org.json.JSONArray;
import org.json.JSONObject;

import mobile.com.application.SharedObjectManager;
import mobile.com.location.GPSLocation;
import mobile.com.web.Department;
import mobile.com.web.Direction;
import mobile.com.web.Request;
import mobile.com.web.RequestType;
import mobile.com.web.Step;
import mobile.com.web.URLHelper;

/**
 * Created by administrator on 4/20/15.
 */
public class DrivingDirectionClient {
    public static Direction findDrivingDirections(Department department) {
        //?latitude=35.79449833333333&longitude=-78.60169833333333
        //get gps location from memory
        GPSLocation gpsLocation = (GPSLocation)SharedObjectManager.getMapValue(SharedObjectManager.OBJECT_NAME.GPSLOCATION);
        mobile.com.web.Request request = new Request();
        String fullURL = URLHelper.FIND_DRIVING_DIRECTIONS.getFullURL();
        StringBuilder queryBuilder =  new StringBuilder();
        queryBuilder.append("?latitudeStart=");
        queryBuilder.append(gpsLocation.getLatitude());
        queryBuilder.append("&longitudeStart=");
        queryBuilder.append(gpsLocation.getLongitude());
        queryBuilder.append("&latitudeEnd=");
        queryBuilder.append(department.getLatitude());
        queryBuilder.append("&longitudeEnd=");
        queryBuilder.append(department.getLongitude());
        fullURL = fullURL + queryBuilder.toString();
        //SplashScreenActivity.this.getApplicationContext().getString(R.string.getAllCategoriesAndEvents_url);
        request.setURL(fullURL);
        request.setRequestType(RequestType.OBJECT);
        try {
            JSONObject jsonOrgObject = (JSONObject) mobile.com.web.WebServiceHelper.getHTTPGetJSonRestful(request);
            String startingAddress = jsonOrgObject.getString(Direction.PROPERTY_STARTING_ADDRESS);
            String endingAddress = jsonOrgObject.getString(Direction.PROPERTY_ENDING_ADDRESS);
            Direction direction = new Direction(startingAddress,endingAddress);
            //loop thru steps
            JSONArray jsonArray = jsonOrgObject.getJSONArray(Direction.PROPERTY_STEPS);


            if (jsonOrgObject == null) {
                throw new IllegalStateException("Did not find json");
            }

            if (jsonArray != null && jsonArray.length() > 0) {

                for (int x = 0; x < jsonArray.length(); x++) {
                    JSONObject jsonStepObj = jsonArray.getJSONObject(x);
                    String distance = jsonStepObj.getString(Step.PROPERTY_DISTANCE);
                    String duration = jsonStepObj.getString(Step.PROPERTY_DURATION);
                    String instruction = jsonStepObj.getString(Step.PROPERTY_INSTRUCTION);
                    String maneuver = null;
                    try {
                        maneuver = jsonStepObj.getString(Step.PROPERTY_MANEUVER);
                    } catch(Exception e) {

                    }
                    Step step = new Step(distance, duration, instruction, maneuver);
                    direction.getSteps().add(step);

                }//end of for
                return direction;
            }
        } catch(Exception ex) {
             return null;
        }
        return null;
    }
}
