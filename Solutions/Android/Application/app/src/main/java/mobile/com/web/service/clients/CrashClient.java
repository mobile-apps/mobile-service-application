package mobile.com.web.service.clients;

import org.json.JSONObject;

import mobile.com.application.LoggingTag;
import mobile.com.web.Crash;
import mobile.com.web.Request;
import mobile.com.web.RequestType;
import mobile.com.web.URLHelper;
import mobile.com.util.LoggerHelper;

/**
 * Created by wtccuser on 7/5/15.
 */
public class CrashClient {
    /**
     * try to remotely record crash report
     * @param crash
     */
    public static  void remoteLogCrashReport(Crash crash) {
        try {
            mobile.com.web.Request request = new Request();
            String fullURL = URLHelper.CRASH.getFullURL();
            request.setURL(fullURL);
            request.setJsonImpl(crash);
            request.setRequestType(RequestType.POST);
            LoggerHelper.d(
                    LoggingTag.CRASHREPORT.name(),
                    "attempt to add crash report ", CrashClient.class.getSimpleName());

            JSONObject jsonObj = mobile.com.web.WebServiceHelper.getHTTPPostJSonRestful(request);
            LoggerHelper.d(
                    LoggingTag.CRASHREPORT.name(),
                    "return from crash restful service ", CrashClient.class.getSimpleName());

        } catch (Exception ex) {
            LoggerHelper.e(
                    LoggingTag.CRASHREPORT.name(),
                    "error  " + ex.getMessage(), CrashClient.class.getSimpleName());

        }
    }
}
