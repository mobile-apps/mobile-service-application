package mobile.com.web;

import java.util.Date;

/**
 * Created by wtccuser on 9/6/14.
 */
public class Department {

    public final static String PROPERTY_LOCATION = "location";
    public final static String PROPERTY_LONGITUDE = "longitude";
    public final static String PROPERTY_LATITUDE = "latitude";
    public final static String PROPERTY_ICON = "icon";
    public final static String PROPERTY_EMAIL = "email";
    public final static String PROPERTY_NAME = "name";
    public final static String PROPERTY_PHONE = "phone";
    public final static String PROPERTY_ID = "departmentId";
    public final static String PROPERTY_GENERAL_INFORMATION = "generalInformation";

    public Department(String location, double latitude, double longitude, String icon, String generalInformation, String email, int id, String name, String phone) {
        this.location = location;
        this.latitude = latitude;
        this.longitude = longitude;
        this.icon = icon;
        this.generalInformation = generalInformation;
        this.email = email;
        this.id = id;
        this.name = name;
        this.phone = phone;
    }

    private String location;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getGeneralInformation() {
        return generalInformation;
    }

    public void setGeneralInformation(String generalInformation) {
        this.generalInformation = generalInformation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private double latitude;
    private double longitude;
    private String icon;
    private String generalInformation;
    private String email;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private int id;
    private String name;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    private String phone;

}
