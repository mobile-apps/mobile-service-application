package mobile.com.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;



import mobile.com.activities.R;

public class DonateFragment extends Fragment {

    public DonateFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_donate, container, false);
        final WebView wv= (WebView)rootView.findViewById(R.id.webPage);
        final String url = getString(R.string.url_donate);
        final ImageView imageView = (ImageView)rootView.findViewById(R.id.imagePaypal);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String postData = "cmd=_donations&business=cokccfinance@gmail.com&lc=US&item_name=Christ Our King Community Church&no_note=0&currency_code=USD&bn=PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest";
              //  wv.postUrl(url, EncodingUtils.getBytes(postData, "BASE64"));

            }
        });
       // WebView webViewInstruction= (WebView)rootView.findViewById(R.id.webDonatePageInstructions);
        //webViewInstruction.loadUrl("file:///android_asset/donate.html");
        TextView textViewContent = (TextView)rootView.findViewById(R.id.textViewContent);
        textViewContent.setMovementMethod(new ScrollingMovementMethod());
       // wv.postUrl("https://www.paypal.com/cgi-bin/webscr", EncodingUtils.getBytes(postData, "BASE64"));
        return rootView;
    }
}
