package mobile.com.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import mobile.com.activities.R;
import mobile.com.application.LoggingTag;
import mobile.com.util.LoggerHelper;
import mobile.com.web.Podcast;

public class PodcastDetailFragment extends Fragment {
    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }
    public static final String PODCAST_NAME="podcast";
    private Podcast podcast;

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        // Get the Camera instance as the activity achieves full user focus

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    public Podcast getPodcast() {
        return podcast;
    }

    public void setPodcast(Podcast podcast) {
        this.podcast = podcast;
    }

    //parent activity
    private Context mContext;
    private SeekBar mSeekBar;
    private TextView mTextViewDuration;
    private Handler durationHandler = new Handler();
    private MediaPlayer mMediaPlayer = new MediaPlayer();
    /**
     * help to toggle between play and pause.
     */
    private boolean playPause;
    /**
     * remain false till media is not completed, inside OnCompletionListener make it true.
     */
    private boolean intialStage = true;
    private double mTimeElapsed;
    private double mFinalTime;
    private int mForwardTime = 2000, mBackwardTime = 2000;
    private Button mButton;
    private ImageButton mImageButtonPlay;

    public PodcastDetailFragment() {
    }


    //handler to change seekBarTime
    private Runnable updateSeekBarTime = new Runnable() {
        public void run() {
            if (mMediaPlayer == null) {
                return;
            }
            mFinalTime = mMediaPlayer.getDuration();
            //get current position
            mTimeElapsed = mMediaPlayer.getCurrentPosition();
            //set seekbar progress
            mSeekBar.setProgress((int) mTimeElapsed);
            //set time remaing
            double timeRemaining = mFinalTime - mTimeElapsed;
            mTextViewDuration.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes((long) timeRemaining), TimeUnit.MILLISECONDS.toSeconds((long) timeRemaining) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) timeRemaining))));

            //repeat yourself that again in 100 miliseconds
            durationHandler.postDelayed(this, 100);
        }
    };
    @Override
        public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(PODCAST_NAME, podcast);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LoggerHelper.d(
                LoggingTag.PODCAST.name(),
                "starting onCreateView ", this.getClass().getSimpleName());
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        View rootView = inflater.inflate(R.layout.fragment_podcast_player, container, false);

        if (savedInstanceState != null) {
            // Restore last state
            LoggerHelper.d(
                    LoggingTag.PODCAST.name(),
                    "Restore last state", this.getClass().getSimpleName());
            podcast = (Podcast)savedInstanceState.getSerializable(PODCAST_NAME);
        }


        if (podcast == null) {
            LoggerHelper.d(
                    LoggingTag.PODCAST.name(),
                    "podcast is null ", this.getClass().getSimpleName());
            return rootView;
        }
        TextView textViewTitle = (TextView) rootView.findViewById(R.id.songName);
        textViewTitle.setText(podcast.getTitle());
        mSeekBar = (SeekBar) rootView.findViewById(R.id.seekBar);
        TextView textViewPublishDate = (TextView) rootView.findViewById(R.id.publishDate);
        textViewPublishDate.setText(podcast.getPublishDate());
        TextView textViewSongDescription = (TextView) rootView.findViewById(R.id.songDescription);
        textViewSongDescription.setText(podcast.getDescription());
        mTextViewDuration = (TextView) rootView.findViewById(R.id.songDuration);
//songDescription
        ImageButton pauseImageButton = (ImageButton) rootView.findViewById(R.id.media_pause);
        pauseImageButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mMediaPlayer.pause();
            }
        });
        ImageButton forwardImageButton = (ImageButton) rootView.findViewById(R.id.media_ff);
        forwardImageButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                LoggerHelper.d(
                        LoggingTag.PODCAST.name(),
                        "starting onClick ", this.getClass().getSimpleName());
                //mFinalTime
                //check if we can go forward at forwardTime seconds before song endes
                if ((mTimeElapsed + mForwardTime) <= mFinalTime) {
                    mTimeElapsed = mTimeElapsed + mForwardTime;

                    //seek to the exact second of the track
                    mMediaPlayer.seekTo((int) mTimeElapsed);
                }
                LoggerHelper.d(
                        LoggingTag.PODCAST.name(),
                        "ending onClick ", this.getClass().getSimpleName());

            }
        });
        ImageButton rewindImageButton = (ImageButton) rootView.findViewById(R.id.media_rew);
        rewindImageButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                LoggerHelper.d(
                        LoggingTag.PODCAST.name(),
                        "starting onClick ", this.getClass().getSimpleName());
                //mFinalTime
                ///check if we can go back at backwardTime seconds after song starts
                if ((mTimeElapsed - mBackwardTime) > 0) {
                    mTimeElapsed = mTimeElapsed - mBackwardTime;

                    //seek to the exact second of the track
                    mMediaPlayer.seekTo((int) mTimeElapsed);
                }
                LoggerHelper.d(
                        LoggingTag.PODCAST.name(),
                        "ending onClick ", this.getClass().getSimpleName());

            }
        });

        mImageButtonPlay = (ImageButton) rootView.findViewById(R.id.media_play);
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mImageButtonPlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                LoggerHelper.d(
                        LoggingTag.PODCAST.name(),
                        "starting onClick ", this.getClass().getSimpleName());
                /*
                try {
                    URL url = new URL(podcast.getMediaLocation());
                    URI uri = url.toURI();
                    Intent viewIntent = new Intent(Intent.ACTION_VIEW);

                    viewIntent.setDataAndType(Uri.parse(podcast.getMediaLocation()), "audio/*");
                    startActivity(Intent.createChooser(viewIntent, null));

                } catch (Exception ex) {

                }
                */
                new Player()
                        .execute(podcast.getMediaLocation());
                LoggerHelper.d(
                        LoggingTag.PODCAST.name(),
                        "ending onClick ", this.getClass().getSimpleName());


            }
        });

        LoggerHelper.d(
                LoggingTag.PODCAST.name(),
                "ending onCreateView ", this.getClass().getSimpleName());
        return rootView;
    }


    /**
     * preparing mediaplayer will take sometime to buffer the content so prepare it inside the background thread and starting it on UI thread.
     *
     * @author piyush
     */

    class Player extends AsyncTask<String, Void, Boolean> {
        private ProgressDialog progress;

        @Override
        protected Boolean doInBackground(String... params) {
            LoggerHelper.d(
                    LoggingTag.PODCAST.name(),
                    "starting doInBackground ", this.getClass().getSimpleName());
            // TODO Auto-generated method stub
            Boolean prepared;
            try {

                if (mMediaPlayer == null) {
                    return false;
                }

                mMediaPlayer.setDataSource(params[0]);


                mMediaPlayer.prepare();
                prepared = true;
                LoggerHelper.d(
                        LoggingTag.PODCAST.name(),
                        "starting doInBackground ", this.getClass().getSimpleName());
            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                LoggerHelper.e(
                        LoggingTag.PODCAST.name(),
                        e.getMessage(), this.getClass().getSimpleName());
                Log.d("IllegarArgument", e.getMessage());
                prepared = false;
                e.printStackTrace();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                prepared = false;
                LoggerHelper.e(
                        LoggingTag.PODCAST.name(),
                        e.getMessage(), this.getClass().getSimpleName());
                e.printStackTrace();
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                prepared = false;
                LoggerHelper.e(
                        LoggingTag.PODCAST.name(),
                        e.getMessage(), this.getClass().getSimpleName());
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                LoggerHelper.e(
                        LoggingTag.PODCAST.name(),
                        e.getMessage(), this.getClass().getSimpleName());
                prepared = false;
                e.printStackTrace();
            }
            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            LoggerHelper.d(
                    LoggingTag.PODCAST.name(),
                    "starting onPostExecute ", this.getClass().getSimpleName());
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (progress.isShowing()) {
                progress.cancel();
            }
            Log.d("Prepared", "//" + result);
            mMediaPlayer.start();
            double finalTime = mMediaPlayer.getDuration();
            mSeekBar.setMax((int) finalTime);
            mSeekBar.setClickable(false);
            durationHandler.postDelayed(updateSeekBarTime, 100);
            intialStage = false;
            LoggerHelper.d(
                    LoggingTag.PODCAST.name(),
                    "ending onPostExecute ", this.getClass().getSimpleName());
        }

        public Player() {
            progress = new ProgressDialog(PodcastDetailFragment.this.getActivity());
        }

        @Override
        protected void onPreExecute() {
            LoggerHelper.d(
                    LoggingTag.PODCAST.name(),
                    "starting onPreExecute ", this.getClass().getSimpleName());
            // TODO Auto-generated method stub
            super.onPreExecute();
            String message = getResources().getString(R.string.media_Downloading);
            this.progress.setMessage(message);
            this.progress.show();
            LoggerHelper.d(
                    LoggingTag.PODCAST.name(),
                    "starting onPreExecute ", this.getClass().getSimpleName());

        }
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        if (mMediaPlayer != null) {
            mMediaPlayer.reset();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }
}
