package mobile.com.web;

import android.os.Build;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by wtccuser on 2/16/15.
 */
public class Crash implements Serializable,JSonImpl {
    private String logCat;

    public String getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }

    private String androidVersion;
    public static final String CRASH_NAME="CRASH";


    public Crash(String logCat, String androidVersion, String appVersionCode, String reportId, String appVersionName, String phoneModel, String brand, String deviceId, String packageName, String stackTrace) {
        this.logCat = (logCat == null) ? "none" : logCat;
        this.androidVersion = (androidVersion == null) ? "none" : androidVersion;
        this.appVersionCode = (appVersionCode == null) ? "none" : appVersionCode;
        this.reportId = (reportId == null) ? "none" : reportId;
        this.appVersionName = (appVersionName == null) ? "none" : appVersionName;
        this.phoneModel = (phoneModel == null) ? "none" : phoneModel;
        this.brand = (brand == null) ? "none" : brand;
        this.deviceId = (deviceId == null) ? "none" : deviceId;
        this.packageName = (packageName == null) ? "none" : packageName;
        this.stackTrace = (stackTrace == null) ? "none" : stackTrace;
    }

    public String getAppVersionCode() {
        return appVersionCode;
    }

    public void setAppVersionCode(String appVersionCode) {
        if (appVersionCode == null) {
            appVersionCode = "none";
        }
        this.appVersionCode = appVersionCode;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        if (reportId == null) {
            reportId = "none";
        }
        this.reportId = reportId;
    }

    public String getAppVersionName() {
        return appVersionName;
    }

    public void setAppVersionName(String appVersionName) {
        if (appVersionName == null) {
            appVersionName = "none";
        }
        this.appVersionName = appVersionName;
    }

    public String getPhoneModel() {
        return phoneModel;
    }

    public void setPhoneModel(String phoneModel) {
        if (phoneModel == null) {
            phoneModel = "none";
        }
        this.phoneModel = phoneModel;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        if (brand == null) {
            brand = "none";
        }
        this.brand = brand;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        if (deviceId == null) {
            deviceId = "none";
        }
        this.deviceId = deviceId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        if (packageName == null) {
            packageName = "none";
        }
        this.packageName = packageName;
    }

    private String appVersionCode;
    private String reportId;
    private String appVersionName;
    private String phoneModel;
    private String brand;
    private String deviceId;
    private String packageName;

    /**
     * Set default values
     *
     * @return
     */
    public Crash(Exception ex) {
        String appVersionCode = Build.VERSION.CODENAME;
        String reportId = Build.ID;
        appVersionName = Build.VERSION.RELEASE;
        phoneModel = Build.MODEL;
        brand = Build.BRAND;
        String deviceId = Build.DEVICE;
        StringBuffer buffer = new StringBuffer();
        if (ex.getStackTrace()[0] != null) {
            buffer.append(ex.getStackTrace()[0].getClassName());
            buffer.append(".");
            buffer.append(ex.getStackTrace()[0].getMethodName());
            buffer.append(" line number[");
            buffer.append(ex.getStackTrace()[0].getLineNumber());
            buffer.append("]");
            packageName = buffer.toString();
        }
        //ex.getStackTrace()[0].
        // private String packageName;
    }

    public Crash(String logCat, String stackTrace) {
        this.logCat = logCat;
        this.stackTrace = stackTrace;
    }

    public String getStackTrace() {
        return stackTrace;

    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    public String getLogCat() {
        return logCat;
    }

    public void setLogCat(String logCat) {
        this.logCat = logCat;
    }

    private String stackTrace;

    @Override
    public String toJSon() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("logCat", logCat);
            jsonObject.put("stackTrace", stackTrace);
            jsonObject.put("androidVersion", androidVersion);
            jsonObject.put("appVersionCode", appVersionCode);
            jsonObject.put("reportId", reportId);
            jsonObject.put("appVersionName", appVersionName);
            jsonObject.put("phoneModel", phoneModel);
            jsonObject.put("brand", brand);

            jsonObject.put("deviceId", deviceId);
            jsonObject.put("appVersionName", appVersionName);
            jsonObject.put("packageName", packageName);


            return jsonObject.toString();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

    }
}
