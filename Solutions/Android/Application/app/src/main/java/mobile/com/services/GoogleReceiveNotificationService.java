package mobile.com.services;

/**
 * Created by administrator on 8/22/14.
 * <service android:name="mobile.com.services.RegistrationService" />
 */

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.support.v4.app.NotificationCompat;

import mobile.com.activities.R;
import mobile.com.activities.SplashScreenActivity;
import mobile.com.application.LoggingTag;
import android.content.Context;
import mobile.com.receiver.GoogleBroadcastReceiver;
import mobile.com.util.LoggerHelper;


public class GoogleReceiveNotificationService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    String TAG = "GcmIntentService";
    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     * Project name = MobileApplication
     * GOOGLE_PROJECT_ID = "shining-relic-675";
     * mobileapps7777@gmail.com
     * mobile-apps = password
     */
    private final static String GOOGLE_PROJECT_ID = "142883741177";


    public GoogleReceiveNotificationService() {
        super("GoogleReceiveNotificationService");

    }

    /**
     * registration for google key
     * The IntentService calls this method from the default worker thread with
     * the intent that started the service. When this method returns, IntentService
     * stops the service, as appropriate.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        LoggerHelper.d(
                LoggingTag.GOOGLEMESSAGE.name(),
                "starting onHandleIntent ", this.getClass().getSimpleName());
        Bundle extras = intent.getExtras();

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " +
                        extras.toString());
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // This loop represents the service doing some work.
                GoogleMessage googleMessage = new GoogleMessage();
                // Post notification of received message.
                String message = intent.getStringExtra("message");
                googleMessage.setMessage(message);
                String title = intent.getStringExtra("title");
                googleMessage.setTitle(title);
                sendNotification(googleMessage);

            }
        }

        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GoogleBroadcastReceiver.completeWakefulIntent(intent);



    }

    /**
     * Send push notification to phone notification panel
     * this method is called by push errors
     * @param msg
     */
    private void sendNotification(String msg) {
        LoggerHelper.d(
                LoggingTag.GOOGLEMESSAGE.name(),
                "starting sendNotification ", this.getClass().getSimpleName());
        //establish the notification
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, SplashScreenActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(msg)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        LoggerHelper.d(
                LoggingTag.GOOGLEMESSAGE.name(),
                "ending sendNotification ", this.getClass().getSimpleName());
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(GoogleMessage googleMessage) {
        LoggerHelper.d(
                LoggingTag.GOOGLEMESSAGE.name(),
                "starting sendNotification ", this.getClass().getSimpleName());
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, SplashScreenActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(googleMessage.getTitle())
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(googleMessage.getMessage()))
                        .setContentText(googleMessage.getMessage());

        mBuilder.setContentIntent(contentIntent);
        LoggerHelper.d(
                LoggingTag.GOOGLEMESSAGE.name(),
                "ending sendNotification ", this.getClass().getSimpleName());
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}









