package mobile.com.task;



public interface OnTaskCompleteListener {
    // Notifies about task completeness
    void onTaskComplete(AbstractTask task);
}