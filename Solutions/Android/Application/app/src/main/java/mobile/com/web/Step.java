package mobile.com.web;

import android.graphics.drawable.Drawable;
import mobile.com.activities.R;

/**
 * Created by administrator on 4/20/15.
 */
public class Step {
    public final static String PROPERTY_DISTANCE = "distance";
    public final static String PROPERTY_DURATION = "duration";
    public final static String PROPERTY_MANEUVER= "maneuver";
    public final static String PROPERTY_INSTRUCTION= "instruction";
    public String getManeuver() {
        return maneuver;
    }

    public Step(String distance, String duration, String htmlInstruction, String maneuver) {
        this.distance = distance;
        this.duration = duration;
        this.htmlInstruction = htmlInstruction;
        this.maneuver = maneuver;
    }

    public void setManeuver(String maneuver) {
        this.maneuver = maneuver;
    }

    public Step(String distance, String duration, String htmlInstruction) {
        this.distance = distance;
        this.duration = duration;
        this.htmlInstruction = htmlInstruction;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getHtmlInstruction() {
        return htmlInstruction;
    }

    public void setHtmlInstruction(String htmlInstruction) {
        this.htmlInstruction = htmlInstruction;
    }

    private String distance;
    private String duration;
    private String htmlInstruction;
    private String maneuver;
}
