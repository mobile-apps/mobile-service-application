package mobile.com.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import mobile.com.activities.MainActivity;
import mobile.com.activities.R;
import mobile.com.application.LoggingTag;
import mobile.com.listeners.EventDetailFragmentCalendarEventListener;
import mobile.com.listeners.EventDetailFragmentDrivingDirectionListener;
import mobile.com.listeners.EventDetailFragmentEmailListener;
import mobile.com.listeners.EventDetailFragmentPhoneListener;
import mobile.com.web.Event;
import mobile.com.util.LoggerHelper;

public class EventDetailFragment extends Fragment  {
    public Event getEvent() {
        return mEvent;
    }

    public void setEvent(Event mEvent) {
        this.mEvent = mEvent;
    }
    private MainActivity mMainActivity;
    private Event mEvent;

    public EventDetailFragment() {

    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mMainActivity = (MainActivity)activity;

    }
    //this method is only called once
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }


    public void drivingFragmentClicked(View v) {
        mMainActivity.launchNavigator(mEvent.getDepartment());

        //Toast.makeText(getActivity(), "ImageView clicked for the row = " + mEvent.getDepartment().getName(), Toast.LENGTH_SHORT).show();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_event_detail, container, false);
        final TextView textViewEventName = (TextView) rootView.findViewById(R.id.textViewEventName);
        final TextView textViewMessage = (TextView) rootView.findViewById(R.id.textViewMessage);
        final TextView textViewDate = (TextView) rootView.findViewById(R.id.textViewDate);
        final TextView textViewLocation = (TextView) rootView.findViewById(R.id.textViewLocation);
        final TextView textViewPhone = (TextView) rootView.findViewById(R.id.textViewPhone);
        ImageView imageView = (ImageView) rootView.findViewById(R.id.ivDepartmentIcon);
        ImageView imageViewPhoneIcon = (ImageView) rootView.findViewById(R.id.ivPhoneIcon);
        final ImageView imageViewDrivingDirectionIcon = (ImageView) rootView.findViewById(R.id.ivDepartmentIcon);
        final ImageView imageViewEmailIcon = (ImageView) rootView.findViewById(R.id.imageViewEmailIcon);
        final ImageView imageViewCalendarEventIcon = (ImageView) rootView.findViewById(R.id.ivCalendarEventIcon);

        //Phone Listener
        EventDetailFragmentPhoneListener eventDetailFragmentPhoneListener = new EventDetailFragmentPhoneListener(this);
        imageViewPhoneIcon.setOnClickListener(eventDetailFragmentPhoneListener);

        //Email Listener
        EventDetailFragmentEmailListener eventDetailFragmentEmailListener = new EventDetailFragmentEmailListener(this);
        imageViewEmailIcon.setOnClickListener(eventDetailFragmentEmailListener);

        //Driving Direction Listener
        EventDetailFragmentDrivingDirectionListener eventDetailFragmentDrivingDirectionListener = new EventDetailFragmentDrivingDirectionListener(this);
        imageViewDrivingDirectionIcon.setOnClickListener(eventDetailFragmentDrivingDirectionListener);

        //Calendar Event Listener
        EventDetailFragmentCalendarEventListener eventDetailFragmentCalendarEventListener = new EventDetailFragmentCalendarEventListener(this);
        imageViewCalendarEventIcon.setOnClickListener(eventDetailFragmentCalendarEventListener);

        if (mEvent != null) {

            if (textViewEventName != null) {
                textViewEventName.setText(mEvent.getName());
            }

            if (textViewDate != null) {
                textViewDate.setText(mEvent.formattedDate());
            }

            if (textViewMessage != null) {
                String message = mEvent.getGeneralInformation();
               textViewMessage.setText(mEvent.getGeneralInformation());
            }

            if (textViewPhone != null) {
                textViewPhone.setText(mEvent.getPhone());
            }

            if (mEvent.getDepartment() != null) {
                if (textViewLocation != null) {

                    textViewLocation.setText( mEvent.getDepartment().getLocation());
                }

                if (imageView != null) {
                    String icon =  mEvent.getDepartment().getIcon();
                    if (icon != null) {
                        String strBase64 = icon;
                        byte[] decodedString = null;
                        try {
                            decodedString = Base64.decode(strBase64, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            //todo we need a bigger image imageView.setImageBitmap(decodedByte);
                            imageView.setContentDescription(mEvent.getName());
                        } catch (Exception ex) {
                            LoggerHelper.d(
                                    LoggingTag.APPLICATION.name(),
                                    "Error  " + ex.getMessage(), this.getClass().getSimpleName());
                        }
                    }
                }

            }

        } else {
            throw new IllegalStateException("Event should not be null");
        }
       // return InputFragmentView;
        return rootView;
    }
}
