package mobile.com.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import mobile.com.activities.MainActivity;
import mobile.com.activities.R;
import mobile.com.application.LoggingTag;
import mobile.com.listeners.ContactFragmentDrivingDirectionListener;
import mobile.com.listeners.ContactFragmentEmailListener;
import mobile.com.listeners.ContactFragmentPhoneListener;
import mobile.com.web.Organization;
import mobile.com.web.service.clients.OrganizationClient;
import mobile.com.util.LoggerHelper;

public class ContactFragment extends Fragment {
    private MainActivity mMainActivity;
    private Organization mOrganization;
    private TextView mTextViewMessage = null;
    private TextView mTextViewName = null;
    private TextView mTextViewPhone = null;
    private  TextView mTextViewLocation = null;
    private TextView mTextEmail= null;
    private Organization organization;

    public Organization getOrganization() {
        return organization;
    }

    public void setLayoutValues (Organization org) {
        organization = org;
        mTextViewName.setText(org.getName());
        mTextViewPhone.setText(org.getPhone());
        mTextViewMessage.setText(org.getGeneralinformation());
        mTextViewLocation.setText(org.getLocation());
        mTextEmail.setText(org.getEmail());
    }
    public ContactFragment() {
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mMainActivity = (MainActivity)activity;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_contact, container, false);
        mTextViewMessage = (TextView) rootView.findViewById(R.id.textViewMessage);
        mTextViewName = (TextView) rootView.findViewById(R.id.textViewName);
        mTextViewPhone = (TextView) rootView.findViewById(R.id.textPhone);
        mTextEmail = (TextView) rootView.findViewById(R.id.textEmail);
        //textViewEmailLabel
        ContactFragmentEmailListener contactFragmentEmailListener = new ContactFragmentEmailListener(this);
        final ImageView imageViewEmailIcon = (ImageView) rootView.findViewById(R.id.imageViewEmailIcon);
        imageViewEmailIcon.setOnClickListener(contactFragmentEmailListener);
        final ImageView imageViewPhoneIcon = (ImageView) rootView.findViewById(R.id.ivPhoneIcon);
        ContactFragmentPhoneListener contactFragmentPhoneListener = new ContactFragmentPhoneListener(this);
        imageViewPhoneIcon.setOnClickListener(contactFragmentPhoneListener);
        mTextViewLocation = (TextView) rootView.findViewById(R.id.textViewLocation);
        final ImageView imageViewDrivingDirectionIcon = (ImageView) rootView.findViewById(R.id.ivDrivingDirectionIcon);
        ContactFragmentDrivingDirectionListener contactFragmentDrivingDirectionListener = new ContactFragmentDrivingDirectionListener(this);
        imageViewDrivingDirectionIcon.setOnClickListener(contactFragmentDrivingDirectionListener);
        new PrefetchData().execute();
        return rootView;
    }
    /*
     * Async Task to make http callå
     */
    private class PrefetchData extends AsyncTask<Void, Void, Void> {
        Organization organization;
        /** progress dialog to show user that the backup is processing. */
        private ProgressDialog dialog;
        /*

        /*
* Will make http call here This call will
* download required data
* before launching the app
*/
        @Override
        protected Void doInBackground(Void... arg0) {
            LoggerHelper.d(
                    LoggingTag.SPLASH.name(),
                    "starting doInBackground ", this.getClass().getSimpleName());
            organization = OrganizationClient.getOrg();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
            if (organization != null) {
                setLayoutValues(organization);
            }


        }


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            dialog = new ProgressDialog(mMainActivity);
            // dialog.setIndeterminateDrawable(R.drawable.splash);
            dialog.setMessage("Loading, please wait.");
            dialog.show();


        }
    }
}
