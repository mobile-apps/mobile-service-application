package mobile.com.web.service.clients;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mobile.com.application.LoggingTag;
import mobile.com.web.Department;
import mobile.com.web.Request;
import mobile.com.web.RequestType;
import mobile.com.web.URLHelper;
import mobile.com.util.LoggerHelper;

/**
 * Created by administrator on 4/20/15.
 */
public class DepartmentClient {
    /**
     * get all of the 
     * @return
     */
    public static List<Department> findAll() {
        LoggerHelper.d(
                LoggingTag.DEPARTMENT.name(),
                "starting findPodcasts ", DepartmentClient.class.getSimpleName());
        List<Department> list = new ArrayList<Department>();
        Request request = new Request();
        String fullURL = URLHelper.DEPARTMENTS.getFullURL();

        //SplashScreenActivity.this.getApplicationContext().getString(R.string.getAllCategoriesAndEvents_url);
        request.setURL(fullURL);
        request.setRequestType(RequestType.OBJECT);
        try {
            JSONObject jsonOrgObject = (JSONObject) mobile.com.web.WebServiceHelper.getHTTPGetJSonRestful(request);

            JSONArray jsonArray = jsonOrgObject.getJSONArray("departments");


            if (jsonOrgObject == null) {
                throw new IllegalStateException("Did not find json");
            }

            if (jsonArray != null && jsonArray.length() > 0) {

                for (int x = 0; x < jsonArray.length(); x++) {
                    JSONObject jsonStepObj = jsonArray.getJSONObject(x);
                    String name = jsonStepObj.getString(Department.PROPERTY_NAME);
                    String phone = jsonStepObj.getString(Department.PROPERTY_PHONE);
                    String location = jsonStepObj.getString(Department.PROPERTY_LOCATION);
                    String email = jsonStepObj.getString(Department.PROPERTY_EMAIL);
                    String generalInformation = jsonStepObj.getString(Department.PROPERTY_GENERAL_INFORMATION);
                    String icon = jsonStepObj.getString(Department.PROPERTY_ICON);

                    String latitude = jsonStepObj.getString(Department.PROPERTY_LATITUDE);
                    String longitude = jsonStepObj.getString(Department.PROPERTY_LONGITUDE);
                    String id  = jsonStepObj.getString(Department.PROPERTY_ID);
                    double longitudeDouble = Double.parseDouble(longitude);
                    double latitudeDouble = Double.parseDouble(latitude);
                    int idInt = Integer.parseInt(id);
                    Department department = new Department(location,latitudeDouble,longitudeDouble,icon,generalInformation,email,idInt,name,phone);
                   //public Department(String location, double latitude, double longitude, String icon, String generalInformation, String email, int id, String name) {
                    list.add(department);

                    }//end of for
                LoggerHelper.d(
                        LoggingTag.PODCAST.name(),
                        "ending findPodcasts ", DepartmentClient.class.getSimpleName());
                return list;
            }
        } catch(Exception ex) {
            LoggerHelper.d(
                    LoggingTag.PODCAST.name(),
                    "Error  " + ex.getMessage(), DepartmentClient.class.getSimpleName());
             return null;
        }

        return list;
    }
}
