package mobile.com.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import mobile.com.fragments.SlideFragment;

//import android.util.Log;


public class SlideActivity extends FragmentActivity {



    private Fragment contentFragment;
    SlideFragment homeFragment;

    public int getSlideCurrentPosition() {
        return slideCurrentPosition;
    }

    public void clickMe(View v) {
        startMainActivity(slideCurrentPosition);
    }
    public void clickSkip(View v) {
        mobile.com.security.SecurityManager.saveSkipSlide(this,true);
        startMainActivity(slideCurrentPosition);
    }
    public void setSlideCurrentPosition(int slideCurrentPosition) {
        this.slideCurrentPosition = slideCurrentPosition;
    }

    private int slideCurrentPosition;

    public void startMainActivity(int position) {
        Intent i = new Intent(SlideActivity.this, MainActivity.class);
        i.putExtra(MainActivity.NAME, position);
        startActivity(i);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("content")) {
                String content = savedInstanceState.getString("content");

            }
            if (fragmentManager.findFragmentByTag(SlideFragment.ARG_ITEM_ID) != null) {
                homeFragment = (SlideFragment) fragmentManager
                        .findFragmentByTag(SlideFragment.ARG_ITEM_ID);
                contentFragment = homeFragment;
            }
        } else {
            homeFragment = new SlideFragment();
            switchContent(homeFragment, SlideFragment.ARG_ITEM_ID);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (contentFragment instanceof SlideFragment) {
            outState.putString("content", SlideFragment.ARG_ITEM_ID);
        }
        super.onSaveInstanceState(outState);
    }

    public void switchContent(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        while (fragmentManager.popBackStackImmediate())
            ;

        if (fragment != null) {
            FragmentTransaction transaction = fragmentManager
                    .beginTransaction();
            transaction.replace(R.id.content_frame, fragment, tag);
            // Only ProductDetailFragment is added to the back stack.
            if (!(fragment instanceof SlideFragment)) {
                transaction.addToBackStack(tag);
            }
            transaction.commit();
            contentFragment = fragment;
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            super.onBackPressed();
        } else if (contentFragment instanceof SlideFragment
                || fm.getBackStackEntryCount() == 0) {
            finish();
        }
    }


}
