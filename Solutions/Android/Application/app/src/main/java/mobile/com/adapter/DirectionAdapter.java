package mobile.com.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.text.Html;
import java.util.List;

import mobile.com.activities.R;
import mobile.com.application.LoggingTag;
import mobile.com.web.Step;
import mobile.com.util.LoggerHelper;

/**
 * Created by wtccuser on 2/14/15.
 */
public class DirectionAdapter extends ArrayAdapter<Step>  {
    public DirectionAdapter(Context context, List<Step> steps) {
        super(context, 0, steps);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        
        // Get the data item for this position
        Step step = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_directions, parent, false);
        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.ivManeuverIcon);

        if (step.getManeuver() != null) {
            Drawable drawableIcon = convertView.getResources().getDrawable(R.drawable.transparent);
            imageView.setImageDrawable(drawableIcon);

            if (step.getManeuver().equals("turn-left")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.turn_left);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("turn-right")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.turn_right);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("slight-left")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.slight_left);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("slight-right")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.slight_right);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("route-start")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.route_start);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("route-end")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.route_end);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("ramp-left")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.ramp_left);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("ramp-right")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.ramp_right);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("fork-left")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.fork_left);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("fork-right")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.fork_right);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("turn-slight-left")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.turn_slight_left);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("turn-slight-right")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.turn_slight_right);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("turn-sharp-left")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.turn_sharp_left);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("turn-sharp-right")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.turn_sharp_right);
                imageView.setImageDrawable(drawableIcon);
            }

            //if (step.getManeuver().equals("keep-left")) {
                //drawableIcon = convertView.getResources().getDrawable(R.drawable.keep_left);
                //imageView.setImageDrawable(drawableIcon);
           //}

            //if (step.getManeuver().equals("keep-right")) {
                //drawableIcon = convertView.getResources().getDrawable(R.drawable.keep_right);
                //imageView.setImageDrawable(drawableIcon);
            //}

            if (step.getManeuver().equals("uturn-left")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.uturn_left);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("uturn-right")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.uturn_right);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("roundabout-left")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.roundabout_left);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("roundabout-right")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.roundabout_right);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("straight")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.straight);
                imageView.setImageDrawable(drawableIcon);
            }

            if (step.getManeuver().equals("merge")) {
                drawableIcon = convertView.getResources().getDrawable(R.drawable.merge);
                imageView.setImageDrawable(drawableIcon);
            }
        }

        TextView tvInstruction = (TextView) convertView.findViewById(R.id.textViewInstruction);
        tvInstruction.setText(Html.fromHtml(step.getHtmlInstruction()));

        return convertView;
    }

}