package mobile.com.listeners;

import android.view.View;

import mobile.com.activities.MainActivity;
import mobile.com.fragments.ContactFragment;
import mobile.com.web.Organization;

/**
 * Created by wtccuser on 6/29/15.
 */
public class ContactFragmentDrivingDirectionListener implements View.OnClickListener {
    private ContactFragment mContactFragment;
    public ContactFragmentDrivingDirectionListener(ContactFragment contactFragment){
        mContactFragment = contactFragment;
    }

    @Override
    public void onClick(View view) {
        if (mContactFragment != null) {
            Organization organization = mContactFragment.getOrganization();
            if (organization != null) {
                MainActivity mainActivity = (MainActivity)mContactFragment.getActivity();
                mainActivity.launchNavigator(organization.getLocation());
            }
        }

    }

}
