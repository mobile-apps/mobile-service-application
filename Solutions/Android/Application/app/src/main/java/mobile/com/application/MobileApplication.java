package mobile.com.application;


import android.content.Intent;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import mobile.com.activities.R;
import mobile.com.google.message.GoogleHelper;
import mobile.com.services.RegistrationService;
import mobile.com.util.LoggerHelper;

/**
 * Created by administrator on 8/12/14.
 * This ia the enrty class
 */

@ReportsCrashes(formKey = "dGVacG0ydVHnaNHjRjVTUTEtb3FPWGc6MQ",
        mode = ReportingInteractionMode.TOAST,
        httpMethod = org.acra.sender.HttpSender.Method.PUT,
        resToastText = R.string.crash_toast_text, // optional, displayed as soon as the crash occurs, before collecting data which can take a few seconds
        resDialogText = R.string.crash_dialog_text,
        resDialogIcon = android.R.drawable.ic_dialog_info, //optional. default is a warning sign
        resDialogTitle = R.string.crash_dialog_title, // optional. default is your application name
        resDialogCommentPrompt = R.string.crash_dialog_comment_prompt, // optional. when defined, adds a user text field input with this text resource as a label
        resDialogOkToast = R.string.crash_dialog_ok_toast // optional. displays a Toast message when the user accepts to send a report.

)
public class MobileApplication extends GDApplication {
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private Intent mServiceIntent;
    private Tracker mTracker;
    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);


            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            String trackingId = getString(R.string.ga_trackingId);
        // R.string.ga_trackingId.
          mTracker = analytics.newTracker(trackingId);
        }
        return mTracker;
    }

    /*
* Creates a new Intent to start the RSSPullService
* IntentService. Passes a URI in the
* Intent's "data" field.
*/
    private void registerInBackground() {
        mServiceIntent = new Intent(this, mobile.com.services.RegistrationService.class);
        //ths is the google push notification service

        // Starts the IntentService
        this.startService(mServiceIntent);
        // mobile.com.services.RegistrationService service = new mobile.com.services.RegistrationService(this);
        //service.st
        // secu.org.google.cloud.message.RegistrationTask task = new secu.org.google.cloud.message.RegistrationTask(this);
        //task.execute("");
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If it
     * doesn't, display a dialog that allows users to download the APK from the
     * Google Play Store or enable it in the device's system settings.
     */
    private boolean hasCheckPlayServices() {
        LoggerHelper.d(
                LoggingTag.APPLICATION.name(),
                "starting hasCheckPlayServices ", this.getClass().getSimpleName());
        int resultCode = 0;
        try {

            resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        } catch (Exception ex) {
            Log.e("error", ex.getMessage());
        }


        if (resultCode != ConnectionResult.SUCCESS) {
            String errorMessage = GooglePlayServicesUtil.getErrorString(resultCode);
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                // GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                // PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(LoggingTag.APPLICATION.name(), "This device is not supported.");
                //finish();
            }
            LoggerHelper.d(
                    LoggingTag.APPLICATION.name(),
                    "ending hasCheckPlayServices false ", this.getClass().getSimpleName());
            return false;
        } else {
            registerInBackground();

        }
        LoggerHelper.d(
                LoggingTag.APPLICATION.name(),
                "ending hasCheckPlayServices false ", this.getClass().getSimpleName());

        return true;
    }

    /**
     * this is the entry point to the application
     */
    @Override
    public void onCreate() {
        super.onCreate();
        LoggerHelper.d(
                LoggingTag.APPLICATION.name(),
                "starting onCreate ", this.getClass().getSimpleName());

        //DO

        ResourceHelper.getInstance(this);//initial

        // The following line triggers the initialization of ACRA
        //this setups our general error handling
        ACRA.init(this);
        ACRAReportSender acraReportSender = new ACRAReportSender(this);
        ACRA.getErrorReporter().setReportSender(acraReportSender);


        //todo need to display message when service is off
        boolean hasGooglePlayService = hasCheckPlayServices();
        boolean test = GoogleHelper.isGooglePlayServiceInstalled(this);
        boolean isRegistered = mobile.com.security.SecurityManager.isRegistered(this.getApplicationContext());
        LoggerHelper.d(
                LoggingTag.APPLICATION.name(),
                "isRegistered [" + isRegistered + "]", this.getClass().getSimpleName());


        if (!isRegistered) {
            //start an async web service
            LoggerHelper.d(
                    LoggingTag.APPLICATION.name(),
                    "NOT registered  start registration service", this.getClass().getSimpleName());

            Intent serviceIntent = new Intent(this, RegistrationService.class);


            // Starts the IntentService
            this.startService(serviceIntent);
            //Intent intent = new Intent(this, RegistrationActivity.class);
            // Intent intent = new Intent(this, MainActivity.class);

            //intent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // startActivity(intent);
        }
        LoggerHelper.d(
                LoggingTag.APPLICATION.name(),
                "ending onCreate ", this.getClass().getSimpleName());


    }
}
