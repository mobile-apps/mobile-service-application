package mobile.com.web;

import java.util.List;

/**
 * Created by wtccuser on 9/6/14.
 */
public class Organization {
    public final static String PROPERTY_LOCATION = "location";
    public final static String PROPERTY_LONGITUDE = "longitude";
    public final static String PROPERTY_LATITUDE = "latitude";
    public final static String PROPERTY_ICON = "image";
    public final static String PROPERTY_EMAIL = "email";
    public final static String PROPERTY_NAME = "name";
    public final static String PROPERTY_PHONE = "phone";
    public final static String PROPERTY_GENERAL_INFORMATION = "generalinformation";

    private String email;
    public Organization() {}
    public Organization(String email, String generalinformation, String location, double latitude, double longitude, String image, String name, String phone) {
        this.email = email;
        this.generalinformation = generalinformation;
        this.location = location;
        this.latitude = latitude;
        this.longitude = longitude;
        this.image = image;
        this.name = name;
        this.phone = phone;
    }

    private String generalinformation;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGeneralinformation() {
        return generalinformation;
    }

    public void setGeneralinformation(String generalinformation) {
        this.generalinformation = generalinformation;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private String location;
    private double latitude;
    private double longitude;
    private String image;


    private java.util.List<Department> list;

    public List<Department> getList() {
        return list;
    }

    public void setList(List<Department> list) {
        this.list = list;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private int id;
    private String name;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    private String phone;

}
