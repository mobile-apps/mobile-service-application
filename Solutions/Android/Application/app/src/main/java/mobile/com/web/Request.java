package mobile.com.web;


/**
 * Created by wtccuser on 9/1/14.
 */
public class Request {
    private String URL;

    public JSonImpl getJsonImpl() {
        return jsonImpl;
    }

    public void setJsonImpl(JSonImpl jsonImpl) {
        this.jsonImpl = jsonImpl;
    }

    private JSonImpl jsonImpl;


    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    private RequestType requestType;

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }


}
