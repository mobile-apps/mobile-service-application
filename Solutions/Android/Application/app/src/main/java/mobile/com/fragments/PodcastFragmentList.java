package mobile.com.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import mobile.com.activities.MainActivity;
import mobile.com.activities.R;
import mobile.com.adapter.PodcastAdapter;
import mobile.com.application.LoggingTag;
import mobile.com.listeners.PodcastDetailListener;
import mobile.com.util.LoggerHelper;
import mobile.com.web.Podcast;
import mobile.com.web.service.clients.PodcastClient;

public class PodcastFragmentList extends Fragment {
    public MainActivity getmMainActivity() {
        return mMainActivity;
    }

   //public void setmMainActivity(MainActivity mMainActivity) {
     //   this.mMainActivity = mMainActivity;
    //}

    private MainActivity mMainActivity;

    private ListView mListView;
    private TextView mTextView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //aah setRetainInstance(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        LoggerHelper.d(
                LoggingTag.PODCAST.name(),
                "starting onCreateView ", this.getClass().getSimpleName());


        View rootView = inflater.inflate(R.layout.fragment_podcast, container, false);
        mListView = (ListView)rootView.findViewById(R.id.listView);
        PodcastDetailListener podcastDetailListener = new PodcastDetailListener(mMainActivity);
        mListView.setOnItemClickListener(podcastDetailListener);
        mTextView = (TextView)rootView.findViewById(R.id.textView);
        LoggerHelper.d(
                LoggingTag.SPLASH.name(),
                "ending onCreateView ", this.getClass().getSimpleName());

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mMainActivity = (MainActivity)activity;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new PrefetchData().execute();

    }

    public void noPodcastFound() {
        String message = getString(R.string.no_podcast);
        mTextView.setText(message);
    }

    /**
     * fill podcast list
     */
    public void fillList(List<Podcast> podcasts) {
        LoggerHelper.d(
                LoggingTag.SPLASH.name(),
                "starting filllist ", this.getClass().getSimpleName());

        //get the events from memory
        PodcastAdapter podcastAdapter = new PodcastAdapter(this.getActivity(),podcasts);
        if (mListView != null) {
            mListView.setAdapter(podcastAdapter);
        }
        LoggerHelper.d(
                LoggingTag.SPLASH.name(),
                "ending fillList ", this.getClass().getSimpleName());


    }


    /**
     * get podcast list
     */

   /*
     * Async Task to make http call
     */
    private class PrefetchData extends AsyncTask<Void, Void, Void> {
      List<Podcast> podcasts;
        /** progress dialog to show user that the backup is processing. */
        private ProgressDialog dialog;
        /*
* Will make http call here This call will
* download required data
* before launching the app
*/
        @Override
        protected Void doInBackground(Void... arg0) {
            LoggerHelper.d(
                    LoggingTag.SPLASH.name(),
                    "starting doInBackground ", this.getClass().getSimpleName());
            podcasts = PodcastClient.findPodcasts();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            LoggerHelper.d(
                    LoggingTag.PODCAST.name(),
                    "starting onPostExecute ", this.getClass().getSimpleName());

            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (dialog != null && dialog.isShowing()) {
                try {
                    dialog.dismiss();
                } catch (Exception e) {
                    LoggerHelper.d(
                            LoggingTag.PODCAST.name(),
                            "Error " + e.getMessage(), this.getClass().getSimpleName());

                }
            }
            if (podcasts != null) {
                PodcastFragmentList.this.fillList(podcasts);
            } else {
                PodcastFragmentList.this.noPodcastFound();
            }


        }


        @Override
        protected void onPreExecute() {

            // TODO Auto-generated method stub
            super.onPreExecute();
            LoggerHelper.d(
                    LoggingTag.SPLASH.name(),
                    "starting onPreExecute ", this.getClass().getSimpleName());

            dialog = new ProgressDialog(mMainActivity);

            // dialog.setIndeterminateDrawable(R.drawable.splash);
            dialog.setMessage("Loading podcasts, please wait.");
            dialog.show();

            LoggerHelper.d(
                    LoggingTag.SPLASH.name(),
                    "ending onPreExecute ", this.getClass().getSimpleName());

        }
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

    }
}
