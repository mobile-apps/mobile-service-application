package mobile.com.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import mobile.com.activities.MainActivity;
import mobile.com.activities.R;
import mobile.com.adapter.EventAdapter;
import mobile.com.application.LoggingTag;
import mobile.com.application.SharedObjectManager;
import mobile.com.listeners.EventDetailListener;
import mobile.com.web.Event;
import mobile.com.util.LoggerHelper;


public class HomeFragment extends Fragment {


    private MainActivity mMainActivity;

    //this method is only called once
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mMainActivity = (MainActivity)activity;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LoggerHelper.d(
                LoggingTag.HOME.name(),
                "starting onActivityCreated ", this.getClass().getSimpleName());
        final ListView listview = (ListView) this.getActivity().findViewById(R.id.listview);
        final TextView textView = (TextView)this.getActivity().findViewById(R.id.textView);
        //get the events from memory
        final List<Event> events = (List<Event>) mobile.com.application.SharedObjectManager.getMapValue(SharedObjectManager.OBJECT_NAME.EVENTS);
        if (events == null || events.size() < 1) {
            String message = getResources().getString(R.string.no_events);
            textView.setText(message);

            LoggerHelper.e(
                    LoggingTag.HOME.name(),
                    "Error SplashScreen web service did not get events ", this.getClass().getSimpleName());
        } else {
            EventAdapter eventAdapter = new EventAdapter(this.getActivity(), events);
            listview.setAdapter(eventAdapter);
            EventDetailListener eventDetailListener = new EventDetailListener(mMainActivity);
            listview.setOnItemClickListener(eventDetailListener);

        }





        LoggerHelper.d(
                LoggingTag.HOME.name(),
                "ending onActivityCreated ", this.getClass().getSimpleName());
        // listview.setAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, list));
    }


    public HomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LoggerHelper.d(
                LoggingTag.HOME.name(),
                "starting onCreateView ", this.getClass().getSimpleName());
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        LoggerHelper.d(
                LoggingTag.HOME.name(),
                "starting onCreateView ", this.getClass().getSimpleName());
        return rootView;
    }
}
