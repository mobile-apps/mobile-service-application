package mobile.com.web.service.clients;



import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mobile.com.application.LoggingTag;
import mobile.com.application.SharedObjectManager;
import mobile.com.web.Category;
import mobile.com.web.Department;
import mobile.com.web.Event;
import mobile.com.web.Organization;
import mobile.com.web.RegisterUser;
import mobile.com.web.Request;
import mobile.com.web.RequestType;
import mobile.com.web.URLHelper;
import mobile.com.util.LoggerHelper;
/**
 * Created by wtccuser on 3/8/15.
 */
public class OrganizationClient {

    public static Organization getOrg() {
        LoggerHelper.d(
                LoggingTag.ORG.name(),
                "starting getOrg ", OrganizationClient.class.getSimpleName());

        Request request = new Request();
        String fullURL = URLHelper.ORG.getFullURL();
        Organization organization = null;
        //SplashScreenActivity.this.getApplicationContext().getString(R.string.getAllCategoriesAndEvents_url);
        request.setURL(fullURL);
        request.setRequestType(RequestType.OBJECT);
        try {
            JSONObject jsonOrgObject = (JSONObject) mobile.com.web.WebServiceHelper.getHTTPGetJSonRestful(request);


            if (jsonOrgObject == null) {
                throw new IllegalStateException("Did not find json");
            }
            String name = jsonOrgObject.getString(Organization.PROPERTY_NAME);
            String phone = jsonOrgObject.getString(Organization.PROPERTY_PHONE);
            String location = jsonOrgObject.getString(Organization.PROPERTY_LOCATION);
            String email = jsonOrgObject.getString(Organization.PROPERTY_EMAIL);
            String generalInformation = jsonOrgObject.getString(Organization.PROPERTY_GENERAL_INFORMATION);
            String image = null;
            try {
                image = jsonOrgObject.getString(Organization.PROPERTY_ICON);
            } catch (Exception ex) {
                LoggerHelper.d(
                        LoggingTag.ORG.name(),
                        "no name ", OrganizationClient.class.getSimpleName());
            }
            String latitude = jsonOrgObject.getString(Organization.PROPERTY_LATITUDE);
            String longitude = jsonOrgObject.getString(Organization.PROPERTY_LONGITUDE);

            double longitudeDouble = Double.parseDouble(longitude);
            double latitudeDouble = Double.parseDouble(latitude);
            organization = new Organization(email, generalInformation
                    , location, latitudeDouble, longitudeDouble
                    , image, name, phone);

                LoggerHelper.d(
                        LoggingTag.ORG.name(),
                        "ending getOrg ", OrganizationClient.class.getSimpleName());

        } catch(Exception ex) {
            LoggerHelper.d(
                    LoggingTag.ORG.name(),
                    "Error  " + ex.getMessage(), DepartmentClient.class.getSimpleName());
            return null;
        }

        return organization;
    }
    /**
     * get initial remove data
     * @param context
     */
    public static void initial(android.content.Context context) {
        LoggerHelper.d(
                LoggingTag.REGISTRATION.name(),
                "starting initial ", OrganizationClient.class.getSimpleName());
        String registrationId = mobile.com.security.SecurityManager.getRegistrationId(context);

        if (registrationId == null || registrationId.isEmpty()) {
            registrationId = "0";
        }

        mobile.com.web.Request request = new Request();
        String fullURL = URLHelper.EVENTS_CATEGORIES_USER.getFullURL();
        fullURL = fullURL.replace("{0}", registrationId);
        //SplashScreenActivity.this.getApplicationContext().getString(R.string.getAllCategoriesAndEvents_url);
        request.setURL(fullURL);
        request.setRequestType(RequestType.OBJECT);

        try {
            JSONObject jsonOrgObject = (JSONObject) mobile.com.web.WebServiceHelper.getHTTPGetJSonRestful(request);
            if (jsonOrgObject == null) {
                throw new IllegalStateException("Did not find json");
            }

            try {
                JSONObject jsonUserObject = jsonOrgObject.getJSONObject("registerUser");
                if (jsonUserObject != null) {
                    RegisterUser registerUser = new RegisterUser();
                    registerUser.setRegisteredUserId(Integer.parseInt(registrationId));
                    try {
                        String name = jsonUserObject.getString(RegisterUser.PROPERTY_NAME);
                        registerUser.setUsername(name);
                    } catch (Exception e){
                        LoggerHelper.d(
                                LoggingTag.SPLASH.name(),
                                e.getMessage(), OrganizationClient.class.getSimpleName());
                    }
                    try {
                        String status = jsonUserObject.getString(RegisterUser.PROPERTY_STATUS);
                        registerUser.setStatus(status);
                    } catch(Exception e) {
                        LoggerHelper.d(
                                LoggingTag.SPLASH.name(),
                                e.getMessage(), OrganizationClient.class.getSimpleName());
                    }
                    mobile.com.application.SharedObjectManager.setMapValue(SharedObjectManager.OBJECT_NAME.REGISTERUSER, registerUser);

                }
            } catch (Exception e) {
                LoggerHelper.e(
                        LoggingTag.SPLASH.name(),
                        e.getMessage(), OrganizationClient.class.getSimpleName());

            }


            //loop thru and add category
            JSONArray jsonArray = jsonOrgObject.getJSONArray("categories");
            if (jsonArray == null) {
                throw new IllegalStateException("Did not find categories array in json");
            }
            List<Category> categories = new ArrayList<Category>();
            for (int x = 0; x < jsonArray.length(); x++) {
                JSONObject jsonCategoryObj = jsonArray.getJSONObject(x);
                String name = jsonCategoryObj.getString(Category.PROPERTY_NAME);
                int categoryId = jsonCategoryObj.getInt(Category.PROPERTY_CATEGORY_ID);
                Category category = new Category(categoryId, name);
                categories.add(category);
            }
            mobile.com.application.SharedObjectManager.setMapValue(SharedObjectManager.OBJECT_NAME.CATEGORIES, categories);

            //add events
            //find events
            JSONArray jsonEventArray = jsonOrgObject.getJSONArray("events");
            //loop thru and add category
            if (jsonEventArray != null && jsonEventArray.length() > 0) {
                List<Event> events = new ArrayList<Event>();
                for (int x = 0; x < jsonEventArray.length(); x++) {
                    JSONObject jsonEventObj = jsonEventArray.getJSONObject(x);
                    String name = jsonEventObj.getString(Event.PROPERTY_NAME);
                    String phone = jsonEventObj.getString(Event.PROPERTY_PHONE);
                    long startDate = jsonEventObj.getLong(Event.PROPERTY_START_DATE);
                    Date start = new Date(startDate);
                    long endDate = jsonEventObj.getLong(Event.PROPERTY_END_DATE);
                    Date end = new Date(endDate);
                    String generalInformation = jsonEventObj.getString(Event.PROPERTY_GENERAL_INFORMATION);
                    Event event = new Event(start, end, name, generalInformation, phone);
                    //get department
                    JSONObject jsonDepartmentObject = jsonEventObj.getJSONObject(Event.PROPERTY_DEPARTMENT);
                    String departmentName = jsonDepartmentObject.getString(Department.PROPERTY_NAME);
                    String departmentGeneralInformation = jsonDepartmentObject.getString(Department.PROPERTY_GENERAL_INFORMATION);
                    String icon = null;
                    try {
                        icon = jsonDepartmentObject.getString(Department.PROPERTY_ICON);
                    }
                    catch(Exception e) {
                        LoggerHelper.e(
                                LoggingTag.SPLASH.name(),
                                e.getMessage(), OrganizationClient.class.getSimpleName());
                    }

                    //if needed remove base64 content
                    String searchStr = "base64,";
                    int base64Pos = (icon == null) ? -1 :  icon.indexOf(searchStr);
                    String newIcon = "";
                    if (base64Pos > 0) {
                        base64Pos = base64Pos + searchStr.length();
                        //remove
                        String replaceText = icon.substring(0,base64Pos);
                        newIcon = icon.replace(replaceText,"");
                        icon = newIcon;
                    }


                    String latString = jsonDepartmentObject.getString(Department.PROPERTY_LATITUDE);
                    String longString = jsonDepartmentObject.getString(Department.PROPERTY_LONGITUDE);
                    String email = jsonDepartmentObject.getString(Department.PROPERTY_EMAIL);
                    String idStr = jsonDepartmentObject.getString(Department.PROPERTY_ID);
                    String location = jsonDepartmentObject.getString(Department.PROPERTY_LOCATION);
                    String departmentPhone = jsonDepartmentObject.getString(Department.PROPERTY_PHONE);
                    int id = Integer.valueOf(idStr);
                    icon = (icon != null && icon.length() > 5) ? icon : null;
                    double latitude = (latString.equals("null")) ? 0 : Double.valueOf(latString);
                    double longitude = (longString.equals("null")) ? 0 : Double.valueOf(longString);
                    Department department = new Department(location, latitude, longitude, icon, departmentGeneralInformation, email, id, departmentName, departmentPhone);
                    event.setDepartment(department);
                    events.add(event);

                }//end of events for
                mobile.com.application.SharedObjectManager.setMapValue(SharedObjectManager.OBJECT_NAME.EVENTS, events);
                LoggerHelper.d(
                        LoggingTag.SPLASH.name(),
                        "ending doInBackground ", OrganizationClient.class.getSimpleName());
            }


        } catch (Exception e) {
            e.printStackTrace();
            LoggerHelper.e(
                    LoggingTag.SPLASH.name(),
                    "error doInBackground [" + fullURL + "] " + e.getMessage(), OrganizationClient.class.getSimpleName());
          throw new IllegalStateException(e);
        }


        }
}
