package mobile.com.location;

/**
 * Created by administrator on 4/16/15.
 */
public class GPSLocation {
    public boolean isLocationAvailable() {
        return isLocationAvailable;
    }

    public GPSLocation(boolean isLocationAvailable, boolean isNetWorkAvailable) {
        this.isLocationAvailable = isLocationAvailable;
        this.isNetWorkAvailable = isNetWorkAvailable;
    }

    public void setLocationAvailable(boolean isLocationAvailable) {
        this.isLocationAvailable = isLocationAvailable;
    }

    public boolean isNetWorkAvailable() {
        return isNetWorkAvailable;
    }

    public void setNetWorkAvailable(boolean isNetWorkAvailable) {
        this.isNetWorkAvailable = isNetWorkAvailable;
    }

    private boolean isLocationAvailable;
    private boolean isNetWorkAvailable;
    public GPSLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    private double latitude;
    private double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
