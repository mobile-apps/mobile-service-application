package mobile.com.activities.wizard.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;

import mobile.com.activities.R;
import mobile.com.application.LoggingTag;
import mobile.com.util.LoggerHelper;

public class RegistrationFragment extends Fragment {
	protected static final String ARG_KEY = "key";

	private com.tech.freak.wizardpager.ui.PageFragmentCallbacks mCallbacks;
	private String mKey;
	private Page mPage;

	protected EditText mEditTextInput;
    public void populateText(View rootView) {
        LoggerHelper.d(
                LoggingTag.WIZARD.name(),
                "start populateText ", this.getClass().getSimpleName());

        TextView textView = (TextView) rootView.findViewById(mobile.com.activities.R.id.registrationinfo);
        if (textView == null) {
            LoggerHelper.e(
                    LoggingTag.WIZARD.name(),
                    "populateText null textView", this.getClass().getSimpleName());
          return;
        }
        try {
            mobile.com.util.ComponentHelper.addHTMLToTextView(textView, getResources(), mobile.com.activities.R.raw.registration);
        } catch (Exception e) {
            LoggerHelper.e(
                    LoggingTag.WIZARD.name(),
                    "populateText ERROR " + e.getMessage(), this.getClass().getSimpleName());
            return;
        }


  }
	public static RegistrationFragment create(String key) {
		Bundle args = new Bundle();
		args.putString(ARG_KEY, key);

		RegistrationFragment f = new RegistrationFragment();
		f.setArguments(args);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		mKey = args.getString(ARG_KEY);
		mPage = mCallbacks.onGetPage(mKey);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_page_registration_info,
				container, false);
		((TextView) rootView.findViewById(android.R.id.title)).setText(mPage
			.getTitle());
        populateText(rootView);
		//mEditTextInput = (EditText) rootView.findViewById(R.id.editTextInput);
		//mEditTextInput.setText(mPage.getData().getString(Page.SIMPLE_DATA_KEY));
		return rootView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		if (!(activity instanceof com.tech.freak.wizardpager.ui.PageFragmentCallbacks)) {
			throw new ClassCastException(
					"Activity must implement PageFragmentCallbacks");
		}

		mCallbacks = (PageFragmentCallbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = null;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

	}



	@Override
	public void setMenuVisibility(boolean menuVisible) {
		super.setMenuVisibility(menuVisible);

		// In a future update to the support library, this should override
		// setUserVisibleHint
		// instead of setMenuVisibility.

	}

}
