package mobile.com.web;

import java.util.Date;

/**
 * Created by administrator on 4/14/15.
 */
public class RegisterUser {
    public final static String PROPERTY_REGISTREDUSER_ID = "registerUserId";
    public final static String PROPERTY_EMAIL = "email";
    public final static String PROPERTY_STATUS = "status";
    public final static String PROPERTY_NAME = "name";
    private Integer registeredUserId;
    private String email;
    private String username;
    private String phone;

    public Integer getRegisteredUserId() {
        return registeredUserId;
    }

    public void setRegisteredUserId(Integer registeredUserId) {
        this.registeredUserId = registeredUserId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNotificationmethod() {
        return notificationmethod;
    }

    public void setNotificationmethod(String notificationmethod) {
        this.notificationmethod = notificationmethod;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public boolean isVerified() {
        if (status != null && status.equalsIgnoreCase("verified"))  {
            return true;
        } else {
            return false;
        }
    }

    private String activationCode;
    private String status;
    private String notificationmethod;
    private java.util.Date createdDate;
    private java.util.Date lastLoginDate;
}
