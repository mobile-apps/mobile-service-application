package mobile.com.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import mobile.com.application.LoggingTag;
import mobile.com.application.SharedObjectManager;
import mobile.com.util.LoggerHelper;

public class CrashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoggerHelper.d(
                LoggingTag.CRASHREPORT.name(),
                "starting onCreate ", this.getClass().getSimpleName());
        setContentView(R.layout.activity_crash);
        TextView stackTraceTextView = (TextView) findViewById(R.id.stackTraceTextView);
        TextView logCatTextView = (TextView) findViewById(R.id.logCatTextView);

        mobile.com.web.Crash crash = (mobile.com.web.Crash)mobile.com.application.SharedObjectManager.getMapValue(SharedObjectManager.OBJECT_NAME.CRASH);
        stackTraceTextView.setText( crash.getStackTrace());
        logCatTextView.setText( crash.getLogCat());
        LoggerHelper.d(
                LoggingTag.CRASHREPORT.name(),
                "ending onCreate ", this.getClass().getSimpleName());

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_crash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
