package mobile.com.web;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by administrator on 4/20/15.
 */
public class Direction {
    public final static String PROPERTY_STARTING_ADDRESS = "startingAddress";
    public final static String PROPERTY_ENDING_ADDRESS = "endingAddress";
    public final static String PROPERTY_STEPS= "steps";

    public Direction(String startingAddress, String endingAddress) {
        this.startingAddress = startingAddress;
        this.endingAddress = endingAddress;
    }

    private String startingAddress;

    public String getStartingAddress() {
        return startingAddress;
    }

    public void setStartingAddress(String startingAddress) {
        this.startingAddress = startingAddress;
    }

    public String getEndingAddress() {
        return endingAddress;
    }

    public void setEndingAddress(String endingAddress) {
        this.endingAddress = endingAddress;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    private String endingAddress;
    private List<Step> steps = new ArrayList<Step>();

}
