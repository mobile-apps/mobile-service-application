package mobile.com.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;

import mobile.com.activities.MainActivity;
import mobile.com.activities.R;
import mobile.com.application.LoggingTag;
import mobile.com.security.SecurityManager;
import mobile.com.util.LoggerHelper;

/**
 * Created by wtccuser on 8/4/15.
 */
public class PrefsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener  {
    private MainActivity mMainActivity;
    private boolean mDisalbeSlide;
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        LoggerHelper.d(
                LoggingTag.PREFERENCE.name(),
                "starting onSharedPreferenceChanged ", PrefsFragment.class.getSimpleName());

        if (key.equals(SecurityManager.SKIP_SLIDE)) {
            mDisalbeSlide = sharedPreferences.getBoolean(key, false);
            SecurityManager.saveSkipSlide(mMainActivity,mDisalbeSlide);
           // new PrefetchData().execute();


            LoggerHelper.d(
                    LoggingTag.PREFERENCE.name(),
                    "ending onSharedPreferenceChanged ", PrefsFragment.class.getSimpleName());

        }
    }
    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen()
                .getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen()
                .getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mMainActivity = (MainActivity)activity;

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        LoggerHelper.d(
                LoggingTag.PREFERENCE.name(),
                "starting onCreate ", PrefsFragment.class.getSimpleName());

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
        // show the current value in the settings screen
        for (int i = 0; i < getPreferenceScreen().getPreferenceCount(); i++) {
            pickPreferenceObject(getPreferenceScreen().getPreference(i));
        }
        LoggerHelper.d(
                LoggingTag.PREFERENCE.name(),
                "ending onSharedPreferenceChanged ", PrefsFragment.class.getSimpleName());

    }

    /**
     * load the preferences
     * @param p
     */
    private void pickPreferenceObject(Preference p) {
        LoggerHelper.d(
                LoggingTag.PREFERENCE.name(),
                "starting pickPreferenceObject ", PrefsFragment.class.getSimpleName());

        if (p instanceof PreferenceCategory) {
            PreferenceCategory cat = (PreferenceCategory) p;
            for (int i = 0; i < cat.getPreferenceCount(); i++) {
                pickPreferenceObject(cat.getPreference(i));
            }
        } else {
            initSummary(p);
        }
        LoggerHelper.d(
                LoggingTag.PREFERENCE.name(),
                "ending pickPreferenceObject ", PrefsFragment.class.getSimpleName());

    }

    /**
     * set the slide check box
     * @param p
     */
    private void initSummary(Preference p) {
        LoggerHelper.d(
                LoggingTag.PREFERENCE.name(),
                "starting initSummary ", SecurityManager.class.getSimpleName());


        if (p instanceof  CheckBoxPreference) {
            CheckBoxPreference checkBoxPreferenceSlide = (CheckBoxPreference) p;
            boolean disableSlides = SecurityManager.skipSlides(mMainActivity);
            checkBoxPreferenceSlide.setChecked(disableSlides);
        }
        LoggerHelper.d(
                LoggingTag.PREFERENCE.name(),
                "ending initSummary ", PrefsFragment.class.getSimpleName());

        // More logic for ListPreference, etc...
    }
    /*
     * Async Task to make http call
     */
    private class PrefetchData extends AsyncTask<Void, Void, Void> {

        /** progress dialog to show user that the backup is processing. */
        private ProgressDialog dialog;
        /*
* Will make http call here This call will
* download required data
* before launching the app
*/
        @Override
        protected Void doInBackground(Void... arg0) {
            LoggerHelper.d(
                    LoggingTag.PREFERENCE.name(),
                    "starting doInBackground ", this.getClass().getSimpleName());
            SecurityManager.saveSkipSlide(mMainActivity,mDisalbeSlide);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            LoggerHelper.d(
                    LoggingTag.PREFERENCE.name(),
                    "starting onPostExecute ", this.getClass().getSimpleName());

            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (dialog != null && dialog.isShowing()) {
                try {
                    dialog.dismiss();
                } catch (Exception e) {
                    LoggerHelper.d(
                            LoggingTag.PODCAST.name(),
                            "Error " + e.getMessage(), this.getClass().getSimpleName());

                }
            }



        }


        @Override
        protected void onPreExecute() {

            // TODO Auto-generated method stub
            super.onPreExecute();
            LoggerHelper.d(
                    LoggingTag.PREFERENCE.name(),
                    "starting onPreExecute ", this.getClass().getSimpleName());

            dialog = new ProgressDialog(mMainActivity);

            // dialog.setIndeterminateDrawable(R.drawable.splash);
            dialog.setMessage("Loading podcasts, please wait.");
            dialog.show();

            LoggerHelper.d(
                    LoggingTag.SPLASH.name(),
                    "ending onPreExecute ", this.getClass().getSimpleName());

        }
    }

}
