package mobile.com.web;

/**
 * Created by administrator on 2/10/15.
 */
import org.json.JSONObject;
import java.util.List;
public class Subscriber implements JSonImpl {
    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    private List<Category> categories;

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    private List<Event> events;
    private String id;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String status;
    private String userName="";
    private String email="unknown";
    private String errorMessage;
    public final static String PROPERTY_REGISTERUSERID = "registerUserId";
    public final static String PROPERTY_STATUS = "status";
    public final static String PROPERTY_CATEGORIES = "categories";
    public final static String PROPERTY_ERRORMESSAGE = "errorMessage";
    public final static String PROPERTY_EVENTS = "eventList";
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    private int registerUserId;

    public int getRegisterUserId() {
        return registerUserId;
    }

    public void setRegisterUserId(int registerUserId) {
        this.registerUserId = registerUserId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    @Override
    public String toJSon() {
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("id", id);
            jsonObject.put("registerUserId", registerUserId);
            jsonObject.put("errorMessage", errorMessage);
            jsonObject.put("email", email);
            jsonObject.put("userName", userName);


            return jsonObject.toString();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

    }
}
