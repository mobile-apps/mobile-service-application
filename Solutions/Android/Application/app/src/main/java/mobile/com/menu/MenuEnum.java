package mobile.com.menu;

/**
 * Created by wtccuser on 6/27/15.
 */
public enum MenuEnum {
    UpcomingEvents(0), Ministries(1), Donate(2), Podcasts(3),ContactUs(4), Settings(5);

    private int numVal;
    public int getNumVal() {
        return numVal;
    }
    MenuEnum (int numVal) {
        this.numVal = numVal;
    }
}
