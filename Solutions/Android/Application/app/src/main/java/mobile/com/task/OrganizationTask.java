package mobile.com.task;

/**
 * Created by wtccuser on 9/1/14.
 */
import android.app.ProgressDialog;
import android.content.Context;

import mobile.com.activities.R;
import mobile.com.web.Request;
import mobile.com.web.RequestType;
import mobile.com.web.URLHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class OrganizationTask extends AbstractTask {
    private Context mContext;
    private ProgressDialog dialog;
    private JSONObject mJSONObject;
    public OrganizationTask(Context context) {
        mContext = context;
    }
    @Override
    protected String doInBackground(Context... params) {
        mobile.com.web.Request request = new Request();
        request.setURL(URLHelper.EVENTS_CATEGORIES.getFullURL());
        request.setRequestType(RequestType.GET);
        mJSONObject  =(JSONObject) mobile.com.web.WebServiceHelper.getHTTPGetJSonRestful(request);

        return null;
    }

    @Override
    protected void onPreExecute() {

        dialog = new ProgressDialog(mContext);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Down loading driving directions. Please wait");
        dialog.show();

        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        try {
            JSONArray organizationArray =  mJSONObject.getJSONArray("organization");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dialog.dismiss();

    }
}
