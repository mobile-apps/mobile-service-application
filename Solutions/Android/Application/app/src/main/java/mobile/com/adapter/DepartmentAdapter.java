package mobile.com.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import mobile.com.activities.R;
import mobile.com.application.LoggingTag;
import mobile.com.web.Department;
import mobile.com.util.LoggerHelper;

/**
 * Created by wtccuser on 2/14/15.
 */
public class DepartmentAdapter extends ArrayAdapter<Department>  {
   private Context mContext;
    private List<Department> mDepartments;
    public DepartmentAdapter(Context context, List<Department> departments) {
        super(context, 0, departments);
        mContext = context;
        mDepartments = departments;
    }

    /**
     * Get department view and set click image events
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Department department = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_department, parent, false);
        }
        ImageView imageViewUser = (ImageView) convertView.findViewById(R.id.ivUserIcon);
        imageViewUser.setTag(new Integer(position));
        imageViewUser.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                int rowSelected = Integer.parseInt(view.getTag().toString());
                Department selectedDepartment = mDepartments.get(rowSelected);

                //Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + selectedDepartment.getLocation()));

                //startActivity(i);
                ((mobile.com.activities.MainActivity) mContext).displaDepartmentDetailFragment(selectedDepartment);

                //Toast.makeText(mContext, "ImageView clicked for the row = " + selectedDepartment.getName(), Toast.LENGTH_SHORT).show();
            }
        });



        ImageView imageDrivingView = (ImageView) convertView.findViewById(R.id.ivDrivingDirectionIcon);
        imageDrivingView.setTag(new Integer(position));
        imageDrivingView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                int rowSelected = Integer.parseInt(view.getTag().toString());
               Department selectedDepartment = mDepartments.get(rowSelected);

                //Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + selectedDepartment.getLocation()));

                //startActivity(i);
                ((mobile.com.activities.MainActivity) mContext).launchNavigator(selectedDepartment);

               //Toast.makeText(mContext, "ImageView clicked for the row = " + selectedDepartment.getName(), Toast.LENGTH_SHORT).show();
            }
        });

        // Lookup view for data population
        TextView textViewName = (TextView) convertView.findViewById(R.id.textViewName);
        TextView textViewLocation = (TextView) convertView.findViewById(R.id.textViewLocation);
        //TextView textViewEmail = (TextView) convertView.findViewById(R.id.textViewEmail);
        // Populate the data into the template view using the data object
        textViewName.setText(department.getName());
        textViewLocation.setText(department.getLocation());
       // textViewEmail.setText(department.getEmail());
        ImageView imageView = (ImageView)convertView.findViewById(R.id.ivUserIcon);
        if (department != null) {
            String icon = department.getIcon();
            if (icon!= null) {
                //remove any bogus characters
                int positionBogus = icon.indexOf(",");
                if (positionBogus > -1) {
                    icon = icon.substring(positionBogus + 1,icon.length());
                }
                String strBase64 = icon;
                byte[] decodedString = null;
                try {
                    decodedString = Base64.decode(strBase64, Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    imageView.setImageBitmap(decodedByte);
                } catch (Exception ex) {
                    LoggerHelper.d(
                            LoggingTag.APPLICATION.name(),
                            "Error  " + ex.getMessage(), this.getClass().getSimpleName());
                }
            }
        }
      //  textViewDesc.setText(podcast.getDescription());
        //textViewDate.setText(podcast.getPublishDate());
        // Return the completed view to render on screen
        return convertView;
    }
}