package mobile.com.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import mobile.com.adapter.NavDrawerListAdapter;
import mobile.com.application.LoggingTag;
import mobile.com.menu.MenuEnum;
import mobile.com.model.NavDrawerItem;
import mobile.com.util.LoggerHelper;

public class HomeActivity extends Activity {
    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    private ListView mDrawerList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

        // nav drawer icons from resources
        navMenuIcons = getResources()
                .obtainTypedArray(R.array.nav_drawer_icons);
        mDrawerList = (ListView) findViewById(R.id.listView);

        navDrawerItems = new ArrayList<NavDrawerItem>();
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[MenuEnum.UpcomingEvents.getNumVal()], navMenuIcons.getResourceId(MenuEnum.UpcomingEvents.getNumVal(), -1)));
        // Find People
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[MenuEnum.Ministries.getNumVal()], navMenuIcons.getResourceId(MenuEnum.Ministries.getNumVal(), -1)));
        // Photos
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[MenuEnum.Donate.getNumVal()], navMenuIcons.getResourceId(MenuEnum.Donate.getNumVal(), -1)));
        // Pages
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[MenuEnum.Podcasts.getNumVal()], navMenuIcons.getResourceId(MenuEnum.Podcasts.getNumVal(), -1)));
        // What's hot, We  will add a counter here
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[MenuEnum.ContactUs.getNumVal()], navMenuIcons.getResourceId(MenuEnum.ContactUs.getNumVal(), -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[MenuEnum.Settings.getNumVal()], navMenuIcons.getResourceId(MenuEnum.Settings.getNumVal(), -1)));
// setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        try {

            getActionBar().setDisplayHomeAsUpEnabled(true);
           // getActionBar().setHomeButtonEnabled(true);
            //setTitle("My new title");
            //getSupportActionBar().setIcon(R.drawable.ic_launcher);
        } catch(Exception ex) {
            LoggerHelper.e(
                    LoggingTag.MAIN_ACTIVITY.name(),
                    "ERROR " + ex.getMessage(), this.getClass().getSimpleName());
        }
    }



    /**
     * Slide menu item click listener
     * */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            startMainActivity(position);
        }
        public void startMainActivity(int position) {
            Intent i = new Intent(HomeActivity.this, MainActivity.class);
            i.putExtra(MainActivity.NAME, position);
            startActivity(i);
        }
    }
}
