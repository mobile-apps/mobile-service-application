package mobile.com.google.message;
/**
 * Created by wtccuser on 11/11/15.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import java.util.Date;
import com.google.android.gms.gcm.GcmListenerService;

import mobile.com.activities.MainActivity;
import mobile.com.activities.R;
import mobile.com.web.Event;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
       // String message = data.getString("message");

       // Log.d(TAG, "From: " + from);
       // Log.d(TAG, "Message: " + message);

        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        sendNotification(data);
        // [END_EXCLUDE]
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(Bundle data) {
        // Sets up the Snooze and Dismiss action buttons that will appear in the
        //<editor-fold desc="parse data">

        String message = data.getString("generalInformation");
        String id = data.getString("id");
        String name = data.getString("name");
        String phone = data.getString("phone");
        String departmentName = data.getString("department");
        String endDateLong = data.getString("endDateLong");
        String startDateLong = data.getString("startDateLong");
        int idInt = 1;
        if (message == null || message.length() < 1) {
            message ="unknown message";
        }
        if (id == null || id.length() < 1) {
            id ="unknown id";
        } else {
            idInt = Integer.parseInt(id);
        }
        if (name == null || name.length() < 1) {
            name ="unknown name";
        }
        if (phone == null || phone.length() < 1) {
            phone ="unknown phone";
        }
        if (departmentName == null || departmentName.length() < 1) {
            departmentName ="unknown departmentName";
        }
        Date endDate = new Date();
        if (endDateLong == null || endDateLong.length() < 1) {
            endDateLong ="unknown endDateLong";
        } else {
            long endDTlong= Long.parseLong(endDateLong);
            endDate = new Date(endDTlong);
        }
        Date startDate = new Date();
        if (startDateLong == null || startDateLong.length() < 1) {
            startDateLong ="unknown startDateLong";
        } else {
            long startDTlong= Long.parseLong(startDateLong);
            startDate = new Date(startDTlong);
        }
       //</editor-fold>


        //<editor-fold desc="Sets up the Snooze and Dismiss action buttons">

        //<editor-fold desc="Sets result activity">

        Intent resultIntent = new Intent(this, MainActivity.class);
         // Because clicking the notification opens a new ("special") activity, there's
        // no need to create an artificial back stack.
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        //</editor-fold>
        //</editor-fold>
        long startDTlong= Long.parseLong(startDateLong);
        // Constructs the Builder object.
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setWhen(startDTlong)
                        .setContentIntent(resultPendingIntent)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(departmentName)
                        .setContentText(name)
                        .setDefaults(Notification.DEFAULT_ALL) // requires VIBRATE permission
        /*
         * Sets the big view "big text" style and supplies the
         * text (the user's reminder message) that will be displayed
         * in the detail area of the expanded notification.
         * These calls are ignored by the support library for
         * pre-4.1 devices.
         */
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(message));



        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(idInt /* ID of notification */, builder.build());
    }
}