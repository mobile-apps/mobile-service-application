package mobile.com.google.message;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * Created by wtccuser on 2/12/15.
 */
public class GoogleHelper {
    /**
     * Determine if google play is installed
     * @param context
     * @return
     */
    public static boolean isGooglePlayServiceInstalled(	android.content.Context context) {

        int val=GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        boolean playInstalled = false;
        if(val== ConnectionResult.SUCCESS)
        {
            playInstalled=true;
        }
        else
        {
            playInstalled=false;
        }
        return playInstalled;
    }
}
