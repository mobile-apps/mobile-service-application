package mobile.com.web;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import mobile.com.application.LoggingTag;
import mobile.com.util.LoggerHelper;

/**
 * Created by wtccuser on 9/1/14.
 */
public class WebServiceHelper {
    private static int readTimeOut = 10000;
    private static int connectionTimeOut = 15000;

    /**
     * Get Json Post request
     *
     * @param request
     * @return
     */
    public static JSONObject getHTTPPostJSonRestful(Request request) {
        LoggerHelper.d(
                LoggingTag.WEBSERVICE.name(),
                "starting getHTTPPostJSonRestful ", WebServiceHelper.class.getName());

        JSONObject jsonObject = null;
        JSONArray jsonArray = null;

        String jsonStr = request.getJsonImpl().toJSon();
        LoggerHelper.d(
                LoggingTag.WEBSERVICE.name(),
                "json " + jsonStr, WebServiceHelper.class.getName());



        HttpURLConnection conn = null;
        InputStream is = null;
        try {
            URL url = new URL(request.getURL());
             conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-type", "application/json");
            conn.setReadTimeout(readTimeOut /* milliseconds */);
            conn.setConnectTimeout(connectionTimeOut /* milliseconds */);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.connect();
            OutputStream os = conn.getOutputStream();
            os.write(jsonStr.getBytes());
            os.flush();

            // Starts the query
            //conn.connect();
            int response = conn.getResponseCode();
            if(response == HttpURLConnection.HTTP_OK){
                // Convert the InputStream into a string
                is = conn.getInputStream();
                String contentAsString = streamToString(is);

                jsonObject = new JSONObject(contentAsString);

            } else {
                Log.e(WebServiceHelper.class.toString(), "Failed invoke web service ["
                        +  request.getURL()
                        + "] code [" + response + "]");
                throw new IllegalStateException("error calling " + request.getURL() + " code " + response);

            }



        } catch (Exception e) {
            Log.e(WebServiceHelper.class.toString(), "Failed invoke web service ["
                    +  request.getURL()
                    + "][" + e.getMessage() + "]");
            throw new IllegalStateException("error calling " + request.getURL() + " error " + e.getMessage());


        } finally {
            if (is != null && conn != null) {
                try {
                    is.close();
                    conn.disconnect();

                } catch (Exception ex) {
                    Log.e(WebServiceHelper.class.toString(), "Failed invoke web service");
                    throw new IllegalStateException("error calling " + request.getURL() + " error" + ex.getMessage());

                }
            }
        }

        return jsonObject;


    }

    /**
     * Convert stream to string
     *
     * @param in
     * @return
     * @throws IOException
     */
    static String streamToString(InputStream in) throws IOException {
        StringBuilder out = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        for (String line = br.readLine(); line != null; line = br.readLine())
            out.append(line);
        br.close();
        return out.toString();
    }

    /**
     * get http response from  web service
     *
     * @param request
     * @return
     */
    public static Object getHTTPGetJSonRestful(Request request) {
        LoggerHelper.d(
                LoggingTag.WEBSERVICE.name(),
                "starting getHTTPPostJSonRestful ", WebServiceHelper.class.getName());

        JSONObject jsonObject = null;
        JSONArray jsonArray = null;

        InputStream is = null;
        HttpURLConnection conn = null;
        try {
            URL url = new URL(request.getURL());
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(readTimeOut /* milliseconds */);
            conn.setConnectTimeout(connectionTimeOut /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            // Convert the InputStream into a string
            is = conn.getInputStream();
            String contentAsString = streamToString(is);
            if (request.getRequestType().equals(RequestType.ARRAY)) {
                jsonArray = new JSONArray(contentAsString);
            } else {
                jsonObject = new JSONObject(contentAsString);
            }


        } catch (Exception e) {
            Log.e(WebServiceHelper.class.toString(), "Failed invoke web service");
            throw new IllegalStateException("error calling " + request.getURL() + " error" + e.getMessage());


        } finally {
            if (is != null && conn != null) {
                try {
                    is.close();
                    conn.disconnect();
                } catch (Exception ex) {
                    Log.e(WebServiceHelper.class.toString(), "Failed invoke web service");
                    throw new IllegalStateException("error calling " + request.getURL() + " error" + ex.getMessage());

                }
            }
        }
        if (request.getRequestType().equals(RequestType.ARRAY)) {
            return jsonArray;
        } else {
            return jsonObject;
        }

    }


}
