package mobile.com.listeners;

import android.view.View;

import mobile.com.activities.MainActivity;
import mobile.com.fragments.DepartmentDetailFragment;
import mobile.com.web.Department;

/**
 * Created by wtccuser on 6/29/15.
 */
public class DepartmentFragmentDrivingDirectionListener implements View.OnClickListener {
    private DepartmentDetailFragment mDepartmentDetailFragment;
    public DepartmentFragmentDrivingDirectionListener(DepartmentDetailFragment departmentDetailFragment){
        mDepartmentDetailFragment = departmentDetailFragment;
    }

    @Override
    public void onClick(View view) {
        if (mDepartmentDetailFragment != null) {
            Department department = mDepartmentDetailFragment.getDepartment();
            if (department != null) {
                MainActivity mainActivity = (MainActivity)mDepartmentDetailFragment.getActivity();
                mainActivity.launchNavigator(department);
            }
        }

    }

}
