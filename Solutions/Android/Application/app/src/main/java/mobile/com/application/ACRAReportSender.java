package mobile.com.application;

import android.content.Context;

import org.acra.ReportField;
import org.acra.collector.CrashReportData;
import org.acra.sender.ReportSender;
import org.acra.sender.ReportSenderException;

import mobile.com.web.Crash;
import mobile.com.util.LoggerHelper;

/**
 * Created by wtccuser on 2/15/15.
 */
public class ACRAReportSender implements ReportSender {
    private Context mContext = null;

    public ACRAReportSender(Context context) {
        mContext = context;
    }

    @Override
    public void send(CrashReportData report) throws ReportSenderException {
        // Iterate over the CrashReportData instance and do whatever
        // you need with each pair of ReportField key / String value
        LoggerHelper.d(
                LoggingTag.CRASHREPORT.name(),
                "starting send", this.getClass().getSimpleName());
        //get values from report map
        String stackTrace = report.getProperty(ReportField.STACK_TRACE);
        String logCat = report.getProperty(ReportField.LOGCAT);
        String androidVersion = report.getProperty(ReportField.ANDROID_VERSION);
        String appVersionCode = report.getProperty(ReportField.APP_VERSION_CODE);
        String appVersionName = report.getProperty(ReportField.APP_VERSION_NAME);
        String reportId = report.getProperty(ReportField.REPORT_ID);
        String phoneModel = report.getProperty(ReportField.PHONE_MODEL);
        String brand = report.getProperty(ReportField.BRAND);
        String deviceId = report.getProperty(ReportField.DEVICE_ID);
        String packageName = report.getProperty(ReportField.PACKAGE_NAME);

        Crash crash;
        crash = new Crash(logCat, androidVersion
                ,appVersionCode, reportId,
                appVersionName, phoneModel,
                brand, deviceId, packageName,
                stackTrace);

        //call webservice to log crash report
        mobile.com.web.service.clients.CrashClient.remoteLogCrashReport(crash);

    }
}
