package mobile.com.application;
import android.content.Context;
/**
 * Created by wtccuser on 3/6/15.
 */
public class ResourceHelper {
    private static ResourceHelper instance;

    public Context getContext() {
        return mContext;
    }



    private Context mContext;

    public ResourceHelper(Context context) {
        mContext = context;
    }

    public static ResourceHelper getInstance(Context context) {
        if  (instance == null) {
            instance  = new ResourceHelper(context);
        }
        return instance;
    }

    public static ResourceHelper getInstance() {
        return instance;
    }



}
