package mobile.com.helpers;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.DateFormat;

/**
 * Created by wtccuser on 2/14/15.
 */
public class DateHelper {

    public static String getDefaultFormatDate(Date date){
        //formatting Date with time information
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, d MMM, hh:mm a");
        String dateStr = dateFormat.format(date);
        //dateStr = DateFormat.getTimeInstance(DateFormat.MEDIUM).format(date);
        return dateStr;
    }
}
