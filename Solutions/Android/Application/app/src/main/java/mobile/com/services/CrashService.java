package mobile.com.services;

import android.app.IntentService;
import android.content.Intent;

import mobile.com.application.LoggingTag;
import mobile.com.web.Crash;
import mobile.com.util.LoggerHelper;

/**
 * Created by wtccuser on 7/5/15.
 */
public class CrashService extends IntentService {
   public CrashService() {
        super("CrashService");

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        LoggerHelper.d(
                LoggingTag.GOOGLEMESSAGE.name(),
                "starting onHandleIntent ", this.getClass().getSimpleName());
        Crash crash = (Crash)intent.getSerializableExtra(Crash.CRASH_NAME);
        mobile.com.web.service.clients.CrashClient.remoteLogCrashReport(crash);

    }
}
