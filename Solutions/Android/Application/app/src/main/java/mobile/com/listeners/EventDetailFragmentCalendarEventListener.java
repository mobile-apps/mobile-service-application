package mobile.com.listeners;


import android.content.Intent;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.view.View;

import java.util.Calendar;
import java.util.GregorianCalendar;

import mobile.com.activities.MainActivity;
import mobile.com.fragments.EventDetailFragment;
import mobile.com.web.Event;

/**
 * Created by HopeBenziger on 7/3/15.
 */
public class EventDetailFragmentCalendarEventListener implements View.OnClickListener {
    private EventDetailFragment mEventDetailFragment;
    public EventDetailFragmentCalendarEventListener(EventDetailFragment eventDetailFragment){
        mEventDetailFragment = eventDetailFragment;
    }

    @Override
    public void onClick(View view) {
        if (mEventDetailFragment != null) {
            Event event = mEventDetailFragment.getEvent();
            if (event != null) {
                MainActivity mainActivity = (MainActivity)mEventDetailFragment.getActivity();
                //mainActivity.launchPhone(event.getPhone());

                Intent calIntent = new Intent(Intent.ACTION_INSERT);
                calIntent.setType("vnd.android.cursor.item/event");


                calIntent.putExtra(Events.TITLE, event.getName());
                calIntent.putExtra(Events.EVENT_LOCATION, event.getLocation());
                calIntent.putExtra(Events.DESCRIPTION, event.getGeneralInformation());

                GregorianCalendar gregorianCalendarStartDate = new GregorianCalendar();
                gregorianCalendarStartDate.setTime(event.getStartDate());
                GregorianCalendar gregorianCalendarEndDate = new GregorianCalendar();
                gregorianCalendarEndDate.setTime(event.getEndDate());

                //taking a timestamp apart using default timezone.
                int year = gregorianCalendarStartDate.get( Calendar.YEAR );
                int month = gregorianCalendarStartDate.get( Calendar.MONTH ) + 1;
                int day = gregorianCalendarStartDate.get( Calendar.DAY_OF_MONTH );
                int hour24 = gregorianCalendarEndDate.get( Calendar.HOUR_OF_DAY );
                int hour12 = gregorianCalendarEndDate.get( Calendar.HOUR );
                int amIs0OrPmIs1 = gregorianCalendarEndDate.get( Calendar.AM_PM );
                int minute = gregorianCalendarEndDate.get( Calendar.MINUTE );
                int second = gregorianCalendarEndDate.get( Calendar.SECOND );

                // building a timestamp using default TimeZone.
                GregorianCalendar stamp = new GregorianCalendar();
                stamp.clear();
                stamp.set( year, month-1, day, hour24, minute, second );
                long endTimestamp = stamp.getTimeInMillis();

                calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                        gregorianCalendarStartDate.getTimeInMillis());
                calIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
                        endTimestamp);

                mainActivity.startActivity(calIntent);
            }
        }

    }

}