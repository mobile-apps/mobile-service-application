package mobile.com.listeners;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import mobile.com.activities.MainActivity;
import mobile.com.application.LoggingTag;
import mobile.com.web.Event;
import mobile.com.util.LoggerHelper;
//
/**
 * Created by administrator on 3/12/15.
 */
public class EventDetailListener implements OnItemClickListener {
    private MainActivity mMainActivity;
    public EventDetailListener(MainActivity mainActivity) {
        mMainActivity = mainActivity;

    }
    @Override
    public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
        LoggerHelper.d(
                LoggingTag.EVENT.name(),
                "start onItemClick ", this.getClass().getSimpleName());

        if (adapter == null) {
            LoggerHelper.e(
                    LoggingTag.EVENT.name(),
                    "null adapter ", this.getClass().getSimpleName());
            throw new IllegalStateException("null adapter");
        }



        Event value = (Event)adapter.getItemAtPosition(position);
        if (value == null) {
            LoggerHelper.e(
                    LoggingTag.EVENT.name(),
                    "null event ", this.getClass().getSimpleName());
            throw new IllegalStateException("null event");
        }
        mMainActivity.displayEventDetailFragment(value);

    }
}
