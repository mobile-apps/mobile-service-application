package mobile.com.ui;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import mobile.com.activities.R;

/**
 * Created by wtccuser on 5/30/15.
 */
public class SlideDownHelper {

    public static void slide_down(Context ctx, View v){
        Animation a = AnimationUtils.loadAnimation(ctx, R.anim.slide_down);
        if(a != null) {
            a.reset();
        }
            if(v != null){
                v.clearAnimation();
                v.startAnimation(a);

            }

        }
    public static void slide_up(Context ctx, View v){
        Animation a = AnimationUtils.loadAnimation(ctx, R.anim.slide_down);
        if(a != null) {
            a.reset();
        }
        if(v != null){
            v.clearAnimation();
            v.startAnimation(a);

        }

    }

    }
