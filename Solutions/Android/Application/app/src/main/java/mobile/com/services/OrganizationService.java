package mobile.com.services;

import android.app.IntentService;
import android.content.Intent;

import mobile.com.activities.R;
import mobile.com.application.SharedObjectManager;
import mobile.com.web.Request;
import mobile.com.web.RequestType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobile.com.web.*;

/**
 * BugSenseHandler.initAndStartSession(MyActivity.this, "d5cb2b58");
 * Created by wtccuser on 9/6/14.
 */

public class OrganizationService extends IntentService {

    public OrganizationService() {
        super("OrganizationService");

    }
    @Override
    protected void onHandleIntent(Intent intent) {
        mobile.com.web.Request request = new Request();
        request.setURL(URLHelper.EVENTS_CATEGORIES.getFullURL());
        request.setRequestType(RequestType.GET);
        JSONObject jSONObject  =(JSONObject) mobile.com.web.WebServiceHelper.getHTTPGetJSonRestful(request);
        java.util.List<Organization> organizationList = new ArrayList<Organization>();
        try {
            JSONArray organizationArray =  jSONObject.getJSONArray("organizations");
            for (int i = 0; i < organizationArray.length(); i++) {
                JSONObject rowOrg = organizationArray.getJSONObject(i);

                String name = rowOrg.getString("name");
                Organization organization = new Organization();
                organization.setName(name);
                JSONArray departmentArray =  rowOrg.getJSONArray("departments");
                java.util.List<Department> departmentList = new ArrayList<Department>();
                for (int x = 0; x < departmentArray.length(); x++) {
                    JSONObject rowDept = departmentArray.getJSONObject(x);
                    String nameDept = rowDept.getString("name");

                    Department department = null;
                    department.setName(nameDept);
                    departmentList.add(department);

                }

                organization.setList(departmentList);
                organizationList.add(organization);

            }//end of for (int i=0
            mobile.com.application.SharedObjectManager.setMapValue(SharedObjectManager.OBJECT_NAME.ORG,organizationList);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
