package mobile.com.services;

/**
 * Created by administrator on 8/22/14.
 * <service android:name="mobile.com.services.CompleteRegistrationService" />
 */

import android.app.IntentService;
import android.content.Intent;

import mobile.com.application.LoggingTag;
import mobile.com.google.message.GoogleHelper;
import mobile.com.util.LoggerHelper;


public class CompleteRegistrationService extends IntentService {
    /**
     * Complete registration
     */
    private final static String GOOGLE_PROJECT_ID = "142883741177";


    public CompleteRegistrationService() {
        super("CompleteRegistrationService");

    }

    /**
     * call the complete registration web serive
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        LoggerHelper.d(
                LoggingTag.REGISTRATION.name(),
                "starting onHandleIntent ", this.getClass().getSimpleName());
        boolean isGoogleServiceInstalled = GoogleHelper.isGooglePlayServiceInstalled(this.getBaseContext());
        if (isGoogleServiceInstalled) {
            mobile.com.web.service.clients.RegistrationClient.recordRegistration();
        }

    }
}
