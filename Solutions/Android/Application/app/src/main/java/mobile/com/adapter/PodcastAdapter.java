package mobile.com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import mobile.com.activities.R;
import mobile.com.web.Podcast;

/**
 * Created by wtccuser on 2/14/15.
 */
public class PodcastAdapter extends ArrayAdapter<Podcast>  {
    public PodcastAdapter(Context context, List<Podcast> podcasts) {
        super(context, 0, podcasts);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Podcast podcast = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_podcast, parent, false);
        }



        // Lookup view for data population
        TextView textViewTitle = (TextView) convertView.findViewById(R.id.textViewTitle);
        TextView textViewDesc = (TextView) convertView.findViewById(R.id.textViewDesc);
       // TextView textViewDate = (TextView) convertView.findViewById(R.id.textViewDate);
        // Populate the data into the template view using the data object
        textViewTitle.setText(podcast.getTitle());
        textViewDesc.setText(podcast.getDescription());
        //textViewDate.setText(podcast.getPublishDate());
        // Return the completed view to render on screen
        return convertView;
    }
}