package mobile.com.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import mobile.com.activities.R;
import mobile.com.application.SharedObjectManager;

public class ExceptionFragment extends Fragment {
private String mErrorMessage;
    public ExceptionFragment() {
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mErrorMessage = savedInstanceState.getString(SharedObjectManager.ERROR_MESSGE);
    }
//String
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_crash, container, false);

        final ImageView thumbnailView = (ImageView)rootView.findViewById(R.id.thumbnail_view);
        TextView messageView = (TextView) rootView.findViewById(R.id.message_view);
        String text = getString(R.string.error_general_message);
        final String showError = getString(R.string.show_error);
        final String hideError = getString(R.string.hide_error);
        final TextView txt_help_gest = (TextView) rootView.findViewById(R.id.txt_help_gest);
        thumbnailView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txt_help_gest.isShown()) {
                    // mobile.com.ui.SlideDownHelper.slide_down(rootView,txt_help_gest);
                    txt_help_gest.setVisibility(View.GONE);


                } else {
                    txt_help_gest.setVisibility(View.VISIBLE);

                }



            }
        });


        // hide until its title is clicked
        txt_help_gest.setVisibility(View.GONE);
        if (mErrorMessage != null) {
            txt_help_gest.setText(mErrorMessage);
        }

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        mobile.com.ui.FlowTextHelper.tryFlowText(text, thumbnailView, messageView, display);


        return rootView;
    }
}
