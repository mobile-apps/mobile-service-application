package mobile.com.activities;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import mobile.com.application.SharedObjectManager;

public class ErrorActivity extends Activity {
    private String mErrorMessage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //get error message
        Bundle bundle = getIntent().getExtras();
        mErrorMessage = bundle.getString(SharedObjectManager.ERROR_MESSGE);
        setContentView(R.layout.activity_error);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_error, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        TextView txt_help_gest;
        ErrorActivity mErrorActivity;
        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            mErrorActivity = (ErrorActivity)activity;

        }
        public PlaceholderFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_error, container, false);
            final ImageView thumbnailView = (ImageView)rootView.findViewById(R.id.thumbnail_view);
            TextView messageView = (TextView) rootView.findViewById(R.id.message_view);
            String text = getString(R.string.error_general_message);
            final String showError = getString(R.string.show_error);
            final String hideError = getString(R.string.hide_error);
            txt_help_gest = (TextView) rootView.findViewById(R.id.txt_help_gest);
            thumbnailView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(txt_help_gest.isShown()) {
                       // mobile.com.ui.SlideDownHelper.slide_down(rootView,txt_help_gest);
                        txt_help_gest.setVisibility(View.GONE);


                    } else {
                        txt_help_gest.setVisibility(View.VISIBLE);

                    }



                }
            });


            // hide until its title is clicked
            txt_help_gest.setVisibility(View.GONE);
            if (mErrorActivity.mErrorMessage != null) {
                txt_help_gest.setText(mErrorActivity.mErrorMessage);
            }

            Display display = getActivity().getWindowManager().getDefaultDisplay();
            mobile.com.ui.FlowTextHelper.tryFlowText(text, thumbnailView, messageView, display);
            return rootView;
        }
    }
}
