package mobile.com.task;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
/**
 * Created by wtccuser on 8/30/14.
 */
public abstract class AbstractTask extends AsyncTask<Context, Integer, String> {


    private Boolean mResult;
    private String mProgressMessage;
    private IProgressTracker mProgressTracker;

    int myProgress;

    private ProgressDialog dialog;

    protected AbstractTask() {
    }

    public AbstractTask(Context context) {


        // Initialise initial pre-execute message
       // mProgressMessage = resources
            //    .getString(mobile.com.activities.R.string.task_starting);
    }



    @Override
    protected abstract String doInBackground(Context... params) ;

    // -- gets called just before thread begins
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    // -- called as soon as doInBackground method completes
    // -- notice that the third param gets passed to this method
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        // TODO Auto-generated method stub

    }

    /* UI Thread */
    public void setProgressTracker(IProgressTracker progressTracker) {
        // Attach to progress tracker
        mProgressTracker = progressTracker;
        // Initialise progress tracker with current task state
        if (mProgressTracker != null) {
            mProgressTracker.onProgress(mProgressMessage);
            if (mResult != null) {
                mProgressTracker.onComplete();
            }
        }
    }

}
