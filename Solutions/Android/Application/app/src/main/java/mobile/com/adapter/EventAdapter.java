package mobile.com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.ImageView;

import mobile.com.application.LoggingTag;
import mobile.com.web.Event;
import mobile.com.activities.R;
import mobile.com.util.LoggerHelper;

import java.util.List;
import android.util.Base64;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;

/**
 * Created by wtccuser on 2/14/15.
 */
public class EventAdapter extends ArrayAdapter<Event>  {
    public EventAdapter(Context context, List<Event> events) {
        super(context, 0, events);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Event event = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_events, parent, false);
        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.ivUserIcon);

        mobile.com.web.Department department = event.getDepartment();
        if (department != null) {
            String icon = department.getIcon();
            if (icon!= null) {
                String strBase64 = icon;
                byte[] decodedString = null;
                try {
                    decodedString = Base64.decode(strBase64, Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    imageView.setImageBitmap(decodedByte);
                } catch (Exception ex) {
                    LoggerHelper.d(
                            LoggingTag.APPLICATION.name(),
                            "Error  " + ex.getMessage(), this.getClass().getSimpleName());
                }
            }
        }

        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.textViewName);
        TextView tvDesc = (TextView) convertView.findViewById(R.id.textViewDesc);
        TextView tvDate = (TextView) convertView.findViewById(R.id.textViewDate);

        // Populate the data into the template view using the data object
        tvName.setText(event.getName());
        tvDate.setText(event.formattedDate());
        tvDesc.setText(event.getGeneralInformation());

        // Return the completed view to render on screen
        return convertView;
    }
}