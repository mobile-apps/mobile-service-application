package mobile.com.web;

/**
 * Created by wtccuser on 5/10/15.
 */
public class Podcast implements java.io.Serializable {
    public final static String PROPERTY_TITLE = "title";
    public final static String PROPERTY_DESCRIPTION = "description";
    public final static String PROPERTY_PUBLISH_DATE = "pubDate";
    public final static String PROPERTY_MEDIA_LOCATION = "mediaLocation";

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMediaLocation() {
        return mediaLocation;
    }

    public void setMediaLocation(String mediaLocation) {
        this.mediaLocation = mediaLocation;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public Podcast(String title, String description, String mediaLocation, String publishDate) {
        this.title = title;
        this.description = description;
        this.mediaLocation = mediaLocation;
        this.publishDate = publishDate;
    }

    private String title;
    private String description;
    private String mediaLocation;
    private String publishDate;


}
