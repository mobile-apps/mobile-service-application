package mobile.com.activities;


import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

import mobile.com.activities.wizard.WizardActivity;
import mobile.com.application.LoggingTag;
import mobile.com.application.SharedObjectManager;
import mobile.com.listeners.GPSListener;
import mobile.com.location.GPSLocation;
import mobile.com.services.CrashService;
import mobile.com.util.LoggerHelper;
import mobile.com.web.Crash;
import mobile.com.web.RegisterUser;
import mobile.com.web.service.clients.OrganizationClient;

/**
 * Created by wtccuser on 2/14/15.
 */
public class SplashScreenActivity extends Activity {

    String now_playing, earned;

    String getUsername() {
        AccountManager manager = AccountManager.get(this);
        android.accounts.Account[] accounts = manager.getAccountsByType("com.google");
        List<String> possibleEmails = new LinkedList<String>();

        for (android.accounts.Account account : accounts) {
            // TODO: Check possibleEmail against an email regex or treat
            // account.name as an email address only for certain account.type values.
            possibleEmails.add(account.name);
        }

        if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
            String email = possibleEmails.get(0);
            String[] parts = email.split("@");

            if (parts.length > 1)
                return parts[0];
        }
        return null;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // String name = getUsername();
        LoggerHelper.d(
                LoggingTag.SPLASH.name(),
                "starting onCreate ", this.getClass().getSimpleName());
        setContentView(R.layout.activity_splash);
        /**
         * get geo location  of the phone using Android location Manager
         */
        LocationManager locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);

        //boolean isProvided = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) //is the hardward gps enable
        //boolean isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) is network enabled

        GPSLocation gpsLocation = new GPSLocation(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                , locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
        SharedObjectManager.setMapValue(SharedObjectManager.OBJECT_NAME.GPSLOCATION, gpsLocation);

        mobile.com.listeners.GPSListener locationListener = new GPSListener();
        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 5000, 10, locationListener);//continue to update my latitude and longitude

		/*
         * Showing splashscreen while making network calls to download necessary
		 * data before launching the app Will use AsyncTask to make http call
		 */
        new PrefetchData().execute();
        LoggerHelper.d(
                LoggingTag.SPLASH.name(),
                "ending onCreate ", this.getClass().getSimpleName());

    }

    /*
     * Async Task to make http call
     */
    private class PrefetchData extends AsyncTask<Void, Void, Void> {
        private boolean hasEncounterApplicationError = false;
        /** progress dialog to show user that the backup is processing. */
        private ProgressDialog dialog;
        private String errorMessage;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // before making http calls
            LoggerHelper.d(
                    LoggingTag.SPLASH.name(),
                    "starting onPreExecute ", this.getClass().getSimpleName());
           // dialog = new ProgressDialog(context);
            Log.e("JSON", "Pre execute");
            dialog = new ProgressDialog(SplashScreenActivity.this);
           // dialog.setIndeterminateDrawable(R.drawable.splash);
            dialog.setMessage("Loading events, please wait.");
            dialog.show();
        }

        /*
 * Will make http call here This call will
 * download required data
 * before launching the app
 */
        @Override
        protected Void doInBackground(Void... arg0) {
            LoggerHelper.d(
                    LoggingTag.SPLASH.name(),
                    "starting doInBackground ", this.getClass().getSimpleName());
            //call to get our categories and events
            try {
                OrganizationClient.initial(SplashScreenActivity.this);
            } catch (Exception e) {
                hasEncounterApplicationError = true;
                //try to log remote
                Intent serviceIntent = new Intent(SplashScreenActivity.this, CrashService.class);
                Crash crash = new Crash(e);
                serviceIntent.putExtra(Crash.CRASH_NAME,crash);
                startService(serviceIntent);
                errorMessage = e.getMessage();
            }

            return null;
        }

        /*
        http call is completed
         */
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            LoggerHelper.e(
                    LoggingTag.SPLASH.name(),
                    "starting onPostExecute ", this.getClass().getSimpleName());
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
            //check for application errors

            if (hasEncounterApplicationError) {
                Intent i = new Intent(SplashScreenActivity.this, ErrorActivity.class);
                i.putExtra(SharedObjectManager.ERROR_MESSGE, errorMessage);
                startActivity(i);
                //try to log remote
               // Intent serviceIntent = new Intent(getAc, CrashService.class);


                // Starts the IntentService
               // this.startService(serviceIntent);
                finish();
                return;
            }
            // After completing http call
            // will close this activity and lauch main activity
            boolean isRegister = mobile.com.security.SecurityManager.isRegistered(SplashScreenActivity.this);
            RegisterUser registerUser = (RegisterUser) SharedObjectManager.getMapValue(SharedObjectManager.OBJECT_NAME.REGISTERUSER);

            boolean hasRegistered = false;
            if (registerUser != null && registerUser.isVerified())  {
                hasRegistered = true;
            }

            if (isRegister) {
                hasRegistered = true;
            }

            if (hasRegistered) {
                //Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
               // Intent i = new Intent(SplashScreenActivity.this, SlideActivity.class);
                Intent intent = null;
               if (mobile.com.security.SecurityManager.skipSlides(SplashScreenActivity.this)) {
                   intent  = new Intent(SplashScreenActivity.this, HomeActivity.class);
               } else {
                   intent  = new Intent(SplashScreenActivity.this, SlideActivity.class);
               }

                startActivity(intent);
            } else {
                Intent i = new Intent(SplashScreenActivity.this, WizardActivity.class);
                i.putExtra("now_playing", now_playing);
                i.putExtra("earned", earned);
                startActivity(i);
            }

            // close this activity
            LoggerHelper.e(
                    LoggingTag.SPLASH.name(),
                    "ending onPostExecute ", this.getClass().getSimpleName());
            finish();
        }

    }

}

