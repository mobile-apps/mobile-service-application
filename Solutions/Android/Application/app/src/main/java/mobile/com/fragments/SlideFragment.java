package mobile.com.fragments;

/**
 * Created by wtccuser on 7/19/15.
 */

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.com.activities.R;
import mobile.com.activities.SlideActivity;
import mobile.com.adapter.ImageSlideAdapter;
import mobile.com.slideshow.CirclePageIndicator;
import mobile.com.slideshow.PageIndicator;
import mobile.com.web.Slide;

public class SlideFragment extends Fragment {

    public static final String ARG_ITEM_ID = "home_fragment";

    private static final long ANIM_VIEWPAGER_DELAY = 5000;
    private static final long ANIM_VIEWPAGER_DELAY_USER_VIEW = 10000;
    // UI References
    private ViewPager mViewPager;
    TextView imgNameTxt;
    Button mButton;
    PageIndicator mIndicator;
    AlertDialog alertDialog;
    List<Slide> mSlides = new ArrayList<Slide>();
    boolean stopSliding = false;
    String message;

    private Runnable animateViewPager;
   private Handler handler;


    FragmentActivity activity;
    private void findViewById(View view) {
        mViewPager = (ViewPager) view.findViewById(R.id.view_pager);
        mIndicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        imgNameTxt = (TextView) view.findViewById(R.id.img_name);
        mButton = (Button)view.findViewById(R.id.button);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
    }
    @Override
    public void onResume() {
        if (mSlides == null) {
            createSlides();
        } else {
            mViewPager.setAdapter(new ImageSlideAdapter(activity, mSlides,
                    SlideFragment.this));

            mIndicator.setViewPager(mViewPager);
            mButton.setText(""
                    + ((Slide) mSlides.get(mViewPager.getCurrentItem()))
                    .getButtonText());
            imgNameTxt.setText(""
                    + ((Slide) mSlides.get(mViewPager.getCurrentItem()))
                    .getName());
            runnable(mSlides.size());
            //Re-run callback
            handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY);
        }
        super.onResume();
    }
    /**
     * Create slides from resource file slide.xml
     */
    private void createSlides() {
        String[] names =getResources().getStringArray(R.array.slide_items);
        String[] icons =getResources().getStringArray(R.array.slide_icons);
        String[] button_text =getResources().getStringArray(R.array.slide_button);
        //slide_button

        //int[] iconsInt = getResources().getIntArray(R.array.slide_icons);
        //TypedArray test = getResources().obtainTypedArray(R.array.slide_icons);
        for (int x = 0; x < names.length; x++) {
            Slide slide = new Slide();

            //slide.setImageUrl(iconsInt[x]);
            slide.setName(names[x]);
            slide.setButtonText(button_text[x]);
            slide.setId(x);
            mSlides.add(slide);
        }
    }

    /**
     *
     * @param size
     */
    public void runnable(final int size) {
        handler = new Handler();
        animateViewPager = new Runnable() {
            public void run() {
                if (!stopSliding) {
                    if (mViewPager.getCurrentItem() == size - 1) {
                        mViewPager.setCurrentItem(0);
                    } else {
                        mViewPager.setCurrentItem(
                                mViewPager.getCurrentItem() + 1, true);
                    }
                    handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY);
                }
            }
        };
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dash, container, false);
        findViewById(view);
        createSlides();

        mIndicator.setOnPageChangeListener(new PageChangeListener());
        mViewPager.setOnPageChangeListener(new PageChangeListener());
        mViewPager.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction()) {

                    case MotionEvent.ACTION_CANCEL:
                        break;

                    case MotionEvent.ACTION_UP:
                        // calls when touch release on ViewPager

                        if (mSlides != null && mSlides.size() != 0) {
                            stopSliding = false;
                            runnable(mSlides.size());
                            handler.postDelayed(animateViewPager,
                                    ANIM_VIEWPAGER_DELAY_USER_VIEW);
                        }
                        break;

                    case MotionEvent.ACTION_MOVE:
                        // calls when ViewPager touch
                        if (handler != null && stopSliding == false) {
                            stopSliding = true;
                            handler.removeCallbacks(animateViewPager);
                        }
                        break;
                }
                return false;
            }
        });

        return view;
    }


    private class PageChangeListener implements OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int state) {
            if (state == ViewPager.SCROLL_STATE_IDLE) {
                if (mSlides != null) {
                    mButton.setText(""
                            + ((Slide) mSlides.get(mViewPager
                            .getCurrentItem())).getButtonText());
                    imgNameTxt.setText(""
                            + ((Slide) mSlides.get(mViewPager
                            .getCurrentItem())).getName());
                    SlideActivity slideActivity = (SlideActivity)activity;
                    slideActivity.setSlideCurrentPosition(((Slide) mSlides.get(mViewPager
                            .getCurrentItem())).getId());
                }
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageSelected(int arg0) {
        }

    }


}
