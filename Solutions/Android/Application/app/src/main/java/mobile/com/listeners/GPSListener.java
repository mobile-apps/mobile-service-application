package mobile.com.listeners;

/**
 * Created by administrator on 4/16/15.
 */
import android.location.LocationListener;
import android.location.Location;
import android.os.Bundle;
import mobile.com.application.SharedObjectManager;
public class GPSListener implements LocationListener {



    @Override
    public void onLocationChanged(Location loc) {
      //store changed latitude and longitude
       if (loc != null) {
           mobile.com.location.GPSLocation gpsLocation = (mobile.com.location.GPSLocation)SharedObjectManager.getMapValue(SharedObjectManager.OBJECT_NAME.GPSLOCATION);
          // loc.getLatitude() get the SDK latitude and longitude
           gpsLocation.setLatitude(loc.getLatitude());
           gpsLocation.setLongitude(loc.getLongitude());
           //store the new location in memory
           SharedObjectManager.setMapValue(SharedObjectManager.OBJECT_NAME.GPSLOCATION, gpsLocation);
       }
    }
    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}
}
