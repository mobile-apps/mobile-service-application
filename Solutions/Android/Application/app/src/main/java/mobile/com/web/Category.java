package mobile.com.web;

/**
 * Created by administrator on 2/11/15.
 */
public class Category {
    public final static String PROPERTY_CATEGORY_ID = "categoryId";
    public final static String PROPERTY_NAME = "name";

    public Category(int categoryId, String name) {
        this.categoryId = categoryId;
        this.name = name;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
    private int categoryId;
}
