package mobile.com.web;

/**
 * Created by administrator on 2/13/15.
 */

import java.io.Serializable;
import java.util.Date;

import mobile.com.helpers.DateHelper;

public class Event implements Serializable {
    public final static String PROPERTY_START_DATE = "startDateLong";
    public final static String PROPERTY_END_DATE = "endDateLong";
    public final static String PROPERTY_DEPARTMENT = "department";
    public final static String PROPERTY_LOCATION = "location";
    public final static String PROPERTY_NAME = "name";
    public final static String PROPERTY_PHONE = "phone";
    public final static String PROPERTY_GENERAL_INFORMATION = "generalInformation";
    public final static String PROPERTY_OBJECT_NAME = "event";

    private Department department;
    private Date startDate;
    private Date endDate;
    private String name;
    private String phone;
    private String generalInformation;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    private String location;

    public Department getDepartment() {
        return department;
    }
    public void setDepartment(Department department) {
        this.department = department;
    }

    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getGeneralInformation() {
        return generalInformation;
    }
    public void setGeneralInformation(String generalInformation) {
        this.generalInformation = generalInformation;
    }

    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String formattedDate() {
        String formatted =DateHelper.getDefaultFormatDate(this.startDate);
        return formatted;
    }

    public Event(Date startDate, Date endDate, String name, String generalInformation, String phone) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.name = name;
        this.generalInformation = generalInformation;
        this.phone = phone;
    }
}
