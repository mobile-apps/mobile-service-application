package mobile.com.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

import mobile.com.adapter.NavDrawerListAdapter;
import mobile.com.application.LoggingTag;
import mobile.com.application.MobileApplication;
import mobile.com.fragments.ContactFragment;
import mobile.com.fragments.DepartmentDetailFragment;
import mobile.com.fragments.DepartmentsFragmentList;
import mobile.com.fragments.DonateFragment;
import mobile.com.fragments.DrivingDirectionFragment;
import mobile.com.fragments.EventDetailFragment;
import mobile.com.fragments.HomeFragment;
import mobile.com.fragments.PodcastDetailFragment;
import mobile.com.fragments.PodcastFragmentList;
import mobile.com.fragments.PrefsFragment;
import mobile.com.menu.MenuEnum;
import mobile.com.model.NavDrawerItem;
import mobile.com.util.LoggerHelper;
import mobile.com.web.Department;
import mobile.com.web.Podcast;

public class MainActivity extends Activity {
    //..android.support.v7.app.ActionBarActivity
    public static final String NAME = "slide";
    private int mCurrentFragmentPosition;
    private MenuItem mReturnMenuItem;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private EventDetailFragment mEventDetailFragment;
    private DepartmentDetailFragment mDepartmentDetailFragment;
    private PodcastDetailFragment mPodcastDetailFragment;
    private Podcast mPodcast;
    private mobile.com.web.Event mEvent;
    private Department mDepartment;

    // nav drawer title
    private CharSequence mDrawerTitle;

    // used to store app title
    private CharSequence mTitle;

    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;

    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //setThreadPolicy is a static method
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskWrites()
                    .penaltyLog() //Logs a message to LogCat
                    .penaltyDialog() //Shows a dialog
                    .build());

            //VmPolicy
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectActivityLeaks()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .build());
        }

        //- See more at: http://www.javabeat.net/strictmode-android-1/#sthash.CipSHnay.dpuf
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LoggerHelper.d(
                LoggingTag.MAIN_ACTIVITY.name(),
                "starting onCreateView ", this.getClass().getSimpleName());
        // The Action Bar is a window feature. The feature must be requested
        // before setting a content view. Normally this is set automatically
        // by your Activity's theme in your manifest. The provided system
        // theme Theme.WithActionBar enables this for you. Use it as you would
        // use Theme.NoTitleBar. You can add an Action Bar to your own themes
        // by adding the element <item name="android:windowActionBar">true</item>
        // to your style definition.
        //getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        // setRetainInstance(true);
        mTitle = mDrawerTitle = getTitle();

        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

        // nav drawer icons from resources
        navMenuIcons = getResources()
                .obtainTypedArray(R.array.nav_drawer_icons);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

        navDrawerItems = new ArrayList<NavDrawerItem>();

        // adding nav drawer items to array
        // Home

        navDrawerItems.add(new NavDrawerItem(navMenuTitles[MenuEnum.UpcomingEvents.getNumVal()], navMenuIcons.getResourceId(MenuEnum.UpcomingEvents.getNumVal(), -1)));
        // Find People
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[MenuEnum.Ministries.getNumVal()], navMenuIcons.getResourceId(MenuEnum.Ministries.getNumVal(), -1)));
        // Photos
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[MenuEnum.Donate.getNumVal()], navMenuIcons.getResourceId(MenuEnum.Donate.getNumVal(), -1)));
        // Pages
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[MenuEnum.Podcasts.getNumVal()], navMenuIcons.getResourceId(MenuEnum.Podcasts.getNumVal(), -1)));
        // What's hot, We  will add a counter here
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[MenuEnum.ContactUs.getNumVal()], navMenuIcons.getResourceId(MenuEnum.ContactUs.getNumVal(), -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[MenuEnum.Settings.getNumVal()], navMenuIcons.getResourceId(MenuEnum.Settings.getNumVal(), -1)));


        // Recycle the typed array
        navMenuIcons.recycle();

        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);

        // enabling action bar app icon and behaving it as toggle button

        try {

            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setHomeButtonEnabled(true);
        } catch (Exception ex) {
            LoggerHelper.e(
                    LoggingTag.MAIN_ACTIVITY.name(),
                    "ERROR " + ex.getMessage(), this.getClass().getSimpleName());
        }

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            // on first time display view for first nav item
            Intent mIntent = getIntent();
            int intValue = mIntent.getIntExtra(NAME, -99);
            if (intValue == -99) {
                //this means not initial was passed in by the SlideActivity
                displayView(MenuEnum.UpcomingEvents.getNumVal());
            } else {
                displayView(intValue);
            }
        }
        LoggerHelper.d(
                LoggingTag.MAIN_ACTIVITY.name(),
                "ending onCreateView ", this.getClass().getSimpleName());
    }

    /**
     * Slide menu item click listener
     */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            displayView(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {

            case R.id.action_return:
                displayView(mCurrentFragmentPosition);

                return true;
            // action w
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* *
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
        //todo boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        mReturnMenuItem = menu.findItem(R.id.action_return);
        //todo menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    public void returnToPodcast(View v) {
        displayView(5);
    }

    //call phone number
    public void imageClick(View view) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        String number = "911 389 0567";
        callIntent.setData(Uri.parse("tel:" + Uri.encode(number)));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(callIntent);
    }

    /**
     * @param phone
     */
    public void launchPhone(String phone) {
        if (phone != null && !phone.isEmpty()) {
            try {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);

                callIntent.setData(Uri.parse("tel:" + Uri.encode(phone)));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                this.startActivity(callIntent);
            } catch (Exception ex) {
                LoggerHelper.e(
                        LoggingTag.PHONE.name(),
                        "Error " + ex.getMessage(), this.getClass().getSimpleName());

            }
        }

    }

    /**
     * get driving directions
     *
     * @param v
     */
    public void drivingClicked(View v) {
        //mMainActivity.launchNavigator(mEvent.getDepartment());
        if (mEventDetailFragment != null) {
            mEventDetailFragment.drivingFragmentClicked(v);
        }
        //Toast.makeText(this, "ImageView clicked for the row = " , Toast.LENGTH_SHORT).show();
    }

    /**
     * launch email client
     *
     * @param emailTo
     */
    public void launchEmailClient(String emailTo) {
        if (emailTo != null && !emailTo.isEmpty()) {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", emailTo, null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
            startActivity(Intent.createChooser(emailIntent, "Send email..."));
        }
    }

    public void displayReturnMenuItem(boolean display) {
        //invalidateOptionsMenu();
        if (mReturnMenuItem != null) {
            mReturnMenuItem.setVisible(display);
        }
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mEventDetailFragment != null) {
            mEventDetailFragment.setEvent(mEvent);
        }
    }

    /**
     * open navigator
     *
     * @param location
     */
    public void launchNavigator(String location) {
        LoggerHelper.d(
                LoggingTag.LOCATION.name(),
                "starting launchNavigator ", this.getClass().getSimpleName());

        //if failure open webview
        try {

            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + location));
            startActivity(i);
        } catch (Exception ex) {
            LoggerHelper.e(
                    LoggingTag.LOCATION.name(),
                    "Error " + ex.getMessage(), this.getClass().getSimpleName());

        }
    }

    /**
     * open the app navigation
     *
     * @param department
     */
    public void launchNavigator(Department department) {
        LoggerHelper.d(
                LoggingTag.LOCATION.name(),
                "starting  launchNavigator ", this.getClass().getSimpleName());

        try {

            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + department.getLocation()));
            startActivity(i);
        } catch (Exception ex) {
            try {

                displayDrivingDirectionFragment(department);
            } catch (Exception e) {
                LoggerHelper.e(
                        LoggingTag.LOCATION.name(),
                        "Error " + ex.getMessage(), this.getClass().getSimpleName());

            }
        }


    }

    /**
     * display driving directions
     *
     * @param department
     */
    public void displayDrivingDirectionFragment(Department department) {
        LoggerHelper.d(
                LoggingTag.MAIN_ACTIVITY.name(),
                "starting displayDrivingDirectionFragment ", this.getClass().getSimpleName());
        displayReturnMenuItem(true);
        DrivingDirectionFragment drivingDirectionFragment = new DrivingDirectionFragment();

        drivingDirectionFragment.setDepartment(department);
        if (drivingDirectionFragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, drivingDirectionFragment).commit();

            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(MenuEnum.Ministries.getNumVal(), true);
            mDrawerList.setSelection(MenuEnum.Ministries.getNumVal());
            setTitle(navMenuTitles[MenuEnum.Ministries.getNumVal()]);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
        LoggerHelper.d(
                LoggingTag.MAIN_ACTIVITY.name(),
                "ending displayDrivingDirectionFragment ", this.getClass().getSimpleName());
    }

    /**
     * Display podcast
     *
     * @param podcast
     */
    public void displayPodcastDetailFragment(Podcast podcast) {
        LoggerHelper.d(
                LoggingTag.MAIN_ACTIVITY.name(),
                "starting displayPodcastDetailFragment ", this.getClass().getSimpleName());
        displayReturnMenuItem(true);
        mPodcastDetailFragment = new PodcastDetailFragment();
        mPodcast = podcast;
        mPodcastDetailFragment.setPodcast(podcast);
        if (mPodcastDetailFragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, mPodcastDetailFragment).commit();

            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(MenuEnum.Podcasts.getNumVal(), true);
            mDrawerList.setSelection(MenuEnum.Podcasts.getNumVal());
            setTitle(navMenuTitles[MenuEnum.Podcasts.getNumVal()]);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
        LoggerHelper.d(
                LoggingTag.MAIN_ACTIVITY.name(),
                "ending displayPodcastDetailFragment ", this.getClass().getSimpleName());
    }

    /***
     * Displays the selected event details
     */
    public void displayEventDetailFragment(mobile.com.web.Event event) {
        LoggerHelper.d(
                LoggingTag.MAIN_ACTIVITY.name(),
                "starting displayEventDetailFragment ", this.getClass().getSimpleName());
        displayReturnMenuItem(true);
        mEventDetailFragment = new EventDetailFragment();
        mEvent = event;
        mEventDetailFragment.setEvent(mEvent);
        if (mEventDetailFragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, mEventDetailFragment).commit();

            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(MenuEnum.UpcomingEvents.getNumVal(), true);
            mDrawerList.setSelection(MenuEnum.UpcomingEvents.getNumVal());
            setTitle(navMenuTitles[MenuEnum.UpcomingEvents.getNumVal()]);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
        LoggerHelper.d(
                LoggingTag.MAIN_ACTIVITY.name(),
                "ending displayEventDetailFragment ", this.getClass().getSimpleName());
    }

    /**
     * display department details
     *
     * @param department
     */
    public void displaDepartmentDetailFragment(Department department) {
        LoggerHelper.d(
                LoggingTag.MAIN_ACTIVITY.name(),
                "starting displayEventDetailFragment ", this.getClass().getSimpleName());
        displayReturnMenuItem(true);
        mDepartmentDetailFragment = new DepartmentDetailFragment();
        mDepartment = department;
        mDepartmentDetailFragment.setDepartment(mDepartment);
        if (mDepartmentDetailFragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, mDepartmentDetailFragment).commit();

            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(MenuEnum.Ministries.getNumVal(), true);
            mDrawerList.setSelection(MenuEnum.Ministries.getNumVal());
            setTitle(navMenuTitles[MenuEnum.Ministries.getNumVal()]);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
        LoggerHelper.d(
                LoggingTag.MAIN_ACTIVITY.name(),
                "ending displayEventDetailFragment ", this.getClass().getSimpleName());
    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     */
    private void displayView(int position) {
        LoggerHelper.d(
                LoggingTag.MAIN_ACTIVITY.name(),
                "starting displayView ", this.getClass().getSimpleName());
        // Obtain the shared Tracker instance.
        MobileApplication application = (MobileApplication) getApplication();
        Tracker tracker = application.getDefaultTracker();

        mCurrentFragmentPosition = position;
        displayReturnMenuItem(false);
        // update the main content by replacing fragments
        Fragment fragment = null;
        switch (position) {
            case 0:
                HomeFragment homeFragment = new HomeFragment();
                // homeFragment.setMainActivity(this);
                tracker.setScreenName("Image~" + HomeFragment.class.getName());
                tracker.send(new HitBuilders.ScreenViewBuilder().build());
                fragment = homeFragment;
                break;
            case 1:
                fragment = new DepartmentsFragmentList();
                tracker.setScreenName("Image~" + DepartmentsFragmentList.class.getName());
                tracker.send(new HitBuilders.ScreenViewBuilder().build());
                break;
            case 2:
                fragment = new DonateFragment();
                tracker.setScreenName("Image~" + DonateFragment.class.getName());
                tracker.send(new HitBuilders.ScreenViewBuilder().build());
                break;

            case 4:
                fragment = new ContactFragment();
                tracker.setScreenName("Image~" + ContactFragment.class.getName());
                tracker.send(new HitBuilders.ScreenViewBuilder().build());
                break;
            case 3:
                PodcastFragmentList podcastFragmentList = new PodcastFragmentList();
                tracker.setScreenName("Image~" + PodcastFragmentList.class.getName());
                tracker.send(new HitBuilders.ScreenViewBuilder().build());

                fragment = podcastFragmentList;
                break;
            case 5:
                tracker.setScreenName("Image~" + PrefsFragment.class.getName());
                tracker.send(new HitBuilders.ScreenViewBuilder().build());
                fragment = new PrefsFragment();
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();

            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            try {
                setTitle(navMenuTitles[position]);
            } catch (Exception ex) {
                //todo
            }
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
        LoggerHelper.d(
                LoggingTag.MAIN_ACTIVITY.name(),
                "ending displayView ", this.getClass().getSimpleName());
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        try {
            getActionBar().setTitle(mTitle);
        } catch (Exception ex) {
            LoggerHelper.e(
                    LoggingTag.MAIN_ACTIVITY.name(),
                    "error setTitle " + ex.getMessage(), this.getClass().getSimpleName());
        }
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        LoggerHelper.d(
                LoggingTag.MAIN_ACTIVITY.name(),
                "starting onPostCreate ", this.getClass().getSimpleName());
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
        LoggerHelper.d(
                LoggingTag.MAIN_ACTIVITY.name(),
                "ending onPostCreate ", this.getClass().getSimpleName());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        LoggerHelper.d(
                LoggingTag.MAIN_ACTIVITY.name(),
                "starting onConfigurationChanged ", this.getClass().getSimpleName());
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toogle
        mDrawerToggle.onConfigurationChanged(newConfig);
        LoggerHelper.d(
                LoggingTag.MAIN_ACTIVITY.name(),
                "ending onConfigurationChanged ", this.getClass().getSimpleName());
    }

}
