package mobile.com.web.service.clients;

import com.tech.freak.wizardpager.model.ReviewItem;

import org.json.JSONObject;

import java.util.List;

import mobile.com.application.LoggingTag;
import mobile.com.application.SharedObjectManager;
import mobile.com.util.LoggerHelper;
import mobile.com.web.Request;
import mobile.com.web.RequestType;
import mobile.com.web.Subscriber;
import mobile.com.web.URLHelper;

/**
 * Created by wtccuser on 7/5/15.
 */
public class RegistrationClient {
    /**
     * record the user registration
     */
    public static void recordRegistration() {
        LoggerHelper.d(
                LoggingTag.REGISTRATION.name(),
                "starting onHandleIntent ", RegistrationClient.class.getSimpleName());

        try {

            //call our service to create a google subscriber row
            Request request = new Request();
            String fullURL = URLHelper.UPDATE_SUBSCRIBER.getFullURL();

            request.setURL(fullURL);
            // get subscriber out of memory
            Subscriber subscriber = (Subscriber)
                    mobile.com.application.SharedObjectManager.getMapValue(SharedObjectManager.OBJECT_NAME.SUBSCRIBER);
            ;
            //get reviewed items out of memory
            List<ReviewItem> reviewItems = (List<ReviewItem>) mobile.com.application.SharedObjectManager.getMapValue(SharedObjectManager.OBJECT_NAME.REVIEWEDITEMS);

            //loop thru review items and pull off user name, email, and categories
            for (ReviewItem reviewItem : reviewItems) {

                if (reviewItem.getTitle().equalsIgnoreCase("Your name")) {
                    subscriber.setUserName(reviewItem.getDisplayValue());
                }
                if (reviewItem.getTitle().equalsIgnoreCase("Your email")) {
                    subscriber.setEmail(reviewItem.getDisplayValue());
                }

            }
         request.setJsonImpl(subscriber);
            request.setRequestType(RequestType.POST);
            LoggerHelper.d(
                    LoggingTag.REGISTRATION.name(),
                    "attempt to complete registration ", RegistrationClient.class.getSimpleName());

            JSONObject jsonObj = mobile.com.web.WebServiceHelper.getHTTPPostJSonRestful(request);
            LoggerHelper.d(
                    LoggingTag.REGISTRATION.name(),
                    "return from registration restful service ", RegistrationClient.class.getSimpleName());


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
