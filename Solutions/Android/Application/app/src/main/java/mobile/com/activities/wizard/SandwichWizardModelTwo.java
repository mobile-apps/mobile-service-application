/*
 * Copyright 2012 Roman Nurik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mobile.com.activities.wizard;

import android.content.Context;

import com.tech.freak.wizardpager.model.AbstractWizardModel;
import com.tech.freak.wizardpager.model.PageList;

import java.util.ArrayList;
import java.util.List;

import mobile.com.activities.wizard.model.CustomerInfoPage;
import mobile.com.activities.wizard.model.IntroductionInfoPage;
import mobile.com.application.LoggingTag;
import mobile.com.application.SharedObjectManager;
import mobile.com.util.LoggerHelper;
import mobile.com.web.Category;
import mobile.com.web.Subscriber;

public class SandwichWizardModelTwo extends AbstractWizardModel {
    public SandwichWizardModelTwo(Context context) {
        super(context);
    }

    @Override
    protected PageList onNewRootPageList() {
        LoggerHelper.d(
                LoggingTag.WIZARD.name(),
                "starting onNewRootPageList ", this.getClass().getSimpleName());

        PageList pages = new PageList();
        mobile.com.activities.wizard.model.IntroductionInfoPage registrationPage = new IntroductionInfoPage(this,"Registration");
        pages.add(registrationPage);

        mobile.com.activities.wizard.model.CustomerInfoPage customerInfoPage = new CustomerInfoPage(this,"Your Information");
        customerInfoPage.setRequired(true);
        pages.add(customerInfoPage);
        com.tech.freak.wizardpager.model.MultipleFixedChoicePage pageCategory = new com.tech.freak.wizardpager.model.MultipleFixedChoicePage(this, "Ministries");
        Subscriber subscriber = (Subscriber) SharedObjectManager.getMapValue(SharedObjectManager.OBJECT_NAME.SUBSCRIBER);
        //get a list of categories returned from the web service
        List<Category> categories = null;
        categories = (List<Category>)mobile.com.application.SharedObjectManager.getMapValue(SharedObjectManager.OBJECT_NAME.CATEGORIES);

        if (categories != null) {
            List<String> categoryNames = new ArrayList<String>();

            for (Category category : categories) {
                categoryNames.add(category.getName());
                //categoryPage.addCategory(category.getName());
            }//end of for
            //add multiple category choices




            pageCategory.setChoices(categoryNames.toArray(new String[categoryNames.size()]));
            pages.add(pageCategory);
            pageCategory.setRequired(true);




        }


        return pages;

    }
}
