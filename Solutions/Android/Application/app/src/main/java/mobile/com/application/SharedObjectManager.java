package mobile.com.application;

/**
 * Created by administrator on 2/17/15.
 * The purpose of the class is to pass objects
 * between activities, services, application, and etci
 */

import java.util.HashMap;
import java.util.Map;

public class SharedObjectManager {
    public static final String ERROR_MESSGE = "ERRORMESSGE";
    /*The enumerations values used the the static map*/
    public enum OBJECT_NAME {
        CATEGORIES, CRASH, EVENTS, GPSLOCATION, ORG, REGISTERUSER,REVIEWEDITEMS,SUBSCRIBER, DIRECTIONS,ISPENDING
    }

    /*Create the static map used to passed objects*/
    private static Map<OBJECT_NAME, Object> mMap = new HashMap<OBJECT_NAME, Object>();

    /*
    place value in map- used for different activities
    */
    public static void setMapValue(OBJECT_NAME name, Object object) {
        mMap.put(name, object);
    }

    /*
    get value from map
     */
    public static Object getMapValue(OBJECT_NAME name) {
        return mMap.get(name);
    }

    /*
     remove value from map
    */
    public static void removeMapValue(OBJECT_NAME name) {
        mMap.remove(name);

    }
}
