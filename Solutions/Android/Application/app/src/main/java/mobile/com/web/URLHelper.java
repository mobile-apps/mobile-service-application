package mobile.com.web;

import mobile.com.activities.R;
import mobile.com.application.LoggingTag;
import mobile.com.application.ResourceHelper;
import mobile.com.util.LoggerHelper;

/**
 * Created by wtccuser on 3/3/15.
 */
public enum URLHelper {
    CATEGORIES(ResourceHelper.getInstance().getContext().getString(R.string.url_base)
            ,ResourceHelper.getInstance().getContext().getString(R.string.url_getcategories))
    ,CRASH(ResourceHelper.getInstance().getContext().getString(R.string.url_base)
    ,ResourceHelper.getInstance().getContext().getString(R.string.url_createcrash))
    ,CREATE_SUBSCRIBER(ResourceHelper.getInstance().getContext().getString(R.string.url_base)
            ,ResourceHelper.getInstance().getContext().getString(R.string.url_createsubscriber))
    ,FIND_DRIVING_DIRECTIONS(ResourceHelper.getInstance().getContext().getString(R.string.url_base)
            ,ResourceHelper.getInstance().getContext().getString(R.string.url_findDrivingDirections))
    ,EVENTS_CATEGORIES(ResourceHelper.getInstance().getContext().getString(R.string.url_base)
            ,ResourceHelper.getInstance().getContext().getString(R.string.url_getAllCategoriesAndEvents))
    ,EVENTS_CATEGORIES_USER(ResourceHelper.getInstance().getContext().getString(R.string.url_base)
            ,ResourceHelper.getInstance().getContext().getString(R.string.url_getAllCategoriesEventsUser))
    ,DEPARTMENTS(ResourceHelper.getInstance().getContext().getString(R.string.url_base)
            ,ResourceHelper.getInstance().getContext().getString(R.string.url_getAllDepartments))
    ,MOBILE_MAP(ResourceHelper.getInstance().getContext().getString(R.string.url_base)
            ,ResourceHelper.getInstance().getContext().getString(R.string.url_mobile_map))
    ,ORG(ResourceHelper.getInstance().getContext().getString(R.string.url_base)
            ,ResourceHelper.getInstance().getContext().getString(R.string.url_org))
    ,PODCAST(ResourceHelper.getInstance().getContext().getString(R.string.url_base)
            ,ResourceHelper.getInstance().getContext().getString(R.string.url_getAllPodcasts))
    ,UPDATE_SUBSCRIBER(ResourceHelper.getInstance().getContext().getString(R.string.url_base)
            ,ResourceHelper.getInstance().getContext().getString(R.string.url_updatesubscriber))


    ;

    public String getUrlAddess() {
        return urlAddess;
    }

    public String getFullURL () {
        return baseURL + urlAddess;
    }

    URLHelper(String baseURL, String urlAddess) {
        LoggerHelper.d(
                LoggingTag.URL.name(),
                "starting URLHelper  " + this.getFullURL(), this.getClass().getSimpleName());

        this.urlAddess = urlAddess;
        this.baseURL = baseURL;
    }

    public void setUrlAddess(String urlAddess) {
        this.urlAddess = urlAddess;
    }

    private String urlAddess;
    private String baseURL;

}
