package mobile.com.services;

/**
 * Created by administrator on 8/22/14.
 * <service android:name="mobile.com.services.RegistrationService" />
 */

import android.app.IntentService;
import android.content.Intent;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONObject;

import mobile.com.application.LoggingTag;
import mobile.com.application.SharedObjectManager;
import mobile.com.google.message.GoogleHelper;
import mobile.com.util.LoggerHelper;
import mobile.com.web.Request;
import mobile.com.web.RequestType;
import mobile.com.web.Subscriber;
import mobile.com.web.URLHelper;


public class  RegistrationService extends IntentService {
    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     * Project name = MobileApplication
     * GOOGLE_PROJECT_ID = "shining-relic-675";
     * mobileapps7777@gmail.com
     * mobile-apps = password
     */
    private final static String GOOGLE_PROJECT_ID = "142883741177";


    public RegistrationService() {
        super("RegistrationService");

    }

    /**
     * registration for google key
     * The IntentService calls this method from the default worker thread with
     * the intent that started the service. When this method returns, IntentService
     * stops the service, as appropriate.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        LoggerHelper.d(
                LoggingTag.GOOGLEMESSAGE.name(),
                "starting onHandleIntent ", this.getClass().getSimpleName());

        GoogleCloudMessaging googleCloudMessaging = GoogleCloudMessaging.getInstance(this.getApplicationContext());

        try {

            //check if play store service installed
           boolean isGoogleServiceInstalled= GoogleHelper.isGooglePlayServiceInstalled(this.getBaseContext());
            if (!isGoogleServiceInstalled) {
                mobile.com.security.SecurityManager.saveRegisteredId(this.getApplicationContext(), "9999");
                return;
            }
            String subscriberId = googleCloudMessaging.register(GOOGLE_PROJECT_ID);
            LoggerHelper.d(
                    LoggingTag.GOOGLEMESSAGE.name(),
                    "return from onHandleIntent ", this.getClass().getSimpleName());

            mobile.com.security.SecurityManager.saveGoogleRegistration(this.getApplicationContext(), subscriberId);
            //call our service to create a google subscriber row
            mobile.com.web.Request request = new Request();
            String fullURL = URLHelper.CREATE_SUBSCRIBER.getFullURL();
            request.setURL(fullURL);
            //subscriber
            Subscriber subscriber = new Subscriber();

            subscriber.setId(subscriberId);
            request.setJsonImpl(subscriber);
            request.setRequestType(RequestType.POST);
            LoggerHelper.d(
                    LoggingTag.SUBSCRIBER.name(),
                    "attempt to get registration ", this.getClass().getSimpleName());

            JSONObject jsonObj = mobile.com.web.WebServiceHelper.getHTTPPostJSonRestful(request);
            if (jsonObj == null) {
                LoggerHelper.e(
                        LoggingTag.SUBSCRIBER.name(),
                        "null json object ", this.getClass().getSimpleName());

            }
            LoggerHelper.d(
                    LoggingTag.REGISTRATION.name(),
                    "return from registration restful service " , this.getClass().getSimpleName());

            //this is our internal primary key from table from register_user
            int registeredId =0;
            if (jsonObj != null) {
                try {
                    registeredId = jsonObj.getInt(Subscriber.PROPERTY_REGISTERUSERID);
                    String status = jsonObj.getString(Subscriber.PROPERTY_STATUS);
                    if (status.equalsIgnoreCase("pending")) {

                        SharedObjectManager.setMapValue(SharedObjectManager.OBJECT_NAME.ISPENDING, new Boolean(true));
                    } else {
                        SharedObjectManager.setMapValue(SharedObjectManager.OBJECT_NAME.ISPENDING, new Boolean(false));
                    }

                } catch (Exception e) {
                    LoggerHelper.e(
                            LoggingTag.SUBSCRIBER.name(),
                            "error getting json  " + e.getMessage(), this.getClass().getSimpleName());

                }
            }




            subscriber.setRegisterUserId(registeredId);
            LoggerHelper.d(
                    LoggingTag.REGISTRATION.name(),
                    "Registration id   " + registeredId, this.getClass().getSimpleName());
            if (registeredId > 0 ) {

                String regStr = Integer.toString(registeredId);
              //save to preference and to memory
                mobile.com.security.SecurityManager.saveRegisteredId(this.getApplicationContext(), regStr);
                //store in memory
                mobile.com.application.SharedObjectManager.setMapValue(SharedObjectManager.OBJECT_NAME.SUBSCRIBER,subscriber);
            } else {
                LoggerHelper.e(
                        LoggingTag.REGISTRATION.name(),
                        "error setting registration  ", this.getClass().getSimpleName());
                throw new IllegalStateException("Error setting registration id");
            }
            //get registered id and any error message

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
