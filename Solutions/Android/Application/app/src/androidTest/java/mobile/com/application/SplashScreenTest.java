package mobile.com.application;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.ImageView;
import android.util.Log;

import mobile.com.activities.R;
import mobile.com.activities.SplashScreenActivity;

/**
 * Created by administrator on 3/11/15.
 */
public class SplashScreenTest extends
        ActivityInstrumentationTestCase2<SplashScreenActivity>  {
    private SplashScreenActivity mActivity;
    private static final String TAG = "SplashScreenActivity";
    private ImageView mImageView;
    public SplashScreenTest() {
        super("mobile.com.activities", SplashScreenActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        Log.d(TAG, "setUp");
        super.setUp();
        mActivity = this.getActivity();
        mImageView = (ImageView) mActivity.findViewById(R.id.imgLogo);


    }
    public void testPreconditions() {
        Log.d(TAG, "Running testPreconditions");
        assertNotNull(mImageView);
    }

    @Override
    protected void tearDown() throws Exception {
        Log.d(TAG, "tearDown");
        super.tearDown();
    }




}
