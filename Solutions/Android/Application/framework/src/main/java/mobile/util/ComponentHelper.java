package mobile.util;

import java.io.IOException;
import java.io.InputStream;


import android.content.res.Resources;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.widget.TextView;

public class ComponentHelper {
	/**
	 * Add html content to textview
	 * 
	 * @param textView
	 * @param res
	 * @param resourceFileId
	 * @throws java.io.IOException
	 */
	public static void addHTMLToTextView(TextView textView, Resources res,
			int resourceFileId) throws IOException {

		InputStream in_s = res.openRawResource(resourceFileId);

		byte[] b = new byte[in_s.available()];
		in_s.read(b);
		String message = new String(b);
		textView.setText(Html.fromHtml(message));
		Linkify.addLinks(textView, Linkify.ALL);
		textView.setMovementMethod(LinkMovementMethod.getInstance());

	}

}
