package mobile.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
/**
 * Created by administrator on 8/13/14.
 */
public class PreferenceManagerHelper {

    private static String SHARED_PREFERENCES_NAME = "SHARED_PREFERENCES_NAME";

    public static boolean getBooleanFromSP(String key, Context context) {
        // TODO Auto-generated method stub
        SharedPreferences preferences = context.getApplicationContext()
                .getSharedPreferences(SHARED_PREFERENCES_NAME,
                        android.content.Context.MODE_PRIVATE);
        return preferences.getBoolean(key, false);
    }// getPWDFromSP()

    public static void saveBooleanInSP(Context context, String key,
                                       boolean value) {
        SharedPreferences preferences = context.getApplicationContext()
                .getSharedPreferences(SHARED_PREFERENCES_NAME,
                        android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();

    }

    public static int getIntFromSP(String key, Context context) {
        // TODO Auto-generated method stub
        SharedPreferences preferences = context.getApplicationContext()
                .getSharedPreferences(SHARED_PREFERENCES_NAME,
                        android.content.Context.MODE_PRIVATE);
        return preferences.getInt(key, Integer.MIN_VALUE);
    }

    public static void saveIntInSP(Context context, String key, int value) {
        SharedPreferences preferences = context.getApplicationContext()
                .getSharedPreferences(SHARED_PREFERENCES_NAME,
                        android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void saveStringInSP(Context context, String key, String value) {
        SharedPreferences preferences = context.getApplicationContext()
                .getSharedPreferences(SHARED_PREFERENCES_NAME,
                        android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getStringFromSP(String key, Context context) {
        // TODO Auto-generated method stub
        SharedPreferences preferences = context.getApplicationContext()
                .getSharedPreferences(SHARED_PREFERENCES_NAME,
                        android.content.Context.MODE_PRIVATE);
        return preferences.getString(key,"");
    }
}

