package secu.org.security;


import android.app.Activity;

public class SecuritySettingsSingleton {
	
	private static SecuritySettingsSingleton singletonObject;
	private boolean checkForRootAccess = true;
	private boolean checkForDevicePasscode = true;
	
	public boolean isCheckForRootAccess(Activity activity) {
		String rootAccess = activity.getResources().getString(secu.org.activity.R.string.checkforrootaccess);
        if (rootAccess.toLowerCase().contains("false")) {
        	checkForRootAccess = false;
        }
		return checkForRootAccess;
	}
	
	public boolean isCheckForDevicePasscode(Activity activity) {
		String checkfordevicepassword = activity.getResources().getString(secu.org.activity.R.string.checkfordevicepassword);
        if (checkfordevicepassword.toLowerCase().contains("false")) {
        	checkForDevicePasscode = false;
        }
		return checkForDevicePasscode;
	}
	
	public Object clone()throws CloneNotSupportedException
	{
	    throw new CloneNotSupportedException(); 
	}
	/** A private Constructor prevents any other class from instantiating. */
	private SecuritySettingsSingleton(){
		
	}
	public static synchronized SecuritySettingsSingleton getSingletonObject()
	{
	    if (singletonObject == null){
	    	singletonObject = new SecuritySettingsSingleton();
	    }
	    return singletonObject;
	}
	
	

}
