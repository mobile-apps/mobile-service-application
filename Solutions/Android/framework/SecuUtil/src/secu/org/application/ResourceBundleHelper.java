package secu.org.application;

import java.util.ResourceBundle;



public class ResourceBundleHelper {
	public static final String OFX_SERVER_URL = "ofxurl";
	public static final String TIMEOUT="timeout";
	private static ResourceBundleHelper singletonObject;
	ResourceBundle mProps;
	/** A private Constructor prevents any other class from instantiating. */
	private ResourceBundleHelper(){
		mProps = ResourceBundle.getBundle("secu.org.application.props");
	
	}
	public static synchronized ResourceBundleHelper getSingletonObject()
	{
	    if (singletonObject == null){
	    	singletonObject = new ResourceBundleHelper();
	    }
	    return singletonObject;
	}
    
	public String getValue(String key) {
		String value = mProps.getString(key);
		return value;
	}

}
