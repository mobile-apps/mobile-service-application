package secu.org.application;

import java.io.PrintWriter;
import java.io.StringWriter;



import secu.org.activity.BugActivity;
import android.content.*;
import android.os.Process;
import android.util.Log;

/**
 * @author s16715d
 *
 */
public class ExceptionHandler implements
		java.lang.Thread.UncaughtExceptionHandler {
	private final Context myContext;
	
	

	public ExceptionHandler(Context context) {
		myContext = context;
	
		
	}
    /*
     * (non-Javadoc)
     * @see java.lang.Thread.UncaughtExceptionHandler#uncaughtException(java.lang.Thread, java.lang.Throwable)
     * log and show un handled exception
     */
	public void uncaughtException(Thread thread, Throwable exception) {
		StringWriter stackTrace = new StringWriter();
		exception.printStackTrace(new PrintWriter(stackTrace));
		 Log.e("Error", stackTrace.toString());

		 Intent intent = new Intent(myContext, BugActivity.class);
		 Error error = new Error(stackTrace.toString(), myContext.getClass().getName());
		 
		 intent.putExtra(BugActivity.STACKTRACE, error);
		 myContext.startActivity(intent);

		Process.killProcess(Process.myPid());
		System.exit(10);
	}
}
