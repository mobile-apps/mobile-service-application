package greendroid.widget.itemview;

import greendroid.widget.item.DoubleButtonItem;
import greendroid.widget.item.Item;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.LinearLayout;

import com.cyrilmottier.android.greendroid.R;

public class DoubleButtonItemView extends LinearLayout implements ItemView {
	private Button mButtonOne;
	private Button mButtonTwo;

	public DoubleButtonItemView(Context context) {
		this(context, null);
	}

	public DoubleButtonItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void prepareItemView() {
		mButtonOne = (Button) findViewById(R.id.button1);
		mButtonTwo = (Button) findViewById(R.id.button2);

	}

	@Override
	public void setObject(Item object) {
		// TODO Auto-generated method stub
		 final DoubleButtonItem item = (DoubleButtonItem) object;
		 mButtonOne.setText(item.getmButtonOneText());
		 mButtonTwo.setText(item.getmButtonTwoText());
	      
	}
}
