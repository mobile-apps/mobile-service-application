package greendroid.widget.item;
import greendroid.widget.itemview.ItemView;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ViewGroup;

import com.cyrilmottier.android.greendroid.R;

public class DoubleButtonItem  extends TextItem{

	private String mButtonOneText;
	private String mButtonTwoText;
	
	public String getmButtonOneText() {
		return mButtonOneText;
	}

	public String getmButtonTwoText() {
		return mButtonTwoText;
	}

	public DoubleButtonItem(String textOne, String textTwo) {
		mButtonOneText = textOne;
		mButtonTwoText = textTwo;
	}

	@Override
	public ItemView newView(Context context, ViewGroup parent) {

		return createCellFromXml(context, R.layout.gd_double_button_item_view,
				parent);
	}
	
	 @Override
	    public void inflate(Resources r, XmlPullParser parser, AttributeSet attrs) throws XmlPullParserException, IOException {
	        super.inflate(r, parser, attrs);

	        TypedArray a = r.obtainAttributes(attrs, R.styleable.DoubleButtonItem);
	        //text = a.getString(R.styleable.TextItem_text);
	        a.recycle();
	    }
	
	
}
