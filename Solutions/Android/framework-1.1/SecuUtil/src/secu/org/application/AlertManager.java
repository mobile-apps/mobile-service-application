package secu.org.application;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

public class AlertManager  {
	private Activity mActivity;
	public AlertManager (Activity activity) {
		 mActivity = activity;
	}
	
	public void showAlert( Alert alert) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				mActivity);
		// set title
		alertDialogBuilder.setTitle(alert.getTitle());
		alertDialogBuilder
		.setMessage(alert.getMessage());
		alertDialogBuilder.setCancelable(false);
		
		android.content.DialogInterface.OnClickListener yesListener =new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, just close
				// the dialog box and do nothing
				mActivity.finish();
			}
		};
		android.content.DialogInterface.OnClickListener noListener =new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, just close
				// the dialog box and do nothing
				dialog.cancel();
			}
		};
	    alertDialogBuilder.setPositiveButton("Yes",yesListener);
	    alertDialogBuilder.setPositiveButton("Ok",noListener);
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}



}

