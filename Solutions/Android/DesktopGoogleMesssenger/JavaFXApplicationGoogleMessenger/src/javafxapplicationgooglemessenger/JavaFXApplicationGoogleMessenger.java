/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplicationgooglemessenger;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.VBox;
import javafx.scene.control.RadioButtonBuilder;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;

import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author s16715d
 */
public class JavaFXApplicationGoogleMessenger extends Application {

    private TextArea mTextArea = new TextArea();
    private RadioButton mRadionButtonNotification = new RadioButton("Notification");
    private RadioButton mRadioButtonSecurity = new RadioButton("Security");
    private RadioButton mRadionButtonRatesFees = new RadioButton("Rates and Fees");
    private TextField mTextFieldTitle = new TextField();

    private ToggleGroup getMessageTypes() {
        final ToggleGroup group = new ToggleGroup();

        RadioButton rbNotification = new RadioButton("Notification");
        rbNotification.setToggleGroup(group);
        rbNotification.setSelected(true);

        RadioButton rbSecurity = new RadioButton("Security");
        rbSecurity.setToggleGroup(group);

        RadioButton rbRatesFees = new RadioButton("Rates and Fees");
        rbRatesFees.setToggleGroup(group);
        return group;
    }

    public String getBody() {
        return mTextArea.getText();
    }

    public String getTitle() {
        return mTextFieldTitle.getText();
    }
    public String getMessageType() {
        String messageType = "";
        if (this.mRadionButtonNotification.isSelected()) {
            return "NOTIFICATION";
        }
        if (this.mRadioButtonSecurity.isSelected()) {
            return "SECURITY";
        }
        if (this.mRadionButtonRatesFees.isSelected()) {
            return "RATESFEES";
        }
        return messageType;
    }

    @Override
    public void start(Stage primaryStage) {
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setVmax(440);
        scrollPane.setPrefSize(115, 150);
        javafx.scene.control.Tooltip toolTipText = new javafx.scene.control.Tooltip("Your message must be at least 8 characters in length and should content non-html tags");

        mTextArea.setTooltip(toolTipText);
        scrollPane.setContent(mTextArea);
        Label titleLabel = new Label("Title");
        
        mTextFieldTitle.setMinHeight(20);

        // vb.getChildren().add(scrollPane);
        //     scrollPane.getContent().
        Button btn = new Button();
        ButtonGoogleEventHandler buttonEventHandler = new ButtonGoogleEventHandler(this);
        System.out.println("Assign button handler");
        btn.setOnAction(buttonEventHandler);
        btn.setText("Send Message");
        Label messageLabel = new Label("Message");

        VBox vbox = new VBox();
        vbox.setPadding(new Insets(10));
        vbox.setSpacing(8);
        vbox.getChildren().add(titleLabel);
        vbox.getChildren().add(mTextFieldTitle);
        vbox.getChildren().add(messageLabel);
        vbox.getChildren().add(scrollPane);
        ToggleGroup group = new ToggleGroup();

        mRadionButtonNotification.setToggleGroup(group);
        mRadionButtonNotification.setSelected(true);

        mRadioButtonSecurity.setToggleGroup(group);

        mRadionButtonRatesFees.setToggleGroup(group);

        HBox hbox = new HBox();
        hbox.getChildren().add(mRadionButtonNotification);
        hbox.getChildren().add(mRadioButtonSecurity);
        hbox.getChildren().add(mRadionButtonRatesFees);

        vbox.getChildren().add(hbox);
        vbox.getChildren().add(btn);

        StackPane root = new StackPane();
        //root.setAlignment(Pos.CENTER_RIGHT); 

        //root.getChildren().add(scrollPane);
        //root.getChildren().add(btn);
        Scene scene = new Scene(vbox, 350, 250);

        primaryStage.setTitle("Google Message Generator");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
