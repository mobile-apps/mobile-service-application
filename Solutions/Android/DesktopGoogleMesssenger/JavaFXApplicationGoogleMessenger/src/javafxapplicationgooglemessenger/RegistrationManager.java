/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javafxapplicationgooglemessenger;
import java.io.File;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.application.Application.launch;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
/**
 *
 * @author s16715d
 */
public class RegistrationManager {
    
     public static void main(String[] args) {
        getParticipants();
    }
    public static String getParticipants() {
        StringBuilder builder = new StringBuilder();
        Participants participants = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Participants.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            InputStream inputStream = ButtonGoogleEventHandler.class.getResourceAsStream("participants.xml");
            participants = (Participants) jaxbUnmarshaller.unmarshal(inputStream);
            int counter= 0;
            for (Registration registration : participants.getParticipants() ) {
                
              
                if (counter > 0 && counter  <= participants.getParticipants().size()) {
                    builder.append(",");
                }
                builder.append("\"");
                builder.append(registration.getRegistrationId());
                builder.append("\"");
                counter ++;
            }
        } catch (JAXBException ex) {
            Logger.getLogger(RegistrationManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return builder.toString();
    }
}
