/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplicationgooglemessenger;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;




/**
 *
 * @author s16715d
 */
public class ButtonGoogleEventHandler implements EventHandler {

    JavaFXApplicationGoogleMessenger mJavaFXApplicationGoogleMessenger;
    /**
     * class logger
     */
    private static Logger mLog = Logger.getLogger(ButtonGoogleEventHandler.class.getName());
    private String mDisplayMessage;
    private Stage mDialogStage;
   /**
    * Constructor
    * @param messenger 
    */
    public ButtonGoogleEventHandler(JavaFXApplicationGoogleMessenger messenger) {
        mJavaFXApplicationGoogleMessenger = messenger;
        mLog.info("Create new ButtonGoogleEventHandler!");
    }

    //ignore the certificate checks
    public void ignoreCertificateCheck() {
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        //Create a trust manager that does not validate certificate chains:
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                    return;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                    return;
                }
            }//X509TrustManager
        };//TrustManager[]
        //Install the all-trusting trust manager:
        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ButtonGoogleEventHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            sc.init(null, trustAllCerts, new SecureRandom());
        } catch (KeyManagementException ex) {
            Logger.getLogger(ButtonGoogleEventHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        //avoid "HTTPS hostname wrong: should be <myhostname>" exception:
        HostnameVerifier hv = new HostnameVerifier() {

            public boolean verify(String urlHostName, SSLSession session) {
                if (!urlHostName.equalsIgnoreCase(session.getPeerHost())) {
                    System.out.println("Warning: URL host '" + urlHostName + "' is different to SSLSession host '" + session.getPeerHost() + "'.");
                }
                return true; //also accept different hostname (e.g. domain name instead of IP address)
            }
        };
        HttpsURLConnection.setDefaultHostnameVerifier(hv);

    }
    /**
     * Convert inputstream to string
     * @param in
     * @return
     * @throws IOException 
     */
    public String fromStream(InputStream in) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
        }
        return out.toString();
    }
    /**
     * handle the button  clicked event and send message
     * @param t 
     */
    @Override
    public void handle(Event t) {
        mLog.info("starting event handle");
        com.google.android.gcm.server.Sender sender = new Sender("AIzaSyCfpNfCJTLIGRdl_U69CQJJN6OtJGhyVjY");
         Message.Builder messageBuilder = new Message.Builder();
         messageBuilder.addData("title","titlevalue");
         messageBuilder.addData("type","typevalue");
         messageBuilder.addData("message","messagevalue");
         Message notificationMessage = messageBuilder.build(); //add timeToLive here
         String testid = "APA91bFDT2yqqK__mBhw08SNfI6fclFvxahovEwNmk1CCkMjijqmOM2xRKazzUKfAqp1IlmIPFwgqeD53B_ch_axNtvNiSrIVTfvSHR7_lrawadEZQhria7Hx8sYal0yQYvzgHUClpsv";
//APA91bFDT2yqqK__mBhw08SNfI6fclFvxahovEwNmk1CCkMjijqmOM2xRKazzUKfAqp1IlmIPFwgqeD53B_ch_axNtvNiSrIVTfvSHR7_lrawadEZQhria7Hx8sYal0yQYvzgHUClpsvAPA91bFDT2yqqK__mBhw08SNfI6fclFvxahovEwNmk1CCkMjijqmOM2xRKazzUKfAqp1IlmIPFwgqeD53B_ch_axNtvNiSrIVTfvSHR7_lrawadEZQhria7Hx8sYal0yQYvzgHUClpsv
        URL url;
        StringBuffer messageBuffer = new StringBuffer();// soap message builder
       /*
        if (hasError()) {
            showMessage();
            return;
        }*/

        try {
            Result result = sender.send(notificationMessage, testid, 3);
            
            System.out.println("#######################  try to create Soap Connection");
            url = new URL("https://android.googleapis.com/gcm/send");
            ignoreCertificateCheck();
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();

            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            //these are doNet or java web service
            urlConnection.setRequestProperty("Content-type", "application/json");
                                                                   
            urlConnection.setRequestProperty("Authorization", "key=AIzaSyCfpNfCJTLIGRdl_U69CQJJN6OtJGhyVjY");
            OutputStream outputStream = urlConnection.getOutputStream();
            Writer messageWriter = new OutputStreamWriter(outputStream);

            InputStream is = ButtonGoogleEventHandler.class.getResourceAsStream("googleMessage");
            String message = fromStream(is);
            String title = this.mJavaFXApplicationGoogleMessenger.getTitle();
            String messageType = this.mJavaFXApplicationGoogleMessenger.getMessageType();
            String registrationIds = RegistrationManager.getParticipants();
            String formattedMessage = MessageFormat.format(message, mJavaFXApplicationGoogleMessenger.getBody(), title, messageType,registrationIds);
            mLog.info("formatted message " + formattedMessage);
            messageWriter.write(formattedMessage);
            // send the message
            mLog.info("#######################send the message");
            messageWriter.flush();
            messageWriter.close();
            is.close();

            // save the entire message for display
            java.io.ByteArrayOutputStream outMessageStream = new java.io.ByteArrayOutputStream();
            InputStream inputStream = urlConnection.getInputStream();

            int c;
            while ((c = inputStream.read()) != -1) {
                outMessageStream.write(c);
            }
            inputStream.close();
           // JSONObject  returnJSONObject = new JSONObject(outMessageStream.toString());
            outMessageStream.close();

            int success = -99;
            int failure = 1;
           // success = returnJSONObject.getInt("success");
           // int failure = returnJSONObject.getInt("failure");

            
            
            
            
            if (success >= 1 && failure == 0 ) {
               mDisplayMessage = "Message sent succeful";
            }  
             if (success >= 1 && failure != 0 ) {
               mDisplayMessage = "someone failed";
            }
             this.showMessage();
            mLog.info("###########;############  get the soap reply [ " + outMessageStream.toString() + "]");
        } catch (Exception ex) {
            Logger.getLogger(ButtonGoogleEventHandler.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error " + ex.getMessage());
            mDisplayMessage = "Error " + ex.getMessage();
              this.showMessage();
        }

        System.out.println("Message sent !!"); //To change body of generated methods, choose Tools | Templates.
    }
    /**
     * check for form errors
     * @return 
     */
    private boolean hasError() {
        boolean foundError = false;
        if (this.mJavaFXApplicationGoogleMessenger.getBody() == null || this.mJavaFXApplicationGoogleMessenger.getBody().length() < 8) {
            foundError = true;
            mDisplayMessage = "You must enter message";

        }

        if (this.mJavaFXApplicationGoogleMessenger.getTitle() == null || this.mJavaFXApplicationGoogleMessenger.getTitle().length() < 2) {
            foundError = true;
            mDisplayMessage = mDisplayMessage + " You must enter title";

        }
        return foundError;
    }
   /**
    * show application message
    */
    private void showMessage() {
        mDialogStage = new Stage();
        Button okButton = new Button("Ok");
        okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                mDialogStage.close();
                //Place the code you want to execute on the button click here.
            }
        });
        mDialogStage.initModality(Modality.WINDOW_MODAL);
        mDialogStage.setScene(new Scene(VBoxBuilder.create().
                children(new Text(mDisplayMessage), okButton).
                alignment(Pos.CENTER).padding(new Insets(5)).build()));
        mDialogStage.show();

    }

}
