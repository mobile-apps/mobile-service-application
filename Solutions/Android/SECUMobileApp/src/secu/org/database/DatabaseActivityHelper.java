package secu.org.database;

/**
 * Created by s16715d on 12/3/13.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import secu.org.google.cloud.message.MessageTypeEnumeration;

public class DatabaseActivityHelper {
    private DatabaseHelper mDatabaseHelper;
    private SQLiteDatabase mDatabase;

    /**
     * @param context
     */
    public DatabaseActivityHelper(Context context) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "ending refreshList", this.getClass().getSimpleName());
        mDatabaseHelper = new DatabaseHelper(context);
        mDatabase = mDatabaseHelper.getWritableDatabase();
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "ending refreshList", this.getClass().getSimpleName());
    }

    /**
     * delete rows
     *
     * @param ids
     */
    public void delete(int[] ids) {

        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "starting delete", this.getClass().getSimpleName());
        for (int x = 0; x < ids.length; x++) {
            String where = DatabaseHelper.ID + " = " + ids[x];
            String whereArgs = null;
            mDatabase.delete(DatabaseHelper.DATABASE_TABLE, where, null);
            secu.org.util.LoggerHelper.d(
                    secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                    "ending delete", this.getClass().getSimpleName());
        }
    }

    /**
     * add row to table
     *
     * @param message
     * @param messageType
     * @return
     */
    public long createRecords(String message, MessageTypeEnumeration messageType) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "starting createRecords", this.getClass().getSimpleName());
        ContentValues values = new ContentValues();
        String currentDateTime = secu.org.util.DateManager.getCurrentDateTime();

        values.put(DatabaseHelper.MESSAGE, message);
        values.put(DatabaseHelper.MESSSAGETYPE, messageType.toString());
        values.put(DatabaseHelper.CREATEDATETIME, currentDateTime);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "ending createRecords", this.getClass().getSimpleName());
        return mDatabase.insert(DatabaseHelper.DATABASE_TABLE, null, values);

    }

    /**
     * return all messages
     *
     * @return
     */
    public List<DatabaseMessageRow> findAll() {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "starting findAll", this.getClass().getSimpleName());
        List<DatabaseMessageRow> list = new ArrayList<DatabaseMessageRow>();
        String[] cols = new String[]{DatabaseHelper.ID, DatabaseHelper.CREATEDATETIME, DatabaseHelper.MESSAGE, DatabaseHelper.MESSSAGETYPE};
        Cursor cursor = mDatabase.query(true, DatabaseHelper.DATABASE_TABLE, cols, null
                , null, null, null, DatabaseHelper.ID + " DESC", null);
        if (cursor.moveToFirst()) {
            do {
                DatabaseMessageRow row = new DatabaseMessageRow();
                row.setMessage(cursor.getString(DatabaseHelper.MESSAGE_COLUMN_POSITION));
                row.setCreateDateTime(cursor.getString(DatabaseHelper.CREATEDATETIME_COLUMN_POSITION));
                row.setId(cursor.getInt(DatabaseHelper.ID_COLUMN_POSITION));
                row.setMessage(cursor.getString(DatabaseHelper.MESSAGE_COLUMN_POSITION));
                row.setMessageType(cursor.getString(DatabaseHelper.MESSSAGETYPE_COLUMN_POSITION));

                list.add(row);
                //list.add(cursor.getString(0).toUpperCase());
            } while (cursor.moveToNext());
        }
        //close cursor
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "ending findall", this.getClass().getSimpleName());
        return list;
    }
}
