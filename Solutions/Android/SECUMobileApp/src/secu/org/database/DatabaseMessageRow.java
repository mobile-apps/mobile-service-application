package secu.org.database;

/**
 * Created by s16715d on 12/3/13.
 */
public class DatabaseMessageRow {
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(String createDateTime) {
        this.createDateTime = createDateTime;
    }

    public String computeDateDisplay() {
        String computeDate = "";
        java.util.Date todayDate = secu.org.util.DateManager.getRealCurrentDateTime();
        java.util.Date messageDate = secu.org.util.DateManager.convertToRealDate(createDateTime);
        //compare dates
        long days = secu.org.util.DateManager.computeDaysDifference(messageDate,todayDate);

        if (days > 0 ) {
            computeDate = secu.org.util.DateManager.getMonthDay(createDateTime);
        } else {
            computeDate = secu.org.util.DateManager.getHourMin(createDateTime);
        }

        return computeDate;
    }
    public String getMonthDayDisplay() {
        String monthDay = secu.org.util.DateManager.getMonthDay(createDateTime);
        return monthDay;
    }
    public boolean isSelected() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    private String message;
    private String messageType;
    private int id;
    private String createDateTime;
    private boolean selected = false;

}
