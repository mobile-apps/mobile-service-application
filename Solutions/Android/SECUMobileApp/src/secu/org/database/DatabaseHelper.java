package secu.org.database;

/**
 * Created by s16715d on 12/3/13.
 */
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.content.Context;
import android.util.Log;
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Mobile";
    public static final String DATABASE_TABLE = "Messages";
    public final static String ID="_id"; // id value for employee
    public final static String MESSAGE="message";  // name of employee
    public final static String MESSSAGETYPE="messagetype"; // id value for employee
    public final static String CREATEDATETIME="createdatetime";  // name of employee

    public final static int ID_COLUMN_POSITION=0; // id value for employee
    public final static int MESSAGE_COLUMN_POSITION=2;  // name of employee
    public final static int MESSSAGETYPE_COLUMN_POSITION=3; // id value for employee
    public final static int CREATEDATETIME_COLUMN_POSITION=1; // name of employee
    private static final int DATABASE_VERSION = 2;
    // Database creation sql statement

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * build SQl create table
     * @return
     */
    private String constructCreateTableScript() {
        StringBuilder builder = new StringBuilder();
        builder.append("create table " + DATABASE_TABLE  );
        builder.append(" ( ");
        builder.append(ID);
        builder.append(" integer primary key AUTOINCREMENT,");
        builder.append(MESSAGE);
        builder.append(" text not null,");
        builder.append(MESSSAGETYPE);
        builder.append(" text not null,");
        builder.append(CREATEDATETIME);
        builder.append(" text not null");
        builder.append(" )");

        return builder.toString();
    }
    // Method is called during creation of the database
    @Override
    public void onCreate(SQLiteDatabase database) {
        String sql = constructCreateTableScript();
        database.execSQL(sql);
    }
    // Method is called during an upgrade of the database,
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion,
                          int newVersion)
    {
        Log.w(DatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS MyEmployees");
        onCreate(database);

    }
}
