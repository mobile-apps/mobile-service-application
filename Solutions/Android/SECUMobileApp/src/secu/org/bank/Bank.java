package secu.org.bank;
import java.util.ArrayList;
import java.io.Serializable;
import java.util.List;

import secu.org.account.Account;
import secu.org.mail.Mail;
@SuppressWarnings("serial")
public class Bank implements Serializable {
	private String userId;
	private String password;
	private String code;
	private String severity;
	private Account account;
	private Mail mail;
	private boolean operationStoppedbyUser;
	public boolean isOperationStoppedbyUser() {
		return operationStoppedbyUser;
	}
	public void setOperationStoppedbyUser(boolean operationStoppedbyUser) {
		this.operationStoppedbyUser = operationStoppedbyUser;
	}
	public Mail getMail() {
		return mail;
	}
	public void setMail(Mail mail) {
		this.mail = mail;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public String getCode() {
		return code;
	}
	/**
	 * determine logon status
	 * @return
	 */
	public LogonStatusEnumeration getLogonStatus() {
		if (code.equalsIgnoreCase("0")) {
			return LogonStatusEnumeration.LogonSuccessful;
		}
		if (code.equalsIgnoreCase("15500")) {
			return LogonStatusEnumeration.InvalidCredentials;
		}
		if (code.equalsIgnoreCase("15502")) {
			return LogonStatusEnumeration.FailedAttempts;
		}
		
		
		return LogonStatusEnumeration.InvalidCredentials;
	}
	/*
	 * 
	 */
	public boolean isValidLogon() {
		boolean isValid = true;
		if (!code.equalsIgnoreCase("0")) {
			isValid = false;
		}
		return isValid;
	}

	public void setCode(String code) {
		this.code = code;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	private List<Account> accountList = new ArrayList<Account>();
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<Account> getAccountList() {
		return accountList;
	}
	public void setAccountList(List<Account> accountList) {
		this.accountList = accountList;
	}
	

}
