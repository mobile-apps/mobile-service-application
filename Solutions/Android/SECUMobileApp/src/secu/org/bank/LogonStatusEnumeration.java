package secu.org.bank;

public enum LogonStatusEnumeration {
      InvalidCredentials,
      LogonSuccessful,
      FailedAttempts
}
