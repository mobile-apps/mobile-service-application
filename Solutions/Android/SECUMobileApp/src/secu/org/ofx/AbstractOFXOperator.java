package secu.org.ofx;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import secu.org.ofx.response.OFX;
import android.net.http.AndroidHttpClient;

import com.thoughtworks.xstream.XStream;

/**
 * @author s16715d
 * 
 */
public abstract class AbstractOFXOperator {

	protected static final String OFX_SERVER_URL = "https://onlineaccess.ncsecu.org/secuofx/secu.ofx";
	protected static final String NW_ORG = "SECU";
	protected static final String NW_FID = "1001";

	URL mUrl = null;

	OutputStream mOutputStream = null;
	Writer mMessageWriter = null;

	protected HttpURLConnection createConnection(int requestLength)
			throws Exception {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.OFX.name(),
				"starting HttpURLConnection ", this.getClass().getSimpleName());
		HttpURLConnection connection = null;
		URL url = new URL(OFX_SERVER_URL);
		connection = (HttpURLConnection) url.openConnection();
		// mHttpUrlConnection.setChunkedStreamingMode(0);
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		connection.setRequestProperty("Content-type", "application/x-ofx");
		connection.setReadTimeout(10000);
		connection.setFixedLengthStreamingMode(requestLength);
		connection.connect();
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.OFX.name(),
				"ending HttpURLConnection ", this.getClass().getSimpleName());
		return connection;
	}

	protected secu.org.ofx.response.OFX getOFXResponse(String request) throws Exception{
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.OFX.name(),
				"starting getOFXResponse ", this.getClass().getSimpleName());
		// HttpClient client = new DefaultHttpClient();
		AndroidHttpClient client = AndroidHttpClient.newInstance("Android");

		// HttpClient client = new AndroidHttpClient();
		HttpPost post = new HttpPost(OFX_SERVER_URL);
		post.setHeader("Content-type", "application/x-ofx");
		StringEntity input = new StringEntity(request);
		// input.setContentType("application/x-ofx");
		post.setEntity(input);
		HttpResponse response = client.execute(post);
		BufferedReader br = new BufferedReader(new InputStreamReader(
				(response.getEntity().getContent())));
		String output;
		StringBuilder sb = new StringBuilder();
		while ((output = br.readLine()) != null) {
			sb.append(output);
		}
		secu.org.ofx.response.OFX ofxResponse = null;
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.OFX.name(),
				"HTTpResponse  " +  sb.toString(), this.getClass().getSimpleName());
	
		ofxResponse = constructOFXResponse(sb.toString());
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.OFX.name(),
				"ending getOFXResponse ", this.getClass().getSimpleName());
		client.getConnectionManager().shutdown();
		return ofxResponse;
	}

	//abstract methods
	protected abstract String createMessageRequest(Object object);
	public abstract Object ofxOperation(Object object)
			throws Exception;

																					

	/**
	 * construct Account List
	 * 
	 * @param response
	 * @return
	 */
	protected secu.org.ofx.response.OFX constructOFXResponse(String response)
			throws Exception {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.OFX.name(),
				"starting constructOFXResponse ", this.getClass().getSimpleName());
		//make sure something is in response
		if (response == null || response.length() < 1) {
			String errorMessage = "Response is null or empty";
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.OFX.name(),
					"constructOFXResponse error  " + errorMessage, this.getClass().getSimpleName());
			throw new Exception(errorMessage);
		}
		com.thoughtworks.xstream.io.xml.DomDriver driver = new com.thoughtworks.xstream.io.xml.DomDriver();
		XStream xstream = new XStream(driver);
		xstream.alias("OFX", secu.org.ofx.response.OFX.class);
		xstream.processAnnotations(secu.org.ofx.response.OFX.class);
		xstream.setClassLoader(secu.org.ofx.response.OFX.class.getClassLoader());
		secu.org.ofx.response.OFX ofx = new OFX();
		try {
			ofx = (OFX) xstream.fromXML(response);
		} catch (Exception e) {
			
			secu.org.util.LoggerHelper.e(
					secu.org.application.LoggingTag.OFX.name(),
					"Error " + e.getMessage(), this.getClass().getSimpleName());
		
			throw e;
		}
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.OFX.name(),
				"ending constructOFXResponse ", this.getClass().getSimpleName());
		return ofx;

	}

}
