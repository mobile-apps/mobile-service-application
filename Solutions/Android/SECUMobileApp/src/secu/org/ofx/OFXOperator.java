package secu.org.ofx;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import secu.org.account.Account;
import secu.org.account.AccountTransaction;
import secu.org.account.AccountTransfer;
import secu.org.application.ResourceBundleHelper;
import secu.org.bank.Bank;
import secu.org.ofx.request.BANKACCTFROM;
import secu.org.ofx.request.BANKMSGSRQV1;
import secu.org.ofx.request.EMAILMSGSRQV1;
import secu.org.ofx.request.SIGNONMSGSRQV1;
import secu.org.ofx.request.SONRQ;
import secu.org.ofx.response.OFX;
import secu.org.util.MethodNameHelper;
import secu.org.xmlpullparser.response.parser.HTTPResponseParser;
import android.net.http.AndroidHttpClient;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.reflection.FieldDictionary;
import com.thoughtworks.xstream.converters.reflection.PureJavaReflectionProvider;
import com.thoughtworks.xstream.converters.reflection.SortableFieldKeySorter;

/**
 * @author s16715d
 * 
 */
public class OFXOperator {

	private String OFX_SERVER_URL = "https://onlineaccess.ncsecu.org/secuofx/secu.ofx";
	private final static Logger LOGGER = Logger.getLogger(OFXOperator.class
			.getName());
	URL mUrl = null;

	OutputStream mOutputStream = null;
	Writer mMessageWriter = null;

	/**
	 * URl connection to the OFX 2.0 server
	 * 
	 * @param ofxServerURL
	 */
	public OFXOperator() {
		// secu.org.http.logging.DebugLogConfig.enable();
		LOGGER.setLevel(Level.INFO);
		OFX_SERVER_URL = ResourceBundleHelper.getSingletonObject().getValue(
				ResourceBundleHelper.OFX_SERVER_URL);
	}

	private HttpURLConnection createConnection(int requestLength)
			throws Exception {
		HttpURLConnection connection = null;
		URL url = new URL(OFX_SERVER_URL);
		connection = (HttpURLConnection) url.openConnection();
		// mHttpUrlConnection.setChunkedStreamingMode(0);
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		connection.setRequestProperty("Content-type", "application/x-ofx");
		connection.setReadTimeout(10000);
		connection.setFixedLengthStreamingMode(requestLength);
		connection.connect();
		LOGGER.info("Connecting to server downLoadBankAccountsAndStatements");
		return connection;
	}

	/**
	 * construct Account List
	 * 
	 * @param response
	 * @return
	 */
	private secu.org.ofx.response.OFX constructOFXResponse(String response)
			throws Exception {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"starting constructOFXResponse ", this.getClass()
						.getSimpleName());

		XStream xstream = OFXOperatorHelper.getSingletonObject().getxStream();

		secu.org.ofx.response.OFX ofx = new OFX();
		try {
			ofx = (OFX) xstream.fromXML(response);
		} catch (Exception e) {
			LOGGER.severe(" Response [" + response + "]");
			LOGGER.severe(e.getMessage());
			e.printStackTrace();
			throw e;
		}
		// String xml = xstream.toXML(ofx);
		// LOGGER.info("*** ofx response " + xml);
		// LOGGER.info( xml);
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"starting constructOFXResponse ", this.getClass()
						.getSimpleName());
		return ofx;

	}

	private String createStatementRequest(
			secu.org.ofx.request.Statement statement) {
		String header = OFXRequestBuilder.buidTwoHeader();
		SortableFieldKeySorter sorter = new SortableFieldKeySorter();
		sorter.registerFieldOrder(secu.org.ofx.request.OFX.class, new String[] {
				"SIGNONMSGSRQV1", "BANKMSGSRQV1" });
		sorter.registerFieldOrder(secu.org.ofx.request.SONRQ.class,
				new String[] { "DTCLIENT", "USERID", "USERPASS", "LANGUAGE",
						"FI", "APPID", "APPVER" });
		sorter.registerFieldOrder(secu.org.ofx.request.STMTTRNRQ.class,
				new String[] { "TRNUID", "STMTRQ" });
		sorter.registerFieldOrder(secu.org.ofx.request.BANKACCTFROM.class,
				new String[] { "BANKID", "ACCTID", "ACCTTYPE" });
		sorter.registerFieldOrder(secu.org.ofx.request.FI.class, new String[] {
				"ORG", "FID" });
		// xstream = new XStream(new Sun14ReflectionProvider(new
		// FieldDictionary(sorter)));

		XStream xstream = new XStream(new PureJavaReflectionProvider(
				new FieldDictionary(sorter)));
		// XStream xstream = new XStream();
		xstream.alias("OFX", secu.org.ofx.request.OFX.class);
		secu.org.ofx.request.OFX ofx = new secu.org.ofx.request.OFX();
		// sign on information
		secu.org.ofx.request.SIGNONMSGSRQV1 SIGNONMSGSRQV1 = new secu.org.ofx.request.SIGNONMSGSRQV1();
		secu.org.ofx.request.SONRQ SONRQ = new secu.org.ofx.request.SONRQ();
		SONRQ.setDTCLIENT("19991029101000");
		SONRQ.setUSERID(statement.getUserID());
		SONRQ.setUSERPASS(statement.getPassword());
		SIGNONMSGSRQV1.setsONRQ(SONRQ);
		ofx.setSIGNONMSGSRQV1(SIGNONMSGSRQV1);

		secu.org.ofx.request.BANKMSGSRQV1 BANKMSGSRQV1 = new secu.org.ofx.request.BANKMSGSRQV1();
		secu.org.ofx.request.STMTTRNRQ STMTTRNRQ = new secu.org.ofx.request.STMTTRNRQ();
		secu.org.ofx.request.STMTRQ STMTRQ = new secu.org.ofx.request.STMTRQ();

		BANKACCTFROM BANKACCTFROM = new BANKACCTFROM();
		BANKACCTFROM.setBANKID("253177049");
		BANKACCTFROM.setACCTTYPE(statement.getAccountType().toString());
		BANKACCTFROM.setACCTID(statement.getAccountId());

		secu.org.ofx.request.INCTRAN INCTRAN = new secu.org.ofx.request.INCTRAN();
		INCTRAN.setINCLUDE(statement.getIncludeTransaction());
		BANKMSGSRQV1.setINTRATRNRQ(null);
		BANKMSGSRQV1.setSTMTTRNRQ(STMTTRNRQ);
		STMTTRNRQ.setSTMTRQ(STMTRQ);
		STMTRQ.setBANKACCTFROM(BANKACCTFROM);
		STMTRQ.setINCTRAN(INCTRAN);
		ofx.setBANKMSGSRQV1(BANKMSGSRQV1);

		String request = header + xstream.toXML(ofx);
		return request;
	}

	/**
	 * Create message
	 * 
	 * @return
	 */
	private String createMessageRequest(secu.org.bank.Bank bank) {
		String header = OFXRequestBuilder.buidTwoHeader();
		SortableFieldKeySorter sorter = new SortableFieldKeySorter();
		sorter.registerFieldOrder(secu.org.ofx.request.OFXEmailMessage.class,
				new String[] { "SIGNONMSGSRQV1", "EMAILMSGSRQV1" });
		sorter.registerFieldOrder(secu.org.ofx.request.SONRQ.class,
				new String[] { "DTCLIENT", "USERID", "USERPASS", "LANGUAGE",
						"FI", "APPID", "APPVER" });
		sorter.registerFieldOrder(secu.org.ofx.request.FI.class, new String[] {
				"ORG", "FID" });
		sorter.registerFieldOrder(secu.org.ofx.request.MAILSYNCRQ.class,
				new String[] { "TOKEN", "REJECTIFMISSING", "INCIMAGES",
						"USEHTML" });
		// xstream = new XStream(new Sun14ReflectionProvider(new
		// FieldDictionary(sorter)));

		XStream xstream = new XStream(new PureJavaReflectionProvider(
				new FieldDictionary(sorter)));
		// XStream xstream = new XStream();
		xstream.alias("OFX", secu.org.ofx.request.OFXEmailMessage.class);
		secu.org.ofx.request.OFXEmailMessage ofx = new secu.org.ofx.request.OFXEmailMessage();
		// sign on information
		SIGNONMSGSRQV1 SIGNONMSGSRQV1 = new SIGNONMSGSRQV1();
		SONRQ SONRQ = new SONRQ();
		SONRQ.setUSERID(bank.getUserId());
		SONRQ.setUSERPASS(bank.getPassword());
		ofx.setSIGNONMSGSRQV1(SIGNONMSGSRQV1);
		SIGNONMSGSRQV1.setsONRQ(SONRQ);

		EMAILMSGSRQV1 EMAILMSGSRQV1 = new EMAILMSGSRQV1();

		ofx.setEMAILMSGSRQV1(EMAILMSGSRQV1);
		String request = null;
		try {
			String toXML = xstream.toXML(ofx);
			request = header + toXML;
		} catch (Exception ex) {
			LOGGER.severe(ex.getMessage());
		}

		LOGGER.info("request " + request);
		return request;
	}

	/**
	 * 
	 * @param accountTransfer
	 * @return
	 */
	private String createTransferRequest(
			secu.org.account.AccountTransfer accountTransfer) {
		String header = OFXRequestBuilder.buidTwoHeader();
		SortableFieldKeySorter sorter = new SortableFieldKeySorter();
		sorter.registerFieldOrder(secu.org.ofx.request.OFX.class, new String[] {
				"SIGNONMSGSRQV1", "BANKMSGSRQV1" });
		sorter.registerFieldOrder(secu.org.ofx.request.SONRQ.class,
				new String[] { "DTCLIENT", "USERID", "USERPASS", "LANGUAGE",
						"FI", "APPID", "APPVER" });
		sorter.registerFieldOrder(secu.org.ofx.request.INTRATRNRQ.class,
				new String[] { "TRNUID", "INTRARQ" });
		sorter.registerFieldOrder(secu.org.ofx.request.XFERINFO.class,
				new String[] { "BANKACCTFROM", "BANKACCTTO", "TRNAMT" });
		sorter.registerFieldOrder(secu.org.ofx.request.BANKACCTFROM.class,
				new String[] { "BANKID", "ACCTID", "ACCTTYPE" });
		sorter.registerFieldOrder(secu.org.ofx.request.BANKACCTTO.class,
				new String[] { "BANKID", "ACCTID", "ACCTTYPE" });
		sorter.registerFieldOrder(secu.org.ofx.request.FI.class, new String[] {
				"ORG", "FID" });
		// xstream = new XStream(new Sun14ReflectionProvider(new
		// FieldDictionary(sorter)));

		XStream xstream = new XStream(new PureJavaReflectionProvider(
				new FieldDictionary(sorter)));
		// XStream xstream = new XStream();
		xstream.alias("OFX", secu.org.ofx.request.OFX.class);
		secu.org.ofx.request.OFX ofx = new secu.org.ofx.request.OFX();
		// sign on information
		SIGNONMSGSRQV1 SIGNONMSGSRQV1 = new SIGNONMSGSRQV1();
		SONRQ SONRQ = new SONRQ();
		SONRQ.setUSERID(accountTransfer.getBank().getUserId());
		SONRQ.setUSERPASS(accountTransfer.getBank().getPassword());
		ofx.setSIGNONMSGSRQV1(SIGNONMSGSRQV1);

		SIGNONMSGSRQV1.setsONRQ(SONRQ);
		BANKMSGSRQV1 BANKMSGSRQV1 = new BANKMSGSRQV1();
		BANKMSGSRQV1.getINTRATRNRQ().getINTRARQ().getXFERINFO().getBANKACCTTO()
				.setACCTID(accountTransfer.getToAccount().getAccountNumber());
		BANKMSGSRQV1.getINTRATRNRQ().getINTRARQ().getXFERINFO().getBANKACCTTO()
				.setACCTTYPE(accountTransfer.getToAccount().getAccountType());
		BANKMSGSRQV1.getINTRATRNRQ().getINTRARQ().getXFERINFO()
				.getBANKACCTFROM()
				.setACCTID(accountTransfer.getFromAccount().getAccountNumber());
		BANKMSGSRQV1.getINTRATRNRQ().getINTRARQ().getXFERINFO()
				.getBANKACCTFROM()
				.setACCTTYPE(accountTransfer.getFromAccount().getAccountType());
		BANKMSGSRQV1.getINTRATRNRQ().getINTRARQ().getXFERINFO()
				.setTRNAMT(accountTransfer.getAmount());
		BANKMSGSRQV1.setSTMTTRNRQ(null);
		ofx.setBANKMSGSRQV1(BANKMSGSRQV1);
		String request = header + xstream.toXML(ofx);
		LOGGER.info("request " + request);
		return request;
	}

	/**
	 * download all secure messages
	 * 
	 * @param bank
	 * @return
	 * @throws Exception
	 */
	public List<secu.org.message.Mail> downLoadMessages(secu.org.bank.Bank bank)
			throws Exception {
		List<secu.org.message.Mail> mailMessages = new ArrayList<secu.org.message.Mail>();
		String name = new MethodNameHelper().getName();
		LOGGER.info("Starting Method [" + name + "]");
		// HttpClient client = new DefaultHttpClient();
		AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
		LOGGER.info("Create client http client");
		// HttpClient client = new AndroidHttpClient();
		HttpPost post = new HttpPost(OFX_SERVER_URL);
		post.setHeader("Content-type", "application/x-ofx");
		// post.setHeader("Connection", "close");
		String request = this.createMessageRequest(bank);
		StringEntity input = new StringEntity(request);
		// input.setContentType("application/x-ofx");
		post.setEntity(input);
		HttpResponse response = client.execute(post);

		// int responseCode = response.getStatusLine().getStatusCode();

		// String responseReason = response.getStatusLine().getReasonPhrase();

		BufferedReader br = new BufferedReader(new InputStreamReader(
				(response.getEntity().getContent())));
		String output;
		StringBuilder sb = new StringBuilder();
		while ((output = br.readLine()) != null) {
			sb.append(output);
		}
		secu.org.ofx.response.OFX ofxResponse = null;
		LOGGER.info("HTTP response : " + sb.toString());
		try {
			ofxResponse = constructOFXResponse(sb.toString());
			secu.org.ofx.response.EMAILMSGSRSV1 emailMsg = ofxResponse
					.getEMAILMSGSRSV1();
			secu.org.ofx.response.MAILSYNCRS mailSyncrs = emailMsg
					.getMAILSYNCRS();
			List<secu.org.ofx.response.MAILTRNRS> mailTransList = mailSyncrs
					.getMAILTRNRS();
			// loop
			for (secu.org.ofx.response.MAILTRNRS mailTrnrs : mailTransList) {
				secu.org.ofx.response.MAILRS mailrs = mailTrnrs.getMAILRS();

				secu.org.message.Mail mail = new secu.org.message.Mail(mailrs
						.getMAIL().getDTCREATED(), mailrs.getMAIL().getFROM(),
						mailrs.getMAIL().getTO(),
						mailrs.getMAIL().getSUBJECT(), mailrs.getMAIL()
								.getMSGBODY());
				mailMessages.add(mail);

			}

			sb = null;
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
			return mailMessages;

		}

		client.getConnectionManager().shutdown();

		// post.setConnectionRequest(connRequest)
		return mailMessages;
	}

	/**
	 * 
	 * @param accountTransfer
	 * @return
	 * @throws Exception
	 */
	public AccountTransfer transferMoney(
			secu.org.account.AccountTransfer accountTransfer) throws Exception {
		String name = new MethodNameHelper().getName();
		LOGGER.info("Starting Method [" + name + "]");
		// HttpClient client = new DefaultHttpClient();
		AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
		LOGGER.info("Create client http client");
		// HttpClient client = new AndroidHttpClient();
		HttpPost post = new HttpPost(OFX_SERVER_URL);
		post.setHeader("Content-type", "application/x-ofx");
		// post.setHeader("Connection", "close");
		String request = this.createTransferRequest(accountTransfer);
		StringEntity input = new StringEntity(request);
		// input.setContentType("application/x-ofx");
		post.setEntity(input);
		HttpResponse response = client.execute(post);

		BufferedReader br = new BufferedReader(new InputStreamReader(
				(response.getEntity().getContent())));
		String output;
		StringBuilder sb = new StringBuilder();
		while ((output = br.readLine()) != null) {
			sb.append(output);
		}
		secu.org.ofx.response.OFX ofxResponse = null;
		LOGGER.info("HTTP response : " + sb.toString());
		try {
			ofxResponse = constructOFXResponse(sb.toString());
			accountTransfer.setStatus(ofxResponse.getBANKMSGSRSV1()
					.getINTRATRNRS().getSTATUS().getCODE());
			accountTransfer.setMessage(ofxResponse.getBANKMSGSRSV1()
					.getINTRATRNRS().getSTATUS().getMESSAGE());
			accountTransfer.setDateTransfer(ofxResponse.getBANKMSGSRSV1()
					.getINTRATRNRS().getINTRARS().getDTPOSTED());
			accountTransfer.setConfirmationNumber(ofxResponse.getBANKMSGSRSV1()
					.getINTRATRNRS().getTRNUID());
			sb = null;
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
			return accountTransfer;

		}

		client.getConnectionManager().shutdown();

		// post.setConnectionRequest(connRequest)
		return accountTransfer;
	}

	/**
	 * Download statements
	 * 
	 * @param statement
	 * @return
	 * @throws Exception
	 */
	public secu.org.ofx.response.OFX downloadAccountTransactionsHelper(
			secu.org.ofx.request.Statement statement) {
		long startTime   = System.currentTimeMillis();
		// HttpClient client = new DefaultHttpClient();
		secu.org.util.LoggerHelper
				.d(secu.org.application.LoggingTag.GETSTATEMENTS.name(),
						"starting downLoadStatements ", this.getClass()
								.getSimpleName());
		AndroidHttpClient client = null;

		// post.setHeader("Connection", "close");
		String request = createStatementRequest(statement);
		secu.org.ofx.response.OFX ofxResponse = null;
		BufferedReader br = null;
		try {
			StringEntity input = new StringEntity(request);
			client = AndroidHttpClient.newInstance("Android");

			// HttpClient client = new AndroidHttpClient();
			HttpPost post = new HttpPost(OFX_SERVER_URL);
			post.setHeader("Content-type", "application/x-ofx");
			post.setEntity(input);
			HttpResponse response = client.execute(post);

			br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));
			String output;
			StringBuilder sb = new StringBuilder();
			while ((output = br.readLine()) != null) {
				sb.append(output);
			}

			ofxResponse = constructOFXResponse(sb.toString());
			sb = null;
			br.close();
			client.getConnectionManager().shutdown();
			client.close();
		} catch (Exception e) {
			secu.org.util.LoggerHelper.e(
					secu.org.application.LoggingTag.GETSTATEMENTS.name(),
					"Error downLoadStatements " + e.getMessage(), this
							.getClass().getSimpleName());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					secu.org.util.LoggerHelper.e(
							secu.org.application.LoggingTag.GETSTATEMENTS
									.name(),
							"Error downLoadStatements " + e.getMessage(), this
									.getClass().getSimpleName());
				}
				if (client != null) {

					client.getConnectionManager().shutdown();
					client.close();
				}
			}
		}
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS", Locale.getDefault());
		String duration = sdf.format(new Date(totalTime - TimeZone.getDefault().getRawOffset()));
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"ending downLoadStatements duration [" + duration +"]", this.getClass().getSimpleName());
		// post.setConnectionRequest(connRequest)
		return ofxResponse;
	}
    /**
     * improve performance
     * @param userId
     * @param password
     * @param account
     * @return
     * @throws Exception
     */
	public List<AccountTransaction> downLoadAccountStatements(String userId,
			String password, Account account) throws Exception {
		secu.org.util.LoggerHelper
				.d(secu.org.application.LoggingTag.GETSTATEMENTS.name(),
						"starting downLoadAccountStatements ", this.getClass()
								.getSimpleName());
		// establish the connection
		List<AccountTransaction> transactionList = new ArrayList<AccountTransaction>();
		
		try {
			
			Bank bank = new Bank();
			bank.setPassword(password);
			bank.setUserId(userId);
			secu.org.ofx.response.OFX ofx;

			secu.org.ofx.request.Statement statement = new secu.org.ofx.request.Statement();
			statement.setIncludeTransaction("Y");
			statement.setUserID(userId);
			statement.setPassword(password);
			statement.setAccountId(account.getAccountNumber());

			statement.setAccountType(account.getAccountTypeEnum());

			ofx = this.downloadAccountTransactionsHelper(statement);

			List<secu.org.ofx.response.STMTTRN> statementList = new ArrayList<secu.org.ofx.response.STMTTRN>();
			if (ofx.getBANKMSGSRSV1().getSTMTTRNRS().getSTMTRS()
					.getBANKTRANLIST() != null) {
				statementList = ofx.getBANKMSGSRSV1().getSTMTTRNRS()
						.getSTMTRS().getBANKTRANLIST().getSTMTTRN();

			}
			if (ofx.getSIGNONMSGSRSV1().getSonrs().getStatus().getCODE()
					.equalsIgnoreCase("0")) { // get balance
				account.setAccountBalance(ofx.getBANKMSGSRSV1().getSTMTTRNRS()
						.getSTMTRS().getAVAILBAL().getBALAMT());
				account.setDateOfBalance(ofx.getBANKMSGSRSV1().getSTMTTRNRS()
						.getSTMTRS().getLEDGERBAL().getDTASOF());

				if (statementList.size() > 0) {
					for (secu.org.ofx.response.STMTTRN transaction : statementList) {
						AccountTransaction accountTransaction = new AccountTransaction();
						accountTransaction
								.setAccountTransactionType(transaction
										.getTRNTYPE());
						accountTransaction.setAmount(transaction.getTRNAMT());
						accountTransaction.setDatePosted(transaction
								.getDTPOSTED());
						accountTransaction.setName(transaction.getNAME());
						accountTransaction.setMemo(transaction.getMEMO());
						transactionList.add(accountTransaction);

					}
				}
			}

			//
			ofx = null;
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.GETSTATEMENTS.name(),
					"ending downLoadAccountStatements ", this.getClass()
							.getSimpleName());
			return transactionList;
		} catch (Exception ex) {
			secu.org.util.LoggerHelper.e(
					secu.org.application.LoggingTag.GETSTATEMENTS.name(),
					"Error downLoadAccountStatements " + ex.getMessage(), this
							.getClass().getSimpleName());
			throw new Exception(ex);
		}
		// return null;
	}
	
	public List<AccountTransaction> downloadAccountTransactionsHelperPerformance(
			secu.org.ofx.request.Statement statement) {
		long startTime   = System.currentTimeMillis();
		// HttpClient client = new DefaultHttpClient();
		secu.org.util.LoggerHelper
				.d(secu.org.application.LoggingTag.GETSTATEMENTS.name(),
						"starting downLoadStatements ", this.getClass()
								.getSimpleName());
		AndroidHttpClient client = null;
		 List<AccountTransaction> transactionList = null;
		// post.setHeader("Connection", "close");
		String request = createStatementRequest(statement);
		
		BufferedReader br = null;
		try {
			StringEntity input = new StringEntity(request);
			client = AndroidHttpClient.newInstance("Android");

			// HttpClient client = new AndroidHttpClient();
			HttpPost post = new HttpPost(OFX_SERVER_URL);
			post.setHeader("Content-type", "application/x-ofx");
			post.setEntity(input);
			HttpResponse response = client.execute(post);

			br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));
			String output;
			StringBuilder sb = new StringBuilder();
			while ((output = br.readLine()) != null) {
				sb.append(output);
			}

			transactionList = HTTPResponseParser.getSingletonObject().parseAccountTransactions(sb.toString());
			sb = null;
			br.close();
			client.getConnectionManager().shutdown();
			client.close();
		} catch (Exception e) {
			secu.org.util.LoggerHelper.e(
					secu.org.application.LoggingTag.GETSTATEMENTS.name(),
					"Error downLoadStatements " + e.getMessage(), this
							.getClass().getSimpleName());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					secu.org.util.LoggerHelper.e(
							secu.org.application.LoggingTag.GETSTATEMENTS
									.name(),
							"Error downLoadStatements " + e.getMessage(), this
									.getClass().getSimpleName());
				}
				if (client != null) {

					client.getConnectionManager().shutdown();
					client.close();
				}
			}
		}
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS", Locale.getDefault());
		String duration = sdf.format(new Date(totalTime - TimeZone.getDefault().getRawOffset()));
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"ending downLoadStatements duration [" + duration +"]", this.getClass().getSimpleName());
		// post.setConnectionRequest(connRequest)
		return transactionList;
	}
	
	
	
	public List<AccountTransaction> downLoadAccountTransactionsPerformance(String userId,
			String password, Account account) throws Exception {
		secu.org.util.LoggerHelper
				.d(secu.org.application.LoggingTag.GETSTATEMENTS.name(),
						"starting downLoadStatements ", this.getClass()
								.getSimpleName());
		// establish the connection
		List<AccountTransaction> transactionList = new ArrayList<AccountTransaction>();
		
		try {
			
			Bank bank = new Bank();
			bank.setPassword(password);
			bank.setUserId(userId);
			

			secu.org.ofx.request.Statement statement = new secu.org.ofx.request.Statement();
			statement.setIncludeTransaction("Y");
			statement.setUserID(userId);
			statement.setPassword(password);
			statement.setAccountId(account.getAccountNumber());

			statement.setAccountType(account.getAccountTypeEnum());

			transactionList = this.downloadAccountTransactionsHelperPerformance(statement);

			

			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.GETSTATEMENTS.name(),
					"ending downLoadStatements ", this.getClass()
							.getSimpleName());
			
		} catch (Exception ex) {
			secu.org.util.LoggerHelper.e(
					secu.org.application.LoggingTag.GETSTATEMENTS.name(),
					"Error downLoadStatements " + ex.getMessage(), this
							.getClass().getSimpleName());
			throw new Exception(ex);
		}
		 return transactionList;
	}
	
	
	
	
	
	
	
	
	public List<AccountTransaction> downLoadAccountTransactions(String userId,
			String password, Account account) throws Exception {
		secu.org.util.LoggerHelper
				.d(secu.org.application.LoggingTag.GETSTATEMENTS.name(),
						"starting downLoadStatements ", this.getClass()
								.getSimpleName());
		// establish the connection
		List<AccountTransaction> transactionList = new ArrayList<AccountTransaction>();
		
		try {
			
			Bank bank = new Bank();
			bank.setPassword(password);
			bank.setUserId(userId);
			secu.org.ofx.response.OFX ofx;

			secu.org.ofx.request.Statement statement = new secu.org.ofx.request.Statement();
			statement.setIncludeTransaction("Y");
			statement.setUserID(userId);
			statement.setPassword(password);
			statement.setAccountId(account.getAccountNumber());

			statement.setAccountType(account.getAccountTypeEnum());

			ofx = this.downloadAccountTransactionsHelper(statement);

			List<secu.org.ofx.response.STMTTRN> statementList = new ArrayList<secu.org.ofx.response.STMTTRN>();
			if (ofx.getBANKMSGSRSV1().getSTMTTRNRS().getSTMTRS()
					.getBANKTRANLIST() != null) {
				statementList = ofx.getBANKMSGSRSV1().getSTMTTRNRS()
						.getSTMTRS().getBANKTRANLIST().getSTMTTRN();

			}
			if (ofx.getSIGNONMSGSRSV1().getSonrs().getStatus().getCODE()
					.equalsIgnoreCase("0")) { // get balance
				account.setAccountBalance(ofx.getBANKMSGSRSV1().getSTMTTRNRS()
						.getSTMTRS().getAVAILBAL().getBALAMT());
				account.setDateOfBalance(ofx.getBANKMSGSRSV1().getSTMTTRNRS()
						.getSTMTRS().getLEDGERBAL().getDTASOF());

				if (statementList.size() > 0) {
					for (secu.org.ofx.response.STMTTRN transaction : statementList) {
						AccountTransaction accountTransaction = new AccountTransaction();
						accountTransaction
								.setAccountTransactionType(transaction
										.getTRNTYPE());
						accountTransaction.setAmount(transaction.getTRNAMT());
						accountTransaction.setDatePosted(transaction
								.getDTPOSTED());
						accountTransaction.setName(transaction.getNAME());
						accountTransaction.setMemo(transaction.getMEMO());
						transactionList.add(accountTransaction);

					}
				}
			}

			//
			ofx = null;
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.GETSTATEMENTS.name(),
					"ending downLoadStatements ", this.getClass()
							.getSimpleName());
			return transactionList;
		} catch (Exception ex) {
			secu.org.util.LoggerHelper.e(
					secu.org.application.LoggingTag.GETSTATEMENTS.name(),
					"Error downLoadStatements " + ex.getMessage(), this
							.getClass().getSimpleName());
			throw new Exception(ex);
		}
		// return null;
	}

	public List<String> downloadAccountBalance(
			secu.org.ofx.request.Statement statement) {
		// HttpClient client = new DefaultHttpClient();
		secu.org.util.LoggerHelper
				.d(secu.org.application.LoggingTag.GETSTATEMENTS.name(),
						"starting downLoadStatements ", this.getClass()
								.getSimpleName());
		AndroidHttpClient client = null;

		// post.setHeader("Connection", "close");
		String request = createStatementRequest(statement);
		String balance = null;
		BufferedReader br = null;
		List<String> tags = null;
		try {
			StringEntity input = new StringEntity(request);
			client = AndroidHttpClient.newInstance("Android");

			// HttpClient client = new AndroidHttpClient();
			HttpPost post = new HttpPost(OFX_SERVER_URL);
			post.setHeader("Content-type", "application/x-ofx");
			// input.setContentType("application/x-ofx");
			post.setEntity(input);
			HttpResponse response = client.execute(post);

			// int responseCode = response.getStatusLine().getStatusCode();

			// String responseReason =
			// response.getStatusLine().getReasonPhrase();

			br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));
			String output;
			StringBuilder sb = new StringBuilder();
			while ((output = br.readLine()) != null) {
				sb.append(output + "\n");
			}
			
			tags =secu.org.xmlpullparser.response.parser.HTTPResponseParser.getSingletonObject().parseAccountListBalance(sb.toString());
			sb = null;
			br.close();
			client.getConnectionManager().shutdown();
			client.close();
		} catch (Exception e) {
			secu.org.util.LoggerHelper.e(
					secu.org.application.LoggingTag.GETSTATEMENTS.name(),
					"Error downLoadStatements " + e.getMessage(), this
							.getClass().getSimpleName());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					secu.org.util.LoggerHelper.e(
							secu.org.application.LoggingTag.GETSTATEMENTS
									.name(),
							"Error downLoadStatements " + e.getMessage(), this
									.getClass().getSimpleName());
				}
				if (client != null) {

					client.getConnectionManager().shutdown();
					client.close();
				}
			}
		}
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"ending downLoadStatements ", this.getClass().getSimpleName());
		// post.setConnectionRequest(connRequest)
		return tags;
	}
	
	
	
	
	
	
	/**
	 * use xmlpullparser to increase speed
	 * @param userId
	 * @param password
	 * @return
	 */
	public Bank downLoadBankAccounts(String userId,
			String password) {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETACCOUNTS.name(),
				"starting downLoadBankAccounts ", this.getClass()
						.getSimpleName());
		
		long startTime = System.currentTimeMillis();
		Bank bank = new Bank();

		try {
			
			bank.setPassword(password);
			bank.setUserId(userId);
			
			bank = downLoadAccountList(userId, password);
			
			if (bank == null) {
				bank.setOperationStoppedbyUser(true);
				return bank;
			}
		
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.GETACCOUNTS.name(),
					"ending downLoadBankAccountsAndStatements ", this
							.getClass().getSimpleName());
			
			for (Account account : bank.getAccountList()) {
				secu.org.ofx.request.Statement statement = new secu.org.ofx.request.Statement();
				statement.setUserID(userId);
				statement.setPassword(password);
				statement.setAccountId(account.getAccountNumber());

				statement.setAccountType(account.getAccountTypeEnum());
				List<String> tags = this.downloadAccountBalance(statement);
				account.setAccountBalance(tags.get(0));
				account.setDateOfBalance(tags.get(1));
			}
			long endTime   = System.currentTimeMillis();
			long totalTime = endTime - startTime;
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS", Locale.getDefault());
			String duration = sdf.format(new Date(totalTime - TimeZone.getDefault().getRawOffset()));
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.GETACCOUNTS.name(),
					"ending downLoadBankAccounts duration [" + duration + "]", this.getClass()
							.getSimpleName());
			
			
		} catch (Exception ex) {
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.GETACCOUNTS.name(),
					"Error downLoadBankAccountsAndStatements "
							+ ex.getMessage(), this.getClass().getSimpleName());
			return null;
			// throw new Exception(ex);
		
		}
		return bank;

	}
	
	
	
	
	

	/**
	 * download account and balances
	 * 
	 * @param userId
	 * @param password
	 * @return
	 */
	/**
	 * @param userId
	 * @param password
	 * @return
	 */
	public Bank downLoadBankAccountsAndStatements(String userId, String password) {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETACCOUNTS.name(),
				"starting downLoadBankAccountsAndStatements ", this.getClass()
						.getSimpleName());
	

		// establish the connection
		Bank bank = new Bank();
		long startTime = System.currentTimeMillis();

		try {
			
			bank.setPassword(password);
			bank.setUserId(userId);
			secu.org.ofx.response.OFX ofx = downLoadAccounts(userId, password);
			if (ofx == null) {
				bank.setOperationStoppedbyUser(true);
				return bank;
			}
			bank.setCode(ofx.getSIGNONMSGSRSV1().getSonrs().getStatus()
					.getCODE());
			List<secu.org.ofx.response.ACCTINFO> accountList = null;
			// loop thru and build account list
			if (bank.isValidLogon()) {
				accountList = ofx.getSignUPSGSRSV1().getACCTINFOTRNRS()
						.getACCTINFORS().getACCTINFO();
				for (secu.org.ofx.response.ACCTINFO accountInfo : accountList) {
					if (accountInfo.getBANKACCTINFO() == null) {
						// skip
						continue;
					}
					Account account = new Account(accountInfo.getBANKACCTINFO()
							.getBANKACCTFROM().getACCTTYPE().toString(),
							accountInfo.getBANKACCTINFO().getBANKACCTFROM()
									.getACCTID().toString());
					// get account statements

					secu.org.ofx.request.Statement statement = new secu.org.ofx.request.Statement();
					statement.setUserID(userId);
					statement.setPassword(password);
					statement.setAccountId(account.getAccountNumber());

					statement.setAccountType(account.getAccountTypeEnum());

					ofx = this.downloadAccountTransactionsHelper(statement);

					List<secu.org.ofx.response.STMTTRN> statementList = new ArrayList<secu.org.ofx.response.STMTTRN>();
					if (ofx.getBANKMSGSRSV1().getSTMTTRNRS().getSTMTRS()
							.getBANKTRANLIST() != null) {
						statementList = ofx.getBANKMSGSRSV1().getSTMTTRNRS()
								.getSTMTRS().getBANKTRANLIST().getSTMTTRN();

					}
					if (ofx.getSIGNONMSGSRSV1().getSonrs().getStatus()
							.getCODE().equalsIgnoreCase("0")) { // get balance
						account.setAccountBalance(ofx.getBANKMSGSRSV1()
								.getSTMTTRNRS().getSTMTRS().getLEDGERBAL()
								.getBALAMT());
						account.setDateOfBalance(ofx.getBANKMSGSRSV1()
								.getSTMTTRNRS().getSTMTRS().getLEDGERBAL()
								.getDTASOF());

						if (statementList.size() > 0) {
							for (secu.org.ofx.response.STMTTRN transaction : statementList) {
								AccountTransaction accountTransaction = new AccountTransaction();
								accountTransaction
										.setAccountTransactionType(transaction
												.getTRNTYPE());
								accountTransaction.setAmount(transaction
										.getTRNAMT());
								accountTransaction.setDatePosted(transaction
										.getDTPOSTED());
								accountTransaction.setName(transaction
										.getNAME());
								accountTransaction.setMemo(transaction
										.getMEMO());
								account.getAccountTransactionList().add(
										accountTransaction);
							}
						}
					}

					bank.getAccountList().add(account);
				}
			}// end of if (bank.isValidLogon())
				//
			ofx = null;
	
			long endTime   = System.currentTimeMillis();
			long totalTime = endTime - startTime;
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS", Locale.getDefault());
			String duration = sdf.format(new Date(totalTime - TimeZone.getDefault().getRawOffset()));
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.GETACCOUNTS.name(),
					"ending downLoadBankAccountsAndStatements duration [" + duration + "]", this
							.getClass().getSimpleName());
			return bank;
		} catch (Exception ex) {
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.GETACCOUNTS.name(),
					"Error downLoadBankAccountsAndStatements "
							+ ex.getMessage(), this.getClass().getSimpleName());
			return null;
			// throw new Exception(ex);
		
		}

	}

	/**
	 * Download accounts
	 * 
	 * @param userName
	 * @param password
	 * @return
	 */
	public void downLoadAllAccounts(String userName, String password) {
		String name = new MethodNameHelper().getName();
		LOGGER.info("Starting Method [" + name + "]");
		// HttpClient client = new DefaultHttpClient();
		AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
		InputStream inputStream = null;
		// HttpClient client = new AndroidHttpClient();
		HttpPost post = new HttpPost(OFX_SERVER_URL);
		post.setHeader("Content-type", "application/x-ofx");
		// post.setHeader("Connection", "close");
		String request = OFXRequestBuilder.buidTwoPointZeroAccountListRequest(
				userName, password);
		try {
			StringEntity input = new StringEntity(request);
			// input.setContentType("application/x-ofx");
			post.setEntity(input);
			HttpResponse response = client.execute(post);
			BufferedReader br = null;
			br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));
			String output;
			StringBuilder sb = new StringBuilder();
			int lineCounter = 0;
			while ((output = br.readLine()) != null) {
				if (lineCounter == 1) {
					lineCounter++;
					continue;
				}
				sb.append(output.trim());
				sb.append("\n");
				lineCounter++;
			}
			br.close();
			StringReader stringReader = new StringReader(sb.toString());
			String temp = sb.toString();
			// inputStream = response.getEntity().getContent();
			// parse response
			secu.org.ofx.response.parser.AccountListParser parser = new secu.org.ofx.response.parser.AccountListParser();
			Bank bank = parser.parse(stringReader);
			// inputStream.close();
			client.getConnectionManager().shutdown();
			client.close();
			client = null;
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
		} finally {

			if (client != null) {
				client.close();
			}

		}
		LOGGER.info("Ending Method [" + name + "]");

	}

	public HttpClient getHttpClient() {

		DefaultHttpClient client = null;

		try {

			KeyStore trustStore = KeyStore.getInstance(KeyStore
					.getDefaultType());
			trustStore.load(null, null);
			org.apache.http.conn.ssl.SSLSocketFactory sf = null;
			sf = new secu.org.security.SecurityManager(trustStore);
			sf.setHostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			// Setting up parameters
			HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, "utf-8");
			params.setBooleanParameter("http.protocol.expect-continue", false);

			// Setting timeout
			HttpConnectionParams.setConnectionTimeout(params, 15000);
			HttpConnectionParams.setSoTimeout(params, 15000);

			// Registering schemes for both HTTP and HTTPS
			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory
					.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));

			// Creating thread safe client connection manager
			ClientConnectionManager ccm = new ThreadSafeClientConnManager(
					params, registry);

			// Creating HTTP client
			client = new DefaultHttpClient(ccm, params);

			// Registering user name and password for authentication
			client.getCredentialsProvider().setCredentials(
					new AuthScope(null, -1),
					new UsernamePasswordCredentials("s16715d", "Secu111111"));

		} catch (Exception e) {
			client = new DefaultHttpClient();
		}

		return client;

	}

	/**
	 * using xmlpullparser instead of XStream - increase performance
	 * 
	 * @param userName
	 * @param password
	 * @return
	 */
	public secu.org.bank.Bank downLoadAccountList(String userName,
			String password) {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETACCOUNTS.name(),
				"starting downLoadAccountList ", this.getClass()
						.getSimpleName());
		long startTime = System.currentTimeMillis();
		// trust all certificates

		// HttpClient client = getHttpClient();
		AndroidHttpClient client = null;

		// String encoded = Base64.
		// post.setHeader("Connection", "close");
		String request = OFXRequestBuilder.buidTwoPointZeroAccountListRequest(
				userName, password);
		secu.org.ofx.response.OFX ofxResponse = null;
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		secu.org.bank.Bank bank = null;
		try {

			// HttpClient client = getHttpClient();
			client = AndroidHttpClient.newInstance("Android");

			HttpConnectionParams
					.setConnectionTimeout(client.getParams(), 15000);
			HttpConnectionParams.setSoTimeout(client.getParams(), 15000);

			// HttpClient client = new AndroidHttpClient();
			HttpPost post = new HttpPost(OFX_SERVER_URL);
			// HttpPost post = new HttpPost("https://tx234217.ncsecu.org");

			post.setHeader("Content-type", "application/x-ofx");

			StringEntity input = new StringEntity(request);
			post.setEntity(input);
			// Add your data

			// input.setContentType("application/x-ofx");
			/*
			 * password = "Secu111111"; byte[] data =
			 * password.getBytes("UTF-8"); String base64Password =
			 * Base64.encodeToString(data, Base64.DEFAULT); String userID =
			 * "s16715d"; byte[] datauserID = userID.getBytes("UTF-8"); String
			 * base64userID = Base64.encodeToString(datauserID, Base64.DEFAULT);
			 * post.addHeader("Authorization", "Basic " + base64userID + ":" +
			 * base64Password);
			 */

			HttpResponse response = client.execute(post);

			br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));
			String output;

			while ((output = br.readLine()) != null) {
				sb.append(output);
			}
			br.close();

			
			bank = secu.org.xmlpullparser.response.parser.HTTPResponseParser.getSingletonObject().parseAccountList(sb.toString());
			sb = null;

			client.getConnectionManager().shutdown();
			// client.close();
			client = null;
		} catch (Exception e) {
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.GETACCOUNTS.name(),
					"errors downLoadAccounts " + e.getMessage(), this
							.getClass().getSimpleName());
		} finally {
			if (client != null) {
				org.apache.http.conn.ClientConnectionManager clientConnectionManager = client
						.getConnectionManager();
				if (clientConnectionManager != null) {
					clientConnectionManager.shutdown();
				}
			}
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS", Locale.getDefault());
		String duration = sdf.format(new Date(totalTime - TimeZone.getDefault().getRawOffset()));
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETACCOUNTS.name(),
				"ending downLoadAccountList * duration [" + duration + "]", this.getClass().getSimpleName());

		return bank;
	}

	/**
	 * Download accounts
	 * 
	 * @param userName
	 * @param password
	 * @return
	 */
	public secu.org.ofx.response.OFX downLoadAccounts(String userName,
			String password) {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETACCOUNTS.name(),
				"starting downLoadAccounts ", this.getClass().getSimpleName());

		// trust all certificates

		// HttpClient client = getHttpClient();
		AndroidHttpClient client = null;

		// String encoded = Base64.
		// post.setHeader("Connection", "close");
		String request = OFXRequestBuilder.buidTwoPointZeroAccountListRequest(
				userName, password);
		secu.org.ofx.response.OFX ofxResponse = null;
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		try {

			// HttpClient client = getHttpClient();
			client = AndroidHttpClient.newInstance("Android");

			HttpConnectionParams
					.setConnectionTimeout(client.getParams(), 15000);
			HttpConnectionParams.setSoTimeout(client.getParams(), 5000);

			// HttpClient client = new AndroidHttpClient();
			HttpPost post = new HttpPost(OFX_SERVER_URL);
			// HttpPost post = new HttpPost("https://tx234217.ncsecu.org");

			post.setHeader("Content-type", "application/x-ofx");

			StringEntity input = new StringEntity(request);
			post.setEntity(input);
			// Add your data

			// input.setContentType("application/x-ofx");
			/*
			 * password = "Secu111111"; byte[] data =
			 * password.getBytes("UTF-8"); String base64Password =
			 * Base64.encodeToString(data, Base64.DEFAULT); String userID =
			 * "s16715d"; byte[] datauserID = userID.getBytes("UTF-8"); String
			 * base64userID = Base64.encodeToString(datauserID, Base64.DEFAULT);
			 * post.addHeader("Authorization", "Basic " + base64userID + ":" +
			 * base64Password);
			 */

			HttpResponse response = client.execute(post);

			br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));
			String output;

			while ((output = br.readLine()) != null) {
				sb.append(output);
			}
			br.close();

			ofxResponse = constructOFXResponse(sb.toString());
			sb = null;

			client.getConnectionManager().shutdown();
			// client.close();
			client = null;
		} catch (Exception e) {
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.GETACCOUNTS.name(),
					"errors downLoadAccounts " + e.getMessage(), this
							.getClass().getSimpleName());
		} finally {
			if (client != null) {
				org.apache.http.conn.ClientConnectionManager clientConnectionManager = client
						.getConnectionManager();
				if (clientConnectionManager != null) {
					clientConnectionManager.shutdown();
				}
			}
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETACCOUNTS.name(),
				"ending downLoadAccounts ", this.getClass().getSimpleName());

		return ofxResponse;
	}
}
