package secu.org.ofx;

import java.util.ArrayList;
import java.util.List;

import secu.org.ofx.request.EMAILMSGSRQV1;
import secu.org.ofx.request.MAIL;
import secu.org.ofx.request.MAILRQ;
import secu.org.ofx.request.MAILTRNRQ;
import secu.org.ofx.request.SIGNONMSGSRQV1;
import secu.org.ofx.request.SONRQ;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.reflection.FieldDictionary;
import com.thoughtworks.xstream.converters.reflection.PureJavaReflectionProvider;
import com.thoughtworks.xstream.converters.reflection.SortableFieldKeySorter;

/**
 * @author s16715d
 * 
 */
public class OFXOperatorSendMessage extends AbstractOFXOperator {

	

	/**
	 * URl connection to the OFX 2.0 server
	 * 
	 * @param ofxServerURL
	 */
	public OFXOperatorSendMessage() {
		// secu.org.http.logging.DebugLogConfig.enable();
		
	}

	/**
	 * Create message
	 * 
	 * @return
	 */
	protected String createMessageRequest(Object object) {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.SENDMESSAGE.name(),
				"starting createMessageRequest ", this.getClass().getSimpleName());
		secu.org.bank.Bank bank = (secu.org.bank.Bank)object;
		String header = OFXRequestBuilder.buidTwoHeader();
		SortableFieldKeySorter sorter = new SortableFieldKeySorter();
		sorter.registerFieldOrder(secu.org.ofx.request.OFXEmailMessage.class, new String[] {
				"SIGNONMSGSRQV1", "EMAILMSGSRQV1" });
		sorter.registerFieldOrder(secu.org.ofx.request.SONRQ.class,
				new String[] { "DTCLIENT", "USERID", "USERPASS", "LANGUAGE",
						"FI", "APPID", "APPVER" });
		sorter.registerFieldOrder(secu.org.ofx.request.FI.class, new String[] {
			"ORG", "FID" });
		sorter.registerFieldOrder(secu.org.ofx.request.MAILTRNRQ.class, new String[] {
				"TRNUID", "MAILRQ"});
		sorter.registerFieldOrder(secu.org.ofx.request.MAIL.class, new String[] {
			"USERID", "DTCREATED", "FROM", "TO","SUBJECT", "MSGBODY","INCIMAGES","USEHTML"});
	//	sorter.registerFieldOrder(secu.org.ofx.request.STATUS.class, new String[] {
		//	"CODE", "STATUS", "MAILRS"});
		// xstream = new XStream(new Sun14ReflectionProvider(new
		// FieldDictionary(sorter)));

	 	XStream xstream = new XStream(new PureJavaReflectionProvider(
				 new FieldDictionary(sorter)));
		// XStream xstream = new XStream();
		xstream.alias("OFX", secu.org.ofx.request.OFXEmailMessage.class);
		secu.org.ofx.request.OFXEmailMessage ofx = new secu.org.ofx.request.OFXEmailMessage();
		// sign on information
		SIGNONMSGSRQV1 SIGNONMSGSRQV1 = new SIGNONMSGSRQV1();
		SONRQ SONRQ = new SONRQ();
		SONRQ.setUSERID(bank.getUserId());
		SONRQ.setUSERPASS(bank.getPassword());
		ofx.setSIGNONMSGSRQV1(SIGNONMSGSRQV1);
		SIGNONMSGSRQV1.setsONRQ(SONRQ);

		EMAILMSGSRQV1 EMAILMSGSRQV1 = new EMAILMSGSRQV1();
		MAILTRNRQ mAILTRNRQ = new MAILTRNRQ();
		MAILRQ mAILRQ = new MAILRQ();
		MAIL mail = new MAIL();
		mail.setUSERID(bank.getUserId());
		mail.setFROM(bank.getMail().getFrom());
		mail.setSUBJECT(bank.getMail().getSubject());
		mail.setMSGBODY(bank.getMail().getBody());
		mAILRQ.setMAIL(mail);
		mAILTRNRQ.setMAILRQ(mAILRQ);
		
		EMAILMSGSRQV1.setMAILTRNRQ(mAILTRNRQ);

		ofx.setEMAILMSGSRQV1(EMAILMSGSRQV1);
		String request = null;
        try {
        	String toXML =xstream.toXML(ofx);
        	request = header + toXML;
        } catch (Exception ex) {
        	
        	
        	 secu.org.util.LoggerHelper.e(
     				secu.org.application.LoggingTag.SENDMESSAGE.name(),
     				"request  " + request, this.getClass().getSimpleName());
        	 secu.org.util.LoggerHelper.d(
     				secu.org.application.LoggingTag.SENDMESSAGE.name(),
     				"error " + ex.getMessage(), this.getClass().getSimpleName());
        	
        }
        
        secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.SENDMESSAGE.name(),
				"starting createMessageRequest ", this.getClass().getSimpleName());
		
		return request;
	}

	
	
    /**
     * download send secure message
     * @param bank
     * @return
     * @throws Exception
     */
	public String ofxOperation(Object object)
			throws Exception {
		secu.org.bank.Bank bank = (secu.org.bank.Bank)object;
		List<secu.org.message.Mail> mailMessages = new ArrayList<secu.org.message.Mail>();
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.SENDMESSAGE.name(),
				"starting ofxOperation ", this.getClass().getSimpleName());

		String request = this.createMessageRequest(bank);
		secu.org.ofx.response.OFX ofxResponse = this.getOFXResponse(request);
		String code = "1";
		try {
			secu.org.ofx.response.EMAILMSGSRSV1 emailMsg = ofxResponse
					.getEMAILMSGSRSV1();
			secu.org.ofx.response.MAILTRNRS mailTrnrs = emailMsg.getMAILTRNRS();
			code = mailTrnrs.getSTATUS().getCODE();
			

		} catch (Exception e) {
			 secu.org.util.LoggerHelper.d(
	     				secu.org.application.LoggingTag.SENDMESSAGE.name(),
	     				"error " + e.getMessage(), this.getClass().getSimpleName());
			return code;

		}

		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.SENDMESSAGE.name(),
				"starting ofxOperation ", this.getClass().getSimpleName());

		// post.setConnectionRequest(connRequest)
		return code;
	}

	
	
	
	
}
