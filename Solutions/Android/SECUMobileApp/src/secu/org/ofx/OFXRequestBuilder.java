package secu.org.ofx;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Builds a String containing the content of an OFX request by combining
 * separate parts of the request together
 * @author Alex Spurling / Chris Thornton
 *
 */
public class OFXRequestBuilder {

	private static final String NW_ORG = "SECU";
	private static final String NW_FID = "1001";
    /**
     * build two header
     * @return
     */
	public static String buidTwoHeader() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<?xml version=\"1.0\">\n");
		buffer.append("<?OFX OFXHEADER=\"200\" VERSION=\"200\" SECURITY=\"NONE\" OLDFILEUID=\"NONE\" NEWFILEUID=\"NONE\"?>\n");
		return buffer.toString();
	}
	/**
	 * 
	 * @return 2.0 header
	 */
	public static String buidTwoPointZeroAccountListRequest(String userName, String password) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<?xml version=\"1.0\" encoding=\"utf-16\"?>\n");
		buffer.append("<?OFX OFXHEADER=\"200\" VERSION=\"200\" SECURITY=\"NONE\" OLDFILEUID=\"NONE\" NEWFILEUID=\"NONE\"?>\n");
		buffer.append("<OFX>");
		buffer.append("<SIGNONMSGSRQV1>");
		buffer.append("<SONRQ>");
		buffer.append("<DTCLIENT>"  + getCurDate() + "</DTCLIENT>");
		buffer.append("<USERID>" + userName + "</USERID>");
		buffer.append("<USERPASS>" + password + "</USERPASS>");
		buffer.append("<LANGUAGE>ENG</LANGUAGE>");
		buffer.append("<FI>");
		buffer.append("<ORG>SECU</ORG>");
		buffer.append("<FID>1001</FID>");
		buffer.append("</FI>");
		buffer.append("<APPID>QWIN</APPID>");
		buffer.append("<APPVER>0500</APPVER>");
		buffer.append("</SONRQ>");
		buffer.append("</SIGNONMSGSRQV1>");
		buffer.append("<SIGNUPMSGSRQV1>");
		buffer.append("<ACCTINFOTRNRQ>");
		buffer.append("<TRNUID> " + generateID() + "</TRNUID>");
		buffer.append("<CLTCOOKIE>4</CLTCOOKIE>");
		buffer.append("<ACCTINFORQ>");
		buffer.append("<DTACCTUP>19700101000000</DTACCTUP>");
		buffer.append("</ACCTINFORQ>");
		buffer.append("</ACCTINFOTRNRQ>");
		buffer.append("</SIGNUPMSGSRQV1>");
		buffer.append("</OFX>");

		return buffer.toString();
	}
	
	
	/**
	 * Returns the OFX header.
	 * @param oldFileID
	 * @param newFileID
	 * @return
	 */
	public static String getOFXHeader(String oldFileID, String newFileID) {
		if (oldFileID == null) oldFileID = "NONE";
		if (newFileID == null) newFileID = "NONE";
		return
		"OFXHEADER:100\n" +
		"DATA:OFXSGML\n" +
		"VERSION:102\n" +
		"SECURITY:NONE\n" +
		"ENCODING:USASCII\n" +
		"CHARSET:1252\n" +
		"COMPRESSION:NONE\n" +
		"OLDFILEUID:" + oldFileID + "\n" +
		"NEWFILEUID:" + newFileID + "\n";
	}

	/**
	 * Returns the OFX tags to make a sign on request
	 * @param userID
	 * @param userPass
	 * @return
	 */
	public static String getSignOnRequest(String userID, String userPass) {
		return
		"<SIGNONMSGSRQV1>\n" +
		"<SONRQ>\n" +
		"<DTCLIENT>" + getCurDate() + "\n" +
		"<USERID>" + userID + "\n" +
		"<USERPASS>" + userPass + "\n" +
		"<LANGUAGE>ENG\n" +
		"<FI>\n" +
		"<ORG>" + NW_ORG + "\n" +
		"<FID>" + NW_FID + "\n" +
		"</FI>\n" +
		"<APPID>QWIN\n" +
		"<APPVER>2000\n" +
		"</SONRQ>\n" +
		"</SIGNONMSGSRQV1>\n";
	}

	/**
	 * Returns the OFX tags to request an account list
	 * @return
	 */
	public static String getAccountListRequest() {
		return
		"<SIGNUPMSGSRQV1>\n" +
		"<ACCTINFOTRNRQ>\n" +
		"<TRNUID>" + generateID() + "\n" +
		"<CLTCOOKIE>4" + "\n" +
		"<ACCTINFORQ>\n" +
		"<DTACCTUP>19700101000000\n" + //The date that we last received the account list
		"</ACCTINFORQ>\n" +
		"</ACCTINFOTRNRQ>\n" +
		"</SIGNUPMSGSRQV1>\n";
	}



	/**
	 * Completes a request by combining the header with the body and ensuring
	 * the body is contained in <OFX></OFX> tags
	 * @param header
	 * @param body
	 * @return
	 */
	public static String getCompleteRequest(String header, String body) {
		if (body != null && !body.startsWith("<OFX>") && !body.endsWith("</OFX>\n")) {
			body = "<OFX>\n" + body + "</OFX>\n";
		}
		return header + "\n" + body;
	}

	/**
	 * Returns the current date in the format yyyyMMddHHmmss eg
	 * 20080613230844
	 * @return
	 */
	private static String getCurDate() {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		return format.format(new Date());
	}

	/**
	 * Generates a unique ID for keeping track of transaction requests
	 * @return
	 */
	public static String generateID() {
		return UUID.randomUUID().toString().toUpperCase();
	}

	/**
	 * Remove unique identifiers to make comparison of requests possible
	 * @param input
	 * @return
	 */
	public static String removeUIDs(String input) {
		return input.replaceAll("(?m)^.*UID.*$", "");
	}

	/**
	 * Remove current dates to make comparison of requests possible
	 * @param input
	 * @return
	 */
	public static String removeClientDates(String input) {
		return input.replaceAll("(?m)^.*<DTCLIENT>.*$", "");
	}
}
