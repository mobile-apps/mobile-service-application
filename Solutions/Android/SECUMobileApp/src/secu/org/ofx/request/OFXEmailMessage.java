package secu.org.ofx.request;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@SuppressWarnings("serial")
public class OFXEmailMessage implements Serializable {
	

	@XStreamAlias("SIGNONMSGSRQV1")
	private SIGNONMSGSRQV1 SIGNONMSGSRQV1 = new SIGNONMSGSRQV1();

	@XStreamAlias("EMAILMSGSRQV1")
	private EMAILMSGSRQV1 EMAILMSGSRQV1;

	
	public EMAILMSGSRQV1 getEMAILMSGSRQV1() {
		return EMAILMSGSRQV1;
	}

	public void setEMAILMSGSRQV1(EMAILMSGSRQV1 eMAILMSGSRQV1) {
		EMAILMSGSRQV1 = eMAILMSGSRQV1;
	}



	public SIGNONMSGSRQV1 getSIGNONMSGSRQV1() {
		return SIGNONMSGSRQV1;
	}

	public void setSIGNONMSGSRQV1(SIGNONMSGSRQV1 signOnMSGSRQV1) {
		this.SIGNONMSGSRQV1 = signOnMSGSRQV1;
	}

}
