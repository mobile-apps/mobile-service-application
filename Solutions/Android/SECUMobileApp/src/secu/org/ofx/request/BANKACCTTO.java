package secu.org.ofx.request;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@SuppressWarnings("serial")
public class BANKACCTTO implements Serializable{
private String BANKID = "253177049";
private String ACCTID;
public String getBANKID() {
	return BANKID;
}
public void setBANKID(String bANKID) {
	BANKID = bANKID;
}
public String getACCTID() {
	return ACCTID;
}
public void setACCTID(String aCCTID) {
	ACCTID = aCCTID;
}
public String getACCTTYPE() {
	return ACCTTYPE;
}
public void setACCTTYPE(String aCCTTYPE) {
	ACCTTYPE = aCCTTYPE;
}
private String ACCTTYPE;
}
