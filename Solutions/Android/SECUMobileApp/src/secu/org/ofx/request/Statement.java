package secu.org.ofx.request;

public class Statement {
	private String userID;
	private String password;
	private String accountId;
	private String includeTransaction = "N";
	public String getIncludeTransaction() {
		return includeTransaction;
	}

	public void setIncludeTransaction(String includeTransaction) {
		this.includeTransaction = includeTransaction;
	}

	private secu.org.account.AccountType accountType;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public secu.org.account.AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(secu.org.account.AccountType accountType) {
		this.accountType = accountType;
	}

}
