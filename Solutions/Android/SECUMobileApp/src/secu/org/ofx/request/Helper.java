package secu.org.ofx.request;

import java.util.UUID;

public class Helper {
	public static String generateID() {
		return UUID.randomUUID().toString().toUpperCase();
	}

}
