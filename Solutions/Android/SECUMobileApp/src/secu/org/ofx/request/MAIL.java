package secu.org.ofx.request;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


import java.io.Serializable;

@SuppressWarnings("serial")
public class MAIL implements Serializable {
private String USERID;
private String DTCREATED = computeToDay();
private String FROM;
private String TO="SECU";
private String SUBJECT;
private String MSGBODY;
private String INCIMAGES = "N";
private String USEHTML="N";

public String getUSERID() {
	return USERID;
}

public void setUSERID(String uSERID) {
	USERID = uSERID;
}

public String getDTCREATED() {
	return DTCREATED;
}

public void setDTCREATED(String dTCREATED) {
	DTCREATED = dTCREATED;
}

public String getFROM() {
	return FROM;
}

public void setFROM(String fROM) {
	FROM = fROM;
}

public String getTO() {
	return TO;
}

public void setTO(String tO) {
	TO = tO;
}

public String getSUBJECT() {
	return SUBJECT;
}

public void setSUBJECT(String sUBJECT) {
	SUBJECT = sUBJECT;
}

public String getMSGBODY() {
	return MSGBODY;
}

public void setMSGBODY(String mSGBODY) {
	MSGBODY = mSGBODY;
}

public String getINCIMAGES() {
	return INCIMAGES;
}

public void setINCIMAGES(String iNCIMAGES) {
	INCIMAGES = iNCIMAGES;
}

public String getUSEHTML() {
	return USEHTML;
}

public void setUSEHTML(String uSEHTML) {
	USEHTML = uSEHTML;
}

public String computeToDay() {
	 Date date = new Date();
	 DateFormat format = new SimpleDateFormat("yyyyMMdd");
	 String formattedDate = format.format(date);
	 return formattedDate;
}
}
