package secu.org.ofx.request;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.io.Serializable;

@SuppressWarnings("serial")
@XStreamAlias("STMTTRNRQ")
public class STMTTRNRQ implements Serializable{
     public STMTRQ getSTMTRQ() {
		return STMTRQ;
	}

	public void setSTMTRQ(STMTRQ sTMTRQ) {
		STMTRQ = sTMTRQ;
	}
	private String TRNUID =generateID();
     @XStreamAlias("STMTRQ")
     private STMTRQ STMTRQ = new STMTRQ();



	public String getTRNUID() {
		return TRNUID;
	}

	public void setTRNUID(String tRNUID) {
		TRNUID = tRNUID;
	}
	/**
	 * Generates a unique ID for keeping track of transaction requests
	 * @return
	 */
	public static String generateID() {
		return UUID.randomUUID().toString().toUpperCase();
	}
}
