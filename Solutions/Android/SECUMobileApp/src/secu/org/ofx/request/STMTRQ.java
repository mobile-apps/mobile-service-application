package secu.org.ofx.request;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@SuppressWarnings("serial")
public class STMTRQ implements Serializable{
	@XStreamAlias("BANKACCTFROM")
private BANKACCTFROM BANKACCTFROM;
	@XStreamAlias("INCTRAN")
private INCTRAN INCTRAN = new INCTRAN();
public INCTRAN getINCTRAN() {
	return INCTRAN;
}

public void setINCTRAN(INCTRAN incTRAN) {
	this.INCTRAN = incTRAN;
}

public BANKACCTFROM getBANKACCTFROM() {
	return BANKACCTFROM;
}

public void setBANKACCTFROM(BANKACCTFROM bankACCTFROM) {
	this.BANKACCTFROM = bankACCTFROM;
}
}
