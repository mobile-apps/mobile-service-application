package secu.org.ofx.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@SuppressWarnings("serial")
public class BANKMAILRQ implements Serializable {
	@XStreamAlias("BANKACCTFROM")
	private BANKACCTFROM BANKACCTFROM = new BANKACCTFROM();
	@XStreamAlias("MAIL")
	private MAIL MAIL = new MAIL();

	public BANKACCTFROM getBANKACCTFROM() {
		return BANKACCTFROM;
	}

	public void setBANKACCTFROM(BANKACCTFROM bANKACCTFROM) {
		BANKACCTFROM = bANKACCTFROM;
	}

	public MAIL getMAIL() {
		return MAIL;
	}

	public void setMAIL(MAIL mAIL) {
		MAIL = mAIL;
	}
}
