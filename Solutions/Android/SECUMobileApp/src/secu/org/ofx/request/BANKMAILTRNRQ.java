package secu.org.ofx.request;
import java.io.Serializable;
import java.util.UUID;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@SuppressWarnings("serial")
public class BANKMAILTRNRQ implements Serializable{
private String TRNUID = Helper.generateID();

public String getTRNUID() {
	return TRNUID;
}
public void setTRNUID(String tRNUID) {
	TRNUID = tRNUID;
}
public BANKMAILRQ getBANKMAILRQ() {
	return BANKMAILRQ;
}
public void setBANKMAILRQ(BANKMAILRQ bANKMAILRQ) {
	BANKMAILRQ = bANKMAILRQ;
}
@XStreamAlias("BANKMAILRQ")
private BANKMAILRQ BANKMAILRQ = new BANKMAILRQ();

}
