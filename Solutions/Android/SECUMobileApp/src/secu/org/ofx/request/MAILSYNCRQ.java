package secu.org.ofx.request;
import java.io.Serializable;

@SuppressWarnings("serial")
public class MAILSYNCRQ implements Serializable{
	private String TOKEN="1";
	private String REJECTIFMISSING="N";
	private String INCIMAGES="N";
	private String USEHTML="N";
	public String getTOKEN() {
		return TOKEN;
	}
	public void setTOKEN(String tOKEN) {
		TOKEN = tOKEN;
	}
	public String getREJECTIFMISSING() {
		return REJECTIFMISSING;
	}
	public void setREJECTIFMISSING(String rEJECTIFMISSING) {
		REJECTIFMISSING = rEJECTIFMISSING;
	}
	public String getINCIMAGES() {
		return INCIMAGES;
	}
	public void setINCIMAGES(String iNCIMAGES) {
		INCIMAGES = iNCIMAGES;
	}
	public String getUSEHTML() {
		return USEHTML;
	}
	public void setUSEHTML(String uSEHTML) {
		USEHTML = uSEHTML;
	}
	
}
