package secu.org.ofx.request;
import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@SuppressWarnings("serial")
public class INTRARQ implements Serializable{
	@XStreamAlias("XFERINFO")
	private XFERINFO XFERINFO = new XFERINFO();

	public XFERINFO getXFERINFO() {
		return XFERINFO;
	}

	public void setXFERINFO(XFERINFO xFERINFO) {
		XFERINFO = xFERINFO;
	}

}
