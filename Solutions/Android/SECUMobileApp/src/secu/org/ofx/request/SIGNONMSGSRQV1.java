package secu.org.ofx.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@SuppressWarnings("serial")
@XStreamAlias("SIGNONMSGSRQV1")
public class SIGNONMSGSRQV1 implements Serializable {

	@XStreamAlias("SONRQ")
	private SONRQ SONRQ = new SONRQ();

	public SONRQ getSONRQ() {
		return SONRQ;
	}

	public void setsONRQ(SONRQ sONRQ) {
		this.SONRQ = sONRQ;
	}
}
