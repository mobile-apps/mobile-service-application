package secu.org.ofx.request;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@SuppressWarnings("serial")
public class BANKMAILSYNCRQ implements Serializable{
private String TOKEN;
private String REJECTIFMISSING="N";
private String INCIMAGES="N";
private String USEHTML="N";
//STMTTRNRQ
@XStreamAlias("BANKACCTFROM")
private BANKACCTFROM BANKACCTFROM = new BANKACCTFROM();
@XStreamAlias("BANKMAILTRNRQ")
private BANKMAILTRNRQ BANKMAILTRNRQ = new BANKMAILTRNRQ();
public BANKMAILTRNRQ getBANKMAILTRNRQ() {
	return BANKMAILTRNRQ;
}
public void setBANKMAILTRNRQ(BANKMAILTRNRQ bANKMAILTRNRQ) {
	BANKMAILTRNRQ = bANKMAILTRNRQ;
}
public BANKACCTFROM getBANKACCTFROM() {
	return BANKACCTFROM;
}
public void setBANKACCTFROM(BANKACCTFROM bANKACCTFROM) {
	BANKACCTFROM = bANKACCTFROM;
}
public String getTOKEN() {
	return TOKEN;
}
public void setTOKEN(String tOKEN) {
	TOKEN = tOKEN;
}
public String getREJECTIFMISSING() {
	return REJECTIFMISSING;
}
public void setREJECTIFMISSING(String rEJECTIFMISSING) {
	REJECTIFMISSING = rEJECTIFMISSING;
}
public String getINCIMAGES() {
	return INCIMAGES;
}
public void setINCIMAGES(String iNCIMAGES) {
	INCIMAGES = iNCIMAGES;
}
public String getUSEHTML() {
	return USEHTML;
}
public void setUSEHTML(String uSEHTML) {
	USEHTML = uSEHTML;
}
}
