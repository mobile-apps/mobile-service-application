package secu.org.ofx.request;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@SuppressWarnings("serial")
public class INCTRAN implements Serializable{
	private String INCLUDE = "N";

	public String getINCLUDE() {
		return INCLUDE;
	}

	public void setINCLUDE(String iNCLUDE) {
		INCLUDE = iNCLUDE;
	}
}
