package secu.org.ofx.request;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@SuppressWarnings("serial")
public class OFX implements Serializable {
	

	@XStreamAlias("SIGNONMSGSRQV1")
	private SIGNONMSGSRQV1 SIGNONMSGSRQV1 = new SIGNONMSGSRQV1();
	@XStreamAlias("BANKMSGSRQV1")
	private BANKMSGSRQV1 BANKMSGSRQV1;


	

	public BANKMSGSRQV1 getBANKMSGSRQV1() {
		return BANKMSGSRQV1;
	}

	public void setBANKMSGSRQV1(BANKMSGSRQV1 bankMSGSRQV1) {
		this.BANKMSGSRQV1 = bankMSGSRQV1;
	}

	public SIGNONMSGSRQV1 getSIGNONMSGSRQV1() {
		return SIGNONMSGSRQV1;
	}

	public void setSIGNONMSGSRQV1(SIGNONMSGSRQV1 signOnMSGSRQV1) {
		this.SIGNONMSGSRQV1 = signOnMSGSRQV1;
	}

}
