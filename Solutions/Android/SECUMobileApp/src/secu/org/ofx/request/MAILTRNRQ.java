package secu.org.ofx.request;
import java.io.Serializable;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@SuppressWarnings("serial")
@XStreamAlias("MAILTRNRQ")
public class MAILTRNRQ implements Serializable {
	private String TRNUID = Helper.generateID();
	public String getTRNUID() {
		return TRNUID;
	}

	public void setTRNUID(String tRNUID) {
		TRNUID = tRNUID;
	}
	@XStreamAlias("MAILRQ")
	private MAILRQ MAILRQ;

	public MAILRQ getMAILRQ() {
		return MAILRQ;
	}

	public void setMAILRQ(MAILRQ mAILRQ) {
		MAILRQ = mAILRQ;
	}
	

}
