package secu.org.ofx.request;
import java.io.Serializable;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@SuppressWarnings("serial")
@XStreamAlias("MAILRQ")
public class MAILRQ implements Serializable {
	@XStreamAlias("MAIL")
	private MAIL MAIL;

	public MAIL getMAIL() {
		return MAIL;
	}

	public void setMAIL(MAIL mAIL) {
		MAIL = mAIL;
	}
	

}
