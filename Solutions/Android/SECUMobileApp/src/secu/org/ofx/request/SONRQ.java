package secu.org.ofx.request;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class SONRQ implements Serializable{
	private String DTCLIENT = getCurDate();
    private String USERID;
    private String USERPASS;
    private String LANGUAGE = "ENG";
	@XStreamAlias("FI")
	private FI FI = new FI();
	private String APPID="QWIN";
	private String APPVER="0500";
    public String getAPPID() {
		return APPID;
	}
	public void setAPPID(String aPPID) {
		APPID = aPPID;
	}
	public String getAPPVER() {
		return APPVER;
	}
	public void setAPPVER(String aPPVER) {
		APPVER = aPPVER;
	}
	public FI getFI() {
		return FI;
	}
	public void setFI(FI fi) {
		this.FI = fi;
	}
	public String getDTCLIENT() {
		return DTCLIENT;
	}
	public void setDTCLIENT(String dTCLIENT) {
		DTCLIENT = dTCLIENT;
	}
	public String getUSERID() {
		return USERID;
	}
	public void setUSERID(String uSERID) {
		USERID = uSERID;
	}
	public String getUSERPASS() {
		return USERPASS;
	}
	public void setUSERPASS(String uSERPASS) {
		USERPASS = uSERPASS;
	}
	public String getLANGUAGE() {
		return LANGUAGE;
	}
	public void setLANGUAGE(String lANGUAGE) {
		LANGUAGE = lANGUAGE;
	}
	private static String getCurDate() {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		return format.format(new Date());
	}

}
