package secu.org.ofx.request;
import java.io.Serializable;
import java.util.UUID;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@SuppressWarnings("serial")
public class INTRATRNRQ implements Serializable{
private String TRNUID = generateID();

@XStreamAlias("INTRARQ")
private INTRARQ INTRARQ = new INTRARQ();

public INTRARQ getINTRARQ() {
	return INTRARQ;
}

public void setINTRARQ(INTRARQ iNTRARQ) {
	INTRARQ = iNTRARQ;
}

public String getTRNUID() {
	return  generateID();
}

public void setTRNUID(String tRNUID) {
	TRNUID = tRNUID;
}
/**
 * Generates a unique ID for keeping track of transaction requests
 * @return
 */
public static String generateID() {
	return UUID.randomUUID().toString().toUpperCase();
}
}
