package secu.org.ofx.request;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@SuppressWarnings("serial")
public class BANKMSGSRQV1 implements Serializable{
	//STMTTRNRQ
	@XStreamAlias("STMTTRNRQ")
    private STMTTRNRQ STMTTRNRQ;
	@XStreamAlias("INTRATRNRQ")
	private INTRATRNRQ INTRATRNRQ;
	@XStreamAlias("BANKMAILSYNCRQ")
	private BANKMAILSYNCRQ BANKMAILSYNCRQ;
	public INTRATRNRQ getINTRATRNRQ() {
		if (this.INTRATRNRQ == null) {
			this.INTRATRNRQ = new INTRATRNRQ();
		}
		return INTRATRNRQ;
	}

	public void setINTRATRNRQ(INTRATRNRQ iNTRATRNRQ) {
		INTRATRNRQ = iNTRATRNRQ;
	}

	public STMTTRNRQ getSTMTTRNRQ() {
		if (this.STMTTRNRQ == null) {
			this.STMTTRNRQ = new STMTTRNRQ();
		}
		return STMTTRNRQ;
	}

	public BANKMAILSYNCRQ getBANKMAILSYNCRQ() {
		if (this.BANKMAILSYNCRQ == null) {
			this.BANKMAILSYNCRQ = new BANKMAILSYNCRQ();
		}
		return BANKMAILSYNCRQ;
	}

	public void setBANKMAILSYNCRQ(BANKMAILSYNCRQ bANKMAILSYNCRQ) {
		BANKMAILSYNCRQ = bANKMAILSYNCRQ;
	}

	public void setSTMTTRNRQ(STMTTRNRQ list) {
		this.STMTTRNRQ = list;
	}
}
