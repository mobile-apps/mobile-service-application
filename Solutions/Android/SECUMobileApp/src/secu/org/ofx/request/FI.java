package secu.org.ofx.request;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@SuppressWarnings("serial")
public class FI implements Serializable{
    private String ORG="SECU";
    public String getORG() {
		return ORG;
	}
	public void setORG(String oRG) {
		ORG = oRG;
	}
	public String getFID() {
		return FID;
	}
	public void setFID(String fID) {
		FID = fID;
	}
	private String FID="1001";
}
