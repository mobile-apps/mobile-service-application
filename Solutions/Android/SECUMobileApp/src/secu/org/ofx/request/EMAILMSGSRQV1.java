package secu.org.ofx.request;
import java.io.Serializable;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@SuppressWarnings("serial")
@XStreamAlias("EMAILMSGSRQV1")
public class EMAILMSGSRQV1 implements Serializable{
	@XStreamAlias("MAILSYNCRQ")
	private MAILSYNCRQ MAILSYNCRQ;
	@XStreamAlias("MAILTRNRQ")
	private MAILTRNRQ MAILTRNRQ;

	public MAILTRNRQ getMAILTRNRQ() {
		return MAILTRNRQ;
	}

	public void setMAILTRNRQ(MAILTRNRQ mAILTRNRQ) {
		MAILTRNRQ = mAILTRNRQ;
	}

	public MAILSYNCRQ getMAILSYNCRQ() {
		return MAILSYNCRQ;
	}

	public void setMAILSYNCRQ(MAILSYNCRQ mAILSYNCRQ) {
		MAILSYNCRQ = mAILSYNCRQ;
	}

}
