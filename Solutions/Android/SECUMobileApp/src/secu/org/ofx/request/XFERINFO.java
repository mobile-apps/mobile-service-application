package secu.org.ofx.request;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@SuppressWarnings("serial")
public class XFERINFO implements Serializable {
	@XStreamAlias("BANKACCTFROM")
	private BANKACCTFROM BANKACCTFROM = new BANKACCTFROM();
	@XStreamAlias("BANKACCTTO")
	private BANKACCTTO BANKACCTTO = new BANKACCTTO();
    public String getTRNAMT() {
		return TRNAMT;
	}

	public void setTRNAMT(String tRNAMT) {
		TRNAMT = tRNAMT;
	}

	private String TRNAMT;
	public BANKACCTFROM getBANKACCTFROM() {
		return BANKACCTFROM;
	}

	public void setBANKACCTFROM(BANKACCTFROM bANKACCTFROM) {
		BANKACCTFROM = bANKACCTFROM;
	}

	public BANKACCTTO getBANKACCTTO() {
		return BANKACCTTO;
	}

	public void setBANKACCTTO(BANKACCTTO bANKACCTTO) {
		BANKACCTTO = bANKACCTTO;
	}
}
