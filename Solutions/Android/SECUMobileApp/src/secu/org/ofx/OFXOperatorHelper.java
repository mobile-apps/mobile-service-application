package secu.org.ofx;

import com.thoughtworks.xstream.XStream;

public class OFXOperatorHelper {

	private static OFXOperatorHelper singletonObject;
    private XStream xStream;
	
	public XStream getxStream() {
		return xStream;
	}

	
	/** A private Constructor prevents any other class from instantiating. */
	private OFXOperatorHelper(){
		xStream = setXStream();
		  //	 Optional Code
	}

	public Object clone()throws CloneNotSupportedException
	{
	    throw new CloneNotSupportedException(); 
	}

	public static synchronized OFXOperatorHelper getSingletonObject()
	{
	    if (singletonObject == null){
	    	singletonObject = new OFXOperatorHelper();
	    }
	    return singletonObject;
	}

	
	private XStream setXStream() {
		// Don't ever try to use DomDriver. They are VERY slow.
		//com.thoughtworks.xstream.io.xml.DomDriver driver = new com.thoughtworks.xstream.io.xml.DomDriver();
	//	com.thoughtworks.xstream.io.xml.StaxDriver driver = new com.thoughtworks.xstream.io.xml.StaxDriver();
		XStream xstream = new XStream();
		xstream.alias("OFX", secu.org.ofx.response.OFX.class);

		xstream.processAnnotations(secu.org.ofx.response.OFX.class);

		xstream.setClassLoader(secu.org.ofx.response.OFX.class.getClassLoader());
		return xstream;
	}
	
	
}
