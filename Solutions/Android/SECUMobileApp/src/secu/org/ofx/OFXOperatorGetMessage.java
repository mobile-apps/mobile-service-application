package secu.org.ofx;

import java.util.ArrayList;
import java.util.List;

import secu.org.ofx.request.EMAILMSGSRQV1;
import secu.org.ofx.request.MAILSYNCRQ;
import secu.org.ofx.request.SIGNONMSGSRQV1;
import secu.org.ofx.request.SONRQ;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.reflection.FieldDictionary;
import com.thoughtworks.xstream.converters.reflection.PureJavaReflectionProvider;
import com.thoughtworks.xstream.converters.reflection.SortableFieldKeySorter;

/**
 * @author s16715d
 * 
 */
public class OFXOperatorGetMessage extends AbstractOFXOperator {

	

	/**
	 * URl connection to the OFX 2.0 server
	 * 
	 * @param ofxServerURL
	 */
	public OFXOperatorGetMessage() {
		// secu.org.http.logging.DebugLogConfig.enable();
		
	}

	/**
	 * Create message
	 * 
	 * @return
	 */
	protected String createMessageRequest(Object object) {
	  	secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.OFX.name(),
				"starting createMessageRequest", this.getClass().getSimpleName());
		secu.org.bank.Bank bank = (secu.org.bank.Bank)object;
		String header = OFXRequestBuilder.buidTwoHeader();
		SortableFieldKeySorter sorter = new SortableFieldKeySorter();
		sorter.registerFieldOrder(secu.org.ofx.request.OFXEmailMessage.class, new String[] {
				"SIGNONMSGSRQV1", "EMAILMSGSRQV1" });
		sorter.registerFieldOrder(secu.org.ofx.request.SONRQ.class,
				new String[] { "DTCLIENT", "USERID", "USERPASS", "LANGUAGE",
						"FI", "APPID", "APPVER" });
		sorter.registerFieldOrder(secu.org.ofx.request.FI.class, new String[] {
			"ORG", "FID" });
		sorter.registerFieldOrder(secu.org.ofx.request.MAILSYNCRQ.class, new String[] {
				"TOKEN", "REJECTIFMISSING", "INCIMAGES", "USEHTML" });
		// xstream = new XStream(new Sun14ReflectionProvider(new
		// FieldDictionary(sorter)));

	 	XStream xstream = new XStream(new PureJavaReflectionProvider(
				 new FieldDictionary(sorter)));
		// XStream xstream = new XStream();
		xstream.alias("OFX", secu.org.ofx.request.OFXEmailMessage.class);
		secu.org.ofx.request.OFXEmailMessage ofx = new secu.org.ofx.request.OFXEmailMessage();
		// sign on information
		SIGNONMSGSRQV1 SIGNONMSGSRQV1 = new SIGNONMSGSRQV1();
		SONRQ SONRQ = new SONRQ();
		SONRQ.setUSERID(bank.getUserId());
		SONRQ.setUSERPASS(bank.getPassword());
		ofx.setSIGNONMSGSRQV1(SIGNONMSGSRQV1);
		SIGNONMSGSRQV1.setsONRQ(SONRQ);

		EMAILMSGSRQV1 EMAILMSGSRQV1 = new EMAILMSGSRQV1();
		MAILSYNCRQ mAILSYNCRQ = new MAILSYNCRQ();
		EMAILMSGSRQV1.setMAILSYNCRQ(mAILSYNCRQ);

		ofx.setEMAILMSGSRQV1(EMAILMSGSRQV1);
		String request = null;
        try {
        	String toXML =xstream.toXML(ofx);
        	request = header + toXML;
        } catch (Exception ex) {
        	secu.org.util.LoggerHelper.d(
    				secu.org.application.LoggingTag.OFX.name(),
    				"Error " + ex.getMessage(), this.getClass().getSimpleName());
        	
        }
		
    	secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.OFX.name(),
				"request " + request, this.getClass().getSimpleName());
        
		return request;
	}

	
	
    /**
     * download all secure messages
     * @param bank
     * @return
     * @throws Exception
     */
	public List<secu.org.message.Mail> ofxOperation(Object object)
			throws Exception {
		secu.org.bank.Bank bank = (secu.org.bank.Bank)object;
		List<secu.org.message.Mail> mailMessages = new ArrayList<secu.org.message.Mail>();
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.OFX.name(),
				"starting ofxOperation ", this.getClass().getSimpleName() );
        

		String request = this.createMessageRequest(bank);
		secu.org.ofx.response.OFX ofxResponse = this.getOFXResponse(request);
		
		try {
			secu.org.ofx.response.EMAILMSGSRSV1 emailMsg = ofxResponse
					.getEMAILMSGSRSV1();
			secu.org.ofx.response.MAILSYNCRS mailSyncrs = emailMsg
					.getMAILSYNCRS();
			List<secu.org.ofx.response.MAILTRNRS> mailTransList = mailSyncrs
					.getMAILTRNRS();
			// loop
			for (secu.org.ofx.response.MAILTRNRS mailTrnrs : mailTransList) {
				secu.org.ofx.response.MAILRS mailrs = mailTrnrs.getMAILRS();

				secu.org.message.Mail mail = new secu.org.message.Mail(mailrs
						.getMAIL().getDTCREATED(), mailrs.getMAIL().getFROM(),
						mailrs.getMAIL().getTO(),
						mailrs.getMAIL().getSUBJECT(), mailrs.getMAIL()
								.getMSGBODY());
				mailMessages.add(mail);

			}

		} catch (Exception e) {
			
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.OFX.name(),
					"error ofxOperation " + e.getMessage(), this.getClass().getSimpleName());
			return mailMessages;

		}

		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.OFX.name(),
				"ending ofxOperation ", this.getClass().getSimpleName() );

		// post.setConnectionRequest(connRequest)
		return mailMessages;
	}

	
	
	
	
}
