package secu.org.ofx.response;
import java.io.IOException;
import com.thoughtworks.xstream.*;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import java.io.StringReader;



import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class OFXConnection {

	private static final String OFX_SERVER_URL= "https://onlineaccess.ncsecu.org/secuofx/secu.ofx";
	private static final String NW_ORG = "SECU";
	private static final String NW_FID = "1001";

	/**
	 * URl connection to the OFX 2.0 server 
	 * @param ofxServerURL
	 */

	private OFX constructAccountList(String response) {
		com.thoughtworks.xstream.io.xml.DomDriver driver = new com.thoughtworks.xstream.io.xml.DomDriver();
		XStream xstream = new XStream(driver);
		xstream.alias("OFX", OFX.class);
		xstream.alias("SIGNONMSGSRSV1", SIGNONMSGSRSV1.class);
		xstream.processAnnotations(OFX.class);
		xstream.setClassLoader(OFX.class.getClassLoader());
        OFX ofx = new OFX();
     	ofx =(OFX)xstream.fromXML(response);
     	String xml = xstream.toXML(ofx);
     	System.out.println("*** ofx response " + xml);
     	System.out.println( xml);
     	return ofx;
		
    
		

	}
	
	public OFX downLoadAccounts(String userName, String password) {
		String result = "";
		System.out.println("*****Sending request*****");
		String request = OFXRequestBuilder.buidTwoPointZeroAccountListRequest(userName, password);
		System.out.println(request);
		URLConnection urlConnection = null;
		URL url = null;
	
		 OFX ofx= null;
		try {
			url = new URL(OFX_SERVER_URL);
			urlConnection = url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.setDoInput(true);
			// these are doNet or java web service
			urlConnection.setRequestProperty("Content-type", "application/x-ofx");
			OutputStream outputStream = urlConnection.getOutputStream();
			Writer messageWriter = new OutputStreamWriter(outputStream);
			messageWriter.write(request);
            messageWriter.flush();
            messageWriter.close();
            
            java.io.ByteArrayOutputStream outMessageStream = new java.io.ByteArrayOutputStream();
            InputStream inputStream = urlConnection.getInputStream();


            int c;
            while ((c = inputStream.read()) != -1) {
                outMessageStream.write(c);
            }
            inputStream.close();
            System.out.println("*****receiving results*****");
            System.out.println(outMessageStream.toString());
    		String response = outMessageStream.toString();
            outMessageStream.close();
    		//Something must have gone wrong
            ofx=  constructAccountList(response);

    		//Check for any status errors and throw exceptions if anything is not right
    		
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ofx;
	}
}
