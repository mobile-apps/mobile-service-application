package secu.org.ofx.response.parser;

import java.io.InputStream;
import java.io.StringReader;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import secu.org.account.Account;
import secu.org.bank.Bank;

import android.util.Xml;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AccountListParser extends AbstractParser {
	private final String CODE = "CODE";
	private final String ACCTINFO = "ACCTINFO";
	private final String ACCTID = "ACCTID";
	private final String ACCTTYPE = "ACCTTYPE";
	//CCTTYPE
	//

	private Bank readSONRS(XmlPullParser parser) throws XmlPullParserException,
			IOException {

		parser.require(XmlPullParser.START_TAG, ns, "SONRS");
		Bank bank = new Bank();
		parser.nextTag();
		String name = parser.getName();
		if (name.equals("STATUS")) {
			parser.nextTag();
			name = parser.getName();
			if (name.equals("CODE")) {
				String code = readCode(parser);
				bank.setCode(code);

			}
		}

		return bank;
	}

	private int convertNameToInt(String name) {
		int value = 0;
		if (name.equals(ACCTINFO)) {
			return 1;
		}
		if (name.equals(ACCTID)) {
			return 2;
		}
		if (name.equals("SIGNONMSGSRSV1")) {
			return 3;
		}
		return value;

	}

	// Processes title tags in the feed.
	private String readCode(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ns, "CODE");
		String title = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "CODE");
		return title;
	}

	/**
	 * Main entry point for read document
	 * 
	 * @param inputStream
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	public Bank parse(StringReader stringReader) throws XmlPullParserException,
			IOException {
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);

		XmlPullParser parser = factory.newPullParser();

		// parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
		parser.setInput(stringReader);
		int eventType = parser.getEventType();
		// parser.setInput(stringReader, null);
		//
		/*
		 * while (eventType != XmlPullParser.END_DOCUMENT) { if(eventType ==
		 * XmlPullParser.START_DOCUMENT) { System.out.println("Start document");
		 * 
		 * } else if(eventType == XmlPullParser.START_TAG) {
		 * System.out.println("Start tag "+parser.getName()); } else
		 * if(eventType == XmlPullParser.END_TAG) {
		 * System.out.println("End tag "+parser.getName()); } else if(eventType
		 * == XmlPullParser.TEXT) {
		 * System.out.println("Text "+parser.getText()); }
		 * 
		 * eventType = parser.next();
		 * 
		 * }
		 */
		Bank bank = new Bank();
		List<Account> accountList = new ArrayList<Account>();
		parser.nextTag();
		String name = null;
		parser.require(XmlPullParser.START_TAG, ns, "OFX");
		while (eventType != XmlPullParser.END_DOCUMENT) {

			if (eventType == XmlPullParser.START_TAG) {
				name = parser.getName();
				int nameInt = this.convertNameToInt(name);
				
				switch (nameInt) {
				case 0:
					break;
				case 1: //get accounts
					parser.nextTag();//BANKACCTINFO
					//name = parser.getName();
					parser.nextTag();//BANKACCTFROM
					//name = parser.getName();
					parser.nextTag();//BANKID
					//name = parser.getName();
					//name = parser.nextText();
					parser.nextTag();//ACCTID
					//name = parser.getName();
					String accountId = this.readElementValue(parser, ACCTID);
					parser.nextTag();//ACCTTYPE
					String accountType = this
							.readElementValue(parser, ACCTTYPE);
					//name = parser.getName();
					Account account = new Account(accountType, accountId);
					accountList.add(account);
					break;
				case 3: //get SIGNONMSGSRSV1
					parser.nextTag();//SONRS
					name = parser.getName();
					parser.nextTag();//STATUS
					parser.nextTag();//CODE
					name = parser.getName();
					String code = this.readElementValue(parser, CODE);
					bank.setCode(code);
					
					break;
			

				}

			}
			eventType = parser.next();
			
			

		}//end of while
		bank.setAccountList(accountList);
		return bank;
	}//end of method

}
