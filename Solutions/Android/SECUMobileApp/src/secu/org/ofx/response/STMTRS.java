package secu.org.ofx.response;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;
@XStreamAlias("STMTRS")
public class STMTRS implements Serializable{
     private String CURDEF;
     @XStreamAlias("AVAILBAL")
     private AVAILBAL AVAILBAL;
     public AVAILBAL getAVAILBAL() {
		return AVAILBAL;
	}

	public void setAVAILBAL(AVAILBAL aVAILBAL) {
		AVAILBAL = aVAILBAL;
	}

	@XStreamAlias("LEDGERBAL")
     private LEDGERBAL LEDGERBAL;
     public LEDGERBAL getLEDGERBAL() {
		return LEDGERBAL;
	}

	public void setLEDGERBAL(LEDGERBAL lEDGERBAL) {
		LEDGERBAL = lEDGERBAL;
	}

	@XStreamAlias("BANKTRANLIST")
     private BANKTRANLIST BANKTRANLIST;
     public BANKTRANLIST getBANKTRANLIST() {
		return BANKTRANLIST;
	}

	public void setBANKTRANLIST(BANKTRANLIST bANKTRANLIST) {
		BANKTRANLIST = bANKTRANLIST;
	}

	@XStreamAlias("BANKACCTFROM")
     private BANKACCTFROM BANKACCTFROM;

	public BANKACCTFROM getBANKACCTFROM() {
		return BANKACCTFROM;
	}

	public void setBANKACCTFROM(BANKACCTFROM bANKACCTFROM) {
		BANKACCTFROM = bANKACCTFROM;
	}

	public String getCURDEF() {
		return CURDEF;
	}

	public void setCURDEF(String cURDEF) {
		CURDEF = cURDEF;
	}
}
