package secu.org.ofx.response;
import com.thoughtworks.xstream.annotations.XStreamAlias;
@XStreamAlias("SIGNUPMSGSRSV1")
public class SIGNUPMSGSRSV1 {
	@XStreamAlias("ACCTINFOTRNRS")
	private ACCTINFOTRNRS ACCTINFOTRNRS;

	public ACCTINFOTRNRS getACCTINFOTRNRS() {
		return ACCTINFOTRNRS;
	}

	public void setACCTINFOTRNRS(ACCTINFOTRNRS aCCTINFOTRNRS) {
		ACCTINFOTRNRS = aCCTINFOTRNRS;
	}

}
