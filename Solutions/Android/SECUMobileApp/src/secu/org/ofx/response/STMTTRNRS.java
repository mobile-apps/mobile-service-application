package secu.org.ofx.response;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.util.UUID;
import java.io.Serializable;
@XStreamAlias("STMTTRNRS")
public class STMTTRNRS implements Serializable{
	private String TRNUID =generateID();
	@XStreamAlias("STATUS")
	private STATUS STATUS;
	@XStreamAlias("STMTRS")
	private STMTRS STMTRS;

	public STMTRS getSTMTRS() {
		return STMTRS;
	}

	public void setSTMTRS(STMTRS sTMTRS) {
		STMTRS = sTMTRS;
	}

	public STATUS getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(STATUS sTATUS) {
		STATUS = sTATUS;
	}

	public String getTRNUID() {
		return TRNUID;
	}

	public void setTRNUID(String tRNUID) {
		TRNUID = tRNUID;
	}
	/**
	 * Generates a unique ID for keeping track of transaction requests
	 * @return
	 */
	public static String generateID() {
		return UUID.randomUUID().toString().toUpperCase();
	}

}
