package secu.org.ofx.response;
import java.util.List;

import com.thoughtworks.xstream.annotations.*;
@XStreamAlias("ACCTINFORS")
public class ACCTINFORS {
	public String getDTACCTUP() {
		return DTACCTUP;
	}
	public void setDTACCTUP(String dTACCTUP) {
		DTACCTUP = dTACCTUP;
	}
	
	private String DTACCTUP;
	@XStreamImplicit
	private List<ACCTINFO> ACCTINFO;
	public List getACCTINFO() {
		return ACCTINFO;
	}
	public void setACCTINFO(List<ACCTINFO> aCCTINFO) {
		ACCTINFO = aCCTINFO;
	}



}
