package secu.org.ofx.response;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@SuppressWarnings("serial")
public class OFX implements Serializable {
	@XStreamAlias("SIGNUPMSGSRSV1")
	private SIGNUPMSGSRSV1 signUPSGSRSV1;
	@XStreamAlias("BANKMSGSRSV1")
	private BANKMSGSRSV1 BANKMSGSRSV1;
	@XStreamAlias("EMAILMSGSRSV1")
	private EMAILMSGSRSV1 EMAILMSGSRSV1;

	
	public EMAILMSGSRSV1 getEMAILMSGSRSV1() {
		return EMAILMSGSRSV1;
	}
	public void setEMAILMSGSRSV1(EMAILMSGSRSV1 eMAILMSGSRSV1) {
		EMAILMSGSRSV1 = eMAILMSGSRSV1;
	}
	public BANKMSGSRSV1 getBANKMSGSRSV1() {
		return BANKMSGSRSV1;
	}
	public void setBANKMSGSRSV1(BANKMSGSRSV1 bANKMSGSRSV1) {
		BANKMSGSRSV1 = bANKMSGSRSV1;
	}
	public SIGNUPMSGSRSV1 getSignUPSGSRSV1() {
		return signUPSGSRSV1;
	}
	public void setSignUPSGSRSV1(SIGNUPMSGSRSV1 signUPSGSRSV1) {
		this.signUPSGSRSV1 = signUPSGSRSV1;
	}
	@XStreamAlias("SIGNONMSGSRSV1")
	private SIGNONMSGSRSV1 SIGNONMSGSRSV1;
	public SIGNONMSGSRSV1 getSIGNONMSGSRSV1() {
		return SIGNONMSGSRSV1;
	}
	public void setSIGNONMSGSRSV1(SIGNONMSGSRSV1 sIGNONMSGSRSV1) {
		SIGNONMSGSRSV1 = sIGNONMSGSRSV1;
	}
}
