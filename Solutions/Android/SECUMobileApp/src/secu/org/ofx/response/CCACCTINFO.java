package secu.org.ofx.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("CCACCTINFO")
public class CCACCTINFO {
	private CCACCTFROM CCACCTFROM;
	private String SUPTXDL; 
    private String XFERSRC;
    private String XFERDEST;
    private String SVCSTATUS;
	public String getSUPTXDL() {
		return SUPTXDL;
	}

	public void setSUPTXDL(String sUPTXDL) {
		SUPTXDL = sUPTXDL;
	}

	public String getXFERSRC() {
		return XFERSRC;
	}

	public void setXFERSRC(String xFERSRC) {
		XFERSRC = xFERSRC;
	}

	public String getXFERDEST() {
		return XFERDEST;
	}

	public void setXFERDEST(String xFERDEST) {
		XFERDEST = xFERDEST;
	}

	public String getSVCSTATUS() {
		return SVCSTATUS;
	}

	public void setSVCSTATUS(String sVCSTATUS) {
		SVCSTATUS = sVCSTATUS;
	}

	public CCACCTFROM getCCACCTFROM() {
		return CCACCTFROM;
	}

	public void setCCACCTFROM(CCACCTFROM cCACCTFROM) {
		CCACCTFROM = cCACCTFROM;
	}
}
