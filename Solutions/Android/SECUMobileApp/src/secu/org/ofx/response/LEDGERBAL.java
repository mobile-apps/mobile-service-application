package secu.org.ofx.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@XStreamAlias("LEDGERBAL")
public class LEDGERBAL implements Serializable {
	private String BALAMT;
	private String DTASOF;
	public String getBALAMT() {
		return BALAMT;
	}
	public void setBALAMT(String bALAMT) {
		BALAMT = bALAMT;
	}
	public String getDTASOF() {
		return DTASOF;
	}
	public void setDTASOF(String dTASOF) {
		DTASOF = dTASOF;
	}
}
