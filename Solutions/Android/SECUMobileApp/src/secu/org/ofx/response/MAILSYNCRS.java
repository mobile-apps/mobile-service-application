package secu.org.ofx.response;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.io.Serializable;
import java.util.List;

@XStreamAlias("MAILSYNCRS")
public class MAILSYNCRS implements Serializable{
	private String TOKEN;
	private String LOSTSYNC;

	@XStreamAlias("STATUS")
	private STATUS STATUS;
	@XStreamImplicit
	private List<MAILTRNRS> MAILTRNRS;
	public List<MAILTRNRS> getMAILTRNRS() {
		return MAILTRNRS;
	}
	public void setMAILTRNRS(List<MAILTRNRS> mAILTRNRS) {
		MAILTRNRS = mAILTRNRS;
	}
	public String getTOKEN() {
		return TOKEN;
	}
	public void setTOKEN(String tOKEN) {
		TOKEN = tOKEN;
	}
	public String getLOSTSYNC() {
		return LOSTSYNC;
	}
	public void setLOSTSYNC(String lOSTSYNC) {
		LOSTSYNC = lOSTSYNC;
	}

	public STATUS getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(STATUS sTATUS) {
		STATUS = sTATUS;
	}
	

}
