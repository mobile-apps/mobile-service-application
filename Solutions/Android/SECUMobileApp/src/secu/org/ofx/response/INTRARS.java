package secu.org.ofx.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class INTRARS {
	
	private String CURDEF;
	private String SRVRTID;
	@XStreamAlias("XFERINFO")
	private XFERINFO XFERINFO;
	private String DTPOSTED;
	private XFERPRCSTS XFERPRCSTS;

	public XFERPRCSTS getXFERPRCSTS() {
		return XFERPRCSTS;
	}

	public void setXFERPRCSTS(XFERPRCSTS xFERPRCSTS) {
		XFERPRCSTS = xFERPRCSTS;
	}

	public XFERINFO getXFERINFO() {
		return XFERINFO;
	}

	public void setXFERINFO(XFERINFO xFERINFO) {
		XFERINFO = xFERINFO;
	}

	public String getDTPOSTED() {
		return DTPOSTED;
	}

	public void setDTPOSTED(String dTPOSTED) {
		DTPOSTED = dTPOSTED;
	}

	public String getSRVRTID() {
		return SRVRTID;
	}

	public void setSRVRTID(String sRVRTID) {
		SRVRTID = sRVRTID;
	}

	public String getCURDEF() {
		return CURDEF;
	}

	public void setCURDEF(String cURDEF) {
		CURDEF = cURDEF;
	}
}
