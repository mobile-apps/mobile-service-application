package secu.org.ofx.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class XFERINFO {
	@XStreamAlias("BANKACCTFROM")
	private BANKACCTFROM BANKACCTFROM;
	@XStreamAlias("BANKACCTTO")
	private BANKACCTTO BANKACCTTO;
	private String TRNAMT;

	public BANKACCTTO getBANKACCTTO() {
		return BANKACCTTO;
	}

	public void setBANKACCTTO(BANKACCTTO bANKACCTTO) {
		BANKACCTTO = bANKACCTTO;
	}

	public String getTRNAMT() {
		return TRNAMT;
	}

	public void setTRNAMT(String tRNAMT) {
		TRNAMT = tRNAMT;
	}

	public BANKACCTFROM getBANKACCTFROM() {
		return BANKACCTFROM;
	}

	public void setBANKACCTFROM(BANKACCTFROM bANKACCTFROM) {
		BANKACCTFROM = bANKACCTFROM;
	}

}
