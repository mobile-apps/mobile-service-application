package secu.org.ofx.response;


import com.thoughtworks.xstream.annotations.XStreamAlias;
@XStreamAlias("STATUS")
public class STATUS {
   private String CODE;
   public String getCODE() {
	return CODE;
}
public void setCODE(String cODE) {
	CODE = cODE;
}
public String getSEVERITY() {
	return SEVERITY;
}
public void setSEVERITY(String sEVERITY) {
	SEVERITY = sEVERITY;
}
private String SEVERITY;
private String MESSAGE="";
public String getMESSAGE() {
	return MESSAGE;
}
public void setMESSAGE(String mESSAGE) {
	MESSAGE = mESSAGE;
}
}
