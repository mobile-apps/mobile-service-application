package secu.org.ofx.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XStreamAlias("BANKTRANLIST")
public class BANKTRANLIST implements Serializable {
	private String DTSTART;
	private String DTEND;
	@XStreamImplicit
	private List<STMTTRN> STMTTRN = new ArrayList<STMTTRN>();

	public List<STMTTRN> getSTMTTRN() {
		return STMTTRN;
	}

	public void setSTMTTRN(List<STMTTRN> sTMTTRN) {
		STMTTRN = sTMTTRN;
	}

	public String getDTSTART() {
		return DTSTART;
	}

	public void setDTSTART(String dTSTART) {
		DTSTART = dTSTART;
	}

	public String getDTEND() {
		return DTEND;
	}

	public void setDTEND(String dTEND) {
		DTEND = dTEND;
	}
}
