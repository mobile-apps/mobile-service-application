package secu.org.ofx.response;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@SuppressWarnings("serial")
public class INTRATRNRS implements Serializable{
private String TRNUID;
@XStreamAlias("STATUS")
private STATUS STATUS;
@XStreamAlias("INTRARS")
private INTRARS INTRARS;
public INTRARS getINTRARS() {
	return INTRARS;
}
public void setINTRARS(INTRARS iNTRARS) {
	INTRARS = iNTRARS;
}
public String getTRNUID() {
	return TRNUID;
}
public void setTRNUID(String tRNUID) {
	TRNUID = tRNUID;
}
public STATUS getSTATUS() {
	return STATUS;
}
public void setSTATUS(STATUS sTATUS) {
	STATUS = sTATUS;
}


}
