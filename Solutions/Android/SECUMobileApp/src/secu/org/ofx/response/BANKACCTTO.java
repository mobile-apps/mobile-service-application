package secu.org.ofx.response;
import com.thoughtworks.xstream.annotations.XStreamAlias;
@XStreamAlias("BANKACCTTO")
public class BANKACCTTO {
	private String BANKID;

	public String getBANKID() {
		return BANKID;
	}

	public void setBANKID(String bANKID) {
		BANKID = bANKID;
	}

	public String getACCTID() {
		return ACCTID;
	}

	public void setACCTID(String aCCTID) {
		ACCTID = aCCTID;
	}

	public String getACCTTYPE() {
		return ACCTTYPE;
	}

	public void setACCTTYPE(String ACCTTYPE) {
		ACCTTYPE = ACCTTYPE;
	}

	private String ACCTID;
	private String ACCTTYPE;
}
