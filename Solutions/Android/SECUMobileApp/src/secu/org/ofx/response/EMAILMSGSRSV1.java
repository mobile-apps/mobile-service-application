package secu.org.ofx.response;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XStreamAlias("EMAILMSGSRSV1")
public class EMAILMSGSRSV1 implements Serializable {
	@XStreamAlias("MAILSYNCRS")
	private MAILSYNCRS MAILSYNCRS;
	@XStreamAlias("MAILTRNRS")
    private MAILTRNRS MAILTRNRS;
	public MAILTRNRS getMAILTRNRS() {
		return MAILTRNRS;
	}

	public void setMAILTRNRS(MAILTRNRS mAILTRNRS) {
		MAILTRNRS = mAILTRNRS;
	}

	public MAILSYNCRS getMAILSYNCRS() {
		return MAILSYNCRS;
	}

	public void setMAILSYNCRS(MAILSYNCRS mAILSYNCRS) {
		MAILSYNCRS = mAILSYNCRS;
	}
	

}
