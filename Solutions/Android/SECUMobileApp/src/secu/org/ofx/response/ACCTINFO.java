package secu.org.ofx.response;


import com.thoughtworks.xstream.annotations.XStreamAlias;
@XStreamAlias("ACCTINFO")
public class ACCTINFO {
@XStreamAlias("BANKACCTINFO")
private BANKACCTINFO BANKACCTINFO;
@XStreamAlias("CCACCTINFO")
private CCACCTINFO CCACCTINFO;
public CCACCTINFO getCCACCTINFO() {
	return CCACCTINFO;
}

public void setCCACCTINFO(CCACCTINFO cCACCTINFO) {
	CCACCTINFO = cCACCTINFO;
}

public BANKACCTINFO getBANKACCTINFO() {
	return BANKACCTINFO;
}

public void setBANKACCTINFO(BANKACCTINFO bANKACCTINFO) {
	BANKACCTINFO = bANKACCTINFO;
}
}
