package secu.org.ofx.response;
import com.thoughtworks.xstream.annotations.XStreamAlias;
@XStreamAlias("CCACCTFROM")
public class CCACCTFROM {
private String ACCTID;

public String getACCTID() {
	return ACCTID;
}

public void setACCTID(String aCCTID) {
	ACCTID = aCCTID;
}
}
