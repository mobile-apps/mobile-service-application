package secu.org.ofx.response;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MAILRS")
public class MAILRS implements Serializable{
	@XStreamAlias("MAIL")
	private MAIL MAIL;

	public MAIL getMAIL() {
		return MAIL;
	}

	public void setMAIL(MAIL mAIL) {
		MAIL = mAIL;
	}
}
