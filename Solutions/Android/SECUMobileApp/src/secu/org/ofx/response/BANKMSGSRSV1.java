package secu.org.ofx.response;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.io.Serializable;

@XStreamAlias("BANKMSGSRSV1")
public class BANKMSGSRSV1 implements Serializable {
	@XStreamAlias("STMTTRNRS")
	private STMTTRNRS STMTTRNRS;
	@XStreamAlias("INTRATRNRS")
	private INTRATRNRS INTRATRNRS;

	public INTRATRNRS getINTRATRNRS() {
		return INTRATRNRS;
	}

	public void setINTRATRNRS(INTRATRNRS iNTRATRNRS) {
		INTRATRNRS = iNTRATRNRS;
	}

	public STMTTRNRS getSTMTTRNRS() {
		return STMTTRNRS;
	}

	public void setSTMTTRNRS(STMTTRNRS sTMTTRNRS) {
		STMTTRNRS = sTMTTRNRS;
	}

}
