package secu.org.ofx.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@XStreamAlias("STMTTRN")
public class STMTTRN implements Serializable {
	private String TRNTYPE;
	private String DTPOSTED;
	private String TRNAMT;
	private String FITID;
	private String NAME;
	private String MEMO;
	private String CHECKNUM;


	public String getCHECKNUM() {
		return CHECKNUM;
	}

	public void setCHECKNUM(String cHECKNUM) {
		CHECKNUM = cHECKNUM;
	}

	public String getTRNTYPE() {
		return TRNTYPE;
	}

	public void setTRNTYPE(String tRNTYPE) {
		TRNTYPE = tRNTYPE;
	}

	public String getDTPOSTED() {
		return DTPOSTED;
	}

	public void setDTPOSTED(String dTPOSTED) {
		DTPOSTED = dTPOSTED;
	}

	public String getTRNAMT() {
		return TRNAMT;
	}

	public void setTRNAMT(String tRNAMT) {
		TRNAMT = tRNAMT;
	}

	public String getFITID() {
		return FITID;
	}

	public void setFITID(String fITID) {
		FITID = fITID;
	}

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public String getMEMO() {
		return MEMO;
	}

	public void setMEMO(String mEMO) {
		MEMO = mEMO;
	}


}
