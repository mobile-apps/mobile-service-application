package secu.org.ofx.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.io.Serializable;
import java.util.List;

@XStreamAlias("MAILTRNRS")
public class MAILTRNRS implements Serializable{
	private String TRNUID;
	@XStreamAlias("STATUS")
	private STATUS STATUS;
	
	private MAILRS MAILRS;
	public String getTRNUID() {
		return TRNUID;
	}
	public void setTRNUID(String tRNUID) {
		TRNUID = tRNUID;
	}
	public STATUS getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(STATUS sTATUS) {
		STATUS = sTATUS;
	}
	public MAILRS getMAILRS() {
		return MAILRS;
	}
	public void setMAILRS(MAILRS mAILRS) {
		MAILRS = mAILRS;
	}

}
