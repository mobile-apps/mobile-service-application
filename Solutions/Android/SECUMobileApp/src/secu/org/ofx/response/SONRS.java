package secu.org.ofx.response;


import com.thoughtworks.xstream.annotations.XStreamAlias;
@XStreamAlias("SONRS")
public class SONRS {
	@XStreamAlias("STATUS")
	private STATUS status;
	private String DTSERVER;
	private String LANGUAGE;
	@XStreamAlias("FI")
	private FI fi;

	public FI getFi() {
		return fi;
	}

	public void setFi(FI fi) {
		this.fi = fi;
	}

	public String getLANGUAGE() {
		return LANGUAGE;
	}

	public void setLANGUAGE(String lANGUAGE) {
		LANGUAGE = lANGUAGE;
	}

	public String getDTSERVER() {
		return DTSERVER;
	}

	public void setDTSERVER(String dTSERVER) {
		DTSERVER = dTSERVER;
	}

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

}
