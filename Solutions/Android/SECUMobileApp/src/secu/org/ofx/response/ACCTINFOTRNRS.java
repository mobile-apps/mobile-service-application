package secu.org.ofx.response;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class ACCTINFOTRNRS {
private String TRNUID;
private String CLTCOOKIE;
@XStreamAlias("ACCTINFORS")
private ACCTINFORS ACCTINFORS;
public ACCTINFORS getACCTINFORS() {
	return ACCTINFORS;
}

public void setACCTINFORS(ACCTINFORS aCCTINFORS) {
	ACCTINFORS = aCCTINFORS;
}

public String getCLTCOOKIE() {
	return CLTCOOKIE;
}

public void setCLTCOOKIE(String cLTCOOKIE) {
	CLTCOOKIE = cLTCOOKIE;
}

@XStreamAlias("STATUS")
private STATUS status;

public STATUS getStatus() {
	return status;
}

public void setStatus(STATUS status) {
	this.status = status;
}

public String getTRNUID() {
	return TRNUID;
}

public void setTRNUID(String tRNUID) {
	TRNUID = tRNUID;
}
}
