package secu.org.ofx.response;
import com.thoughtworks.xstream.annotations.XStreamAlias;
@XStreamAlias("BANKACCTINFO")
public class BANKACCTINFO {
	private BANKACCTFROM BANKACCTFROM;
	private String SUPTXDL;
	private String XFERSRC;
	private String XFERDEST;
	private String SVCSTATUS;
	public BANKACCTFROM getBANKACCTFROM() {
		return BANKACCTFROM;
	}
	public void setBANKACCTFROM(BANKACCTFROM bANKACCTFROM) {
		BANKACCTFROM = bANKACCTFROM;
	}
	public String getSUPTXDL() {
		return SUPTXDL;
	}
	public void setSUPTXDL(String sUPTXDL) {
		SUPTXDL = sUPTXDL;
	}
	public String getXFERSRC() {
		return XFERSRC;
	}
	public void setXFERSRC(String xFERSRC) {
		XFERSRC = xFERSRC;
	}
	public String getXFERDEST() {
		return XFERDEST;
	}
	public void setXFERDEST(String xFERDEST) {
		XFERDEST = xFERDEST;
	}
	public String getSVCSTATUS() {
		return SVCSTATUS;
	}
	public void setSVCSTATUS(String sVCSTATUS) {
		SVCSTATUS = sVCSTATUS;
	}
	
	

}
