package secu.org.ofx.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;
@XStreamAlias("SIGNONMSGSRSV1")
public class SIGNONMSGSRSV1 {
	@XStreamAlias("SONRS")
	private SONRS sonrs;

	public SONRS getSonrs() {
		return sonrs;
	}

	public void setSonrs(SONRS sonrs) {
		this.sonrs = sonrs;
	}

}
