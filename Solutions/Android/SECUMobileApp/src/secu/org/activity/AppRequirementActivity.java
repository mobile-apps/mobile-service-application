package secu.org.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class AppRequirementActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));

        setContentView(R.layout.activity_app_requirement);
        populateText();
    }

    public void populateText() {
        TextView textView = (TextView) this.findViewById(R.id.requirementsinfo);

        try {
            secu.org.application.ComponentHelper.addHTMLToTextView(textView, getResources(), R.raw.apprequirements);
        } catch (Exception e) {
            // e.printStackTrace();
            textView.setText("Error: can't show registration message.");
        }
    }

}
