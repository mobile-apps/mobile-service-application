package secu.org.activity;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import greendroid.widget.ItemAdapter;
import greendroid.widget.item.Item;
import greendroid.widget.item.SeparatorItem;
import greendroid.widget.item.SubtitleItem;

public class OutboxActivity extends ListActivity implements OnItemClickListener {

    /*
     * set rowListener to
     */
    private void setRowListener() {
        ListView listView = getListView();
        listView.setTextFilterEnabled(true);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.OUTBOX.name(),
                "starting onItemClick ", this.getClass().getSimpleName());
        int index = 0;
        if (arg2 > 0) {
            index = (arg2 - (arg2 - 1));
        }
        secu.org.message.Mail mail = secu.org.application.SecureMailSingleton
                .getSingletonObject().getOutBoxMailList().get(index);
        String test = mail.getBody() + "[" + String.valueOf(arg2) + "]";
        // custom dialog
        /*
		 * final Dialog dialog = new Dialog(this);
		 * dialog.setContentView(R.layout.detail_message);
		 * dialog.setTitle(mail.getDisplayFormattedDatePosted());
		 * 
		 * TextView bodyText = (TextView) dialog.findViewById(R.id.text);
		 * bodyText.setMovementMethod(new ScrollingMovementMethod());
		 * bodyText.setText(mail.getDisplayBody());
		 * 
		 * 
		 * Button dialogButton = (Button)
		 * dialog.findViewById(R.id.dialogButtonOK); // if button is clicked,
		 * close the custom dialog dialogButton.setOnClickListener(new
		 * OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { dialog.dismiss(); } });
		 * dialog.show();
		 */
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Create the AlertDialog
        AlertDialog alertDialog;
        builder.setTitle(mail.getDisplayFormattedDatePosted());
        builder.setInverseBackgroundForced(true);
        builder.setCancelable(true);
        builder.setMessage(mail.getDisplayBody());
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        // Create the AlertDialog
        alertDialog = builder.create();

        alertDialog.show();
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.OUTBOX.name(),
                "ending onItemClick ", this.getClass().getSimpleName());

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.OUTBOX.name(),
                "starting onCreate ", this.getClass().getSimpleName());
        buildList();
        setRowListener();
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.OUTBOX.name(),
                "ending onCreate ", this.getClass().getSimpleName());

    }

    /*
     * Build outbox list
     */
    private void buildList() {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.OUTBOX.name(),
                "starting buildList ", this.getClass().getSimpleName());
        List<Item> items = new ArrayList<Item>();
        Collections.sort(secu.org.application.SecureMailSingleton
                .getSingletonObject().getOutBoxMailList());

        for (secu.org.message.Mail mail : secu.org.application.SecureMailSingleton
                .getSingletonObject().getOutBoxMailList()) {
            SubtitleItem sub = new SubtitleItem(mail.getFrom(),
                    mail.compositeMail());
            items.add(sub);
            items.add(new SeparatorItem(""));

        }
        // account for no messages found
        if (items.isEmpty()) {
            String message = getString(R.string.nomessages);
            greendroid.widget.item.DescriptionItem noItems = new greendroid.widget.item.DescriptionItem(
                    message);
            items.add(noItems);

        }
        ItemAdapter listAdapter = new ItemAdapter(this, items);
        setListAdapter(listAdapter);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.OUTBOX.name(),
                "ending onCreate ", this.getClass().getSimpleName());
    }

}
