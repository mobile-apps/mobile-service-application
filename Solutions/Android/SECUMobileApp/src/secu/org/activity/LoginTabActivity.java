package secu.org.activity;


import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

/**
 * @author s16715d
 */
public class LoginTabActivity extends TabActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.logintab);

        TabHost tabHost = getTabHost();
        // Tab for login
        TabSpec loginSpec = tabHost.newTabSpec("Sign On");
        loginSpec.setIndicator("Sign On");
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginSpec.setContent(loginIntent);

        // Tab for help
        TabSpec helpSpec = tabHost.newTabSpec("Help");
        helpSpec.setIndicator("Help");
        Intent helpIntent = new Intent(this, HelpActivity.class);
        helpSpec.setContent(helpIntent);

        //HelpActivity
        // Adding all TabSpec to TabHost

        tabHost.addTab(loginSpec); // Adding login tab
        tabHost.addTab(helpSpec); // Adding login tab
        UserInterfaceUtil util = new UserInterfaceUtil();
        String selectedColor = this.getString(R.color.tab_select);
        String deSelectedColor = this.getString(R.color.tab_deselect);

        util.setTabColor(tabHost, selectedColor, deSelectedColor);


    }


}
