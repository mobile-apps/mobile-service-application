package secu.org.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.util.concurrent.atomic.AtomicInteger;

import secu.org.application.BankApplication;
import secu.org.application.BankSingleton;
import secu.org.service.AlertService;
import secu.org.service.RatesFeesService;


public class SecurityControllerActivity extends Activity {
    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */
    public String SENDER_ID = "791216731496";
    /**
     * Tag used on log messages.
     */
    static final String TAG = "GCMDemo";
    public GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    SharedPreferences prefs;
    Context context;

    String regid;

    /**
     * Check the device to make sure it has the Google Play Services APK. If it
     * doesn't, display a dialog that allows users to download the APK from the
     * Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = 0;
        try {
            resultCode = GooglePlayServicesUtil
                    .isGooglePlayServicesAvailable(this);
        } catch (Exception ex) {
            Log.e("error", ex.getMessage());
        }
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * get registration key
     */
    private void registerInBackground() {
        secu.org.google.cloud.message.RegistrationTask task = new secu.org.google.cloud.message.RegistrationTask(this);
        task.execute("");
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p/>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */
    public String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    public SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }


    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId   registration ID
     */
    public void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }


    private void sendRegistrationIdToBackend(String regid) {
        Intent intent = new Intent(this, AlertService.class);
        secu.org.mail.Mail mail = new secu.org.mail.Mail();
        mail.setFrom("SECU");
        mail.setSubject("Registration ID");
        mail.setSubject(regid);
        BankSingleton.getSingletonObject().setMail(mail);
        startService(intent);
        // Your implementation here.
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SECURITY.name(),
                "starting onCreate ", this.getClass().getSimpleName());
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));
        context = getApplicationContext();
        // Check device for Play Services APK. If check succeeds, proceed with
        //  GCM registration.
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(context);

            if (regid.isEmpty()) {
                registerInBackground();
                //get new rates and fees data
                Intent intent = new Intent(this, RatesFeesService.class);
                startService(intent);

            } else {
                //sendRegistrationIdToBackend(regid);
                secu.org.application.BankSingleton.getSingletonObject().setRegistrationId(regid);
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }

        // stop timer
        BankApplication application = (BankApplication) this.getApplication();
        application.stopTimer();
        boolean isSecure = secu.org.security.SecurityManager.hasDeviceSecurity(this);
        //  boolean isSecure = true;
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SECURITY.name(), "is Secure  "
                + isSecure, this.getClass().getSimpleName());
        boolean isRooted = secu.org.security.SecurityManager.isDeviceRooted(this);
        // boolean isRooted = false;


        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SECURITY.name(), "is rooted  "
                + isRooted, this.getClass().getSimpleName());
        if (!isSecure || isRooted) {
            // goto Reguirement screen
            Intent myIntent = new Intent(this, AppRequirementActivity.class);
            startActivity(myIntent);
            finish();
        } else {
            boolean isReg = isRegistered();
            secu.org.util.LoggerHelper.d(
                    secu.org.application.LoggingTag.SECURITY.name(),
                    "is isReg  " + isReg, this.getClass().getSimpleName());
            if (isReg) {
                //todo if member switch language refresh rates and fee
                Intent intent = new Intent(this, RatesFeesService.class);
                startService(intent);
                //check for notification
                boolean hasNotification = secu.org.application.PreferenceManagerSingleton.getSingletonObject().hasNotification(this);
                if (hasNotification) {
                    Intent myIntent = new Intent(this, NotificationActivity.class);
                    startActivity(myIntent);

                } else {
                    // goto logon screen


                    Intent myIntent = new Intent(this, LoginActivity.class);
                    startActivity(myIntent);
                    finish(); // take activity of stack
                }
            } else {
                // goto registration screen
                Intent myIntent = new Intent(this, RegistrationActivity.class);
                startActivity(myIntent);
                finish();
            }
        }
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SECURITY.name(),
                "ending onCreate ", this.getClass().getSimpleName());
    }

	/*
     *
	 * determine if the member is registered. Registered members will be
	 * directed to login
	 */

    public boolean isRegistered() {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SECURITY.name(),
                "starting isRegistered ", this.getClass().getSimpleName());
        secu.org.security.SecurityManager.hasDeviceSecurity(this);
        boolean isReg = secu.org.security.SecurityManager.isRegistered(this);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SECURITY.name(),
                "ending isRegistered ", this.getClass().getSimpleName());
        return isReg;

    }

}
