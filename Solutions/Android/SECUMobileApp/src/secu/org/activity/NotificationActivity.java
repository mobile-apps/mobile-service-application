package secu.org.activity;

/**
 * Created by s16715d on 11/21/13.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class NotificationActivity extends Activity {

    public void onClickSnooze(View view) {
        Intent myIntent = new Intent(this,
                LoginActivity.class);
        startActivity(myIntent);
    }

    public void onClickDismiss(View view) {
        Intent myIntent = new Intent(this,
                LoginActivity.class);
        //write to preference
        secu.org.application.PreferenceManagerSingleton.getSingletonObject().disableNotification(this);

        startActivity(myIntent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        secu.org.util.LoggerHelper.d(secu.org.application.LoggingTag.REGISTRATION.name(), "starting onCreate ", this.getClass().getSimpleName());
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));

        setContentView(R.layout.activity_notification);
        //get notice message
        TextView messageTextView = (TextView) this.findViewById(R.id.text);
        String message = secu.org.google.cloud.message.NotificationMessengerHandler.readFromFile(this);
        messageTextView.setText(message);

        secu.org.util.LoggerHelper.d(secu.org.application.LoggingTag.REGISTRATION.name(), "ending onCreate ", this.getClass().getSimpleName());

    }
}
