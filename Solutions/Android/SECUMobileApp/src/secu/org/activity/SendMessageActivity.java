package secu.org.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import secu.org.account.Account;
import secu.org.application.BankApplication;
import secu.org.application.BankSingleton;
import secu.org.asyn.OnTaskCompleteListener;
import secu.org.bank.Bank;
import secu.org.internet.InternetManager;
import secu.org.mail.Mail;
import secu.org.task.AbstractTask;
import secu.org.task.AsyncHelperTaskManager;

public class SendMessageActivity extends Activity implements
        OnTaskCompleteListener {

    Button buttonSend;
    EditText textPhoneNo;
    EditText textSMS;
    private Bank mBank;
    private AsyncHelperTaskManager mAsyncTaskManager;
    // Progress Dialog
    private ProgressDialog pDialog;

    /**
     * @param v
     */
    public void onButtonClick(View v) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SENDMESSAGE.name(),
                "starting onButtonClick ", this.getClass().getSimpleName());
        boolean cancel = false;
        View focusView = null;
        TextView textView = (TextView) findViewById(R.id.textmessage);
        final EditText subjectEditText = (EditText) findViewById(R.id.editTextBody);
        String subject = subjectEditText.getText().toString();
        // check for internet connection
        boolean haveNetworkConnection = InternetManager.haveNetworkConnection(
                this.getBaseContext(), this);
        if (!haveNetworkConnection) {
            textView.setTextColor(Color.RED);
            textView.setText(R.string.internetrequired);
            cancel = true;
        }
        if (!cancel && TextUtils.isEmpty(subject)) {
            subjectEditText.setError(getString(R.string.subjectrequired));
            focusView = subjectEditText;
            cancel = true;

        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            try {
                // get ofx info
                BankApplication application = (BankApplication) this
                        .getApplication();
                SendMessageTask sendMessageTask = new SendMessageTask(this,
                        null, null, application, this.getResources());
                // Create and run task and progress dialog
                mAsyncTaskManager.setupTask(sendMessageTask);
            } catch (Exception e) {

                e.printStackTrace();
            }
        }
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SENDMESSAGE.name(),
                "ending onButtonClick ", this.getClass().getSimpleName());
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SENDMESSAGE.name(),
                "starting onCreate ", this.getClass().getSimpleName());
        setContentView(R.layout.login);
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));
        mBank = BankSingleton.getSingletonObject().getBank();
        setContentView(R.layout.compose);
        // Create manager and set this activity as context and listener
        mAsyncTaskManager = new AsyncHelperTaskManager(this, this);
        // Handle task that can be retained before
        mAsyncTaskManager.handleRetainedTask(getLastNonConfigurationInstance());
        Spinner spinner = (Spinner) findViewById(R.id.spinnerSubject);
        // Create an ArrayAdapter using the string array and a default spinner
        // layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.sendmessage_array,
                android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the mAdapter to the spinner
        spinner.setAdapter(adapter);

        buttonSend = (Button) findViewById(R.id.buttonSend);

        addItemsOnSpinnerToAccounts();
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SENDMESSAGE.name(),
                "ending onCreate ", this.getClass().getSimpleName());

    }

    /**
     * add accounts to spinner list
     */
    public void addItemsOnSpinnerToAccounts() {

        Spinner spinnerAccounts = (Spinner) findViewById(R.id.spinnerAccounts);
        List<String> list = new ArrayList<String>();
        List<Account> accountlist = mBank.getAccountList();

        for (Account account : accountlist) {

            list.add(account.getAccountNumber());

        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAccounts.setAdapter(dataAdapter);
    }

    /**
     * Background Async Task to Load all send secure message by making HTTP
     * Request
     */
    class SendMessageTask extends AbstractTask {
        private String mmReturnCode;

        public SendMessageTask(SendMessageActivity activity, String userId,
                               String password, BankApplication bankApplication,
                               Resources resources) {
            super(activity, userId, password, bankApplication, resources);
        }

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            secu.org.util.LoggerHelper.d(
                    secu.org.application.LoggingTag.SENDMESSAGE.name(),
                    "starting onPreExecute ", this.getClass().getSimpleName());
            pDialog = new ProgressDialog(SendMessageActivity.this);
            pDialog.setMessage("Sending Secure DatabaseMessageRow ...");


            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

            try {
                pDialog.show();
                super.onPreExecute();
            } catch (Exception e) {
                pDialog.dismiss();
            }
            secu.org.util.LoggerHelper.d(
                    secu.org.application.LoggingTag.SENDMESSAGE.name(),
                    "ending onPreExecute ", this.getClass().getSimpleName());
        }

        /**
         * send secure message
         */
        protected String doInBackground(Context... paramss) {
            secu.org.util.LoggerHelper
                    .d(secu.org.application.LoggingTag.SENDMESSAGE.name(),
                            "starting doInBackground ", this.getClass()
                            .getSimpleName());
            Spinner spinnerAccounts = (Spinner) findViewById(R.id.spinnerAccounts);
            Spinner spinnerSubject = (Spinner) findViewById(R.id.spinnerSubject);
            String account = String.valueOf(spinnerAccounts.getSelectedItem());
            String subject = String.valueOf(spinnerSubject.getSelectedItem());
            subject = subject + " " + account;
            TextView bodyTextView = (TextView) findViewById(R.id.editTextBody);
            // subjectTextSpacer
            // mApplication.getBank()
            secu.org.ofx.OFXOperatorSendMessage ofxOperatorSendMessage = new secu.org.ofx.OFXOperatorSendMessage();
            secu.org.bank.Bank bank = BankSingleton.getSingletonObject().getBank();
            Mail mail = new Mail();
            mail.setBody(bodyTextView.getText().toString());
            mail.setFrom(bank.getUserId());
            mail.setSubject(subject);
            mail.setTo("SECU");
            bank.setMail(mail);

            try {

                mmReturnCode = ofxOperatorSendMessage.ofxOperation(bank);
                secu.org.util.LoggerHelper.d(
                        secu.org.application.LoggingTag.SENDMESSAGE.name(),
                        "doInBackground return code [" + mmReturnCode + "]",
                        this.getClass().getSimpleName());

            } catch (Exception e1) {
                // TODO Auto-generated catch block
                mmReturnCode = "-99";
                e1.printStackTrace();
            }
            secu.org.util.LoggerHelper.d(
                    secu.org.application.LoggingTag.SENDMESSAGE.name(),
                    "ending doInBackground ", this.getClass().getSimpleName());
            return mmReturnCode;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * *
         */
        protected void onPostExecute(String result) {
            secu.org.util.LoggerHelper.d(
                    secu.org.application.LoggingTag.SENDMESSAGE.name(),
                    "starting onPostExecute ", this.getClass().getSimpleName());
            super.onPostExecute(result);
            // dismiss the dialog

            try {
                pDialog.dismiss();
            } catch (Exception e) {
                secu.org.util.LoggerHelper.d(
                        secu.org.application.LoggingTag.SENDMESSAGE.name(),
                        "user stopped onPostExecute ", this.getClass().getSimpleName());
            }
            if (mmReturnCode.contains("0")) {
                Toast.makeText(getApplicationContext(), "DatabaseMessageRow sent!",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "DatabaseMessageRow faild, please try again later!",
                        Toast.LENGTH_LONG).show();

            }
            secu.org.util.LoggerHelper.d(
                    secu.org.application.LoggingTag.SENDMESSAGE.name(),
                    "ending onPostExecute ", this.getClass().getSimpleName());

        }

    }

    @Override
    public void onTaskComplete(AbstractTask task) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SENDMESSAGE.name(),
                "starting onTaskComplete ", this.getClass().getSimpleName());
        // TODO Auto-generated method stub
        if (task.isCancelled()) {
            // Report about cancel
            Toast.makeText(this, R.string.task_cancelled, Toast.LENGTH_LONG)
                    .show();
        } else {
            // Get result
            Boolean result = null;
            try {
                // result = task.get();
            } catch (Exception e) {
                secu.org.util.LoggerHelper.e(
                        secu.org.application.LoggingTag.SENDMESSAGE.name(),
                        e.getMessage(), this.getClass().getSimpleName());
            }

        }
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SENDMESSAGE.name(),
                "ending onTaskComplete ", this.getClass().getSimpleName());

    }

}