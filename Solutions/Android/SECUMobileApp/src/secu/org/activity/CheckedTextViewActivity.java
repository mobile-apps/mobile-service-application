package secu.org.activity;

import android.app.ListActivity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;

import java.util.HashMap;
import java.util.List;

import secu.org.database.DatabaseActivityHelper;
import secu.org.database.DatabaseMessageRow;

public class CheckedTextViewActivity extends ListActivity implements OnClickListener {
    private DatabaseActivityHelper mDatabaseActivityHelper;

    // store CheckTextView's
    private HashMap<Integer, CheckedTextView> mCheckedList =
            new HashMap<Integer, CheckedTextView>();
    // store state
    private HashMap<Integer, Boolean> mIsChecked =
            new HashMap<Integer, Boolean>();
    // list of fruits

    private List<DatabaseMessageRow> mDatabaseList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_check);

        Button continueButton = (Button) findViewById(R.id.button_select_fruit);
        continueButton.setOnClickListener(this);
        mDatabaseList = getListData();

        setListAdapter(new MyAdapter(this, R.layout.row, mDatabaseList));
    }

    @Override
    public void onClick(View v) {


    }

    private class MyAdapter extends ArrayAdapter<DatabaseMessageRow> {

        private List<DatabaseMessageRow> items;

        public MyAdapter(Context context, int textViewResourceId, List<DatabaseMessageRow> items) {
            super(context, textViewResourceId, items);
            this.items = items;
        }

        public View getView(final int position, View view, ViewGroup parent) {
            // we are not reusing anything today
            LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.row_check, null);

            CheckedTextView ct = (CheckedTextView) view.findViewById(android.R.id.text1);
            ct.setText(items.get(position).getMonthDayDisplay());
            // set current state and color
            if (mIsChecked.get(position) != null) {
                if (mIsChecked.get(position)) {
                    ct.setChecked(true);
                    ct.setTextColor(Color.WHITE);
                } else {
                    ct.setTextColor(Color.parseColor("#c0c0c0"));
                }
            }
            // tag it - used when clicked upon to change state
            ct.setTag(position);
            mCheckedList.put(position, ct);
            ct.setOnClickListener(CheckedTextViewActivity.this);

            return view;
        }
    }

    private List<DatabaseMessageRow> getListData() {
        mDatabaseActivityHelper = new DatabaseActivityHelper(this);
        mDatabaseList = mDatabaseActivityHelper.findAll();


        return mDatabaseList;
    }


}