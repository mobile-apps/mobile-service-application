package secu.org.activity;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import greendroid.app.GDActivity;
import greendroid.widget.ActionBar;
import greendroid.widget.ActionBarHost;
import greendroid.widget.ActionBarItem;
import secu.org.application.BankApplication;
import secu.org.menu.HelperMenu;

public abstract class AbstractActivity extends GDActivity implements ActionBar.OnActionBarListener {


    private MenuHelper mMenuHelper;

    public AbstractActivity() {
        super(ActionBar.Type.Normal);
        mMenuHelper = new MenuHelper(this);
    }

    protected void prepareQuickActionBar() {
        mMenuHelper.prepareQuickActionBar();
    }


    protected void prepareQuickActionGrid() {
        Intent i = getIntent();
        mMenuHelper.prepareQuickActionGrid(i);
    }

    /**
     * construct action grid
     */

    public android.view.View getMenuViewAnchor() {
        android.view.View menuItemView = findViewById(R.id.gd_action_bar);

        return menuItemView;
    }

    abstract GDActivity getCurrentActivity();

    protected void initActionBar() {

        //addActionBarItem(Type.Locate, R.id.action_bar_locate);
        //addActionBarItem(Type.Locate,R.drawable.overflow);
        //ic_menu_moreoverflow
        //greendroid.widget.ActionBarItem.Type.

        // addActionBarItem(ActionBarItem.Type.List, R.id.action_bar_refresh);
        //  addActionBarItem(getGDActionBar().newActionBarItem(NormalActionBarItem.class).setDrawable(R.drawable.icon).setContentDescription(R.string.gd_export), R.id.action_bar_export);
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        BankApplication application = (BankApplication) this.getApplication();
        long currentTime = System.currentTimeMillis();
        application.setLastInteractionTime(currentTime);
    }

    @Override
    public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
        mMenuHelper.onShowGrid(item.getItemView());
        //Toast.makeText(this, "Click-" + String.valueOf(position),
        //Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.layout.menu, menu);
        return true;
    }
    /*
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        HelperMenu helperMenu = new HelperMenu(this);
        helperMenu.selectMenuItem(item);
        return true;

    }

    public void onPreContentChanged() {
        super.onPreContentChanged();
        ActionBarHost actionBarHost = (ActionBarHost) findViewById(com.cyrilmottier.android.greendroid.R.id.gd_action_bar_host);
        if (actionBarHost == null) {
            throw new RuntimeException("Your content must have an ActionBarHost whose id attribute is R.id.gd_action_bar_host");
        }
        //
        actionBarHost.getActionBar().setOnActionBarListener(this);
    }


    public void onActionBarItemClicked(int position) {
        ActionBarHost actionBarHost = (ActionBarHost) findViewById(com.cyrilmottier.android.greendroid.R.id.gd_action_bar_host);
        ActionBar actionBar = actionBarHost.getActionBar();
        ActionBarItem item = actionBar.getItem(position);

        this.mMenuHelper.onShowMenu(getMenuViewAnchor());

    }


}
