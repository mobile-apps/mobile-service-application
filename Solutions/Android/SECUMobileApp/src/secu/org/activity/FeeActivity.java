package secu.org.activity;

import java.util.ArrayList;

import secu.org.ratesfees.Fee;
import secu.org.ratesfees.FeeEntry;
import secu.org.ratesfees.Fees;
import secu.org.ratesfees.LendingRatesSingleton;

public class FeeActivity extends AbstractRateFeesActivity {

    /**
     * Create group list
     */
    @Override
    protected void createGroupList() {
        Fees fees = LendingRatesSingleton.getSingletonObject(this)
                .getLendingRatesHandler().getFees();


        groupList = new ArrayList<String>();

        for (Fee fee : fees.getFeeList()) {
            // mDepoistList.add(createPlanet(deposit.getDescription(),
            // deposit.getDescription()));
            groupList.add(fee.getDescription());
            createCollection(fee);
        }


    }

    /**
     * @param deposit
     */
    @Override
    protected void createCollection(Object obj) {
        Fee fee = (Fee) obj;
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "starting createCollection ", this.getClass().getSimpleName());
        ArrayList<String> itemList = new ArrayList<String>();
        for (FeeEntry feeEntry : fee.getFeeEntryList()) {
            // mDepoistList.add(createPlanet(deposit.getDescription(),
            // deposit.getDescription()));
            itemList.add(feeEntry.toString());

        }
        String[] itemString = itemList.toArray(new String[itemList.size()]);
        loadChild(itemString);
        groupCollection.put(fee.getDescription(), childList);

        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "ending createCollection ", this.getClass().getSimpleName());

    }


}