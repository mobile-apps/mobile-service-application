package secu.org.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import greendroid.app.GDActivity;
import greendroid.widget.ItemAdapter;
import greendroid.widget.item.Item;
import greendroid.widget.item.SeparatorItem;
import greendroid.widget.item.TextItem;
import greendroid.widget.item.ThumbnailItem;
import secu.org.locator.DrivingLeg;
import secu.org.locator.DrivingStep;

public class DrivingDirectionActivity extends AbstractListActivity implements
        OnClickListener {
    DrivingLeg mDrivingLeg;

    @Override
    GDActivity getCurrentActivity() {
        // TODO Auto-generated method stub
        return this;
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        // TODO Auto-generated method stub
        super.onListItemClick(l, v, position, id);
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + mDrivingLeg.getEndAddress()));

        startActivity(i);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));


        populateList();
    }

    private void populateList() {
        Intent i = getIntent();
        mDrivingLeg = (DrivingLeg) i
                .getSerializableExtra(DrivingLeg.SESSION_NAME);
        List<Item> items = new ArrayList<Item>();
        // items.add(new SubtitleItem("Starting address ",
        // mLocation.getStartingAddress()));
        items.add(new ThumbnailItem("Click for map", mDrivingLeg
                .getStartAddress(), R.drawable.startingaddress));
        items.add(new SeparatorItem(""));
        int counter = 1;
        List<DrivingStep> drivingStepList = mDrivingLeg.getmDrivingStepList();
        for (DrivingStep drivingStep : drivingStepList) {
            StringBuilder builder = new StringBuilder();
            builder.append(counter + ". ");
            builder.append(drivingStep.getHtmlinstructions());
            builder.append(" ");
            builder.append(drivingStep.getDistance());
            String html = Html.fromHtml(builder.toString()).toString();
            TextItem item = new TextItem(html);
            items.add(item);
            items.add(new SeparatorItem(""));
            counter++;
        }
        final ItemAdapter adapter = new ItemAdapter(this, items);
        setListAdapter(adapter);

    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub

    }

}
