package secu.org.activity;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import greendroid.app.GDActivity;
import secu.org.asyn.OnTaskCompleteListener;
import secu.org.locator.LocatorResult;
import secu.org.task.AbstractTask;
import secu.org.task.AsyncHelperTaskManager;

public class LocatorActivity extends AbstractListActivity implements
        OnClickListener, LocationListener, OnTaskCompleteListener {
    private AsyncHelperTaskManager mAsyncTaskManager;
    private List<LocatorResult> resultList = new ArrayList<LocatorResult>();
    private secu.org.locator.Location mSecuLocation;
    private LocationManager mLocationManager;
    private String mBestProvider;
    private StringBuilder mLocationBuilder = new StringBuilder();

    public List<LocatorResult> getResultList() {
        return resultList;
    }

    public void setResultList(List<LocatorResult> resultList) {
        this.resultList = resultList;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "starting onCreate ", this.getClass().getSimpleName());
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));
        //prepareQuickActionBar();
        //prepareQuickActionGrid();
        //initActionBar();

        // Create manager and set this activity as context and listener
        mAsyncTaskManager = new AsyncHelperTaskManager(this, this);
        // Handle task that can be retained before
        mAsyncTaskManager.handleRetainedTask(getLastNonConfigurationInstance());
        // Get the location manager
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        //criteria.setAccuracy(Criteria.ACCURACY_FINE);

        boolean isLocationAvailable = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network_enabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        mBestProvider = mLocationManager.getBestProvider(criteria, false);
        printProvider(mBestProvider);
        Location location = mLocationManager.getLastKnownLocation(mBestProvider);

        // location.requestLocationUpdates(mBestProvider, 20000, 1, this);
        printLocation(location);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "isLocationAvailable [" + isLocationAvailable + "]", this.getClass().getSimpleName());
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "location [" + location + "]", this.getClass().getSimpleName());
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "network_enabled [" + network_enabled + "]", this.getClass().getSimpleName());
        if (location != null) {
            double longitude = location.getLongitude();
            double latitude = location.getLatitude();
            secu.org.util.LoggerHelper.d(
                    secu.org.application.LoggingTag.LOCATION.name(),
                    "longitude and  latitude [" + longitude + "," + latitude + "]", this.getClass().getSimpleName());
            Geocoder geocoder = new Geocoder(getApplicationContext(),
                    Locale.getDefault());
            List<Address> addressList = null;
            Address address = null;
            boolean canGetStartingAddress = true;
            String startingAddress = null;
            try {
                secu.org.util.LoggerHelper.d(
                        secu.org.application.LoggingTag.LOCATION.name(),
                        "Geocoder.isPresent [" + Geocoder.isPresent() + "]", this.getClass().getSimpleName());
                if (geocoder.isPresent()) {

                    addressList = geocoder.getFromLocation(latitude, longitude,
                            1);
                    if (addressList != null && addressList.size() > 0) {
                        address = addressList.get(0);
                        StringBuilder builder = new StringBuilder();
                        builder.append(address.getAddressLine(0));
                        builder.append(", ");
                        builder.append(address.getLocality());
                        builder.append(", ");
                        builder.append(address.getAdminArea());
                        builder.append(" ");
                        builder.append(address.getPostalCode());
                        startingAddress = builder.toString();
                    }
                }
            } catch (IOException e) {

                // TODO Auto-generated catch block
                secu.org.util.LoggerHelper.d(
                        secu.org.application.LoggingTag.LOCATION.name(),
                        "can find starting address ", this.getClass().getSimpleName());
                //canGetStartingAddress = false;

            }
            // get default locator values
            if (canGetStartingAddress) {
                String locatorURL = this.getString(R.string.locationsearchurl);
                int numberOfLocation = new Integer(
                        this.getString(R.string.numberoflocations)).intValue();
                mSecuLocation = new secu.org.locator.Location(latitude, longitude,
                        numberOfLocation, locatorURL, startingAddress);
                // get ofx info

                secu.org.task.LocatorJSONTask task = new secu.org.task.LocatorJSONTask(
                        this, "", "", mSecuLocation, this.getResources());

                mAsyncTaskManager.setupTask(task);
            } else {
                Intent intent = new Intent(this, GenericLocatorActivity.class);
                startActivity(intent);
            }

        } else {
            //generic locator
            Intent intent = new Intent(this, GenericLocatorActivity.class);
            startActivity(intent);
            //no longer show location sevice
                /*
			 Builder builder =  new AlertDialog.Builder(this);
	    	 builder.setMessage("GPS is disable");
	    	 builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

	                @Override
	                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
	                    // TODO Auto-generated method stub
	                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	                    startActivity(myIntent);
	                    //get gps
	                }
	            });
	    	 builder.show();
	    	 */


        }
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "ending onCreate ", this.getClass().getSimpleName());
    }

    @Override
    GDActivity getCurrentActivity() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub

    }

    private boolean isLocationServiceOn() {

        boolean gps_enabled, network_enabled;
        gps_enabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        network_enabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean locationNotOn = true;
        if (!gps_enabled && !network_enabled) {
            locationNotOn = false;
            Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("place holder");
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            builder.show();

        }
        return locationNotOn;
    }


    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "starting onListItemClick ", this.getClass().getSimpleName());
        Integer tempInt = new Integer(position / 3);
        int actualPosition = tempInt + 1;
        String googleDrivingDirectionURl = this
                .getString(R.string.googledrivingdirectionurl);
        // select resultsset
        LocatorResult locatorResult = resultList.get(actualPosition);
        locatorResult.setGoogleDrivingDirectionURL(googleDrivingDirectionURl);
        secu.org.task.DrivingDirectionJSONTask task = new secu.org.task.DrivingDirectionJSONTask(
                this, locatorResult, mSecuLocation);
        task.execute(this);
        String display = position + " " + actualPosition;
        // Toast.makeText(this, "Click-" + String.valueOf(display),
        // Toast.LENGTH_SHORT).show();
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "ending onListItemClick ", this.getClass().getSimpleName());
    }

    @Override
    public void onLocationChanged(Location location) {
        printLocation(location);

    }

    @Override
    public void onProviderDisabled(String provider) {
        // let okProvider be bestProvider
        // re-register for updates

        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "Provider Disabled: " + provider, this.getClass().getSimpleName());

    }

    @Override
    public void onProviderEnabled(String provider) {
        // is provider better than bestProvider?
        // is yes, bestProvider = provider

        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "Provider Enabled: " + provider, this.getClass().getSimpleName());

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        String message = "Provider Status Changed: " + provider + ", Status="
                + status + ", Extras=" + extras;
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "onStatusChanged  " + message, this.getClass().getSimpleName());

    }

    /**
     * Register for the updates when Activity is in foreground
     */
    @Override
    protected void onResume() {
        super.onResume();
        mLocationManager.requestLocationUpdates(mBestProvider, 20000, 1, this);
    }

    /**
     * Stop the updates when Activity is paused
     */
    @Override
    protected void onPause() {
        super.onPause();
        mLocationManager.removeUpdates(this);
    }

    private void printProvider(String provider) {
        LocationProvider info = mLocationManager.getProvider(provider);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "printProvider  " + info.toString(), this.getClass().getSimpleName());

    }

    private void printLocation(Location location) {
        String message = null;
        if (location == null)
            message = "Location[unknown]";
        else
            message = location.toString();

        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "location  " + message, this.getClass().getSimpleName());
    }

    @Override
    public void onTaskComplete(AbstractTask task) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOGON.name(),
                "starting onTaskComplete ", this.getClass().getSimpleName());
        // TODO Auto-generated method stub
        if (task.isCancelled()) {
            // Report about cancel
            Toast.makeText(this, R.string.task_cancelled, Toast.LENGTH_LONG)
                    .show();
        }
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOGON.name(),
                "ending onTaskComplete ", this.getClass().getSimpleName());


    }

}
