package secu.org.activity;

import java.util.ArrayList;

import secu.org.ratesfees.LendingRatesSingleton;
import secu.org.ratesfees.Mortgage;
import secu.org.ratesfees.MortgageEntry;
import secu.org.ratesfees.Mortgages;

public class MortgageActivity extends AbstractRateFeesActivity {

    /**
     * Create group list
     */
    @Override
    protected void createGroupList() {
        Mortgages mortgages = LendingRatesSingleton.getSingletonObject(this)
                .getLendingRatesHandler().getMortgages();


        groupList = new ArrayList<String>();

        for (Mortgage mortgage : mortgages.getMortgageList()) {
            // mDepoistList.add(createPlanet(deposit.getDescription(),
            // deposit.getDescription()));
            groupList.add(mortgage.getDescription());
            createCollection(mortgage);
        }


    }

    /**
     * @param deposit
     */
    @Override
    protected void createCollection(Object obj) {
        Mortgage mortgage = (Mortgage) obj;
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "starting createCollection ", this.getClass().getSimpleName());
        ArrayList<String> itemList = new ArrayList<String>();
        for (MortgageEntry mortgageEntry : mortgage.getMortgageEntryList()) {
            // mDepoistList.add(createPlanet(deposit.getDescription(),
            // deposit.getDescription()));
            itemList.add(mortgageEntry.toString());

        }
        String[] itemString = itemList.toArray(new String[itemList.size()]);
        loadChild(itemString);
        groupCollection.put(mortgage.getDescription(), childList);

        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "ending createCollection ", this.getClass().getSimpleName());

    }


}