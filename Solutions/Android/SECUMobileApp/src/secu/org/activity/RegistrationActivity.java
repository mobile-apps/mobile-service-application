package secu.org.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import secu.org.security.SecurityManager;

public class RegistrationActivity extends Activity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        secu.org.util.LoggerHelper.d(secu.org.application.LoggingTag.REGISTRATION.name(), "starting onCreate ", this.getClass().getSimpleName());
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));

        setContentView(R.layout.registration);
        populateText();
        secu.org.util.LoggerHelper.d(secu.org.application.LoggingTag.REGISTRATION.name(), "ending onCreate ", this.getClass().getSimpleName());

    }


    public void populateText() {
        secu.org.util.LoggerHelper.d(secu.org.application.LoggingTag.REGISTRATION.name(), "ending onCreate ", this.getClass().getSimpleName());
        TextView textView = (TextView) this.findViewById(R.id.registrationinfo);

        try {
            secu.org.application.ComponentHelper.addHTMLToTextView(textView, getResources(), R.raw.registration);
        } catch (Exception e) {
            // e.printStackTrace();
            textView.setText("Error: can't show registration message.");
        }
        secu.org.util.LoggerHelper.d(secu.org.application.LoggingTag.REGISTRATION.name(), "ending populateText ", this.getClass().getSimpleName());
    }

    public void onButtonClick(View view) {
        secu.org.util.LoggerHelper.d(secu.org.application.LoggingTag.REGISTRATION.name(), "starting onButtonClick ", this.getClass().getSimpleName());
        final EditText codeEditText = (EditText) findViewById(R.id.editCode);
        String code = codeEditText.getText().toString();
        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(code)) {
            codeEditText.setError(getString(R.string.useridrequired));
            focusView = codeEditText;
            cancel = true;

        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            String[] hashedAndroidList = this.getResources().getStringArray(R.array.security_array);
            //	.getStringArray(R.array.security_array);

            boolean hasAccess = SecurityManager.isValidRegistrationCode(this, code, hashedAndroidList);
            if (!hasAccess) {
                //this.loginActivity.setHasInvalidCredentials(true);
                TextView textView = (TextView) this.findViewById(R.id.codemessage);

                textView.setTextColor(Color.RED);
                textView.setText("Invalid code");
                Toast.makeText(this.getApplicationContext(),
                        "Invalid code", Toast.LENGTH_LONG).show();
            } else {
                //update registration status and goto login
                SecurityManager.updateRegistered(this);
                Intent myIntent = new Intent(this, LoginActivity.class);
                startActivity(myIntent);
                finish(); //take activity of stack

            }

        }

        // setListAdapter(new ImageAdapter(this, R.layout.main, R.id.text1,
        // R.id.image1, listItems, listImages ));
        secu.org.util.LoggerHelper.d(secu.org.application.LoggingTag.REGISTRATION.name(), "ending onButtonClick ", this.getClass().getSimpleName());

    }
}
