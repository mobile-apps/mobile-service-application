package secu.org.activity;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

import greendroid.widget.QuickActionBar;
import greendroid.widget.QuickActionWidget;
import greendroid.widget.QuickActionWidget.OnQuickActionClickListener;
import secu.org.application.BankSingleton;
import secu.org.bank.Bank;
import secu.org.listener.MenuItemClickListener;
import secu.org.listener.MenuQuickActionClickListener;

public class MenuHelper {
    private Activity mActivity;
    private Bank mBank;
    private QuickActionWidget mBar;
    protected secu.org.menu.QuickActionGridTextOnly mGrid;

    public void onShowBar(View v) {
        mBar.show(v);
    }

    public void onShowGrid(View v) {
        int id = v.getId();
        mGrid.show(v);
    }

    public void onShowMenu(android.view.View menuItemView) {
        MenuItemClickListener menuItemClickListener = new MenuItemClickListener(mActivity);
        boolean hasLogon = BankSingleton.getSingletonObject().hasLogon();
        if (menuItemView != null) {
            PopupMenu popup = new PopupMenu(mActivity, menuItemView);
            popup.setOnMenuItemClickListener(menuItemClickListener);
            // PopupMenu popup = new PopupMenu(mActivity,mActivity.);
            if (hasLogon) {
                mActivity.getMenuInflater().inflate(R.menu.menu_login, popup.getMenu());
            } else {
                //not logon
                mActivity.getMenuInflater().inflate(R.menu.menu_not_login, popup.getMenu());
            }
            popup.show();
        }
        // mActivity.openOptionsMenu();

    }

    public MenuHelper(Activity activity) {

        mActivity = activity;

    }

    public void prepareQuickActionBar() {

        mBar = new QuickActionBar(mActivity);
        mBar.addQuickAction(new MyQuickAction(mActivity,
                R.drawable.gd_action_bar_compose, R.string.gd_compose));
        mBar.addQuickAction(new MyQuickAction(mActivity,
                R.drawable.gd_action_bar_export, R.string.gd_export));
        mBar.addQuickAction(new MyQuickAction(mActivity,
                R.drawable.gd_action_bar_share, R.string.gd_share));

        mBar.setOnQuickActionClickListener(mActionListener);
    }


    /**
     * construct action grid
     */
    public void prepareQuickActionGrid(Intent i) {
        //mGrid = new QuickActionGrid(mActivity);
        mGrid = new secu.org.menu.QuickActionGridTextOnly(mActivity);

        boolean hasLogon = BankSingleton.getSingletonObject().hasLogon();
        // mGrid.addQuickAction(new MyQuickAction(mActivity,
        // R.drawable.gd_action_bar_compose, R.string.menu_accounts));
        if (hasLogon) {
            mGrid.addQuickAction(new MyQuickAction(mActivity,
                    secu.org.activity.R.drawable.ic_menu_help,
                    R.string.menu_about));//about
            mGrid.addQuickAction(new MyQuickAction(mActivity,
                    R.drawable.access_accounts, R.string.menu_accounts)); //accounts
            mGrid.addQuickAction(new MyQuickAction(mActivity,
                    secu.org.activity.R.drawable.ic_menu_start_conversation,
                    R.string.menu_contact_us));//contact us
            mGrid.addQuickAction(new MyQuickAction(mActivity,
                    R.drawable.gd_action_bar_search, R.string.menu_locator));//atm /branch locator
            mGrid.addQuickAction(new MyQuickAction(mActivity,
                    R.drawable.ic_menu_notifications, R.string.menu_notification)); //notifications google
            mGrid.addQuickAction(new MyQuickAction(mActivity, R.drawable.read,
                    R.string.menu_messages)); //secure messages
            mGrid.addQuickAction(new MyQuickAction(mActivity,
                    secu.org.activity.R.drawable.ic_menu_preferences,
                    R.string.menu_preference)); //preferences
            mGrid.addQuickAction(new MyQuickAction(mActivity, R.drawable.ratesfees,
                    R.string.menu_rates));//rates and fees
            mGrid.addQuickAction(new MyQuickAction(mActivity,
                    secu.org.activity.R.drawable.transfer, R.string.menu_transfer)); //move money
            mGrid.addQuickAction(new MyQuickAction(mActivity,
                    secu.org.activity.R.drawable.gd_action_bar_exit,
                    R.string.menu_exit));
        } else { //not logon
            mGrid.addQuickAction(new MyQuickAction(mActivity,
                    secu.org.activity.R.drawable.ic_menu_help,
                    R.string.menu_about));//about
            mGrid.addQuickAction(new MyQuickAction(mActivity,
                    secu.org.activity.R.drawable.ic_menu_start_conversation,
                    R.string.menu_contact_us));//contact us
            mGrid.addQuickAction(new MyQuickAction(mActivity,
                    R.drawable.gd_action_bar_search, R.string.menu_locator)); //locator branch/atm
            mGrid.addQuickAction(new MyQuickAction(mActivity,
                    R.drawable.ic_menu_notifications, R.string.menu_notification)); //notifications google
            mGrid.addQuickAction(new MyQuickAction(mActivity,
                    secu.org.activity.R.drawable.ic_menu_preferences,
                    R.string.menu_preference)); //preferences
            mGrid.addQuickAction(new MyQuickAction(mActivity,
                    R.drawable.ratesfees, R.string.menu_rates)); //rate and fees

            mGrid.addQuickAction(new MyQuickAction(mActivity,
                    secu.org.activity.R.drawable.gd_action_bar_exit,
                    R.string.menu_exit));

        }
        mBank = BankSingleton.getSingletonObject().getBank();

        OnQuickActionClickListener actionListener = new MenuQuickActionClickListener(
                mActivity);
        mGrid.setOnQuickActionClickListener(actionListener);
    }

    private OnQuickActionClickListener mActionListener = new OnQuickActionClickListener() {
        public void onQuickActionClicked(QuickActionWidget widget, int position) {
            Toast.makeText(mActivity, "Item " + position + " clicked",
                    Toast.LENGTH_SHORT).show();
        }
    };

}
