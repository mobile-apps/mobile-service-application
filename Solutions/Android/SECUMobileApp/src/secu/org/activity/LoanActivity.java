package secu.org.activity;

import java.util.ArrayList;

import secu.org.ratesfees.LendingRatesSingleton;
import secu.org.ratesfees.Loan;
import secu.org.ratesfees.LoanEntry;
import secu.org.ratesfees.Loans;

public class LoanActivity extends AbstractRateFeesActivity {

    /**
     * Create group list
     */
    @Override
    protected void createGroupList() {
        Loans loans = LendingRatesSingleton.getSingletonObject(this)
                .getLendingRatesHandler().getLoans();


        groupList = new ArrayList<String>();

        for (Loan loan : loans.getLoanList()) {
            // mDepoistList.add(createPlanet(deposit.getDescription(),
            // deposit.getDescription()));
            groupList.add(loan.getDescription());
            createCollection(loan);
        }


    }

    /**
     * @param deposit
     */
    @Override
    protected void createCollection(Object obj) {
        Loan loan = (Loan) obj;
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "starting createCollection ", this.getClass().getSimpleName());
        ArrayList<String> itemList = new ArrayList<String>();
        for (LoanEntry loanEntry : loan.getLoanEntryList()) {
            // mDepoistList.add(createPlanet(deposit.getDescription(),
            // deposit.getDescription()));
            itemList.add(loanEntry.toString());

        }
        String[] itemString = itemList.toArray(new String[itemList.size()]);
        loadChild(itemString);
        groupCollection.put(loan.getDescription(), childList);

        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "ending createCollection ", this.getClass().getSimpleName());

    }


}