/*
 * Copyright (C) 2010 Cyril Mottier (http://www.cyrilmottier.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package secu.org.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TabHost.TabSpec;

import secu.org.tab.AbstractTabActivity;

public class MessageCenterActivity extends AbstractTabActivity {

    //AbstractTabActivity
    private static final String OUTBOX_SPEC = "Inbox";
    private static final String PROFILE_SPEC = "Compose";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(this));

        //initActionBar();
        //	 prepareQuickActionBar();
        //	prepareQuickActionGrid();

        setTitle("Message Center");

        // addTab(TAB1, "In Box", Color.BLACK, "Content of tab #1");
        //addTab(TAB2, "Out Box", Color.rgb(20, 20, 20), "Content of tab #2");
        //addTab(TAB3, "Compose", Color.rgb(40, 40, 40), "Content of tab #3");


        // Outbox Tab
        TabSpec outboxSpec = getParentTabHost().newTabSpec(OUTBOX_SPEC);
        outboxSpec.setIndicator(OUTBOX_SPEC, getResources().getDrawable(R.drawable.icon_inbox));
        Intent outboxIntent = new Intent(this, InboxActivity.class);
        outboxSpec.setContent(outboxIntent);
        addTab(outboxSpec);


        // Inbox Tab
        TabSpec inboxSpec = getParentTabHost().newTabSpec("Outbox");
        // Tab Icon
        inboxSpec.setIndicator("Outbox", getResources().getDrawable(R.drawable.icon_outbox));
        Intent inboxIntent = new Intent(this, OutboxActivity.class);
        // Tab Content
        inboxSpec.setContent(inboxIntent);
        addTab(inboxSpec);

        // Profile Tab
        TabSpec profileSpec = getParentTabHost().newTabSpec(PROFILE_SPEC);
        profileSpec.setIndicator(PROFILE_SPEC, getResources().getDrawable(R.drawable.icon_profile));
        /** Creating an intent to initiate view action */
        Intent intent = new Intent("android.intent.action.VIEW");

        /** creates an sms uri */
        Uri data = Uri.parse("sms:");

        /** Setting sms uri to the intent */
        intent.setData(data);

        /** Initiates the SMS compose screen, because the activity contain ACTION_VIEW and sms uri */
        //startActivity(intent);
        Intent profileIntent = new Intent(this, SendMessageActivity.class);
        profileSpec.setContent(profileIntent);
        addTab(profileSpec);

    }


    @Override
    public AbstractTabActivity getCurrentActivity() {
        // TODO Auto-generated method stub
        return this;
    }

}
