package secu.org.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import greendroid.app.GDActivity;
import secu.org.account.Account;
import secu.org.account.AccountTransfer;
import secu.org.application.BankSingleton;
import secu.org.application.ResourceEnumeration;
import secu.org.asyn.OnTaskCompleteListener;
import secu.org.bank.Bank;
import secu.org.task.AbstractTask;
import secu.org.task.AsyncHelperTaskManager;
import secu.org.task.OFXTransferMoneyTask;

public class MoveMoneyActivity extends AbstractActivity implements
        OnTaskCompleteListener {

    private Spinner spinnerToAccount, spinnerFromAccount;
    private Button btnSubmit;
    private final Handler mHandler = new Handler();
    private Bank mBank;
    private AsyncHelperTaskManager mAsyncTaskManager;

    GDActivity getCurrentActivity() {
        return this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.MOVEMONEY.name(),
                "starting onCreate ", this.getClass().getSimpleName());
        // Create manager and set this activity as context and listener
        mAsyncTaskManager = new AsyncHelperTaskManager(this, this);
        // Handle task that can be retained before
        mAsyncTaskManager.handleRetainedTask(getLastNonConfigurationInstance());
        Intent i = getIntent();
        mBank = BankSingleton.getSingletonObject().getBank();
        setActionBarContentView(R.layout.movemoney);
        // setActionBarContentView(R.layout.info);
        addItemsOnSpinnerFromAccounts();
        addItemsOnSpinnerToAccounts();
        initActionBar();
        prepareQuickActionGrid();
        // addListenerOnButton();
        // addListenerOnSpinnerItemSelection();
        //initActionBar();
        // prepareQuickActionBar();
        //prepareQuickActionGrid();
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.MOVEMONEY.name(),
                "ending onCreate ", this.getClass().getSimpleName());
    }

    public void addItemsOnSpinnerToAccounts() {
        secu.org.util.LoggerHelper.d(secu.org.application.LoggingTag.MOVEMONEY
                .name(), "starting addItemsOnSpinnerToAccounts", this
                .getClass().getSimpleName());
        spinnerToAccount = (Spinner) findViewById(R.id.spinnerToAccount);
        List<String> list = new ArrayList<String>();
        List<Account> accountlist = BankSingleton.getSingletonObject().getBank().getAccountList();

        for (Account account : accountlist) {

            list.add(account.compositeAccountInfoDisplay());

        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerToAccount.setAdapter(dataAdapter);
        secu.org.util.LoggerHelper.d(secu.org.application.LoggingTag.MOVEMONEY
                .name(), "ending addItemsOnSpinnerToAccounts", this.getClass()
                .getSimpleName());
    }

    // add items into spinner dynamically
    public void addItemsOnSpinnerFromAccounts() {
        secu.org.util.LoggerHelper.d(secu.org.application.LoggingTag.MOVEMONEY
                .name(), "starting addItemsOnSpinnerFromAccounts", this
                .getClass().getSimpleName());
        spinnerFromAccount = (Spinner) findViewById(R.id.spinnerFromAccount);
        List<String> list = new ArrayList<String>();
        List<Account> accountlist = BankSingleton.getSingletonObject().getBank().getAccountList();

        for (Account account : accountlist) {

            list.add(account.compositeAccountInfoDisplay());

        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFromAccount.setAdapter(dataAdapter);
        secu.org.util.LoggerHelper.d(secu.org.application.LoggingTag.MOVEMONEY
                .name(), "starting addItemsOnSpinnerFromAccounts", this
                .getClass().getSimpleName());
    }

    /*
     * add listener for item drop down selection
     */
    public void addListenerOnSpinnerItemSelection() {
        secu.org.util.LoggerHelper.d(secu.org.application.LoggingTag.MOVEMONEY
                .name(), "starting addListenerOnSpinnerItemSelection", this
                .getClass().getSimpleName());
        spinnerToAccount = (Spinner) findViewById(R.id.spinnerToAccount);
        spinnerToAccount
                .setOnItemSelectedListener(new secu.org.listener.CustomOnItemSelectedListener());
        secu.org.util.LoggerHelper.d(secu.org.application.LoggingTag.MOVEMONEY
                .name(), "ending addListenerOnSpinnerItemSelection", this
                .getClass().getSimpleName());
    }

    /**
     * does account have enough money
     *
     * @param fromAccount
     * @param amountRequested
     * @return
     */
    private boolean doesAccountHaveEnoughMoneyToMove(Account fromAccount, String amountRequested) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.MOVEMONEY.name(),
                "starting doesAccountHaveEnoughMoneyToMove", this.getClass().getSimpleName());
        boolean hasEnough = false;
        double amountRequestedDB = Double.parseDouble(amountRequested);
        double accountAmount = Double.parseDouble(fromAccount.getRawAccountBalance());
        if (amountRequestedDB > accountAmount) {
            hasEnough = false;
        } else {
            hasEnough = true;
        }

        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.MOVEMONEY.name(),
                "starting doesAccountHaveEnoughMoneyToMove", this.getClass().getSimpleName());
        return hasEnough;

    }

    /**
     * perform move money
     *
     * @param view
     */
    public void onButtonClick(View view) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.MOVEMONEY.name(),
                "starting onButtonClick", this.getClass().getSimpleName());
        spinnerToAccount = (Spinner) findViewById(R.id.spinnerToAccount);
        spinnerFromAccount = (Spinner) findViewById(R.id.spinnerFromAccount);
        final EditText amountEditText = (EditText) findViewById(R.id.EditTextAmount);
        String amount = amountEditText.getText().toString();
        String message = "";
        if (TextUtils.isEmpty(amount)) {
            message = ResourceEnumeration.VALIDATION_AMOUNT_REQUIRED.getValue();
        }
        String regex = this.getString(R.string.movemoneyregularexpression);
        Matcher matcher = Pattern.compile(regex).matcher(amount);
        if (!matcher.find()) {
            message = message + " " + ResourceEnumeration.VALIDATION_AMOUNT_FORMAT.getValue();
        }

        //if (!NumberManager.isValidDecimal(amount)) {
        //message = message + " Amount is required";

        //}

        if (spinnerToAccount.getSelectedItemPosition() == spinnerFromAccount
                .getSelectedItemPosition()) {
            message = message + " " + ResourceEnumeration.VALIDATION_MONEY_TO_FROM_SAME.getValue();
        }
        Account fromAccount = mBank.getAccountList().get(
                spinnerFromAccount.getSelectedItemPosition());
        //make sure has enough money to transfer
        boolean hasEnoughMoney = doesAccountHaveEnoughMoneyToMove(fromAccount, amount);
        if (!hasEnoughMoney) {
            message = message + " " + ResourceEnumeration.VALIDATION_NOT_ENOUGH_MONEY.getValue();

        }
        if (message.length() < 1) {
            AccountTransfer accountTransfer = new AccountTransfer();
            accountTransfer.setBank(mBank);
            accountTransfer.setAmount(amount);
            Account toAccount = mBank.getAccountList().get(
                    spinnerToAccount.getSelectedItemPosition());
            accountTransfer.setToAccountIndex(spinnerToAccount.getSelectedItemPosition());
            accountTransfer.setToAccount(toAccount);

            accountTransfer.setFromAccount(fromAccount);
            accountTransfer.setFromAccountIndex(spinnerFromAccount.getSelectedItemPosition());
            message = String.valueOf(spinnerToAccount.getSelectedItem());
            message = " " + spinnerToAccount.getSelectedItemPosition();


            OFXTransferMoneyTask ofxTransferMoneyTask = new OFXTransferMoneyTask(
                    this, "", "", accountTransfer, this.getResources());

            mAsyncTaskManager.setupTask(ofxTransferMoneyTask);

            // Toast.makeText(getApplicationContext(),
            // message, Toast.LENGTH_LONG).show();
        } else {
            TextView textView = (TextView) findViewById(R.id.textmessage);

            textView.setTextColor(Color.RED);
            textView.setText(message);
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG)
                    .show();
        }
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.MOVEMONEY.name(),
                "ending onButtonClick", this.getClass().getSimpleName());
    }

    @Override
    public void onTaskComplete(AbstractTask task) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.MOVEMONEY.name(),
                "starting onTaskComplete ", this.getClass().getSimpleName());
        // TODO Auto-generated method stub
        if (task.isCancelled()) {
            // Report about cancel
            Toast.makeText(this, R.string.task_cancelled, Toast.LENGTH_LONG)
                    .show();
        } else {
            // Get result
            Boolean result = null;
            try {
                // result = task.get();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.MOVEMONEY.name(),
                "ending onTaskComplete ", this.getClass().getSimpleName());

    }

}