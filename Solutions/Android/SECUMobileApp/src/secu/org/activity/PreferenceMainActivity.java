package secu.org.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import secu.org.application.ResourceEnumeration;

public class PreferenceMainActivity extends AbstractTabActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.PREFERENCE.name(),
                "starting onCreate ", this.getClass().getSimpleName());
        //initActionBar();
        //prepareQuickActionGrid();
        //addActionBarItem(Type.Edit);

        setTitle("Preference");
        String TAB1 = "Preference";
        String TAB2 = "About";
        String TAB3 = "Contact";
        String TAB4 = "Messages";
//
        final Intent intentPreference = new Intent(this, UserSettingActivity.class);
        addTab(TAB1, ResourceEnumeration.TAB_PREFERENCE_TITLE.getValue(), Color.BLACK, "Content of tab #1",
                intentPreference);

        //MessagesActivity
        //AboutActivity
        getTabHost().getTabWidget().setBackgroundResource(R.drawable.tab_selector);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.PREFERENCE.name(),
                "ending onCreate ", this.getClass().getSimpleName());
    }

    /**
     * add tab
     *
     * @param tag
     * @param label
     * @param color
     * @param text
     * @param intent
     */
    private void addTab(String tag, CharSequence label, int color, String text,
                        Intent intent) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "starting addTab ", this.getClass().getSimpleName());
        String EXTRA_COLOR = "com.cyrilmottier.android.gdcatalog.TabbedActionBarActivity$FakeActivity.extraColor";
        String EXTRA_TEXT = "com.cyrilmottier.android.gdcatalog.TabbedActionBarActivity$FakeActivity.extraText";

        intent.putExtra(EXTRA_COLOR, color);
        intent.putExtra(EXTRA_TEXT, text);
        addTab(tag, label, intent);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "ending addTab ", this.getClass().getSimpleName());
    }


}
