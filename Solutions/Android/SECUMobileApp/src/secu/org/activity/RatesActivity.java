package secu.org.activity;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import secu.org.application.ResourceEnumeration;

public class RatesActivity extends AbstractTabActivity {

    private static final String OUTBOX_SPEC = "Outbox";
    private static final String PROFILE_SPEC = "Compose";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "starting onCreate ", this.getClass().getSimpleName());
        // initActionBar();
        // prepareQuickActionBar();
        // prepareQuickActionGrid();
        //prepareQuickActionBar();
        //initActionBar();
        //prepareQuickActionGrid();
        //addActionBarItem(Type.Edit);

        setTitle(ResourceEnumeration.RATES_AND_FEES_ACTIVITY_TITLE.getValue());

        // deposit Tab
        // TabSpec depositSpec = getParentTabHost().newTabSpec("Deposits");
        // Tab Icon

        // depositSpec.setIndicator("Deposits");
        // Intent depositIntent = new Intent(this, DepositActivity.class);
        // Tab Content
        // depositSpec.setContent(depositIntent);
        // addTab(depositSpec);

        // deposit Tab
        // TabSpec mortgageSpec = getParentTabHost().newTabSpec("Mortgages");
        // Tab Icon
        // mortgageSpec.setIndicator("Mortgages");
        // Intent mortgageIntent = new Intent(this, MortgageActivity.class);
        // Tab Content
        // depositSpec.setContent(depositIntent);
        // addTab(depositSpec);
        String TAB1 = "Deposits";
        String TAB2 = "Mortgages";
        String TAB3 = "Loans";
        String TAB4 = "Fees";

        final Intent intentDeposit = new Intent(this, DepositActivity.class);
        addTab(TAB1, ResourceEnumeration.TAB_DEPOSIT_TITLE.getValue(), Color.BLACK, "Content of tab #1",
                intentDeposit);
        final Intent intentMortgage = new Intent(this, MortgageActivity.class);
        addTab(TAB2, ResourceEnumeration.TAB_MORTGAGES_TITLE.getValue(), Color.BLACK, "Content of tab #2",
                intentMortgage);
        final Intent intentLoans = new Intent(this, LoanActivity.class);
        addTab(TAB3, ResourceEnumeration.TAB_LOANS_TITLE.getValue(), Color.BLACK, "Content of tab #3", intentLoans);
        final Intent intentFees = new Intent(this, FeeActivity.class);
        addTab(TAB4, ResourceEnumeration.TAB_FEES_TITLE.getValue(), Color.BLACK, "Content of tab #4", intentFees);

        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "ending onCreate ", this.getClass().getSimpleName());
        getTabHost().getTabWidget().setBackgroundResource(R.drawable.tab_selector);

        //this.getTabHost().getTabWidget().

    }

    /**
     * add tab
     *
     * @param tag
     * @param label
     * @param color
     * @param text
     * @param intent
     */
    private void addTab(String tag, CharSequence label, int color, String text,
                        Intent intent) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "starting addTab ", this.getClass().getSimpleName());
        String EXTRA_COLOR = "com.cyrilmottier.android.gdcatalog.TabbedActionBarActivity$FakeActivity.extraColor";
        String EXTRA_TEXT = "com.cyrilmottier.android.gdcatalog.TabbedActionBarActivity$FakeActivity.extraText";

        intent.putExtra(EXTRA_COLOR, color);
        intent.putExtra(EXTRA_TEXT, text);
        addTab(tag, label, intent);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "ending addTab ", this.getClass().getSimpleName());
    }


}
