package secu.org.activity;

import android.os.Bundle;
import android.widget.TextView;

import greendroid.app.GDActivity;

public class AboutActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarContentView(R.layout.activity_about);
        secu.org.util.LoggerHelper.d(secu.org.application.LoggingTag.APPLICATION.name(), "starting onCreate ", this.getClass().getSimpleName());
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));

        //prepareQuickActionBar();
        // prepareQuickActionGrid();
        // addActionBarItem(Type.Edit);
        populateText();
    }


    public void populateText() {
        secu.org.util.LoggerHelper.d(secu.org.application.LoggingTag.REGISTRATION.name(), "ending onCreate ", this.getClass().getSimpleName());
        TextView textView = (TextView) this.findViewById(R.id.abouttextview);

        try {
            secu.org.application.ComponentHelper.addHTMLToTextView(textView, getResources(), R.raw.about);
        } catch (Exception e) {
            // e.printStackTrace();
            textView.setText("Error: can't show about message.");
        }
        secu.org.util.LoggerHelper.d(secu.org.application.LoggingTag.REGISTRATION.name(), "ending populateText ", this.getClass().getSimpleName());
    }

    @Override
    GDActivity getCurrentActivity() {
        return null;
    }


    //@Override
    //GDActivity getCurrentActivity() {
    // TODO Auto-generated method stub
    //return null;
    //}

}
