package secu.org.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class BugActivity extends Activity {
    public static final String STACKTRACE = "stacktrace";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bug);
        TextView textViewError = (TextView) findViewById(R.id.TextBug);
        TextView textViewInfo = (TextView) findViewById(R.id.TextBugActivityInfo);
        //TextBugActivityInfo
        final secu.org.application.Error error = (secu.org.application.Error) getIntent().getSerializableExtra(STACKTRACE);
        textViewError.setText(error.getErrorMessage());
        textViewInfo.setText(error.getInfo());


    }
}
