package secu.org.activity;

import android.os.Bundle;
import android.webkit.WebView;

import greendroid.app.GDActivity;

public class GenericLocatorActivity extends AbstractActivity {
    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "starting onCreate ", this.getClass().getSimpleName());

        //setActionBarContentView(R.layout.generic_locator);
        //initActionBar();
        // prepareQuickActionBar();
        //prepareQuickActionGrid();
        mWebView = (WebView) findViewById(R.id.webViewLocator);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(getString(R.string.genericlocatorurl));
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "ending onCreate ", this.getClass().getSimpleName());
    }

    @Override
    GDActivity getCurrentActivity() {
        // TODO Auto-generated method stub
        return this;
    }

}
