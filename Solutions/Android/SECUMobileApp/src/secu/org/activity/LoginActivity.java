package secu.org.activity;


import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import greendroid.app.GDActivity;
import secu.org.application.BankApplication;
import secu.org.application.PreferenceManagerSingleton;
import secu.org.asyn.OnTaskCompleteListener;
import secu.org.internet.InternetManager;
import secu.org.ofx.OFXOperatorHelper;
import secu.org.task.AbstractTask;
import secu.org.task.AsyncHelperTaskManager;
import secu.org.task.OFXDownloadTask;

/**
 * @author s16715d
 *
 */

/**
 * @author s16715d
 */
public class LoginActivity extends AbstractActivity implements OnTaskCompleteListener {


    private AsyncHelperTaskManager mAsyncTaskManager;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //MenuInflater menuInflater = getMenuInflater();
        //menuInflater.inflate(R.layout.menu, menu);
        return true;
    }
/*
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.layout.menu,menu);
    }

*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //HelperMenu helperMenu = new HelperMenu(this);
        //helperMenu.selectMenuItem(item);
        return true;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOGON.name(),
                "starting onCreate ", this.getClass().getSimpleName());
        setActionBarContentView(R.layout.login);
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));
        initActionBar();
        //add typeface
        TextView textViewCondensedBold = (TextView) this.findViewById(R.id.texApplicationMessage);
        Typeface typefaceCondensedBold = Typeface.createFromAsset(getAssets(), "font/RobotoCondensed-Bold.ttf");
        textViewCondensedBold.setTypeface(typefaceCondensedBold);
        //set enrollment link
        String enrollText = getResources().getString(R.string.enrolllink);
        final TextView enrollTextView = (TextView) findViewById(R.id.enrolllinkId);
        enrollTextView.setMovementMethod(LinkMovementMethod.getInstance());
        enrollTextView.setText(Html.fromHtml(enrollText));
        // prepareQuickActionBar();
        prepareQuickActionGrid();
        // Create manager and set this activity as context and listener
        mAsyncTaskManager = new AsyncHelperTaskManager(this, this);
        // Handle task that can be retained before
        mAsyncTaskManager.handleRetainedTask(getLastNonConfigurationInstance());
        // check security

        final Button button = (Button) findViewById(R.id.btnLogin);
        if (!BuildConfig.DEBUG) {
            EditText passwordEditText = (EditText) findViewById(R.id.editPassword);
            passwordEditText.setText("");

            EditText userEditText = (EditText) findViewById(R.id.editUserId);
            userEditText.setText("");
        }
        // for speed improvement add xstream instance
        // create the Singleton Object..
        OFXOperatorHelper.getSingletonObject();
        showUserSettings();
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOGON.name(),
                "ending onCreate ", this.getClass().getSimpleName());

    }

    /*
     * get user settup values
     */
    private void showUserSettings() {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOGON.name(),
                "starting showUserSettings ", this.getClass().getSimpleName());
        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        String userName = sharedPrefs.getString(
                PreferenceManagerSingleton.USERNAME,
                PreferenceManagerSingleton.NOTFOUND);
        if (!userName.equals(PreferenceManagerSingleton.NOTFOUND)) {
            final EditText userEditText = (EditText) findViewById(R.id.editUserId);
            userEditText.setText(userName);
        }
        String userPassword = sharedPrefs.getString(
                PreferenceManagerSingleton.USERPASSWORD,
                PreferenceManagerSingleton.NOTFOUND);
        if (!userPassword
                .equals(PreferenceManagerSingleton.NOTFOUND)) {
            final EditText passwordEditText = (EditText) findViewById(R.id.editPassword);
            passwordEditText.setText(userPassword);
        }

        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOGON.name(),
                "ending showUserSettings ", this.getClass().getSimpleName());

    }

    /*
     * click event
     */
    public void onButtonClick(View view) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOGON.name(),
                "starting onButtonClick ", this.getClass().getSimpleName());
        final EditText passwordEditText = (EditText) findViewById(R.id.editPassword);
        String password = passwordEditText.getText().toString();
        String message = "";
        final EditText userEditText = (EditText) findViewById(R.id.editUserId);
        String userId = userEditText.getText().toString();
        boolean cancel = false;
        View focusView = null;

        // check for internet connection
        boolean haveNetworkConnection = InternetManager.haveNetworkConnection(
                this.getBaseContext(), this);
        if (!haveNetworkConnection) {
            TextView textView = (TextView) findViewById(R.id.textmessage);

            textView.setTextColor(Color.RED);
            textView.setText(R.string.internetrequired);
            cancel = true;
        }

        if (!cancel && TextUtils.isEmpty(userId)) {
            userEditText.setError(getString(R.string.useridrequired));
            focusView = userEditText;
            cancel = true;

        }
        if (!cancel && TextUtils.isEmpty(password)) {
            passwordEditText.setError(getString(R.string.passwordrequired));
            focusView = passwordEditText;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // get ofx info
            BankApplication application = (BankApplication) this
                    .getApplication();

            //re start timeout thread
            application.restartTimer();
            final OFXDownloadTask ofxTask = new OFXDownloadTask(this, userId,
                    password, application, this.getResources());
            // Create and run task and progress dialog
            mAsyncTaskManager.setupTask(ofxTask);

            // ofxTask.execute();

        }
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOGON.name(),
                "ending onButtonClick ", this.getClass().getSimpleName());
        // setListAdapter(new ImageAdapter(this, R.layout.main, R.id.text1,
        // R.id.image1, listItems, listImages ));

    }

    @Override
    public void onTaskComplete(AbstractTask task) {
        

    }

    @Override
    GDActivity getCurrentActivity() {
        // TODO Auto-generated method stub
        return this;
    }
}