package secu.org.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import greendroid.app.GDActivity;
import greendroid.widget.ItemAdapter;
import greendroid.widget.item.Item;
import greendroid.widget.item.SeparatorItem;
import greendroid.widget.item.SubtitleItem;
import secu.org.account.Account;
import secu.org.application.BankSingleton;
import secu.org.application.PreferenceManagerSingleton;
import secu.org.asyn.OnTaskCompleteListener;
import secu.org.bank.Bank;
import secu.org.service.AlertService;
import secu.org.task.AbstractTask;
import secu.org.task.AsyncHelperTaskManager;
import secu.org.task.OFXDownloadAccountTransactions;

public class AccountListActivity extends AbstractListActivity implements
        OnClickListener, OnTaskCompleteListener {

    private Bank mBank;

    private AsyncHelperTaskManager mAsyncTaskManager;

    @Override
    GDActivity getCurrentActivity() {
        // TODO Auto-generated method stub
        return this;
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        // TODO Auto-generated method stub
        super.onListItemClick(l, v, position, id);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GETSTATEMENTS.name(),
                "starting onListItemClick ", this.getClass().getSimpleName());
        List<Account> list = mBank.getAccountList();
        int addjustedPosition = position / 2;
        Account selectedAccount = list.get(addjustedPosition);

        OFXDownloadAccountTransactions ofxTask = new OFXDownloadAccountTransactions(
                this, null, null, selectedAccount,
                this.getResources());

        mAsyncTaskManager.setupTask(ofxTask);

        // Toast.makeText(this, "Click-" + String.valueOf(position),
        // Toast.LENGTH_SHORT).show();
    }

    /**
     * populate list
     */
    private void populateList() {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GETSTATEMENTS.name(),
                "starting populateList ", this.getClass().getSimpleName());
        List<Item> items = new ArrayList<Item>();
        mBank = BankSingleton.getSingletonObject().getBank();
        try {

            List<Account> list = mBank.getAccountList();

            for (Account account : list) {
                SubtitleItem sub = new SubtitleItem(account.getAccountType(),
                        account.compositeAccountInfoDisplay());
                items.add(sub);
                // items.add(accountItem);
                items.add(new SeparatorItem(""));

            }

        } catch (Exception ex) {
            System.out.println("FATAL " + ex.getMessage());
        }
        // final ProgressItem progressItem = new
        // ProgressItem("Updating Accounts", true);
        // items.add(progressItem);
        // AccountAdapter accountAdapter = new AccountAdapter(this, items);
        final ItemAdapter adapter = new ItemAdapter(this, items);
        try {
            setListAdapter(adapter);
        } catch (Exception ex) {
            String error = "ERROR - [" + ex.getMessage() + "]";
            secu.org.util.LoggerHelper.d(
                    secu.org.application.LoggingTag.GETSTATEMENTS.name(),
                    "err populateList " + error, this.getClass().getSimpleName());

        }
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GETSTATEMENTS.name(),
                "ending populateList ", this.getClass().getSimpleName());
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GETSTATEMENTS.name(),
                "starting onCreate ", this.getClass().getSimpleName());


        // Create manager and set this activity as context and listener
        mAsyncTaskManager = new AsyncHelperTaskManager(this, this);
        // Handle task that can be retained before
        mAsyncTaskManager.handleRetainedTask(getLastNonConfigurationInstance());
        populateList();
        //register id
        registerGoogleId();

        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GETSTATEMENTS.name(),
                "ending populateList ", this.getClass().getSimpleName());

    }

    /*
     Once logon in register google push service id
     */
    private void registerGoogleId() {

        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GETSTATEMENTS.name(),
                "starting registerGoogleId ", this.getClass().getSimpleName());
        boolean hasRegistered = PreferenceManagerSingleton.getSingletonObject().hasGoogleRegistration(this);
        if (hasRegistered) {
            return;
        }

        secu.org.mail.Mail mail = new secu.org.mail.Mail();
        mail.setFrom("SECU");
        mail.setSubject("Test Google Registration");
        mail.setBody(secu.org.application.BankSingleton.getSingletonObject().getRegistrationId());
        secu.org.application.BankSingleton.getSingletonObject().setMail(mail);
        Intent intent = new Intent(this, AlertService.class);
        startService(intent);
        PreferenceManagerSingleton.getSingletonObject().enableGoogleRegistration(this);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GETSTATEMENTS.name(),
                "ending registerGoogleId ", this.getClass().getSimpleName());
    }

    public void onClick(View arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onRestart() {
        super.onRestart();
        populateList();

    }

    @Override
    public void onTaskComplete(AbstractTask task) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GETSTATEMENTS.name(),
                "starting onTaskComplete ", this.getClass().getSimpleName());
        // TODO Auto-generated method stub
        if (task.isCancelled()) {
            // Report about cancel
            Toast.makeText(this, R.string.task_cancelled, Toast.LENGTH_LONG)
                    .show();
        } else {
            try {
                // result = task.get();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GETSTATEMENTS.name(),
                "ending onTaskComplete ", this.getClass().getSimpleName());

    }

}
