package secu.org.activity;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

public class UserSettingActivity extends PreferenceActivity {


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.PREFERENCE.name(),
                "starting onCreate ", this.getClass().getSimpleName());
        this.getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
        //initActionBar();
        //prepareQuickActionBar();
        //prepareQuickActionGrid();

        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.PREFERENCE.name(),
                "ending onCreate ", this.getClass().getSimpleName());

    }


    public static class MyPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings);

        }
        // @Override
        ///public boolean onCreateOptionsMenu(Menu menu) {
        // menu.add(Menu.NONE, 0, 0, "Show current settings");
        //MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.layout.menu, menu);
        // return super.onCreateOptionsMenu(menu);
        //}
    }


}
