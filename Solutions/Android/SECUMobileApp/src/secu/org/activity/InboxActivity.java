package secu.org.activity;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import greendroid.widget.ItemAdapter;
import greendroid.widget.item.Item;
import greendroid.widget.item.SeparatorItem;
import greendroid.widget.item.SubtitleItem;
import secu.org.application.BankApplication;
import secu.org.application.BankSingleton;
import secu.org.asyn.OnTaskCompleteListener;
import secu.org.task.AbstractTask;
import secu.org.task.AsyncHelperTaskManager;
import secu.org.task.LoadInboxTask;

public class InboxActivity extends ListActivity implements OnItemClickListener,
        OnTaskCompleteListener {
    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    ArrayList<HashMap<String, String>> inboxList;
    List<secu.org.message.Mail> mOutboxList = new ArrayList<secu.org.message.Mail>();

    BankApplication mApplication;
    ItemAdapter mListAdapter;
    List<Item> mItems = new ArrayList<Item>();
    private AsyncHelperTaskManager mAsyncTaskManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplication = (BankApplication) this.getApplication();
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.INBOX.name(),
                "starting onCreate ", this.getClass().getSimpleName());
        // setContentView(R.layout.inbox_list);

        // mListAdapter = new ItemAdapter(this, mItems);
        // Hashmap for ListView
        inboxList = new ArrayList<HashMap<String, String>>();

        // Create manager and set this activity as context and listener
        mAsyncTaskManager = new AsyncHelperTaskManager(this, this);
        // Handle task that can be retained before
        mAsyncTaskManager.handleRetainedTask(getLastNonConfigurationInstance());
        // setContentView(R.layout.inbox_list);
        LoadInboxTask loadInboxTask = new LoadInboxTask(this, null, null,
                BankSingleton.getSingletonObject().getBank(), this.getResources());
        // try{
        // Handle task that can be retained before
        mAsyncTaskManager.handleRetainedTask(loadInboxTask);
        mAsyncTaskManager.setupTask(loadInboxTask);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.INBOX.name(),
                "ending onCreate ", this.getClass().getSimpleName());
        // Loading INBOX in Background Thread

        // }

    }

    /*
     * set rowListener to
     */
    public void setRowListener() {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.INBOX.name(),
                "starting setRowListener ", this.getClass().getSimpleName());
        ListView listView = getListView();
        listView.setTextFilterEnabled(true);
        listView.setOnItemClickListener(this);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.INBOX.name(),
                "ending setRowListener ", this.getClass().getSimpleName());
    }

    /*
     * Build outbox list
     */
    public void buildList() {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.INBOX.name(),
                "starting buildList ", this.getClass().getSimpleName());
        List<Item> items = new ArrayList<Item>();
        Collections.sort(secu.org.application.SecureMailSingleton
                .getSingletonObject().getInBoxMailList());

        for (secu.org.message.Mail mail : secu.org.application.SecureMailSingleton
                .getSingletonObject().getInBoxMailList()) {
            SubtitleItem sub = new SubtitleItem(mail.getFrom(),
                    mail.compositeMail());
            items.add(sub);
            items.add(new SeparatorItem(""));

        }
        // account for no messages found
        if (items.isEmpty()) {
            String message = getString(R.string.nomessages);
            greendroid.widget.item.DescriptionItem noItems = new greendroid.widget.item.DescriptionItem(
                    message);
            items.add(noItems);

        }
        ItemAdapter listAdapter = new ItemAdapter(this, items);
        setListAdapter(listAdapter);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.INBOX.name(),
                "ending onCreate ", this.getClass().getSimpleName());
    }

    @Override
    public void onTaskComplete(AbstractTask task) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.INBOX.name(),
                "starting onTaskComplete ", this.getClass().getSimpleName());
        // TODO Auto-generated method stub
        if (task.isCancelled()) {
            // Report about cancel
            Toast.makeText(this, R.string.task_cancelled, Toast.LENGTH_LONG)
                    .show();
        } else {
            // Get result
            buildList();
            setRowListener();
            try {
                // result = task.get();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.INBOX.name(),
                "ending onTaskComplete ", this.getClass().getSimpleName());

    }

    /**
     * display the detail message
     */
    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.INBOX.name(),
                "starting onItemClick ", this.getClass().getSimpleName());
        int index = 0;
        if (arg2 > 0) {
            index = (arg2 - (arg2 - 1));
        }
        secu.org.message.Mail mail = secu.org.application.SecureMailSingleton
                .getSingletonObject().getInBoxMailList().get(index);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Create the AlertDialog
        AlertDialog alertDialog;
        builder.setTitle(mail.getDisplayFormattedDatePosted());
        builder.setInverseBackgroundForced(true);
        builder.setCancelable(true);
        builder.setMessage(mail.getDisplayBody());
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        // Create the AlertDialog
        alertDialog = builder.create();

        alertDialog.show();
    /*	// custom dialog
		final CustomProgressDialog dialog = new CustomProgressDialog(this);
		dialog.setContentView(R.layout.detail_message);
		dialog.setTitle(mail.getDisplayFormattedDatePosted());

		TextView bodyText = (TextView) dialog.findViewById(R.id.text);
		bodyText.setMovementMethod(new ScrollingMovementMethod());
		bodyText.setText(mail.getDisplayBody());

		Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();*/
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.INBOX.name(),
                "ending onItemClick ", this.getClass().getSimpleName());

    }

}