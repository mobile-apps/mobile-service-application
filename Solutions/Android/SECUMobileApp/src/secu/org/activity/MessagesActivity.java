package secu.org.activity;

import android.content.Context;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import greendroid.app.GDActivity;
import secu.org.application.LoggingTag;
import secu.org.database.DatabaseActivityHelper;
import secu.org.database.DatabaseMessageRow;

/**
 * Created by s16715d on 12/3/13.
 */
public class MessagesActivity extends AbstractActivity implements
        OnClickListener {
    private ListView mListView;
    private List<DatabaseMessageRow> mList;
    private ArrayAdapter<String> mAdapter;
    private MyAdapter mMyAdapter;
    private DatabaseActivityHelper mDatabaseActivityHelper;
    private String[] mStringList = null;
    private List<String> mItems = new ArrayList<String>();


    /*

     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "starting onCreate ", this.getClass().getSimpleName());
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));
        setActionBarContentView(R.layout.message_listview);
        //initActionBar();
        // prepareQuickActionGrid();
        //setContentView(R.layout.activity_messages);
        //  buildList();
        getApplicationMessages();
        Button button = (Button) this.findViewById(R.id.deletebutton);
        button.setOnClickListener(this);

        secu.org.util.LoggerHelper.d(
                LoggingTag.GOOGLEMESSAGE.name(),
                "ending onCreate ", this.getClass().getSimpleName());

    }

    private void uncheckAllChildrenCascade(ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View v = vg.getChildAt(i);
            if (v instanceof CheckBox) {
                ((CheckBox) v).setChecked(false);
            } else if (v instanceof ViewGroup) {
                uncheckAllChildrenCascade((ViewGroup) v);
            }
        }
    }

    /**
     * delete the selected messages
     *
     * @param
     */
    public void onClick(View view) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "starting onClick ", this.getClass().getSimpleName());
        SparseBooleanArray checked = mListView.getCheckedItemPositions();
        try {


            List<DatabaseMessageRow> dataList = mMyAdapter.databaseMessageList;
            List<DatabaseMessageRow> deletedDdataList = new ArrayList<DatabaseMessageRow>();
            for (int i = 0; i < dataList.size(); i++) {
                DatabaseMessageRow dataRow = dataList.get(i);
                if (dataRow.isSelected()) {
                    deletedDdataList.add(dataRow);
                }
            }

            int[] deletedRowIds = new int[deletedDdataList.size()];
            for (int z = 0; z < deletedRowIds.length; z++) {
                deletedRowIds[z] = deletedDdataList.get(z).getId();
            }
            //remove from list
            for (DatabaseMessageRow dataRow : deletedDdataList) {
                mMyAdapter.databaseMessageList.remove(dataRow);
            }

            if (deletedRowIds.length > 0) {
                mDatabaseActivityHelper.delete(deletedRowIds);
            }
            //clear all choices
            uncheckAllChildrenCascade(mListView);
            mMyAdapter.notifyDataSetChanged();

            //end add

/*
            int[] rowIds = new int[checked.size()];
            int[] positionIds = new int[checked.size()];
            DatabaseMessageRow[] deletedRows = new DatabaseMessageRow[checked.size()];
            for (int i = 0; i < checked.size(); i++) {
                // Item position in mAdapter
                int position = checked.keyAt(i);
                positionIds[i] = position;
                deletedRows[i] = mList.get(position);
                // listView.SetItemChecked()
                rowIds[i] = mList.get(position).getId();
                // Add sport if it is checked i.e.) == TRUE!

            }
            //loop thru id to remove for rows
            for (int x = 0; x < deletedRows.length; x++) {


                mMyAdapter.remove(deletedRows[x]);

            }


            mMyAdapter.notifyDataSetChanged();
            mListView.clearChoices();
            mDatabaseActivityHelper.delete(rowIds);*/


            //refreshList();

        } catch (Exception e) {
            secu.org.util.LoggerHelper.e(
                    secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                    "Error  removing items [" + e.getMessage() + "]", this.getClass().getSimpleName());
        }
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "ending onClick ", this.getClass().getSimpleName());
    }


    /**
     * get list of messages from database
     */
    public void getApplicationMessages() {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "starting getApplicationMessages ", this.getClass().getSimpleName());

        mDatabaseActivityHelper = new DatabaseActivityHelper(this);

        mList = mDatabaseActivityHelper.findAll();
        if (mList != null && mList.size() > 0) {
            //mStringList = new String[mList.size()];
            for (int x = 0; x < mList.size(); x++) {
                // mStringList[x] = mList.get(x).getMessage();
                mItems.add(mList.get(x).getMessage());

            }

        }

        mAdapter = (new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_multiple_choice, mItems));

        mMyAdapter = new MyAdapter(this, android.R.layout.simple_list_item_multiple_choice, mList);
        // mAdapter = (new ArrayAdapter<String>(this,
        //    android.R.layout.simple_list_item_multiple_choice, mItems));

        mListView = (ListView) this.findViewById(R.id.listView);
        mListView.setAdapter(mMyAdapter);

        // mListView.setAdapter(mAdapter);

        mListView.setItemsCanFocus(false);
        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);


        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "ending getApplicationMessages ", this.getClass().getSimpleName());

    }

    @Override
    GDActivity getCurrentActivity() {
        return null;
    }

    private class MyAdapter extends ArrayAdapter<DatabaseMessageRow> {

        int textViewResourceId;
        List<DatabaseMessageRow> databaseMessageList;
        List<CheckBox> checkBoxList = new ArrayList<CheckBox>();
        ViewGroup mParent;

        public MyAdapter(Context context, int textViewResourceId, List<DatabaseMessageRow> objects) {
            super(context, textViewResourceId, objects);
            this.textViewResourceId = textViewResourceId;
            this.databaseMessageList = objects;

        }

        private class ViewHolder {
            TextView code;
            CheckBox name;
        }


        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            ViewHolder holder = null;
            if (view == null) {
                // we are not reusing anything today
                LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                // view = vi.inflate(android.R.layout.simple_list_item_multiple_choice, null);
                view = vi.inflate(R.layout.message_row, null);
                mParent = parent;

                //add
                holder = new ViewHolder();
                holder.code = (TextView) view.findViewById(R.id.code);
                holder.name = (CheckBox) view.findViewById(R.id.checkBox1);
                checkBoxList.add(holder.name);

                view.setTag(holder);
                holder.name.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v;
                        DatabaseMessageRow dataRow = (DatabaseMessageRow) cb.getTag();
                        //add
                        String display = dataRow.computeDateDisplay() + " " + dataRow.getMessage();
                        Toast.makeText(MessagesActivity.this, display, Toast.LENGTH_SHORT).show();

                        dataRow.setSelected(cb.isChecked());
                    }
                });

                //add end



          /*  CheckedTextView ct = (CheckedTextView) view.findViewById(android.R.id.text1);
                 DatabaseMessageRow row = mList.get(position);
            String message = row.getMessage() + "-" + row.computeDateDisplay();
            ct.setText(message);*/


            } else {
                holder = (ViewHolder) view.getTag();


            }
            //add
            DatabaseMessageRow row = mList.get(position);
            holder.code.setText(row.computeDateDisplay());
            holder.name.setText(row.getMessage());
            holder.name.setTag(row);
            //end add

            return view;
        }
    }
}