package secu.org.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.Toast;

import java.util.ArrayList;

public class FruitSelectionActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        ArrayList<String> fruitSelection = intent.getStringArrayListExtra("fruits");

        if (fruitSelection != null) {
            // use a toast to display selected fruits
            if (fruitSelection.size() > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("Your selection of fruits\n\n");
                for (String s : fruitSelection) {
                    sb.append("* ").append(s).append("\n");
                }
                Toast toast = Toast.makeText(this, sb.toString().trim(), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                Toast toast = Toast.makeText(this, "Nothing selected", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }
}