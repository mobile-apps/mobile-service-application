package secu.org.activity;


import android.graphics.Color;
import android.widget.TabHost;


public class UserInterfaceUtil {

    public static void setTabColor(TabHost tabhost, String unSelectedColor, String selectedColor) {
        for (int i = 0; i < tabhost.getTabWidget().getChildCount(); i++) {
            tabhost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor(unSelectedColor)); //unselected
        }
        tabhost.getTabWidget().getChildAt(tabhost.getCurrentTab()).setBackgroundColor(Color.parseColor(selectedColor)); // selected
    }
}
