package secu.org.activity;


import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import greendroid.app.GDActivity;
import greendroid.app.GDListActivity;
import greendroid.widget.ActionBar;
import greendroid.widget.ActionBarHost;
import greendroid.widget.ActionBarItem;
import secu.org.application.BankApplication;
import secu.org.menu.HelperMenu;

public abstract class AbstractListActivity extends GDListActivity implements ActionBar.OnActionBarListener {


    private MenuHelper mMenuHelper;

    public AbstractListActivity() {
        mMenuHelper = new MenuHelper(this);
    }

    public android.view.View getMenuViewAnchor() {
        View parent = this.getContentView();
        //  int listChildCount = this.getContentView().getChildCount();
        View childView = this.findViewById(R.id.gd_action_bar);
        return childView;
    }

    protected void prepareQuickActionBar() {
        mMenuHelper.prepareQuickActionBar();
    }

    /**
     * construct action grid
     */
    protected void prepareQuickActionGrid() {
        Intent i = getIntent();
        mMenuHelper.prepareQuickActionGrid(i);
    }

    abstract GDActivity getCurrentActivity();

    protected void initActionBar() {

		/*
       addActionBarItem(getGDActionBar()
                .newActionBarItem(NormalActionBarItem.class)
                .setDrawable(R.drawable.ic_menu_moreoverflow)
                .setContentDescription(R.string.gd_export), R.id.action_bar_export);

		addActionBarItem(Type.Locate, R.id.action_bar_locate);
      */
    }

    @Override
    public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
        switch (item.getItemId()) {
            case R.id.action_bar_locator_return:
                startActivity(new Intent(this, LocatorActivity.class));
                break;
            case R.id.menu_bar_accounts:
                startActivity(new Intent(this, AccountListActivity.class));
                break;
            case R.id.menu_bar_transfer:
                startActivity(new Intent(this, MoveMoneyActivity.class));
                break;
            case R.id.menu_bar_message_center:
                startActivity(new Intent(this, MessageCenterActivity.class));
                break;
            default:
                mMenuHelper.onShowGrid(item.getItemView());
        }

        //Toast.makeText(this, "Click-" + String.valueOf(position),
        //Toast.LENGTH_SHORT).show();
        return true;
    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        BankApplication application = (BankApplication) this.getApplication();
        long currentTime = System.currentTimeMillis();
        application.setLastInteractionTime(currentTime);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.layout.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        HelperMenu helperMenu = new HelperMenu(this);
        helperMenu.selectMenuItem(item);
        return true;

    }

    public void onPreContentChanged() {
        super.onPreContentChanged();
        ActionBarHost actionBarHost = (ActionBarHost) findViewById(com.cyrilmottier.android.greendroid.R.id.gd_action_bar_host);
        if (actionBarHost == null) {
            throw new RuntimeException("Your content must have an ActionBarHost whose id attribute is R.id.gd_action_bar_host");
        }
        //
        actionBarHost.getActionBar().setOnActionBarListener(this);
    }


    public void onActionBarItemClicked(int position) {
        ActionBarHost actionBarHost = (ActionBarHost) findViewById(com.cyrilmottier.android.greendroid.R.id.gd_action_bar_host);
        ActionBar actionBar = actionBarHost.getActionBar();
        ActionBarItem item = actionBar.getItem(position);
        this.mMenuHelper.onShowMenu(getMenuViewAnchor());

    }

}
