package secu.org.activity;

import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import greendroid.app.GDActivity;
import greendroid.widget.ItemAdapter;
import greendroid.widget.item.DescriptionItem;
import greendroid.widget.item.Item;
import greendroid.widget.item.LabelValueItem;
import greendroid.widget.item.SeparatorItem;
import greendroid.widget.item.SubtitleItem;
import secu.org.account.Account;
import secu.org.account.AccountTransaction;
import secu.org.application.PreferenceManagerSingleton;


public class AccountTransactionActivity extends AbstractListActivity {

    @Override
    GDActivity getCurrentActivity() {
        // TODO Auto-generated method stub
        return this;
    }

    private void buildTransactions() {
        List<Item> items = new ArrayList<Item>();
        Intent i = getIntent();
        Account account = (Account) i
                .getSerializableExtra(secu.org.session.ActivitySession.ACCOUNT);

        try {

            List<AccountTransaction> list = account.getAccountTransactionList();
            Collections.sort(list);
            String title = "Account days - "
                    + PreferenceManagerSingleton.getSingletonObject().getTransactionDays();
            items.add(new SubtitleItem(title, account.compositeAccountInfoDisplay()));
            items.add(new SeparatorItem(""));

            //items.add(account.getAccountType());
            for (AccountTransaction accountTransaction : list) {

                LabelValueItem postedLabelValueItem = new LabelValueItem("Posted Date: ", accountTransaction.getDisplayFormattedDatePosted());
                items.add(postedLabelValueItem);

                LabelValueItem descLabelValueItem = new LabelValueItem("Name: ", accountTransaction.getName());
                items.add(descLabelValueItem);

                LabelValueItem creditDebitLabelValueItem = new LabelValueItem(accountTransaction.getAccountTransactionType() + ": ", accountTransaction.getDisplayAmount());
                items.add(creditDebitLabelValueItem);

                DescriptionItem memoDescriptionItem = new DescriptionItem(accountTransaction.getMemo());
                items.add(memoDescriptionItem);
                //items.add(accountItem);
                items.add(new SeparatorItem(""));

            }

        } catch (Exception ex) {
            System.out.println("FATAL " + ex.getMessage());
        }
        final ItemAdapter adapter = new ItemAdapter(this, items);
        try {
            setListAdapter(adapter);
        } catch (Exception ex) {
            String error = "ERROR - [" + ex.getMessage() + "]";
            System.out.println(error);
        }


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(this));

        // prepareQuickActionBar();
        //prepareQuickActionGrid();
        // this.initActionBar();
        buildTransactions();

    }
}
