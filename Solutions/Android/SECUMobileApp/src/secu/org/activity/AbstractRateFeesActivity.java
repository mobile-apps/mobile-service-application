package secu.org.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import secu.org.adapter.ExpandableListAdapter;

public abstract class AbstractRateFeesActivity extends Activity {
    /**
     * Called when the activity is first created.
     */


    //List<Map<String, String>> mSourceList = new ArrayList<Map<String, String>>();


    List<String> groupList;
    List<String> childList;
    Map<String, List<String>> groupCollection = new LinkedHashMap<String, List<String>>();
    ExpandableListView expListView;


    int textlength = 0;

    /**
     * Create group list
     */
    abstract protected void createGroupList();

    /**
     * @param deposit
     */
    abstract protected void createCollection(Object obj);

    /**
     * load child array
     *
     * @param childItems
     */
    protected void loadChild(String[] childItems) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "starting loadChild ", this.getClass().getSimpleName());

        childList = new ArrayList<String>();
        for (String model : childItems)
            childList.add(model);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "ending loadChild ", this.getClass().getSimpleName());

    }

    public String removeHTML(String html) {
        String removedHTML = android.text.Html.fromHtml(html).toString();
        return removedHTML;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "starting onCreate ", this.getClass().getSimpleName());

        setContentView(R.layout.expandablelistview);


        createGroupList();

        // createCollection();

        expListView = (ExpandableListView) findViewById(R.id.expandablelist);
        final ExpandableListAdapter expListAdapter = new ExpandableListAdapter(
                this, groupList, groupCollection);
        expListView.setAdapter(expListAdapter);
        expListView.setOnChildClickListener(new OnChildClickListener() {

            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                final String selected = (String) expListAdapter.getChild(
                        groupPosition, childPosition);
                Toast.makeText(getBaseContext(), selected, Toast.LENGTH_LONG)
                        .show();

                return true;
            }
        });


        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "ending onCreate ", this.getClass().getSimpleName());

    }


}
