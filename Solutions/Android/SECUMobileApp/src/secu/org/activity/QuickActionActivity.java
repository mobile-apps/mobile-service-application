package secu.org.activity;

import android.os.Bundle;

import greendroid.app.GDActivity;
import greendroid.widget.ActionBarItem.Type;

public class QuickActionActivity extends AbstractActivity {


    @Override
    GDActivity getCurrentActivity() {
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setActionBarContentView(R.layout.movemoney);

        prepareQuickActionBar();
        prepareQuickActionGrid();

        addActionBarItem(Type.Edit);
    }


}
