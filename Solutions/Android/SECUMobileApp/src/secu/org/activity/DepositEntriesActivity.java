package secu.org.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

import secu.org.ratesfees.Deposit;
import secu.org.ratesfees.DepositEntry;
import secu.org.ratesfees.LendingRatesSingleton;

public class DepositEntriesActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new secu.org.application.ExceptionHandler(
                this));
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "starting onCreate ", this.getClass().getSimpleName());

        setContentView(R.layout.activity_deposit_entries);//depositEntrylist
        final ListView listview = (ListView) findViewById(R.id.depositEntrylist);
        Deposit deposit = LendingRatesSingleton.getSingletonObject(this).getDeposit();


        // convert deposits
        final ArrayList<String> list = new ArrayList<String>();
        for (DepositEntry depositEntry : deposit.getDepositEntryList()) {
            // mDepoistList.add(createPlanet(deposit.getDescription(),
            // deposit.getDescription()));
            list.add(depositEntry.toString());
        }
        final StableArrayAdapter adapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);
        listview.setAdapter(adapter);
    }


}
