package secu.org.activity;

import java.util.ArrayList;

import secu.org.ratesfees.Deposit;
import secu.org.ratesfees.DepositEntry;
import secu.org.ratesfees.Deposits;
import secu.org.ratesfees.LendingRatesSingleton;

public class DepositActivity extends AbstractRateFeesActivity {

    /**
     * Create group list
     */
    @Override
    protected void createGroupList() {
        Deposits deposits = LendingRatesSingleton.getSingletonObject(this)
                .getLendingRatesHandler().getDeposits();


        groupList = new ArrayList<String>();

        for (Deposit deposit : deposits.getDepositList()) {
            // mDepoistList.add(createPlanet(deposit.getDescription(),
            // deposit.getDescription()));
            groupList.add(deposit.getDescription());
            createCollection(deposit);
        }


    }

    /**
     * @param deposit
     */
    @Override
    protected void createCollection(Object obj) {
        Deposit deposit = (Deposit) obj;
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "starting createCollection ", this.getClass().getSimpleName());
        ArrayList<String> itemList = new ArrayList<String>();
        for (DepositEntry depositEntry : deposit.getDepositEntryList()) {
            // mDepoistList.add(createPlanet(deposit.getDescription(),
            // deposit.getDescription()));
            itemList.add(depositEntry.toString());

        }
        String[] itemString = itemList.toArray(new String[itemList.size()]);
        loadChild(itemString);
        groupCollection.put(deposit.getDescription(), childList);

        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.RATEFEES.name(),
                "ending createCollection ", this.getClass().getSimpleName());

    }


}
