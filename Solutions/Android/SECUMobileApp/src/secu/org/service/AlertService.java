package secu.org.service;

import secu.org.application.BankSingleton;
import secu.org.application.PreferenceManagerSingleton;
import secu.org.mail.Mail;
import android.app.IntentService;
import android.content.Intent;

public class AlertService extends IntentService {
	public AlertService() {
	    super("AlertService");
	  }
	// Will be called asynchronously be Android
	@Override
	protected void onHandleIntent(Intent arg0) {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.SECURITY.name(),
				"starting onHandleIntent ", this.getClass().getSimpleName());
		
		//if (!PreferenceManagerSingleton.getSingletonObject().hasSecurityAlertsTurnedOn()) {
		//	return;
		//}
		secu.org.ofx.OFXOperatorSendMessage ofxOperatorSendMessage = new secu.org.ofx.OFXOperatorSendMessage();
		secu.org.bank.Bank bank = BankSingleton.getSingletonObject().getBank();
        Mail mail = BankSingleton.getSingletonObject().getMail();
       if (mail == null) {
           secu.org.util.LoggerHelper.d(
                   secu.org.application.LoggingTag.SECURITY.name(),
                   "empty mail message ", this.getClass().getSimpleName());
           return;
       }
		bank.setMail(mail);
		try {
			ofxOperatorSendMessage.ofxOperation(bank);
		} catch (Exception e) {
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.SECURITY.name(),
					"error onHandleIntent " + e.getMessage(), this.getClass().getSimpleName());
			e.printStackTrace();
		}
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.SECURITY.name(),
				"ending onHandleIntent ", this.getClass().getSimpleName());
		
	}

	
}
