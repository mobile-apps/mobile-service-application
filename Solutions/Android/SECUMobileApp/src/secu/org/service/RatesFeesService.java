package secu.org.service;

import android.app.IntentService;
import android.content.Intent;

import secu.org.application.BankSingleton;
import secu.org.google.cloud.message.RatesAndFeesMessengerHandler;
import secu.org.mail.Mail;

public class RatesFeesService extends IntentService {
	public RatesFeesService() {
	    super("RatesFeesService");
	  }
	// Will be called asynchronously be Android
	@Override
	protected void onHandleIntent(Intent arg0) {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.SECURITY.name(),
				"starting onHandleIntent ", this.getClass().getSimpleName());

        RatesAndFeesMessengerHandler ratesAndFeesMessengerHandler = new RatesAndFeesMessengerHandler();
       ratesAndFeesMessengerHandler.refresh(this);
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.SECURITY.name(),
				"ending onHandleIntent ", this.getClass().getSimpleName());
		
	}

	
}
