package secu.org.fragment;
import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import greendroid.widget.ItemAdapter;
import secu.org.activity.R;
/**
 * Created by s16715d on 3/6/14.
 */
public class PlanetFragment  extends ListFragment  {

    String[] countries = new String[] {
            "India",
            "Pakistan",
            "Sri Lanka",
            "China",
            "Bangladesh",
            "Nepal",
            "Afghanistan",
            "North Korea",
            "South Korea",
            "Japan"
    };

    public static final String ARG_PLANET_NUMBER = "planet_number";
    private Activity mActivity;
    public PlanetFragment(Activity activity) {
        mActivity = activity;
        // Empty constructor required for fragment subclasses
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        int i = getArguments().getInt(ARG_PLANET_NUMBER);

        String planet = getResources().getStringArray(R.array.menu_array)[i];
        mActivity.setTitle(planet);
        View rootView = inflater.inflate(R.layout.fragment_planet, container, false);

        try {
            //ItemAdapter adapter = ItemAdapter.createFromXml(mActivity, R.xml.items);
           // setListAdapter(adapter);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(inflater.getContext(), android.R.layout.simple_list_item_1,countries);
            setListAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return rootView;
    }

}
