package secu.org.widget.itemview;


import greendroid.widget.item.Item;
import greendroid.widget.itemview.ItemView;
import secu.org.activity.R;
import secu.org.widget.item.AccountItem;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AccountItemView extends LinearLayout implements ItemView {
	 private TextView mAccountGroup;
	 private TextView mAccountDescription;

	 
	 
		public AccountItemView(Context context) {
			this(context, null);
		}

		public AccountItemView(Context context, AttributeSet attrs) {
			super(context, attrs);
			//mAccountListActivityGreen = (AccountListActivityGreen)context;
		}
		
		
		public void prepareItemView() {
			mAccountGroup = (TextView) findViewById(R.id.textViewAccountGroup);
			mAccountDescription = (TextView) findViewById(R.id.textViewAccountDescription);
		
		}

		
		public void setObject(Item object) {
			 final AccountItem item = (AccountItem) object;
			 mAccountGroup.setText(item.getmAccountGroup());
			 mAccountDescription.setText(item.getmAccountDescription());
		}

	
		


}
