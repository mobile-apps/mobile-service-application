package secu.org.widget.item;
import greendroid.widget.item.TextItem;
import greendroid.widget.itemview.ItemView;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.cyrilmottier.android.greendroid.R;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class AccountItem extends TextItem {
	private String mAccountGroup;
	private String mAccountDescription;
	
    public AccountItem(String accountGroup, String accountDescription) 
    {
    	mAccountGroup = accountGroup;
    	mAccountDescription = accountDescription;
    }
    
	@Override
	public ItemView newView(Context context, ViewGroup parent) {

		return createCellFromXml(context, secu.org.activity.R.layout.account_item_view,
				parent);
	}
	
	 @Override
	    public void inflate(Resources r, XmlPullParser parser, AttributeSet attrs) throws XmlPullParserException, IOException {
	        super.inflate(r, parser, attrs);

	        TypedArray a = r.obtainAttributes(attrs, R.styleable.DoubleButtonItem);
	        //text = a.getString(R.styleable.TextItem_text);
	        a.recycle();
	    }

	public String getmAccountGroup() {
		return mAccountGroup;
	}

	public void setmAccountGroup(String mAccountGroup) {
		this.mAccountGroup = mAccountGroup;
	}

	public String getmAccountDescription() {
		return mAccountDescription;
	}

	public void setmAccountDescription(String mAccountDescription) {
		this.mAccountDescription = mAccountDescription;
	}

}
