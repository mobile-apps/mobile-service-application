package secu.org.adapter;
import java.util.List;
import java.util.Map;

import secu.org.activity.R;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
public class ExpandableListAdapter extends BaseExpandableListAdapter{

	  private Activity context;
	    private Map<String, List<String>> groupCollections;
	    private List<String> groupItem;
	 
	    public ExpandableListAdapter(Activity context, List<String> groupItem,
	            Map<String, List<String>> groupCollections) {
	        this.context = context;
	        this.groupCollections = groupCollections;
	        this.groupItem = groupItem;
	    }
	 
	    public Object getChild(int groupPosition, int childPosition) {
	        return groupCollections.get(groupItem.get(groupPosition)).get(childPosition);
	    }
	 
	    public long getChildId(int groupPosition, int childPosition) {
	        return childPosition;
	    }
	 
	    public View getChildView(final int groupPosition, final int childPosition,
	            boolean isLastChild, View convertView, ViewGroup parent) {
	         String laptop = (String) getChild(groupPosition, childPosition);
            laptop = android.text.Html.fromHtml(laptop).toString();
	        LayoutInflater inflater = context.getLayoutInflater();
	 
	        if (convertView == null) {
	            convertView = inflater.inflate(R.layout.child_item, null);
	        }
	 
	        TextView item = (TextView) convertView.findViewById(R.id.child);
	 

	        item.setText(laptop);
	        return convertView;
	    }
	 
	    public int getChildrenCount(int groupPosition) {
	        return groupCollections.get(groupItem.get(groupPosition)).size();
	    }
	 
	    public Object getGroup(int groupPosition) {
	        return groupItem.get(groupPosition);
	    }
	 
	    public int getGroupCount() {
	        return groupItem.size();
	    }
	 
	    public long getGroupId(int groupPosition) {
	        return groupPosition;
	    }
	 
	    public View getGroupView(int groupPosition, boolean isExpanded,
	            View convertView, ViewGroup parent) {
	        String laptopName = (String) getGroup(groupPosition);
	        if (convertView == null) {
	            LayoutInflater infalInflater = (LayoutInflater) context
	                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            convertView = infalInflater.inflate(R.layout.group_item,
	                    null);
	        }
	        TextView item = (TextView) convertView.findViewById(R.id.group);
	        item.setTypeface(null, Typeface.BOLD);
            laptopName = android.text.Html.fromHtml(laptopName).toString();
	        item.setText(laptopName);
	        return convertView;
	    }
	 
	    public boolean hasStableIds() {
	        return true;
	    }
	 
	    public boolean isChildSelectable(int groupPosition, int childPosition) {
	        return true;
	    }
	}