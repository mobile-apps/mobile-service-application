package secu.org.adapter;

import greendroid.widget.item.Item;
import greendroid.widget.itemview.ItemView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class AccountAdapter extends greendroid.widget.ItemAdapter {

	 private Context mContext;
	 private secu.org.activity.AccountListActivity mAccountListActivityGreen;
	public AccountAdapter(Context context) {
		super(context, new ArrayList<Item>());
		mContext= context;
	}
    public AccountAdapter(Context context, List<Item> items) {
        super(context, items, 10);
        // mAccountListActivityGreen = (secu.org.AccountListActivityGreen)context;
        mContext= context;
        LayoutInflater.from(context);
    }

	public AccountAdapter(Context context, Item[] items) {
		super(context, Arrays.asList(items), 10);
		mContext= context;
	}
    public AccountAdapter(Context context, Item[] items, int maxViewTypeCount) {
        super(context, Arrays.asList(items), maxViewTypeCount);
        mContext= context;
    }
    public AccountAdapter(Context context, List<Item> items, int maxViewTypeCount) {
          super(context,items, maxViewTypeCount);
         
          mContext= context;
          
    }
    public View getViewTwo(final int position, View convertView, ViewGroup parent) {
	
    
    	
    	System.out.println(" view [" + mAccountListActivityGreen.getListView().toString() + "]");
		
		/*
		mAccountListActivityGreen.getListView().setOnClickListener(new OnClickListener() {
            private int pos = position;

            @Override
            public void onClick(View v) {
              Toast.makeText(mAccountListActivityGreen, "Click-" + String.valueOf(pos), Toast.LENGTH_SHORT).show();    
            }
          });
        
		Button button = (Button) mAccountListActivityGreen.getListView()
				.findViewById(R.id.ButtonActivity);

		if (button != null) {
			  button.setOnClickListener(new OnClickListener() {
		          private int pos = position;

		          @Override
		          public void onClick(View v) {
		            Toast.makeText(mAccountListActivityGreen, "Delete-" + String.valueOf(pos), Toast.LENGTH_SHORT).show();

		          }
		        });
		}
		
	
		*/
		
		
		
		/*
    	if (convertView != null) {
	        convertView.setOnClickListener(new OnClickListener() {
	            private int pos = position;

	            @Override
	            public void onClick(View v) {
	              Toast.makeText(mAccountListActivityGreen, "Click-" + String.valueOf(pos), Toast.LENGTH_SHORT).show();    
	            }
	          });
	        
			Button button = (Button) convertView
					.findViewById(R.id.ButtonActivity);

			if (button != null) {
				  button.setOnClickListener(new OnClickListener() {
			          private int pos = position;

			          @Override
			          public void onClick(View v) {
			            Toast.makeText(mAccountListActivityGreen, "Delete-" + String.valueOf(pos), Toast.LENGTH_SHORT).show();

			          }
			        });
			}
		}
    	*/
    	
    	
        final Item item = (Item) getItem(position);
        ItemView cell = (ItemView) convertView;

        if (cell == null) {
            cell = item.newView(mContext, null);
            cell.prepareItemView();
        }

        cell.setObject(item);

        return (View) cell;
    }
   
    
}
