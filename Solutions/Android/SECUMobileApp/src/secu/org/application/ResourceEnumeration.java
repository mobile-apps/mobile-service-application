package secu.org.application;

import secu.org.activity.R;
import android.content.Context;


public enum ResourceEnumeration {
//this.loginActivity.getResources().getString(R.string.invalidcredentials);
    ABOUT_ACTIVITY_TITLE(R.string.about_title),
    ACCOUNT_SUMMARY_ACTIVITY_TITLE(R.string.account_summary_activity_title),
    ATM_BRANCH_LOCATOR_ACTIVITY_TITLE(R.string.atm_branch_locator_title),
    CONTACT_US_ACTIVITY_TITLE(R.string.contact_us_title),
    INVALID_CREDENTIALS(R.string.invalidcredentials),
    NOTIFICATIONS_ACTIVITY_TITLE(R.string.notifications_title),
    MESSAGE_CENTER_ACTIVITY_TITLE(R.string.message_center_title),
    MY_PREFERENCE_ACTIVITY_TITLE(R.string.my_preference_title),
    RATES_SERVER_URL(R.string.ratesfessurl),
    RATES_AND_FEES_ACTIVITY_TITLE(R.string.rates_fees_title),
    TAB_DEPOSIT_TITLE(R.string.tab_deposit_title),
    TAB_FEES_TITLE(R.string.tab_fees_title),
    TAB_LOANS_TITLE(R.string.tab_loans_title),
    TAB_MORTGAGES_TITLE(R.string.tab_mortgages_title),
    TAB_PREFERENCE_TITLE(R.string.tab_preference_title),
    TRANSFER_MONEY_ACTIVITY_TITLE(R.string.transfer_money_title),
    VALIDATION_AMOUNT_REQUIRED(R.string.validation_amount_required),
    VALIDATION_AMOUNT_FORMAT(R.string.validation_money_format),
    VALIDATION_NOT_ENOUGH_MONEY(R.string.validation_money_not_enough_money),
    VALIDATION_MONEY_TO_FROM_SAME(R.string.validation_money_to_from),;

    private String value;
    ResourceEnumeration(int stringID) {

        value = secu.org.application.BankApplication.getAppContext().getResources().getString(stringID);
    }

    public String getValue() {
        return value;
    }
}
