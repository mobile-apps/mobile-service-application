package secu.org.application;

import java.util.ArrayList;
import java.util.List;





public class SecureMailSingleton {
	
	private static SecureMailSingleton singletonObject;
	private List<secu.org.message.Mail> inBoxMailList = new ArrayList<secu.org.message.Mail>();
	private List<secu.org.message.Mail> outBoxMailList = new ArrayList<secu.org.message.Mail>();
	
	public List<secu.org.message.Mail> getInBoxMailList() {
		
		return inBoxMailList;
	}

	public void setInBoxMailList(List<secu.org.message.Mail> inBoxMailList) {
		this.inBoxMailList = inBoxMailList;
	}

	public List<secu.org.message.Mail> getOutBoxMailList() {
		
		return outBoxMailList;
	}

	public void setOutBoxMailList(List<secu.org.message.Mail> outBoxMailList) {
		this.outBoxMailList = outBoxMailList;
	}

	/** A private Constructor prevents any other class from instantiating. */
	private SecureMailSingleton(){
		
	}

	public Object clone()throws CloneNotSupportedException
	{
	    throw new CloneNotSupportedException(); 
	}
	
	public static synchronized SecureMailSingleton getSingletonObject()
	{
	    if (singletonObject == null){
	    	singletonObject = new SecureMailSingleton();
	    }
	    return singletonObject;
	}

}
