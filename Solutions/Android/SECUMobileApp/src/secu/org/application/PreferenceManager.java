package secu.org.application;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.Context;

public class PreferenceManager {
	private static String SHARED_PREFERENCES_NAME = "SHARED_PREFERENCES_NAME";
	public static boolean getBooleanFromSP(String key, Context context) {
		// TODO Auto-generated method stub
		SharedPreferences preferences = context.getApplicationContext()
				.getSharedPreferences(SHARED_PREFERENCES_NAME,
						android.content.Context.MODE_PRIVATE);
		return preferences.getBoolean(key, false);
	}// getPWDFromSP()
	
	public static void saveBooleanInSP(Context context,String key, boolean value){
	    SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(SHARED_PREFERENCES_NAME, android.content.Context.MODE_PRIVATE);
	    SharedPreferences.Editor editor =      preferences.edit();
	    editor.putBoolean(key, value);
	    editor.commit();
	    
	}
	





	public static int getIntFromSP(String key, Activity activity) {
		// TODO Auto-generated method stub
		SharedPreferences preferences = activity.getApplicationContext()
				.getSharedPreferences(SHARED_PREFERENCES_NAME,
						android.content.Context.MODE_PRIVATE);
		return preferences.getInt(key, Integer.MIN_VALUE);
	}
	
	public static void saveIntInSP(Activity activity,String key, int value){
	    SharedPreferences preferences = activity.getApplicationContext().getSharedPreferences(SHARED_PREFERENCES_NAME, android.content.Context.MODE_PRIVATE);
	    SharedPreferences.Editor editor =      preferences.edit();
	    editor.putInt(key, value);
	    editor.commit();
	}
}
