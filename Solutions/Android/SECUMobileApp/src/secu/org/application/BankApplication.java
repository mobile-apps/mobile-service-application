package secu.org.application;

import greendroid.app.GDApplication;
import secu.org.activity.AccountListActivity;
import secu.org.activity.LoginActivity;
import secu.org.activity.SecurityControllerActivity;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import java.util.Locale;

public class BankApplication extends GDApplication {
	
	
	private String errorMesasge;
	private long mLastInteractionTime = Long.MIN_VALUE;
	private long mTimeout;
	private long mStartTime = System.currentTimeMillis();
	private boolean mStopThread = false;

    private static Context mContext;

    public static Context getAppContext() {
        return mContext;
    }

	@Override
	public void onCreate() {
		super.onCreate();
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.APPLICATION.name(),
				"starting onCreate ", this.getClass().getSimpleName());

        mContext = getApplicationContext();
		setTimeout();
		PreferenceManagerSingleton.getSingletonObject().setSharedPreferences(this);
		//start formatter
		TextFormatterSingleton.getSingletonObject(this);
		startUserInactivityDetectThread(); // start the thread to detect
											// inactivity
		new ScreenReceiver(); // creating receive SCREEN_OFF and SCREEN_ON
								// broadcast msgs from the device.
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.APPLICATION.name(),
				"ending onCreate ", this.getClass().getSimpleName());
	}
	

    public static String getLanguage() {
        Locale current = getAppContext().getResources().getConfiguration().locale;
        String language = current.getLanguage();
        return language;

    }


	public boolean isDebug() {
		@SuppressWarnings("static-access")
		boolean isDebuggable = (0 != (getApplicationInfo().flags & getApplicationInfo().FLAG_DEBUGGABLE));
		return isDebuggable;
	}
	/**
	 * has application timeout
	 * @return
	 */
	public boolean hasTimeOut()
	{
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.APPLICATION.name(),
				"starting hasTimeOut ", this.getClass().getSimpleName());
		boolean hasTimedOut = false;
		long currentTime = System.currentTimeMillis();
		long compareTime;
		if (mLastInteractionTime != Long.MIN_VALUE) {
			compareTime = (mLastInteractionTime > mStartTime) ? mLastInteractionTime:mStartTime;
		} else {
			compareTime = mStartTime;
			
		}
		long elapsedTime = currentTime - compareTime;
		hasTimedOut = (elapsedTime > this.mTimeout) ? true:false;
		//reset
		mLastInteractionTime = Long.MIN_VALUE;
		mStartTime = System.currentTimeMillis();
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.APPLICATION.name(),
				"ending hasTimeOut ", this.getClass().getSimpleName());
		return hasTimedOut;
	}
	
	/**
	 * Set time out from properties
	 */
	@SuppressLint("UseValueOf")
	private void setTimeout() {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.APPLICATION.name(),
				"starting setTimeout ", this.getClass().getSimpleName());
		String strValue = ResourceBundleHelper.getSingletonObject().getValue(ResourceBundleHelper.TIMEOUT);
		mTimeout = new Long(strValue).longValue();
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.APPLICATION.name(),
				"ending setTimeout ", this.getClass().getSimpleName());
	}

	private class ScreenReceiver extends BroadcastReceiver {

		protected ScreenReceiver() {
			// register receiver that handles screen on and screen off logic
			IntentFilter filter = new IntentFilter();
			filter.addAction(Intent.ACTION_SCREEN_ON);
			filter.addAction(Intent.ACTION_SCREEN_OFF);
			registerReceiver(this, filter);
		}

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
			} else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
			}
		}
	}

	
	public void stopTimer() {
		mStopThread = true;
	}
	public void restartTimer() {
		mStopThread = false;
	}
	public void startUserInactivityDetectThread() {
		
		InActiveThread thread = new InActiveThread();
		thread.start();
		
		
	}
	private class InActiveThread extends Thread {
		@Override
		 public void run() {
			 while(!mStopThread) {
				 try {
					Thread.sleep(15000);
					boolean timeOut = hasTimeOut();
					if(timeOut )
		              {
		                //...... means USER has been INACTIVE over a period of
		                // and you do your stuff like log the user out 
						Intent intent = new Intent(BankApplication.this,SecurityControllerActivity.class);
						//close all on top
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						BankApplication.this.startActivity(intent);
						
		              }
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} // checks every 15sec for inactivity
			 }
		 }
	}

	
	
	public long getLastInteractionTime() {
		return mLastInteractionTime;
	}

	public void setLastInteractionTime(long lastInteractionTime) {
		mLastInteractionTime = lastInteractionTime;
	}

	public String getErrorMesasge() {
		return errorMesasge;
	}

	public void setErrorMesasge(String errorMesasge) {
		this.errorMesasge = errorMesasge;
	}

	public static final String GD_ACTION_BAR_TITLE = "greendroid.app.ActionBarActivity.GD_ACTION_BAR_TITLE";

	@Override
	public Class<?> getHomeActivityClass() {
        return SecurityControllerActivity.class;
        /*
		if (BankSingleton.getSingletonObject().hasLogon()) {
		    return SecurityControllerActivity.class;
		} else {
			 return LoginActivity.class;
		}
		*/
	}



}
