package secu.org.application;




import secu.org.bank.Bank;
import secu.org.mail.Mail;

public class BankSingleton {
	private static BankSingleton singletonObject;
	private Bank bank;
    private Mail mail;

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    private String registrationId;

    public Mail getMail() {
        return mail;
    }

    public void setMail(Mail mail) {
        this.mail = mail;
    }


	public Bank getBank() {
		return bank;
	}
	public void setBank(Bank bank) {
		this.bank = bank;
	}
	/** A private Constructor prevents any other class from instantiating. */
	private BankSingleton(){
		
	}
	public Object clone()throws CloneNotSupportedException
	{
	    throw new CloneNotSupportedException(); 
	}
	
	public static synchronized BankSingleton getSingletonObject()
	{
	    if (singletonObject == null){
	    	singletonObject = new BankSingleton();
	    }
	    return singletonObject;
	}
	/**
	 * logoff
	 * @return
	 */
	public boolean hasLogon() {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.APPLICATION.name(),
				"starting hasLogon ", this.getClass().getSimpleName());
		
		if ( getBank() != null && getBank().isValidLogon()) {
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.APPLICATION.name(),
					"hasLogon true ", this.getClass().getSimpleName());
			return true;
		} else {
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.APPLICATION.name(),
					"hasLogon false", this.getClass().getSimpleName());
			return false;
		}
		
	}
	/**
	 * Logoff 
	 */
	public void logOff() {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.APPLICATION.name(),
				"starting logOff ", this.getClass().getSimpleName());
		setBank(null);
		
		
	}

}
