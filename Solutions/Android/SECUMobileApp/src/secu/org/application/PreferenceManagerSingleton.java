package secu.org.application;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import secu.org.activity.MainActivity;

public class PreferenceManagerSingleton {
	public static final String TRANSACTIONDAYS = "prefTransactiondays";
    public static final String NOTIFICATION = "prefNotification";
    public static final String GOOGLE_REGISTRATION = "googleRegistration";
	public static final String USERNAME = "prefUsername";
	public static final String NOTFOUND = "NOTFOUND";
	public static final String USERPASSWORD = "prefUserpassword";
	private static String SHARED_PREFERENCES_NAME = "SHARED_PREFERENCES_NAME";
	private static PreferenceManagerSingleton singletonObject;
	private SharedPreferences sharedPreferences = null;
	private Context mContext;
    /**
     * has security alerts turn on
     * @return
     */
	public boolean hasSecurityAlertsTurnedOn() {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.SECURITY.name(),
				"starting hasSecurityAlertsTurnedOn", this.getClass().getSimpleName());
		boolean hasSecurity = this.sharedPreferences.getBoolean(NOTIFICATION, false);
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.SECURITY.name(),
				"ending hasSecurityAlertsTurnedOn", this.getClass().getSimpleName());
		return hasSecurity;
	}

    /**
     * has notification
     * @return
     */
    public boolean hasNotification(Context context) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SECURITY.name(),
                "starting hasNotification", this.getClass().getSimpleName());
      boolean hasSecurity =secu.org.application.PreferenceManager.getBooleanFromSP(NOTIFICATION,context);
      //boolean hasSecurity = this.sharedPreferences.getBoolean(NOTIFICATION, true);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SECURITY.name(),
                "ending hasNotification", this.getClass().getSimpleName());
        return hasSecurity;
    }

    /**
     * disable notification
     * @param context
     */
    public void disableNotification (Context context) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SECURITY.name(),
                "starting hasNotication", this.getClass().getSimpleName());
        secu.org.application.PreferenceManager.saveBooleanInSP(context,NOTIFICATION, false );
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SECURITY.name(),
                "ending hasNotication", this.getClass().getSimpleName());
    }

    public void enableGoogleRegistration(Context context) {
        secu.org.application.PreferenceManager.saveBooleanInSP(context,GOOGLE_REGISTRATION, true );
    }

    public void disableGoogleRegistration (Context context) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SECURITY.name(),
                "starting hasNotication", this.getClass().getSimpleName());
        secu.org.application.PreferenceManager.saveBooleanInSP(context,GOOGLE_REGISTRATION, false );
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SECURITY.name(),
                "ending hasNotication", this.getClass().getSimpleName());
    }

    public boolean hasGoogleRegistration(Context context) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SECURITY.name(),
                "starting hasNotification", this.getClass().getSimpleName());
        boolean hasSecurity =secu.org.application.PreferenceManager.getBooleanFromSP(GOOGLE_REGISTRATION,context);
        //boolean hasSecurity = this.sharedPreferences.getBoolean(NOTIFICATION, true);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.SECURITY.name(),
                "ending hasNotification", this.getClass().getSimpleName());
        return hasSecurity;
    }


    /**
     * enable notification
     * @param context
     */
    public void enableNotification(Context context) {
        secu.org.application.PreferenceManager.saveBooleanInSP(context,NOTIFICATION, true );
    }
	/**
     * get transaction days
     * @return
     */
	public int getTransactionDays() {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"starting getTransactionDays", this.getClass().getSimpleName());
		String daysStr = this.sharedPreferences
				.getString(TRANSACTIONDAYS, "30");
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"getTransactionDays [" + daysStr + "]", this
						.getClass().getSimpleName());
		int days = Integer.parseInt(daysStr);
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"ending getTransactionDays ", this.getClass().getSimpleName());
		return days;
	}

	public SharedPreferences getSharedPreferences() {
		return sharedPreferences;
	}

	public void setSharedPreferences(Context context) {
		this.sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);

	}

	private PreferenceManagerSingleton() {

	}

	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

	public static synchronized PreferenceManagerSingleton getSingletonObject() {
		if (singletonObject == null) {
			singletonObject = new PreferenceManagerSingleton();
		}
		return singletonObject;
	}
}
