package secu.org.application;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Error implements Serializable{
	private String errorMessage;
	private String className;
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getClassName() {
		return className;
	}
	public Error(String errorMessage, String className) {
		super();
		this.errorMessage = errorMessage;
		this.className = className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getInfo() {
		StringBuilder builder = new StringBuilder();
		builder.append(className);
		Date date = new Date();
		builder.append(" ");
		builder.append(date.toString());
		return builder.toString();
		
		
	}

}
