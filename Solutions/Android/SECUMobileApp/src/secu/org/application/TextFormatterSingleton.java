package secu.org.application;



import android.app.Application;

public class TextFormatterSingleton {
	private static TextFormatterSingleton singletonObject;
	private String accountMask;
	
	/** A private Constructor prevents any other class from instantiating. */
	private TextFormatterSingleton(){
		
	}
	private TextFormatterSingleton(Application application){
		accountMask = application.getResources().getString(secu.org.activity.R.string.accountmask);
	
	}
	//
	public Object clone()throws CloneNotSupportedException
	{
	    throw new CloneNotSupportedException(); 
	}
	
	public static synchronized TextFormatterSingleton getSingletonObject()
	{
	    if (singletonObject == null){
	    	singletonObject = new TextFormatterSingleton();
	    }
	    return singletonObject;
	}
	
	public String maskAccountNumber(String accountNumber) {
		 
         
		  //String paddedValue = String.format("%010d%n", accountLong);
        String  paddedValue = String.format("%-10s", accountNumber).replace(' ', '0');
      
         int startPosition = (paddedValue.length()) - (accountMask.length());
         String sub = paddedValue.substring(startPosition, paddedValue.length() );

         String rep = paddedValue.replace(sub, accountMask);
         System.out.println(rep);

       
		return rep;
	}
	public static synchronized TextFormatterSingleton getSingletonObject(Application application)
	{
	    if (singletonObject == null){
	    	singletonObject = new TextFormatterSingleton(application);
	    }
	    ;
	    return singletonObject;
	}
	
}
