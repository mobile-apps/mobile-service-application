package secu.org.locator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DrivingLeg implements Serializable{
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private String distance;
private String duration;
private String startAddress;
private List<DrivingStep> mDrivingStepList = new ArrayList<DrivingStep>();
public List<DrivingStep> getmDrivingStepList() {
	return mDrivingStepList;
}

public void setmDrivingStepList(List<DrivingStep> mDrivingStepList) {
	this.mDrivingStepList = mDrivingStepList;
}

public DrivingLeg(String distance, String duration, String startAddress,
		String endAddress) {
	super();
	this.distance = distance;
	this.duration = duration;
	this.startAddress = startAddress;
	this.endAddress = endAddress;
}

private String endAddress;
public static final String DISTANCE="distance";
public static final String DURATION="duration";
public static final String END_ADDRESS="end_address";
public static final String START_ADDRESS="start_address";
public static final String SESSION_NAME = "DrivingLeg";

	public static final String TEXT = "text";

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getStartAddress() {
		return startAddress;
	}

	public void setStartAddress(String startAddress) {
		this.startAddress = startAddress;
	}

	public String getEndAddress() {
		return endAddress;
	}

	public void setEndAddress(String endAddress) {
		this.endAddress = endAddress;
	}

}
