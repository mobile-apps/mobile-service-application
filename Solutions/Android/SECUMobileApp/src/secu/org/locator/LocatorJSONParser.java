package secu.org.locator;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

public class LocatorJSONParser {

	static InputStream is = null;
	static JSONObject jObj = null;
	static String json = "";
	secu.org.locator.Location mLocation;
	// constructor
	public LocatorJSONParser(secu.org.locator.Location location) {
		mLocation = location;

	}

	public JSONObject getJSONFromUrl() throws Exception {
        
		// Making HTTP request
		
			// defaultHttpClient
			HttpParams params = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(params, 10000);
	        HttpConnectionParams.setSoTimeout(params, 10000);
	        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
	        HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
	        HttpProtocolParams.setUseExpectContinue(params, true);
			DefaultHttpClient httpClient = new DefaultHttpClient(params);
			HttpPost httpPost = new HttpPost(mLocation.getUrl());
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("LocationType", "4"));
	        nameValuePairs.add(new BasicNameValuePair("Lat", mLocation.getLatitudeString()));
	        nameValuePairs.add(new BasicNameValuePair("Lng", mLocation.getLongitudeString()));
	        nameValuePairs.add(new BasicNameValuePair("NumOfLocations", mLocation.getNumOfLocationsStr()));
	        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		       
				
			
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();			

		
		
		
			//BufferedReader reader = new BufferedReader(new InputStreamReader(
					//is, "iso-8859-1"), 8);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "utf-8"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
		
		// try parse the string to a JSON object
	
			jObj = new JSONObject(json.substring(json.indexOf("{"), json.lastIndexOf("}") + 1));
			//jObj = new JSONObject(json);
	

		// return JSON String
		return jObj;

	}
}
