package secu.org.locator;

public class Location {
	public Location(double latitude, double longitude, int numOfLocations
			, String url, String startingAddress) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.numOfLocations = numOfLocations;
		this.url = url;
		this.startingAddress = startingAddress;
	}
	private double latitude; 
	private double longitude;
	private int numOfLocations;
	private String startingAddress;
	public String getStartingAddress() {
		return startingAddress;
	}
	public void setStartingAddress(String startingAddress) {
		this.startingAddress = startingAddress;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	private String locationType = "4";
	private String url;
	
	public String getLatitudeString() {
		 String latitudeStr = Double.toString(latitude);
		 return latitudeStr;
	}
	public String getNumOfLocationsStr() {
		return Integer.toString(numOfLocations);
	}
	public String getLongitudeString() {
		 String longitudeStr = Double.toString(longitude);
		 return longitudeStr;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public int getNumOfLocations() {
		return numOfLocations;
	}
	public void setNumOfLocations(int numOfLocations) {
		this.numOfLocations = numOfLocations;
	}
	public String getLocationType() {
		return locationType;
	}
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}
}
