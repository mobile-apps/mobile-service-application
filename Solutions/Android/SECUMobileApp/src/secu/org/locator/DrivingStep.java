package secu.org.locator;

import java.io.Serializable;

public class DrivingStep implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DrivingStep(String distance, String duration, String htmlinstructions) {
		super();
		this.distance = distance;
		this.duration = duration;
		this.htmlinstructions = htmlinstructions;
	}
	public static final String DISTANCE="distance";
	public static final String DURATION="duration";
	public static final String HTML_INSTRUCTIONS="html_instructions";
	public static final String TEXT = "text";

	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getHtmlinstructions() {
		return htmlinstructions;
	}
	public void setHtmlinstructions(String htmlinstructions) {
		this.htmlinstructions = htmlinstructions;
	}
	private String distance;
	private String duration;
	private String htmlinstructions;
}
