package secu.org.locator;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import secu.org.application.BankApplication;

public class DrivingDirectionJSONParser {

	static InputStream is = null;
	static JSONObject jObj = null;

	secu.org.locator.LocatorResult mLocatorResult;
	
	secu.org.locator.Location mLocation;
	// constructor
	public DrivingDirectionJSONParser(secu.org.locator.LocatorResult locatorResult, secu.org.locator.Location location) {
		mLocatorResult = locatorResult;
		
		mLocation = location;
	}

	public JSONObject getJSONFromUrl() {
		String SetServerString = "";
		// Making HTTP request
		try {
			// defaultHttpClient

            String startingAddress    = URLEncoder.encode(mLocation.getStartingAddress(), "UTF-8");
            String endingAddress  = URLEncoder.encode(mLocatorResult.getStreetAddress(), "UTF-8");
            
            String language = BankApplication.getLanguage();
            StringBuilder builder = new StringBuilder();
			builder.append(mLocatorResult.getGoogleDrivingDirectionURL());
			builder.append("origin=");
			builder.append(startingAddress);
			builder.append("&destination=");
			builder.append(endingAddress);
			builder.append("&sensor=false");
            builder.append("&language=");
            builder.append(language);

            HttpGet httpget = new HttpGet(builder.toString());
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            HttpClient Client = new DefaultHttpClient();
            SetServerString = Client.execute(httpget, responseHandler);
			
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
		

		// try parse the string to a JSON object
		try {
			jObj = new JSONObject(SetServerString);
			//jObj = new JSONObject(json);
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}

		// return JSON String
		return jObj;

	}
}
