package secu.org.locator;

/**
 * @author s16715d
 *
 */
public class LocatorResult {
	public final static String STATE = "State";
	public final static String STREET = "Street1";
	public final static String CITY = "City";
	public final static String ZIP = "Zip";
	public final static String DISTANCE = "Distance";
	public final static String TYPE = "Type";
	public final static String ATM24HOURS = "ATM24Hours";
	public final static String MAILINGADDRESS = "MailingAddress";
	public final static String TYPEID = "TypeID";
	public final static String EMAIL = "Email";
	public final static String LOCATIONAME = "LocationName";
	public final static String TELEPONENUMBERS = "TelephoneNumbers";
	public final static String LOCATIONID = "LocationID";

	public LocatorResult(String distance, String type, String atm24hours,
			String mailingAddress, String typeID, String email,
			String locationName, String telephoneNumbers, String lLocationID,
			String latitude, String longitude, String streetAddress) {
		super();
		this.distance = distance;
		this.type = type;
		atm24HOURS = atm24hours;
		this.mailingAddress = mailingAddress;
		this.typeID = typeID;
		this.email = email;
		this.locationName = locationName;
		this.telephoneNumbers = telephoneNumbers;
		this.lLocationID = lLocationID;
		this.latitude = latitude;
		this.longitude = longitude;
		this.streetAddress = streetAddress;
	}
    private String googleDrivingDirectionURL;
	public String getGoogleDrivingDirectionURL() {
		return googleDrivingDirectionURL;
	}

	public void setGoogleDrivingDirectionURL(String googleDrivingDirectionURL) {
		this.googleDrivingDirectionURL = googleDrivingDirectionURL;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAtm24HOURS() {
		return atm24HOURS;
	}

	public void setAtm24HOURS(String atm24hours) {
		atm24HOURS = atm24hours;
	}

	public String getMailingAddress() {
		return mailingAddress;
	}

	public void setMailingAddress(String mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	public String getTypeID() {
		return typeID;
	}

	public void setTypeID(String typeID) {
		this.typeID = typeID;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getTelephoneNumbers() {
		return telephoneNumbers;
	}

	public void setTelephoneNumbers(String telephoneNumbers) {
		this.telephoneNumbers = telephoneNumbers;
	}

	public String getlLocationID() {
		return lLocationID;
	}

	public void setlLocationID(String lLocationID) {
		this.lLocationID = lLocationID;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	@Override
	public String toString() {
		return this.locationName;
	}
	public boolean isATM() {
		if (this.type.equalsIgnoreCase("ATM")) {
			return true;
		} else {
			return false;
		}
	}
	public boolean isBranchWithATM() {
		if (this.type.equalsIgnoreCase("Branch with ATM")) {
			return true;
		} else {
			return false;
		}
	}
	public boolean is24Hours()
	{
		if (atm24HOURS.equalsIgnoreCase("y")) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * @return
	 */
	public boolean isBranchWithOutATM() {
		if (!isBranchWithATM() && !isATM() ) {
			return true;
		} else {
			return false;
		}
	}
    public String longDescription() {
    	StringBuilder builder = new StringBuilder();
    	builder.append("ATM Options: ");
    	builder.append("Accepts Deposits-");
    	builder.append(this.isATM());
    	builder.append("24 Hour Service- ");
    	builder.append(atm24HOURS);
    	return builder.toString();
    }
	public final static String LATITUDE = "Latitude";
	public final static String LONGITUDE = "Longitude";
	public final static String STREETADDRESS = "StreetAddress";

	private String distance;
	private String type;
	private String atm24HOURS;
	private String mailingAddress;
	private String typeID;
	private String email;
	private String locationName;
	private String telephoneNumbers;
	private String lLocationID;
	private String latitude;
	private String longitude;
	private String streetAddress;

}
