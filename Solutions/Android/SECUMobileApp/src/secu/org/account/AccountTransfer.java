package secu.org.account;

import java.io.Serializable;

import secu.org.bank.Bank;
@SuppressWarnings("serial")
public class AccountTransfer implements Serializable {
    private String amount;
    private Account toAccount;
    private Account fromAccount;
    private Bank bank;
    private String dateTransfer;
    private String status = "1";
    private String message;
    private int toAccountIndex;
    private int fromAccountIndex;
    
    public int getToAccountIndex() {
		return toAccountIndex;
	}
	public void setToAccountIndex(int toAccountIndex) {
		this.toAccountIndex = toAccountIndex;
	}
	public int getFromAccountIndex() {
		return fromAccountIndex;
	}
	public void setFromAccountIndex(int fromAccountIndex) {
		this.fromAccountIndex = fromAccountIndex;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	private String confirmationNumber="";
    
    public String getConfirmationNumber() {
		return confirmationNumber;
	}
	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}
	public boolean hasBeenTransfer() {
    	if (status.equalsIgnoreCase("0")) {
    		return true;
    	} else {
    		return false;
    	}
    }
	public String getDateTransfer() {
		return dateTransfer;
	}
	public void setDateTransfer(String dateTransfer) {
		this.dateTransfer = dateTransfer;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Bank getBank() {
		return bank;
	}
	public void setBank(Bank bank) {
		this.bank = bank;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public Account getToAccount() {
		return toAccount;
	}
	public void setToAccount(Account toAccount) {
		this.toAccount = toAccount;
	}
	public Account getFromAccount() {
		return fromAccount;
	}
	public void setFromAccount(Account fromAccount) {
		this.fromAccount = fromAccount;
	}
}
