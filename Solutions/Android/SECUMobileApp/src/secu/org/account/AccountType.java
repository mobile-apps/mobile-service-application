package secu.org.account;

public enum AccountType {
	UNKNOWN,CREDITLINE,CREDITCARD,SAVINGS,CHECKING, MONEYMRKT
}
