package secu.org.account;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import secu.org.application.TextFormatterSingleton;

@SuppressWarnings("serial") 
public class Account implements Serializable {
	
	public Account() {
		
	}
	private List<AccountTransaction> accountTransactionList = new ArrayList<AccountTransaction>();
	
	public AccountType getAccountTypeEnum(){
		if (accountType.equalsIgnoreCase(AccountType.CHECKING.toString())) {
			return AccountType.CHECKING;
		}
		if (accountType.equalsIgnoreCase(AccountType.MONEYMRKT.toString())) {
			return AccountType.MONEYMRKT;
		}
			
		if (accountType.equalsIgnoreCase(AccountType.SAVINGS.toString())) {
			return AccountType.SAVINGS;
		}
		if (accountType.equalsIgnoreCase(AccountType.CREDITCARD.toString())) {
			return AccountType.CREDITCARD;
		}	
		if (accountType.equalsIgnoreCase(AccountType.CREDITLINE.toString())) {
			return AccountType.CREDITLINE;
		}	
		
	     return AccountType.UNKNOWN;
		
		
	}
	public Account (String accountType, String accountNumber){
		this.accountNumber = accountNumber;
		this.accountType = accountType;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	private String accountType;
	private String accountNumber;
	private String accountBalance = "";
	private String dateOfBalance;

	public String getDateOfBalance() {
		return dateOfBalance;
	}
	public void setDateOfBalance(String dateOfBalance) {
		this.dateOfBalance = dateOfBalance;
	}
	public List<AccountTransaction> getAccountTransactionList() {
		return accountTransactionList;
	}
	public void setAccountTransactionList(
			List<AccountTransaction> accountTransactionList) {
		this.accountTransactionList = accountTransactionList;
	}
	public String getRawAccountBalance() {
		return accountBalance;
	}
	public String getAccountBalance() {
		return "$" + accountBalance;
	}
	public void setAccountBalance(String accountBalance) {
		this.accountBalance = accountBalance;
	}
	
	public String getDisplayFormattedDateofBalance() {
		String formattedDate = secu.org.util.DateManager.displayDate(dateOfBalance);
		return formattedDate;
	}
	
	public String getMasked() {
		String formmatted = TextFormatterSingleton.getSingletonObject().maskAccountNumber(accountNumber);
		 return formmatted;
	}
	
	public String compositeAccountInfoDisplay() {
		StringBuilder sb = new StringBuilder();
		sb.append("(" + getMasked() + ")");
		sb.append(" ");
		sb.append(getAccountBalance());
		sb.append(" ");
		sb.append(this.getDisplayFormattedDateofBalance());
		return sb.toString();
				
	}
	

}
