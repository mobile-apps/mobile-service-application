package secu.org.account;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import secu.org.application.PreferenceManagerSingleton;
import secu.org.util.DateManager;

@SuppressWarnings("serial") 
public class AccountTransaction implements Serializable, Comparable<AccountTransaction> {
	
	private String amount;
    private String name;
    private String memo;
  
    private String datePosted;
    private String accountTransactionType;
	public String getAmount() {
		return amount;
	}
	public String getDisplayAmount() {
		return "$" + amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getDatePosted() {
		return datePosted;
	}
	public void setDatePosted(String datePosted) {
		this.datePosted = datePosted;
	}
	public String getAccountTransactionType() {
		return accountTransactionType;
	}
	public void setAccountTransactionType(
			String accountTransactionType) {
		this.accountTransactionType = accountTransactionType;
	}
	public String getDisplayFormattedDatePosted() {
		String formattedDate = secu.org.util.DateManager.displayDate(datePosted);
		return formattedDate;
	}
	
	public boolean shouldInclude(int transactionDays) {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"starting shouldInclude", this.getClass().getSimpleName());
		Date date = DateManager.convertToDate(datePosted);
		Calendar currentDate = Calendar.getInstance(); //Get the current date
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyyy-mm-dd");
		
		long days = DateManager.computeDaysDifference(date, currentDate.getTime());
		
		if (days <= transactionDays) {
			return true;
		} else {
		return false;
		}
	}
	/**
	 * 
	 * @return
	 */
	public Date getComputedDate() {
		Date date = DateManager.convertToDate(datePosted);
		return date;
	}
	@Override
	public int compareTo(AccountTransaction myObject) {
		// TODO Auto-generated method stub
		return myObject.getComputedDate().compareTo(getComputedDate());

	}

}
