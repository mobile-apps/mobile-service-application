package secu.org.listener;

import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.PopupMenu;
import android.widget.Toast;

import secu.org.activity.AboutActivity;
import secu.org.activity.AccountListActivity;
import secu.org.activity.ContactActivity;
import secu.org.activity.LocatorActivity;
import secu.org.activity.LoginActivity;
import secu.org.activity.MessageCenterActivity;
import secu.org.activity.MessagesActivity;
import secu.org.activity.MoveMoneyActivity;
import secu.org.activity.PreferenceMainActivity;
import secu.org.activity.R;
import secu.org.activity.RatesActivity;
import secu.org.activity.TestActivity;
import secu.org.application.BankApplication;
import secu.org.application.BankSingleton;
import secu.org.application.ResourceEnumeration;

/**
 * Created by s16715d on 1/27/14.
 */
public class MenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

   private android.content.Context mContext;

    public MenuItemClickListener(android.content.Context context ) {
        mContext = context;
    }
    public boolean onMenuItemClick(MenuItem item){
      // Toast.makeText(mContext,"You Clicked : " + item.getTitle() + " " + item.getItemId(), Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
            case R.id.menu_accounts:
                Intent accountSummaryIntent = new Intent(mContext, AccountListActivity.class);
                accountSummaryIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, ResourceEnumeration.ACCOUNT_SUMMARY_ACTIVITY_TITLE.getValue());
                mContext.startActivity(accountSummaryIntent);
                break;
            case R.id.menu_about:
                Intent aboutIntent = new Intent(mContext, AboutActivity.class);
                aboutIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, ResourceEnumeration.ABOUT_ACTIVITY_TITLE.getValue());
                mContext.startActivity(aboutIntent);
                break;
            case  R.id.menu_contact_us: //contact
                Intent contactIntent = new Intent(mContext, ContactActivity.class);
                //Intent contactIntent = new Intent(mContext, TestActivity.class);
                String test = ResourceEnumeration.CONTACT_US_ACTIVITY_TITLE.getValue();
                contactIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, ResourceEnumeration.CONTACT_US_ACTIVITY_TITLE.getValue());
                mContext.startActivity(contactIntent);
                //closePreviouslyOpenedApplications();
                break;
            case R.id.menu_locator: //locator
                Intent locatorIntent = new Intent(mContext, LocatorActivity.class);
                locatorIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, ResourceEnumeration.ATM_BRANCH_LOCATOR_ACTIVITY_TITLE.getValue());
                //contactIntent.addFlags(Intent.);
                mContext.startActivity(locatorIntent);
                break;

            case R.id.menu_notification: //notification
                Intent notificationIntent = new Intent(mContext, MessagesActivity.class);
                notificationIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, ResourceEnumeration.NOTIFICATIONS_ACTIVITY_TITLE.getValue());
                //contactIntent.addFlags(Intent.);
                mContext.startActivity(notificationIntent);
                break;


            case R.id.menu_messages: //notification
                Intent messageIntent = new Intent(mContext, MessageCenterActivity.class);
                messageIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, ResourceEnumeration.MESSAGE_CENTER_ACTIVITY_TITLE.getValue());
                //contactIntent.addFlags(Intent.);
                mContext.startActivity(messageIntent);
                break;
            case R.id.menu_preference: //preference
                Intent preferenceMainActivityIntent = new Intent(mContext, PreferenceMainActivity.class);
                preferenceMainActivityIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, ResourceEnumeration.MY_PREFERENCE_ACTIVITY_TITLE.getValue());
                mContext.startActivity(preferenceMainActivityIntent);
                //closePreviouslyOpenedApplications();
                break;
            case R.id.menu_rates://rates
                Intent ratesIntent = new Intent(mContext, RatesActivity.class);
                ratesIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, ResourceEnumeration.RATES_AND_FEES_ACTIVITY_TITLE.getValue());

                mContext.startActivity(ratesIntent);
                break;

            case R.id.menu_transfer: //notification
                Intent transferIntent = new Intent(mContext, MoveMoneyActivity.class);
                transferIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, ResourceEnumeration.TRANSFER_MONEY_ACTIVITY_TITLE);

                //contactIntent.addFlags(Intent.);
                mContext.startActivity(transferIntent);
                break;
            case R.id.menu_exit:
                Intent logonIntent = new Intent(mContext.getApplicationContext(), LoginActivity.class);
                BankSingleton.getSingletonObject().logOff();
                logonIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mContext.startActivity(logonIntent);
                break;

        }
      return true;
    }
}
