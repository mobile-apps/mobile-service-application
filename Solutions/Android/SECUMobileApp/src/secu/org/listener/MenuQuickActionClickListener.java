package secu.org.listener;

import android.app.Activity;
import android.content.Intent;

import greendroid.widget.QuickActionWidget;
import greendroid.widget.QuickActionWidget.OnQuickActionClickListener;
import secu.org.activity.AboutActivity;
import secu.org.activity.AccountListActivity;
import secu.org.activity.ContactActivity;
import secu.org.activity.LocatorActivity;
import secu.org.activity.LoginActivity;
import secu.org.activity.MessageCenterActivity;
import secu.org.activity.MessagesActivity;
import secu.org.activity.MoveMoneyActivity;
import secu.org.activity.PreferenceMainActivity;
import secu.org.activity.RatesActivity;
import secu.org.application.BankApplication;
import secu.org.application.BankSingleton;

public class MenuQuickActionClickListener implements OnQuickActionClickListener {
    private Activity mGDListActivity;


    public MenuQuickActionClickListener(Activity activity) {
        mGDListActivity = activity;

    }

    public void onQuickActionClickedOustide(QuickActionWidget widget, int position) {
        //closePreviouslyOpenedApplications();
        //Intent logonIntent = new Intent(mGDListActivity.getApplicationContext(),LoginActivity.class);
        switch (position) {

            case 0: //about
                //close all on top
                Intent aboutIntent = new Intent(mGDListActivity, AboutActivity.class);
                aboutIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, "About");
                mGDListActivity.startActivity(aboutIntent);
                //closePreviouslyOpenedApplications();
                break;

            case 1: //contact
                Intent contactIntent = new Intent(mGDListActivity, ContactActivity.class);
                contactIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, "Contact US");
                mGDListActivity.startActivity(contactIntent);
                //closePreviouslyOpenedApplications();
                break;

            case 2: //locator
                Intent locatorIntent = new Intent(mGDListActivity, LocatorActivity.class);
                locatorIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, "ATM/Branch Locator");
                //contactIntent.addFlags(Intent.);
                mGDListActivity.startActivity(locatorIntent);
                break;
            case 3: //notification
                Intent notificationIntent = new Intent(mGDListActivity, MessagesActivity.class);
                notificationIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, "Notifications");
                //contactIntent.addFlags(Intent.);
                mGDListActivity.startActivity(notificationIntent);
                break;
            case 4: //preference
                Intent preferenceMainActivityIntent = new Intent(mGDListActivity, PreferenceMainActivity.class);
                preferenceMainActivityIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, "My Preference");
                mGDListActivity.startActivity(preferenceMainActivityIntent);
                //closePreviouslyOpenedApplications();
                break;
            case 5://rates
                Intent ratesIntent = new Intent(mGDListActivity, RatesActivity.class);
                ratesIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, "Rates and Fees");

                mGDListActivity.startActivity(ratesIntent);
                break;


            case 6: //exit
                //close all on top
                Intent logonIntent = new Intent(mGDListActivity.getApplicationContext(), LoginActivity.class);
                BankSingleton.getSingletonObject().logOff();
                logonIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mGDListActivity.startActivity(logonIntent);
                //closePreviouslyOpenedApplications();
                break;
        }


    }

    /**
     * open menu selection
     */
    public void onQuickActionClicked(QuickActionWidget widget, int position) {
        //closePreviouslyOpenedApplications();
        boolean hasLogon = BankSingleton.getSingletonObject().hasLogon();
        if (!hasLogon) {
            onQuickActionClickedOustide(widget, position);
            return;
        }
        Intent logonIntent = new Intent(mGDListActivity.getApplicationContext(), LoginActivity.class);
        switch (position) {

            case 0: //about
                //close all on top
                Intent aboutIntent = new Intent(mGDListActivity, AboutActivity.class);
                aboutIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, "About");
                mGDListActivity.startActivity(aboutIntent);
                //closePreviouslyOpenedApplications();
                break;

            case 1: //account
                if (hasLogon) {
                    Intent accountSummaryIntent = new Intent(mGDListActivity, AccountListActivity.class);
                    accountSummaryIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, "Account Summary");

                    mGDListActivity.startActivity(accountSummaryIntent);
                } else {
                    logonIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mGDListActivity.startActivity(logonIntent);
                }
                break;
            case 2: //contact
                Intent contactIntent = new Intent(mGDListActivity, ContactActivity.class);
                contactIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, "Contact US");
                mGDListActivity.startActivity(contactIntent);
                //closePreviouslyOpenedApplications();
                break;
            case 3: //locator
                Intent locatorIntent = new Intent(mGDListActivity, LocatorActivity.class);
                locatorIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, "ATM/Branch Locator");

                mGDListActivity.startActivity(locatorIntent);
                break;
            case 4: //notification
                Intent notificationIntent = new Intent(mGDListActivity, MessagesActivity.class);
                notificationIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, "Notifications");
                //contactIntent.addFlags(Intent.);
                mGDListActivity.startActivity(notificationIntent);
                break;
            case 5: //message center
                if (hasLogon) {
                    Intent messageIntent = new Intent(mGDListActivity, MessageCenterActivity.class);
                    messageIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, "DatabaseMessageRow Center");

                    mGDListActivity.startActivity(messageIntent);
                } else {
                    logonIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mGDListActivity.startActivity(logonIntent);
                }
                break;
            case 6: //preference
                Intent preferenceMainActivityIntent = new Intent(mGDListActivity, PreferenceMainActivity.class);
                preferenceMainActivityIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, "My Preference");
                mGDListActivity.startActivity(preferenceMainActivityIntent);
                //closePreviouslyOpenedApplications();
                break;
            case 7://rates
                Intent ratesIntent = new Intent(mGDListActivity, RatesActivity.class);
                ratesIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, "Rates");

                mGDListActivity.startActivity(ratesIntent);
                break;
            case 8://move money
                if (hasLogon) {
                    Intent transferIntent = new Intent(mGDListActivity, MoveMoneyActivity.class);
                    transferIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE, "Transfer Money");

                    mGDListActivity.startActivity(transferIntent);
                } else {
                    logonIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mGDListActivity.startActivity(logonIntent);
                }
                break;
            case 9: //exit
                //close all on top
                BankSingleton.getSingletonObject().logOff();
                logonIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mGDListActivity.startActivity(logonIntent);
                //closePreviouslyOpenedApplications();
                break;
        }


    }

}
