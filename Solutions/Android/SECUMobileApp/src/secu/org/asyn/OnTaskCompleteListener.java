package secu.org.asyn;

import secu.org.task.AbstractTask;

public interface OnTaskCompleteListener {
    // Notifies about task completeness
    void onTaskComplete(AbstractTask task);
}