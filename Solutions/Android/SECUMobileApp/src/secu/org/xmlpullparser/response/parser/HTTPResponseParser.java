package secu.org.xmlpullparser.response.parser;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xml.sax.helpers.DefaultHandler;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import secu.org.account.AccountTransaction;
import secu.org.application.BankSingleton;
import secu.org.application.PreferenceManagerSingleton;
import secu.org.ofx.OFXOperator;

import android.util.Xml;

public class HTTPResponseParser extends DefaultHandler {
	private static HTTPResponseParser singletonObject;
	private StringReader mXmlReader;
	private XmlPullParser mParser = null;

	/** A private Constructor prevents any other class from instantiating. */
	private HTTPResponseParser() {
		mParser = Xml.newPullParser();

	}

	public static synchronized HTTPResponseParser getSingletonObject() {
		if (singletonObject == null) {
			singletonObject = new HTTPResponseParser();
		}
		return singletonObject;
	}

	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

	/**
	 * parse transactions
	 * @param xml
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	public List<AccountTransaction> parseAccountTransactions(String xml) throws XmlPullParserException, IOException{
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"starting parseAccountTransactions", this.getClass().getSimpleName());
		List<AccountTransaction> transactionList = new ArrayList<AccountTransaction>();
		mXmlReader = new StringReader(xml);
		mParser.setInput(mXmlReader);
		// The current event returned by the parser
		int eventType = mParser.getEventType();
		int transactionDays = PreferenceManagerSingleton.getSingletonObject().getTransactionDays();
		while (eventType != XmlPullParser.END_DOCUMENT  ) {
			String xmlNodeName;
			switch (eventType) {
			case XmlPullParser.START_DOCUMENT:
				break;
			case XmlPullParser.START_TAG:
				xmlNodeName = mParser.getName();
				if ("STMTTRN".equalsIgnoreCase(xmlNodeName)) {
					mParser.nextTag();
					xmlNodeName = mParser.getName();
					AccountTransaction transaction = new AccountTransaction();
					if ("TRNTYPE".equals(xmlNodeName)) {
						transaction.setAccountTransactionType(mParser.nextText());
						mParser.nextTag();
						xmlNodeName = mParser.getName();
						if ("DTPOSTED".equals(xmlNodeName)) {
							transaction.setDatePosted(mParser.nextText());
							mParser.nextTag();
							xmlNodeName = mParser.getName();	
							if ("TRNAMT".equals(xmlNodeName)) {
								transaction.setAmount(mParser.nextText());
								mParser.nextTag();
								xmlNodeName = mParser.getName();
								if ("FITID".equals(xmlNodeName)) {
									String test = mParser.nextText();
									mParser.nextTag();
									xmlNodeName = mParser.getName();
									if ("NAME".equals(xmlNodeName)) {
										transaction.setName(mParser.nextText());
										mParser.nextTag();
										xmlNodeName = mParser.getName();
										if ("MEMO".equals(xmlNodeName)) {
											transaction.setMemo(mParser.nextText());
											if (transaction.shouldInclude(transactionDays)) {
											transactionList.add(transaction);
											}
											break;
										} 
									} //end of if ("NAME".equals(xmlNodeName))
								}//end of if ("FITID".equals(xmlNodeName))

								
								
								
							}//end of if ("TRNAMT".equals(xmlNodeName))
						}//end of if ("DTPOSTED".equals(xmlNodeName))
						
						
					}//end of if ("TRNTYPE".equals(xmlNodeName))
					
				}//end if ("STMTTRN".equalsIgnoreCase(xmlNodeName))
				break;
			//end of switch
			}
			eventType = mParser.next();
		}//end if while
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"ending parseAccountTransactions", this.getClass().getSimpleName());
		return transactionList;
	}
	/**
	 * parse request for account balance
	 * 
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	public List<String> parseAccountListBalance(String xml)
			throws XmlPullParserException, IOException {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETACCOUNTS.name(),
				"starting  parseAccountListBalance", this.getClass().getSimpleName());
		final List<String> tagValues = new ArrayList<String>();
		mXmlReader = new StringReader(xml);
		mParser.setInput(mXmlReader);
		// The current event returned by the parser
		int eventType = mParser.getEventType();
		boolean continueWhile = true;
		while (eventType != XmlPullParser.END_DOCUMENT ||continueWhile ) {
			String xmlNodeName;
			switch (eventType) {
			case XmlPullParser.START_DOCUMENT:
				break;
			case XmlPullParser.START_TAG:
				xmlNodeName = mParser.getName();
				if ("AVAILBAL".equalsIgnoreCase(xmlNodeName)) {
					mParser.nextTag();
					xmlNodeName = mParser.getName();
					if ("BALAMT".equalsIgnoreCase(xmlNodeName)) {
						tagValues.add(mParser.nextText());
						mParser.nextTag();
						xmlNodeName = mParser.getName();
						if ("DTASOF".equalsIgnoreCase(xmlNodeName)) {
							tagValues.add(mParser.nextText());	
						}
						continueWhile = false;
						break;
					}
					
				}
			}//end of switch
			eventType = mParser.next();
		}
		// if (matcher.find()) {
		// int count = matcher.groupCount();

		// tagValues.add(matcher.group(1));
		// tagValues.add(matcher.group(2));
		// }
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETACCOUNTS.name(),
				"ending  parseAccountListBalance", this.getClass().getSimpleName());
		return tagValues;

	}

	/**
	 * Parse logon account response
	 * 
	 * @param xmlStream
	 */
	public secu.org.bank.Bank parseAccountList(String xml)
			throws XmlPullParserException, IOException {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETACCOUNTS.name(),
				"starting  parseAccountList", this.getClass().getSimpleName());
		secu.org.bank.Bank bank = new secu.org.bank.Bank();
		mXmlReader = new StringReader(xml);

		mParser.setInput(mXmlReader);
		// The current event returned by the parser
		int eventType = mParser.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			String xmlNodeName;
			switch (eventType) {
			case XmlPullParser.START_DOCUMENT:
				break;
			case XmlPullParser.START_TAG:
				xmlNodeName = mParser.getName();
				if ("SONRS".equalsIgnoreCase(xmlNodeName)) {
					mParser.nextTag();
					xmlNodeName = mParser.getName();
					if ("STATUS".equalsIgnoreCase(xmlNodeName)) {
						mParser.nextTag();
						xmlNodeName = mParser.getName();
						if ("CODE".equalsIgnoreCase(xmlNodeName)) {
							bank.setCode(mParser.nextText());
						}
					}
				}

				if ("ACCTID".equalsIgnoreCase(xmlNodeName)) {
					secu.org.account.Account account = new secu.org.account.Account();
					account.setAccountNumber(mParser.nextText());
					mParser.nextTag();
					xmlNodeName = mParser.getName();
					if ("ACCTTYPE".equalsIgnoreCase(xmlNodeName)) {
						account.setAccountType(mParser.nextText());
						bank.getAccountList().add(account);
					}
				}

				break;

			case XmlPullParser.END_TAG:
				xmlNodeName = mParser.getName();
				break;
			}// end of switch
			eventType = mParser.next();
		}// end of while (eventType != XmlPullParser.END_DOCUMENT)
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETACCOUNTS.name(),
				"ending  parseAccountList", this.getClass().getSimpleName());
		return bank;
	}

}
