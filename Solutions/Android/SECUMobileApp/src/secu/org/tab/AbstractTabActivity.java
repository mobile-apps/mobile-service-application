package secu.org.tab;

import greendroid.app.GDTabActivity;
import greendroid.widget.ActionBarItem;
import greendroid.widget.NormalActionBarItem;
import secu.org.activity.MenuHelper;
import secu.org.activity.R;
import android.content.Intent;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public abstract class AbstractTabActivity extends GDTabActivity{

	
	
	private MenuHelper mMenuHelper;
	
	public AbstractTabActivity () {
		 mMenuHelper = new MenuHelper(this);
	}

	protected void prepareQuickActionBar() {
		mMenuHelper.prepareQuickActionBar();
	
	}

	/**
	 * construct action grid
	 */
	protected void prepareQuickActionGrid() {
		Intent i = getIntent();
		this.mMenuHelper.prepareQuickActionGrid(i);
		//this.mMenuHelper.prepareQuickActionBar();
		//mMenuHelper.prepareQuickActionGrid(i);	
	}

	abstract public AbstractTabActivity getCurrentActivity();

	protected void initActionBar() {

		//addActionBarItem(Type.Locate, R.id.action_bar_locate);
		 addActionBarItem(getGDActionBar()
	                .newActionBarItem(NormalActionBarItem.class)
	                .setDrawable(R.drawable.ic_menu_moreoverflow)
	                .setContentDescription(R.string.gd_export), R.id.action_bar_export);
	
	}

	@Override
	public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
		mMenuHelper.onShowGrid(item.getItemView());
		//Toast.makeText(this, "Click-" + String.valueOf(position),
				//Toast.LENGTH_SHORT).show();
		return true;
	}

	
	
	
	 public TabHost getParentTabHost() {
		 @SuppressWarnings("deprecation")
		TabHost tabHost = getTabHost();
		 return tabHost;
	 }
	
	 public void addTab(TabSpec tabSpec) {
		 @SuppressWarnings("deprecation")
		TabHost tabHost = getTabHost();
		 tabHost.addTab(tabSpec);
		  //  host.addTab(host.newTabSpec(tag).setIndicator(indicator).setContent(intent));
	        //addTab(tag, getString(labelId), intent);
	    }

}
