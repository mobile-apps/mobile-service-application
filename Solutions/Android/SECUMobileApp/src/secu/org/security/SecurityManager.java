package secu.org.security;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.conn.ssl.SSLSocketFactory;


import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.res.Resources;
import android.provider.Settings.Secure;

import com.android.internal.widget.LockPatternUtils;

public class SecurityManager extends SSLSocketFactory {

	private SSLContext sslContext = SSLContext.getInstance("TLS");

	private static String REGISTERED = "REGISTERED";
	private static String ANDROIDID = "ANDROIDID";

	public SecurityManager(KeyStore truststore)
			throws NoSuchAlgorithmException, KeyManagementException,
			KeyStoreException, UnrecoverableKeyException {
		super(truststore);
		TrustManager tm = new X509TrustManager() {
			public void checkClientTrusted(X509Certificate[] chain,
					String authType) throws CertificateException {
			}

			public void checkServerTrusted(X509Certificate[] chain,
					String authType) throws CertificateException {
			}

			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

		};

		sslContext.init(null, new TrustManager[] { tm }, null);

	}

	@Override
	public Socket createSocket(Socket socket, String host, int port,
			boolean autoClose) throws IOException, UnknownHostException {
		return sslContext.getSocketFactory().createSocket(socket, host, port,
				autoClose);
	}

	@Override
	public Socket createSocket() throws IOException {
		return sslContext.getSocketFactory().createSocket();
	}

	public static void lockDevice(Activity activity) {
		LockPatternUtils lockPatternUtils = new LockPatternUtils(activity);

		DevicePolicyManager mgr = lockPatternUtils.getDevicePolicyManager();
		mgr.lockNow();

	}

	public static boolean isLockPasswordEnabled(Activity activity) {
		LockPatternUtils lockPatternUtils = new LockPatternUtils(activity);
		boolean isSecure = lockPatternUtils.isLockPasswordEnabled();

		return isSecure;
	}

	/**
	 * Is device secured
	 * 
	 * @param activity
	 * @return
	 */
	public static boolean hasDeviceSecurity(Activity activity) {
		// are we testing for a secure device
		boolean isCheckforDevicePassword = SecuritySettingsSingleton
				.getSingletonObject().isCheckForDevicePasscode(activity);
		if (isCheckforDevicePassword) {
			LockPatternUtils lockPatternUtils = new LockPatternUtils(activity);
			boolean isSecure = false;
			isSecure = lockPatternUtils.isSecure();

			return isSecure;
		} else {
			// skip security check
			return true;
		}
	}

	// ignore the certificate checks
	public static void ignoreCertificateCheck() {
		// Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		// Create a trust manager that does not validate certificate chains:
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {

			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkServerTrusted(X509Certificate[] certs,
					String authType) throws CertificateException {
				return;
			}

			public void checkClientTrusted(X509Certificate[] certs,
					String authType) throws CertificateException {
				return;
			}
		} // X509TrustManager
		};// TrustManager[]
			// Install the all-trusting trust manager:
		SSLContext sc = null;
		try {
			sc = SSLContext.getInstance("SSL");
		} catch (NoSuchAlgorithmException ex) {
			Logger.getLogger(SecurityManager.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		try {
			sc.init(null, trustAllCerts, new SecureRandom());
		} catch (KeyManagementException ex) {
			Logger.getLogger(SecurityManager.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		// avoid "HTTPS hostname wrong: should be <myhostname>" exception:
		HostnameVerifier hv = new HostnameVerifier() {

			public boolean verify(String urlHostName, SSLSession session) {
				if (!urlHostName.equalsIgnoreCase(session.getPeerHost())) {
					System.out.println("Warning: URL host '" + urlHostName
							+ "' is different to SSLSession host '"
							+ session.getPeerHost() + "'.");
				}
				return true; // also accept different hostname (e.g. domain name
								// instead of IP address)
			}
		};
		HttpsURLConnection.setDefaultHostnameVerifier(hv);

	}

	/*
	 * has the member registered
	 */
	public static boolean isRegistered(Activity activity) {
		boolean isReg = PreferenceManager
				.getBooleanFromSP(REGISTERED, activity);
		return isReg;
	}

	/**
	 * Update their registration record
	 * 
	 * @param activity
	 */

	public static void updateRegistered(Activity activity) {
		PreferenceManager.saveBooleanInSP(activity, REGISTERED, true);
		String androidId = Secure.getString(activity.getContentResolver(),
				Secure.ANDROID_ID);
		int hashedId = androidId.hashCode();
		PreferenceManager.saveIntInSP(activity, ANDROIDID, hashedId);
	}

	/*
	 * After registration is the android id still the same
	 */
	public static boolean hasAccess(Activity activity) {

		boolean isReg = PreferenceManager
				.getBooleanFromSP(REGISTERED, activity);
		if (isReg) {
			String androidId = Secure.getString(activity.getContentResolver(),
					Secure.ANDROID_ID);
			int hashedId = androidId.hashCode();
			int saveId = PreferenceManager.getIntFromSP(ANDROIDID, activity);
			isReg = (hashedId == saveId) ? true : false;

		}
		return isReg;
	}

	/**
	 * Did the member enter a valid registration code
	 * 
	 * @param activity
	 * @param code
	 * @return
	 */
	public static boolean isValidRegistrationCode(Activity activity,
			String code, String[] hashedAndroidList) {
		Resources resources = activity.getResources();
		// String[] hashedAndroidList = resources
		// .getStringArray(R.array.security_array);

		// hash the code
		int hashCode = code.hashCode();
		String hashAndroidString = String.valueOf(hashCode);
		boolean found = Arrays.asList(hashedAndroidList).contains(
				hashAndroidString);
		// hashedAndroidList.

		return found;
	}

	/**
	 * Checks if the device is rooted.
	 * 
	 * @return <code>true</code> if the device is rooted, <code>false</code>
	 *         otherwise.
	 */
	public static boolean isDeviceRooted(Activity activity) {
		boolean isCheckforRootAccess = SecuritySettingsSingleton
				.getSingletonObject().isCheckForRootAccess(activity);
		// get from build info
		if (isCheckforRootAccess) {
			String buildTags = android.os.Build.TAGS;
			if (buildTags != null && buildTags.contains("test-keys")) {
				return true;
			}

			// check if /system/app/Superuser.apk is present
			try {
				File file = new File("/system/app/Superuser.apk");
				if (file.exists()) {
					return true;
				}
			} catch (Throwable e1) {
				// ignore
			}
		} else {
			//skipp root check
			return true;
		}

		return false;
	}

}
