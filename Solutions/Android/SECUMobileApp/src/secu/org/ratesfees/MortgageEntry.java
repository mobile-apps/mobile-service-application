package secu.org.ratesfees;

public class MortgageEntry {
	private String description;
	private String descriptionType;
	private String term;
	private String rate;
	private String APR;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescriptionType() {
		return descriptionType;
	}

	public void setDescriptionType(String descriptionType) {
		this.descriptionType = descriptionType;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getAPR() {
		return APR;
	}

	public void setAPR(String aPR) {
		APR = aPR;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(description);
		if (rate != null) {
			builder.append("-");
			builder.append(rate);
		}
		if (term != null) {
			builder.append("-");
			builder.append(term);
		}

		return builder.toString();
	}

}
