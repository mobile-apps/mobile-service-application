package secu.org.ratesfees;

import javax.xml.parsers.SAXParser;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class LendingRatesHandler extends DefaultHandler {

	
	
	
	public LendingRatesHandler(SAXParser saxParser) {
		this.saxParser = saxParser;
		this.deposits = new Deposits();
		this.mortgages = new Mortgages();
		this.loans = new Loans();
		this.fees = new Fees();
		
		
	}
	public Deposits getDeposits() {
		return deposits;
	}

	public void setDeposits(Deposits deposits) {
		this.deposits = deposits;
	}

	public Mortgages getMortgages() {
		return mortgages;
	}

	public void setMortgages(Mortgages mortgages) {
		this.mortgages = mortgages;
	}

	public Fees getFees() {
		return fees;
	}

	public void setFees(Fees fees) {
		this.fees = fees;
	}

	private SAXParser saxParser;
	private Loans loans;
	public Loans getLoans() {
		return loans;
	}
	public void setLoans(Loans loans) {
		this.loans = loans;
	}

	private Deposits deposits;
	private Mortgages mortgages;
	private Fees fees;
	Boolean currentElement = false;
	String currentValue = "";
	  // Called when tag closing 
    @Override
    public void endElement(String uri, String localName, String qName)
    throws SAXException {

        currentElement = false;

        /** set value */
    
    }

 // Called to get tag characters 
    @Override
    public void characters(char[] ch, int start, int length)
    throws SAXException {

        if (currentElement) {
            currentValue = currentValue +  new String(ch, start, length);
        }

    }
	
	
	// Called when tag starts
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {

		if (localName.equals("deposit")) {
			this.saxParser.getXMLReader().setContentHandler(new DepositHandler(this,saxParser.getXMLReader()));	
			return;
		}
		if (localName.equals("mortgage")) {
			this.saxParser.getXMLReader().setContentHandler(new MortgageHandler(this,saxParser.getXMLReader()));	
			return;
		}
		

	}

}
