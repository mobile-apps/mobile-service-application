package secu.org.ratesfees;

import java.util.ArrayList;

public class Loans {
	private ArrayList<Loan> loanList = new ArrayList<Loan>();

	public ArrayList<Loan> getLoanList() {
		return loanList;
	}

	public void setLoanList(ArrayList<Loan> loanList) {
		this.loanList = loanList;
	}

}
