package secu.org.ratesfees;

import java.util.ArrayList;
import java.util.List;

public class Fee {
	private String description;
	private String effectiveDateLabel;
	private String effectiveDate;
	private String footnote;
	private List<FeeEntry> feeEntryList = new ArrayList<FeeEntry>();
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEffectiveDateLabel() {
		return effectiveDateLabel;
	}
	public void setEffectiveDateLabel(String effectiveDateLabel) {
		this.effectiveDateLabel = effectiveDateLabel;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getFootnote() {
		return footnote;
	}
	public void setFootnote(String footnote) {
		this.footnote = footnote;
	}
	public List<FeeEntry> getFeeEntryList() {
		return feeEntryList;
	}
	public void setFeeEntryList(List<FeeEntry> feeEntryList) {
		this.feeEntryList = feeEntryList;
	}

}
