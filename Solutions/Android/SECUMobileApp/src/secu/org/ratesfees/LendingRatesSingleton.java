package secu.org.ratesfees;

import android.app.Activity;
import android.net.http.AndroidHttpClient;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.params.HttpConnectionParams;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import secu.org.activity.R;
import secu.org.application.BankApplication;
import secu.org.application.ResourceEnumeration;
import secu.org.google.cloud.message.GcmIntentService;

public class LendingRatesSingleton extends DefaultHandler {
    private LendingRatesHandler lendingRatesHandler;
    private Deposit deposit;

    public Deposit getDeposit() {
        return deposit;
    }

    public void setDeposit(Deposit deposit) {
        this.deposit = deposit;
    }

    private static LendingRatesSingleton singletonObject;

    public static final  String filename = "ratesAndFees.xml";
    private android.content.Context mContext;

    /**
     * A private Constructor prevents any other class from instantiating.
     */
    private LendingRatesSingleton() {

    }

    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }


    public static synchronized LendingRatesSingleton getSingletonObject(android.content.Context activity) {
        if (singletonObject == null) {
            singletonObject = new LendingRatesSingleton(activity);
        }
        return singletonObject;
    }

    /**
     * read from local storage
     *
     * @param activity
     * @return
     */
    private InputStream readFromFile() {


        java.io.InputStream inputStream = null;
        try {
            inputStream = BankApplication.getAppContext().openFileInput(filename);

        } catch (java.io.FileNotFoundException e) {
            android.util.Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            android.util.Log.e("login activity", "Can not read file: " + e.toString());
        }

        return inputStream;
    }

    /**
     * refresh from  web site
     */
    public void refresh() {
        AndroidHttpClient client = null;
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        client = AndroidHttpClient.newInstance("Android");

        HttpConnectionParams
                .setConnectionTimeout(client.getParams(), 15000);
        HttpConnectionParams.setSoTimeout(client.getParams(), 5000);
        // HttpClient client = new AndroidHttpClient();
        HttpPost post = new HttpPost(ResourceEnumeration.RATES_SERVER_URL.getValue());
        try {
            HttpResponse response = client.execute(post);
            br = new BufferedReader(new InputStreamReader(
                    (response.getEntity().getContent())));
            String output;

            while ((output = br.readLine()) != null) {
                sb.append(output);
            }
            br.close();
            client.getConnectionManager().shutdown();
            // client.close();
            client = null;

            //save file
            // java.io.File file = new java.io.File(gcmIntentService.getFilesDir(), "aa.xml");


            java.io.FileOutputStream outputStream;
            outputStream = mContext.openFileOutput(filename, android.content.Context.MODE_PRIVATE);
            outputStream.write(sb.toString().getBytes());
            outputStream.close();


        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public LendingRatesSingleton( android.content.Context activity) {
        mContext = activity;
        InputStream inputStream = null;
        try {
            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser sp = spf.newSAXParser();
            XMLReader xr = sp.getXMLReader();
            lendingRatesHandler = new LendingRatesHandler(sp);
            xr.setContentHandler(lendingRatesHandler);
            //read from local storage
            inputStream = readFromFile();
            if (inputStream == null) {
                //read from default values
                inputStream = activity.getResources().openRawResource(R.raw.ratessampledata);
               // inputStream = activity.getResources().openRawResource(filename);
            }
            InputSource inputSource = new InputSource(inputStream);
            xr.parse(inputSource);
            int a = 9;
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public LendingRatesHandler getLendingRatesHandler() {
        return lendingRatesHandler;
    }

    public void setLendingRatesHandler(LendingRatesHandler lendingRatesHandler) {
        this.lendingRatesHandler = lendingRatesHandler;
    }
}
