package secu.org.ratesfees;

import java.util.ArrayList;

public class Fees {
	private String menuLabel;
	private Fee fee = null;
	private ArrayList<Fee> feeList = new ArrayList<Fee>();
	public String getMenuLabel() {
		return menuLabel;
	}
	public void setMenuLabel(String menuLabel) {
		this.menuLabel = menuLabel;
	}
	public Fee getFee() {
		return fee;
	}
	public void setFee(Fee fee) {
		this.fee = fee;
	}
	public ArrayList<Fee> getFeeList() {
		return feeList;
	}
	public void setFeeList(ArrayList<Fee> feeList) {
		this.feeList = feeList;
	}
}
