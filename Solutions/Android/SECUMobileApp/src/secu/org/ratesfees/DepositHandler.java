package secu.org.ratesfees;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class DepositHandler extends DefaultHandler {
	
	private Boolean currentElement = false;
	private String currentValue = "";
	 private XMLReader reader;
	    private LendingRatesHandler parentHandler;
	    public LendingRatesHandler getParentHandler() {
			return parentHandler;
		}
		public void setParentHandler(LendingRatesHandler parentHandler) {
			this.parentHandler = parentHandler;
		}
		public DepositHandler(LendingRatesHandler handler, XMLReader xmlReader) {
	    	this.reader = xmlReader;
	    	this.parentHandler = handler;
	    	deposit = new Deposit();
	    	handler.getDeposits().getDepositList().add(this.deposit);
	    	
	    }
	public Deposit getDeposit() {
		return deposit;
	}

	public void setDeposit(Deposit deposit) {
		this.deposit = deposit;
	}


	private Deposit deposit = new Deposit();
	// Called when tag starts
		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {

			 if (localName.equals("depositEntry")) {
		    	   DepositEntryHandler depositEntryhandler = new DepositEntryHandler(this, reader);
		            this.reader.setContentHandler(depositEntryhandler);
		       }
			

		}
	// Called when tag closing 
    @Override
    public void endElement(String uri, String localName, String qName)
    throws SAXException {

        currentElement = false;
        if (localName.equals("description")) {
			this.deposit.setDescription(currentValue);
			return;
		}
        if (localName.equals("effectiveDate")) {
			this.deposit.setEffectiveDate(currentValue);
			return;
		}
        if (localName.equals("footnote")) {
			this.deposit.setFootnote(currentValue);
			return;
		}
      
        // 

        /** set value */
    
    }

 // Called to get tag characters 
    @Override
    public void characters(char[] ch, int start, int length)
    throws SAXException {

       
            currentValue =   new String(ch, start, length);
   

    }
	
	
}
