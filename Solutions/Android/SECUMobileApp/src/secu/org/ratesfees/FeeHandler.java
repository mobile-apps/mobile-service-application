package secu.org.ratesfees;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class FeeHandler extends DefaultHandler {
	
	private Boolean currentElement = false;
	private String currentValue = "";
	 private XMLReader reader;
	 private LendingRatesHandler parentHandler;
	    public LendingRatesHandler getParentHandler() {
		return parentHandler;
	}
	public void setParentHandler(LendingRatesHandler parentHandler) {
		this.parentHandler = parentHandler;
	}
		public FeeHandler(LendingRatesHandler handler, XMLReader xmlReader) {
	    	this.reader = xmlReader;
	    	this.parentHandler = handler;
	    	fee = new Fee();
	    	handler.getFees().getFeeList().add(this.fee);
	    	
	    }
	public Fee getFee() {
		return fee;
	}

	

	private Fee fee = new Fee();
	// Called when tag starts
		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {

			 if (localName.equals("feeEntry")) {
				FeeEntryHandler feeEntryHandler =  new FeeEntryHandler(this, reader);
		         this.reader.setContentHandler(feeEntryHandler);
		       }
			

		}
	// Called when tag closing 
    @Override
    public void endElement(String uri, String localName, String qName)
    throws SAXException {

        currentElement = false;
      
        if (localName.equals("description")) {
			this.fee.setDescription(currentValue);
			return;
		}
        if (localName.equals("effectiveDateLabel")) {
  			this.fee.setEffectiveDateLabel(currentValue);
  			return;
  		}
        if (localName.equals("effectiveDate")) {
  			this.fee.setEffectiveDate(currentValue);
  			return;
  		}
        if (localName.equals("footnote")) {
  			this.fee.setFootnote(currentValue);
  			return;
  		}
      
        // 

        /** set value */
    
    }

 // Called to get tag characters 
    @Override
    public void characters(char[] ch, int start, int length)
    throws SAXException {

       
            currentValue =   new String(ch, start, length);
   

    }
	
	
}
