package secu.org.ratesfees;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class LoanEntryHandler extends DefaultHandler {

	private Boolean currentElement = false;
	private LoanEntry loanEntry;
	private String currentValue = "";
	private XMLReader reader;
	private LoanHandler parentHandler;

	public LoanEntryHandler(LoanHandler handler, XMLReader xmlReader) {
		this.reader = xmlReader;
		this.parentHandler = handler;
		loanEntry = new LoanEntry();
		loan = handler.getLoan();
		loan.getLoanEntryList().add(loanEntry);

	}

	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

	private Loan loan = new Loan();

	// Called when tag starts
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {

		if (localName.equals("description")) {
			loanEntry.setLabel(attributes.getValue("label"));
			return;
		}
		// fees
		if (localName.equals("fees")) {
			FeeHandler feeHandler = new FeeHandler(
					parentHandler.getParentHandler(), reader);
			this.reader.setContentHandler(feeHandler);
		}

		if (localName.equals("loanEntry")) {
			LoanEntryHandler loanEntryhandler = new LoanEntryHandler(
					parentHandler, reader);
			this.reader.setContentHandler(loanEntryhandler);
		}
		if (localName.equals("loan")) {
			LoanHandler loanhandler = new LoanHandler(
					parentHandler.getParentHandler(), reader);
			this.reader.setContentHandler(loanhandler);
		}
		//loan

	}

	// Called when tag closing
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		currentElement = false;

		if (localName.equals("description")) {
			this.loanEntry.setDescription(currentValue);
			return;
		}
		if (localName.equals("descriptionType")) {
			this.loanEntry.setDescriptionType(currentValue);
			return;
		}
		if (localName.equals("term")) {
			this.loanEntry.setTerm(currentValue);
		}
		if (localName.equals("payrollAPR")) {
			this.loanEntry.setPayrollAPR(currentValue);
		}
		if (localName.equals("directPayAPR")) {
			this.loanEntry.setDirectPayAPR(currentValue);
		}
		//

		/** set value */

	}

	// Called to get tag characters
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {

		currentValue = new String(ch, start, length);

	}

}
