package secu.org.ratesfees;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class FeeEntryHandler extends DefaultHandler {

	private Boolean currentElement = false;
	private FeeEntry feeEntry;
	private String currentValue = "";
	private XMLReader reader;
	private FeeHandler parentHandler;

	public FeeEntryHandler(FeeHandler handler, XMLReader xmlReader) {
		this.reader = xmlReader;
		this.parentHandler = handler;
		feeEntry = new FeeEntry();
		fee = handler.getFee();
		fee.getFeeEntryList().add(feeEntry);

	}

	public Fee getFee() {
		return fee;
	}

	public void setFee(Fee fee) {
		this.fee = fee;
	}

	private Fee fee = new Fee();

	// Called when tag starts
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {

		if (localName.equals("feeDescription")) {
			feeEntry.setFeeDescription(attributes.getValue("label"));
			return;
		}
		// mortgages
		
		if (localName.equals("feeEntry")) {
			FeeEntryHandler feeEntryhandler = new FeeEntryHandler(
					parentHandler, reader);
			this.reader.setContentHandler(feeEntryhandler);
		}
		if (localName.equals("fee")) {
			FeeHandler feehandler = new FeeHandler(
					parentHandler.getParentHandler(), reader);
			this.reader.setContentHandler(feehandler);
		}

	}

	// Called when tag closing
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		currentElement = false;
		
		if (localName.equals("feeAmount")) {
			this.feeEntry.setFeeAmount(currentValue);
			return;
		}
		
		//

		/** set value */

	}

	// Called to get tag characters
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {

		currentValue = new String(ch, start, length);

	}

}
