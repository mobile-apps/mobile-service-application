package secu.org.ratesfees;

import java.util.ArrayList;
import java.util.List;

public class Deposit {
	private String description;
	
	

	private String effectiveDateLabel;
	private String effectiveDate;
	private String footnote;
	private List<DepositEntry> depositEntryList = new ArrayList<DepositEntry>();

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEffectiveDateLabel() {
		return effectiveDateLabel;
	}

	public void setEffectiveDateLabel(String effectiveDateLabel) {
		this.effectiveDateLabel = effectiveDateLabel;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getFootnote() {
		return footnote;
	}

	public void setFootnote(String footnote) {
		this.footnote = footnote;
	}

	public List<DepositEntry> getDepositEntryList() {
		return depositEntryList;
	}

	public void setDepositEntryList(List<DepositEntry> depositEntryList) {
		this.depositEntryList = depositEntryList;
	}

	@Override
	public String toString() {

		return effectiveDate;
	}

}
