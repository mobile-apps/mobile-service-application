package secu.org.ratesfees;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class LoanHandler extends DefaultHandler {
	
	public LendingRatesHandler getParentHandler() {
		return parentHandler;
	}
	public void setParentHandler(LendingRatesHandler parentHandler) {
		this.parentHandler = parentHandler;
	}



	private Boolean currentElement = false;
	private String currentValue = "";
	 private XMLReader reader;
	 private LendingRatesHandler parentHandler;
	    public LoanHandler(LendingRatesHandler handler, XMLReader xmlReader) {
	    	this.reader = xmlReader;
	    	this.parentHandler = handler;
	    	loan = new Loan();
	    	handler.getLoans().getLoanList().add(this.loan);
	    	
	    }
	public Loan getLoan() {
		return loan;
	}

	

	private Loan loan = new Loan();
	// Called when tag starts
		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {

			 if (localName.equals("loanEntry")) {
				LoanEntryHandler loanEntryHandler =  new LoanEntryHandler(this, reader);
		         this.reader.setContentHandler(loanEntryHandler);
		       }
			

		}
	// Called when tag closing 
    @Override
    public void endElement(String uri, String localName, String qName)
    throws SAXException {

        currentElement = false;
      
        if (localName.equals("footnote")) {
			this.loan.setFootnote(currentValue);
			return;
		}
        if (localName.equals("description")) {
			this.loan.setDescription(currentValue);
			return;
		}
      
        // 

        /** set value */
    
    }

 // Called to get tag characters 
    @Override
    public void characters(char[] ch, int start, int length)
    throws SAXException {

       
            currentValue =   new String(ch, start, length);
   

    }
	
	
}
