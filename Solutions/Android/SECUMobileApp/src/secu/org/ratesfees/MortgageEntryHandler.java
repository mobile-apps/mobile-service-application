package secu.org.ratesfees;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class MortgageEntryHandler extends DefaultHandler {

	private Boolean currentElement = false;
	private MortgageEntry mortgageEntry;
	private String currentValue = "";
	private XMLReader reader;
	private MortgageHandler parentHandler;

	public MortgageEntryHandler(MortgageHandler handler, XMLReader xmlReader) {
		this.reader = xmlReader;
		this.parentHandler = handler;
		mortgageEntry = new MortgageEntry();
		mortgage = handler.getMortgage();
		mortgage.getMortgageEntryList().add(mortgageEntry);

	}

	public Mortgage getMortgage() {
		return mortgage;
	}

	public void setMortgage(Mortgage mortgage) {
		this.mortgage = mortgage;
	}

	private Mortgage mortgage = new Mortgage();

	// Called when tag starts
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {

		if (localName.equals("description")) {
			mortgageEntry.setDescription(attributes.getValue("label"));
			return;
		}
		// mortgages
		
		if (localName.equals("mortgageEntry")) {
			MortgageEntryHandler mortgageEntryhandler = new MortgageEntryHandler(
					parentHandler, reader);
			this.reader.setContentHandler(mortgageEntryhandler);
		}
		if (localName.equals("loans")) {
			LoanHandler loanHandler = new LoanHandler(
					parentHandler.getParentHandler(), reader);
			this.reader.setContentHandler(loanHandler);
		}
		if (localName.equals("mortgage")) {
			MortgageHandler mortgageHandler = new MortgageHandler(
					parentHandler.getParentHandler(), reader);
			this.reader.setContentHandler(mortgageHandler);
		}
		//mortgage

	}

	// Called when tag closing
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		currentElement = false;
		
		if (localName.equals("descriptionType")) {
			this.mortgageEntry.setDescriptionType(currentValue);
			return;
		}
		if (localName.equals("term")) {
			this.mortgageEntry.setTerm(currentValue);
			return;
		}
		if (localName.equals("rate")) {
			this.mortgageEntry.setRate(currentValue);
		}
		if (localName.equals("APR")) {
			this.mortgageEntry.setAPR(currentValue);
		}
		//

		/** set value */

	}

	// Called to get tag characters
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {

		currentValue = new String(ch, start, length);

	}

}
