package secu.org.ratesfees;

import java.util.ArrayList;
import java.util.List;

public class Mortgage {
private String mortgage;
private String footnote;
private String description;
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
private List<MortgageEntry> mortgageEntryList = new ArrayList<MortgageEntry>();
public String getMortgage() {
	return mortgage;
}
public void setMortgage(String mortgage) {
	this.mortgage = mortgage;
}
public String getFootnote() {
	return footnote;
}
public void setFootnote(String footnote) {
	this.footnote = footnote;
}
public List<MortgageEntry> getMortgageEntryList() {
	return mortgageEntryList;
}
public void setMortgageEntryList(List<MortgageEntry> mortgageEntryList) {
	this.mortgageEntryList = mortgageEntryList;
}

}
