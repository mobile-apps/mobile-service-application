package secu.org.ratesfees;

public class LoanEntry {
	private String description;
	private String descriptionType;
	private String label;
	private String term;
	private String payrollAPR;
	private String directPayAPR;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescriptionType() {
		return descriptionType;
	}

	public void setDescriptionType(String descriptionType) {
		this.descriptionType = descriptionType;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getPayrollAPR() {
		return payrollAPR;
	}

	public void setPayrollAPR(String payrollAPR) {
		this.payrollAPR = payrollAPR;
	}

	public String getDirectPayAPR() {
		return directPayAPR;
	}

	public void setDirectPayAPR(String directPayAPR) {
		this.directPayAPR = directPayAPR;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.label);
		if (this.term != null) {
			builder.append("term");
			builder.append(this.term);
			builder.append("-");
			builder.append(this.payrollAPR);
		}

		return builder.toString();
	}
}
