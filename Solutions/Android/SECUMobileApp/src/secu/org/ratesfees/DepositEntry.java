package secu.org.ratesfees;

public class DepositEntry {
	private String label;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	private String description;
	private String openingDeposit;
	private String rate;
	private String apy;
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getOpeningDeposit() {
		return openingDeposit;
	}
	public void setOpeningDeposit(String openingDeposit) {
		this.openingDeposit = openingDeposit;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getApy() {
		return apy;
	}
	public void setApy(String apy) {
		this.apy = apy;
	}
	@Override
	public String toString() {
		
		return label + " Deposit " + openingDeposit + " Rate " + rate ;
	}
}
