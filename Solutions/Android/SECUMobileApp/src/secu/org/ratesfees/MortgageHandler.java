package secu.org.ratesfees;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class MortgageHandler extends DefaultHandler {
	
	private Boolean currentElement = false;
	private String currentValue = "";
	 private XMLReader reader;
	    public LendingRatesHandler getParentHandler() {
		return parentHandler;
	}
	public void setParentHandler(LendingRatesHandler parentHandler) {
		this.parentHandler = parentHandler;
	}


		private LendingRatesHandler parentHandler;
	    public MortgageHandler(LendingRatesHandler handler, XMLReader xmlReader) {
	    	this.reader = xmlReader;
	    	this.parentHandler = handler;
	    	mortgage = new Mortgage();
	    	handler.getMortgages().getMortgageList().add(this.mortgage);
	    	
	    }
	public Mortgage getMortgage() {
		return mortgage;
	}

	public void setDeposit(Mortgage mortgage) {
		this.mortgage = mortgage;
	}


	private Mortgage mortgage = new Mortgage();
	// Called when tag starts
		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {

			 if (localName.equals("mortgageEntry")) {
				 MortgageEntryHandler mortgageEntryHandler =  new MortgageEntryHandler(this, reader);
		    	//   DepositEntryHandler depositEntryhandler = new DepositEntryHandler(this, reader);
		         this.reader.setContentHandler(mortgageEntryHandler);
		       }
			

		}
	// Called when tag closing 
    @Override
    public void endElement(String uri, String localName, String qName)
    throws SAXException {

        currentElement = false;
        if (localName.equals("description")) {
			this.mortgage.setDescription(currentValue);
			return;
		}
        
        if (localName.equals("footnote")) {
			this.mortgage.setFootnote(currentValue);
			return;
		}
      
        // 

        /** set value */
    
    }

 // Called to get tag characters 
    @Override
    public void characters(char[] ch, int start, int length)
    throws SAXException {

       
            currentValue =   new String(ch, start, length);
   

    }
	
	
}
