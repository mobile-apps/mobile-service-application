package secu.org.ratesfees;

import java.util.ArrayList;
import java.util.List;

public class Loan {
	private String description;
	private String footnote;
	private List<LoanEntry> loanEntryList = new ArrayList<LoanEntry>();
	public List<LoanEntry> getLoanEntryList() {
		return loanEntryList;
	}
	public void setLoanEntryList(List<LoanEntry> loanEntryList) {
		this.loanEntryList = loanEntryList;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getFootnote() {
		return footnote;
	}
	public void setFootnote(String footnote) {
		this.footnote = footnote;
	}
	
	
}
