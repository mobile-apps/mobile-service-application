package secu.org.ratesfees;

import java.util.ArrayList;

public class Deposits {
private String menuLabel;
private Deposit deposit = null;
private ArrayList<Deposit> depositList = new ArrayList<Deposit>();
public String getMenuLabel() {
	return menuLabel;
}
public void setMenuLabel(String menuLabel) {
	this.menuLabel = menuLabel;
}
public Deposit getDeposit() {
	return deposit;
}
public void setDeposit(Deposit deposit) {
	this.deposit = deposit;
}
public ArrayList<Deposit> getDepositList() {
	return depositList;
}
public void setDepositList(ArrayList<Deposit> depositList) {
	this.depositList = depositList;
}
}
