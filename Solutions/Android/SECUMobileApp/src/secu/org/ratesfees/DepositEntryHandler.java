package secu.org.ratesfees;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class DepositEntryHandler extends DefaultHandler {

	private DepositEntry depositEntry;
	private String currentValue = "";
	private XMLReader reader;
	private DepositHandler parentHandler;

	public DepositEntryHandler(DepositHandler handler, XMLReader xmlReader) {
		this.reader = xmlReader;
		this.parentHandler = handler;
		depositEntry = new DepositEntry();
		deposit = handler.getDeposit();
		deposit.getDepositEntryList().add(depositEntry);

	}

	public Deposit getDeposit() {
		return deposit;
	}

	public void setDeposit(Deposit deposit) {
		this.deposit = deposit;
	}

	private Deposit deposit = new Deposit();

	// Called when tag starts
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {

		if (localName.equals("description")) {
			depositEntry.setLabel(attributes.getValue("label"));
			return;
		}
		// mortgages
		if (localName.equals("mortgages")) {
			MortgageHandler mortgageHandler = new MortgageHandler(
					parentHandler.getParentHandler(), reader);
			this.reader.setContentHandler(mortgageHandler);
		}
		if (localName.equals("depositEntry")) {
			DepositEntryHandler depositEntryhandler = new DepositEntryHandler(
					parentHandler, reader);
			this.reader.setContentHandler(depositEntryhandler);
		}
		if (localName.equals("deposit")) {
			DepositHandler deposithandler = new DepositHandler(
					parentHandler.getParentHandler(), reader);
			this.reader.setContentHandler(deposithandler);
		}
		
		//this.saxParser.getXMLReader().setContentHandler(new DepositHandler(this,saxParser.getXMLReader()));	

	}

	// Called when tag closing
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		if (localName.equals("description")) {
			this.depositEntry.setDescription(currentValue);
			return;
		}
		if (localName.equals("openingDeposit")) {
			this.depositEntry.setOpeningDeposit(currentValue);
			return;
		}
		if (localName.equals("rate")) {
			this.depositEntry.setRate(currentValue);
			return;
		}
		if (localName.equals("APY")) {
			this.depositEntry.setApy(currentValue);
		}
		//

		/** set value */

	}

	// Called to get tag characters
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {

		currentValue = new String(ch, start, length);

	}

}
