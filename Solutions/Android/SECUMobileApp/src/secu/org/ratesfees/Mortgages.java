package secu.org.ratesfees;

import java.util.ArrayList;

public class Mortgages {
	private String menuLabel;
	private Mortgage mortgage = null;
	private ArrayList<Mortgage> mortgageList = new ArrayList<Mortgage>();
	public String getMenuLabel() {
		return menuLabel;
	}
	public void setMenuLabel(String menuLabel) {
		this.menuLabel = menuLabel;
	}
	public Mortgage getMortgage() {
		return mortgage;
	}
	public void setMortgage(Mortgage mortgage) {
		this.mortgage = mortgage;
	}
	public ArrayList<Mortgage> getMortgageList() {
		return mortgageList;
	}
	public void setMortgageList(ArrayList<Mortgage> mortgageList) {
		this.mortgageList = mortgageList;
	}

}
