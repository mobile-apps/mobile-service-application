package secu.org.ratesfees;

public class FeeEntry {
private String feeDescription;
private String feeAmount;
private String label;
public String getLabel() {
	return label;
}
public String getFeeDescription() {
	return feeDescription;
}
public void setFeeDescription(String feeDescription) {
	this.feeDescription = feeDescription;
}
public String getFeeAmount() {
	return feeAmount;
}
public void setFeeAmount(String feeAmount) {
	this.feeAmount = feeAmount;
}
@Override
public String toString() {
	StringBuilder builder = new StringBuilder();
	
	if (this.feeDescription != null) {
		builder.append(this.feeDescription);
		builder.append("-");
		builder.append(this.feeAmount);

	}

	return builder.toString();
}
}
