package secu.org.util;

import java.util.*;
import java.text.*;

public class DateManager {
    private static final String DATEFORMAT = "yyyy/MM/dd HH:mm:ss";
	/**
	 * compute days between dates
	 * 
	 * @param earlierDate
	 * @param currentDate
	 * @return
	 */
	public static long computeDaysDifference(Date earlierDate, Date currentDate) {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"starting computeDaysDifference", DateManager.class.getSimpleName());
		Calendar calendarEarlierDate = Calendar.getInstance();
		Calendar calendarCurrentDate = Calendar.getInstance();
		calendarEarlierDate.setTime(earlierDate);
		calendarCurrentDate.setTime(currentDate);
		long milliseconds1 = calendarEarlierDate.getTimeInMillis();
		long milliseconds2 = calendarCurrentDate.getTimeInMillis();
		long diff = milliseconds2 - milliseconds1;
		long diffDays = diff / (24 * 60 * 60 * 1000);
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"ending computeDaysDifference",  DateManager.class.getSimpleName());
		return diffDays;
	}

	/**
	 * convertToDate
	 * 
	 * @param dateStr
	 * @return
	 */

	public static Date convertToDate(String dateStr) {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"starting convertToDate",  DateManager.class.getSimpleName());
		dateStr = dateStr.substring(0, 8);
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"extract Date[" + dateStr + "]",  DateManager.class.getSimpleName());
		 String year = dateStr.substring(0, 4);
		 String month = dateStr.substring(4);
		 month = month.substring(0,2);
		 String day = dateStr.substring(6);
		int intYear = Integer.parseInt(year);
		int intMonth = Integer.parseInt(month);
		
		int intDay = Integer.parseInt(day);

		Calendar calendar = new GregorianCalendar(intYear, intMonth - 1, intDay);
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"convertToDate [" + calendar.getTime().toString() + "]",
				"DateManager");
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"starting convertToDate",  DateManager.class.getSimpleName());
		return calendar.getTime();
	}

	/**
	 * format date
	 * 
	 * @param dateToDisplay
	 * @return
	 */
	public static String displayDate(String dateToDisplay) {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"starting displayDate",  DateManager.class.getSimpleName());
		dateToDisplay = dateToDisplay.substring(0, 8);
		DateFormat formatter;
		Date date = null;
		formatter = new SimpleDateFormat("yyyyMMdd");
		String formattedDate = "";

		try {

			date = (Date) formatter.parse(dateToDisplay);

			formattedDate = DateFormat.getDateInstance(DateFormat.MEDIUM)
					.format(date);

		} catch (ParseException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"starting displayDate",  DateManager.class.getSimpleName());
		return formattedDate;
	}

	public static void main(String args[]) {
		displayDate("20120517120000.000[0:GMT]");
	}

    public static String getMonthDay(String dateTime) {
        //"h:mm a"
        try {
            Date date = new SimpleDateFormat(DATEFORMAT).parse(dateTime);
            SimpleDateFormat hourTimeSimpleDateFormat = new SimpleDateFormat("EEE, MMM d");
            return hourTimeSimpleDateFormat.format(date).toString();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }



    public static String getHourMin(String dateTime) {
        //"h:mm a"
        try {
            Date date = new SimpleDateFormat(DATEFORMAT).parse(dateTime);
            SimpleDateFormat hourTimeSimpleDateFormat = new SimpleDateFormat("h:mm a");
            return hourTimeSimpleDateFormat.format(date).toString();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * get current datetime
     * @return
     */
    public static String getCurrentDateTime() {
        DateFormat dateFormat = new SimpleDateFormat(DATEFORMAT);
        //get current date time with Date()
        Date date = new Date();
        System.out.println(dateFormat.format(date));

        //get current date time with Calendar()
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }


    public static Date getRealCurrentDateTime() {
      //get current date time with Calendar()
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }


    public static Date convertToRealDate(String dateTime) {
        //"h:mm a"
        try {
            Date date = new SimpleDateFormat(DATEFORMAT).parse(dateTime);
            SimpleDateFormat hourTimeSimpleDateFormat = new SimpleDateFormat("h:mm a");
            return date;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
