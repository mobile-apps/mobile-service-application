package secu.org.util;

import java.util.logging.Logger;
import java.util.logging.Level;

import secu.org.activity.BuildConfig;
import android.util.Log;

public class LoggerHelper {

	private Logger mLogger;

	public LoggerHelper(String loggerName) {
		mLogger = Logger.getLogger(loggerName);
	}

	public void startMethod(String methodName) {
		mLogger.info("Starting Method [" + methodName + "]");

	}

	public void endMethod(String methodName) {
		mLogger.info("End of Method [" + methodName + "]");

	}

	public void info(String message) {
		mLogger.info(message);
	}

	public void debug(String message) {
		// mLogger.log(Level.,message)
		mLogger.log(Level.FINE, message);
	}

	public void severe(String message) {
		// mLogger.log(Level.,message)
		mLogger.log(Level.SEVERE, message);
	}

	public void warning(String message) {
		// mLogger.log(Level.,message)
		mLogger.log(Level.WARNING, message);
	}

	public static void d(String tag, String msg) {
		 //if (Log.isLoggable(tag, Log.DEBUG) || BuildConfig.DEBUG) {
		Log.d(tag, msg);
		// }
	}

	public static void d(String tag, String msg,
			String className) {
		// if (Log.isLoggable(tag, Log.DEBUG)) {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		builder.append(className);
		builder.append("]");
		builder.append(" ");
		builder.append(msg);
		d(tag, builder.toString());
		// }
	}

	public static void i(String tag, String msg) {
		if (Log.isLoggable(tag, Log.INFO)) {
			Log.i(tag, msg);
		}
	}
	public static void i(String tag, String msg,
			String className) {
		// if (Log.isLoggable(tag, Log.DEBUG)) {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		builder.append(className);
		builder.append("]");
		builder.append(" ");
		builder.append(msg);
		i(tag, builder.toString());
		// }
	}


	public static void e(String tag, String msg) {
		if (Log.isLoggable(tag, Log.ERROR)) {
			Log.e(tag, msg);
		}
	}
	public static void e(String tag, String msg,
			String className) {
		// if (Log.isLoggable(tag, Log.DEBUG)) {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		builder.append(className);
		builder.append("]");
		builder.append(" ");
		builder.append(msg);
		e(tag, builder.toString());
		// }
	}


	public static void v(String tag, String msg) {
		if (Log.isLoggable(tag, Log.VERBOSE)) {
			Log.v(tag, msg);
		}
	}
	public static void v(String tag, String msg,
			String className) {
		// if (Log.isLoggable(tag, Log.DEBUG)) {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		builder.append(className);
		builder.append("]");
		builder.append(" ");
		builder.append(msg);
		v(tag, builder.toString());
		// }
	}


	public static void w(String tag, String msg) {
		if (Log.isLoggable(tag, Log.WARN)) {
			Log.w(tag, msg);
		}
	}
	public static void w(String tag, String msg,
			String className) {
		// if (Log.isLoggable(tag, Log.DEBUG)) {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		builder.append(className);
		builder.append("]");
		builder.append(" ");
		builder.append(msg);
		w(tag, builder.toString());
		// }
	}


}
