package secu.org.message;

import java.io.Serializable;
import java.util.Date;

import secu.org.util.DateManager;

@SuppressWarnings("serial")
public class Mail implements Serializable , Comparable<Mail> {
	public Mail(String dateCreated, String from, String to, String subject,
			String body) {
		super();
		this.dateCreated = dateCreated;
		this.from = from;
		this.to = to;
		this.subject = subject;
		this.body = body;
	}
	private String dateCreated;
	private String from;
	private String to;
	private String subject;
	private String body;
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	
	public Date getComputedDate() {
		Date date = DateManager.convertToDate(getDateCreated());
		return date;
	}
	@Override
	public int compareTo(Mail myObject) {
		// TODO Auto-generated method stub
		return myObject.getComputedDate().compareTo(getComputedDate());

	}
	public String getDisplayFormattedDatePosted() {
		String formattedDate = secu.org.util.DateManager.displayDate(getDateCreated());
		return formattedDate;
	}
	
	public String compositeMail() {
		String content = secu.org.util.StringManager.formatRightPad(getBody(), 30, 4, '.');
		content = content + this.getDisplayFormattedDatePosted();
		return content;
	}
	/**
	 * Remove extra body content. The remove information is found in other
	 * class variables
	 * @return
	 */
	public String getDisplayBody() {
		int endPosition = this.body.indexOf(".");
		if (endPosition > 0) {
			String displaybody = this.body.substring(0, endPosition);
			return displaybody;
		} else {
		  return this.body;
		}
	}

	
}
