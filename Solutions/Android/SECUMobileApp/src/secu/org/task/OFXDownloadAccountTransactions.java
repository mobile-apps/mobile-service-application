package secu.org.task;
import java.util.List;
import java.util.logging.Logger;

import secu.org.account.Account;
import secu.org.account.AccountTransaction;
import secu.org.activity.AccountListActivity;
import secu.org.activity.LoginActivity;
import secu.org.application.BankApplication;
import secu.org.application.BankSingleton;
import secu.org.bank.Bank;
import secu.org.ofx.OFXOperator;
import secu.org.ofx.response.OFX;
import secu.org.session.ActivitySession;
import secu.org.util.MethodNameHelper;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class OFXDownloadAccountTransactions extends AbstractTask
{
	int myProgress;

    private ProgressDialog dialog;

    private OFX ofx = null;
    private Bank bank = null;
    List<AccountTransaction> mTransactionList;
    private AccountListActivity mAccountListActivityGreen = null;
    Account mAccount;
	public OFXDownloadAccountTransactions(AccountListActivity accountListActivityGreen, String userId, String password, Account account,Resources resources) {
		super(accountListActivityGreen, userId,
				password, account,
				resources);
		this.mAccountListActivityGreen = accountListActivityGreen;
		this.mAccount = account;
	}
	

	
	@Override
	protected String doInBackground(Context... params) {
		
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"starting doInBackground ", this.getClass().getSimpleName());
		// TODO Auto-generated method stub
		String returnMessage = "GOOD";
		OFXOperator OFXConnection = new OFXOperator();
		try{
			String userId = BankSingleton.getSingletonObject().getBank().getUserId();
			String password = BankSingleton.getSingletonObject().getBank().getPassword();
			//mTransactionList = OFXConnection.downLoadAccountTransactions(userId, password, mAccount);
			mTransactionList = OFXConnection.downLoadAccountTransactionsPerformance(userId, password, mAccount);
		} catch(Exception ex) {
			mTransactionList = null;
			Log.i( "bank error[", ex.getMessage() + "]" );
			
		}
		
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"ending doInBackground ", this.getClass().getSimpleName());
		return returnMessage;
	}
	// -- gets called just before thread begins
    @Override
    protected void onPreExecute() 
    {
    	secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"starting onPreExecute ", this.getClass().getSimpleName());
    	dialog = new ProgressDialog(mAccountListActivityGreen);
    	dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    	dialog.setMessage("Down loading transactions. Please wait");
    	dialog.show();
 
    	Log.i( "ofx", "onPreExecute()" );
          
            super.onPreExecute();
            
    }
 // -- called as soon as doInBackground method completes
    // -- notice that the third param gets passed to this method
    @Override
    protected void onPostExecute( String result ) 
    {
    	secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
				"starting onPostExecute ", this.getClass().getSimpleName());
            super.onPostExecute(result);
            super.onPostExecute(result);
    		try {
    		dialog.dismiss();
    		} catch(Exception e) {
    			secu.org.util.LoggerHelper.d(
    					secu.org.application.LoggingTag.LOGON.name(),
    					"user stopped onPostExecute ", this.getClass().getSimpleName());
    		}
            if (mTransactionList == null) {
            	Toast.makeText( this.mAccountListActivityGreen.getApplicationContext(), 
    	                "transaction download error", Toast.LENGTH_LONG).show();
            	return;
            }
            //AccountTransactionActivity
            Intent myIntent = new Intent(this.mAccountListActivityGreen, secu.org.activity.AccountTransactionActivity.class);
    		if (mTransactionList != null) {  
    			mAccount.setAccountTransactionList(mTransactionList);
    			myIntent.putExtra(ActivitySession.ACCOUNT, mAccount);
    			myIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE,
    					"Account Transactions");

    			
    			this.mAccountListActivityGreen.startActivity(myIntent);
    		}  else {
    			Toast.makeText( this.mAccountListActivityGreen.getApplicationContext(), 
    	                "transaction download error", Toast.LENGTH_LONG).show();
    		}
            
           
    		secu.org.util.LoggerHelper.d(
    				secu.org.application.LoggingTag.GETSTATEMENTS.name(),
    				"ending onPostExecute ", this.getClass().getSimpleName());
           
    }
    @Override
	protected void onProgressUpdate(Integer... values) {
		// TODO Auto-generated method stub
    
	}




}

