package secu.org.task;

import greendroid.widget.ItemAdapter;
import greendroid.widget.item.Item;
import greendroid.widget.item.SeparatorItem;
import greendroid.widget.item.SubtitleItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.http.NameValuePair;

import secu.org.activity.OutboxActivity;
import secu.org.activity.InboxActivity;
import secu.org.activity.R;
import secu.org.bank.Bank;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.widget.ListView;

public class LoadInboxTask extends AbstractTask {
	// Progress Dialog
		private ProgressDialog pDialog;
	private InboxActivity mInboxActivity;
	private Bank mBank;
	private List<secu.org.message.Mail> mInboxList;
	private List<secu.org.message.Mail> mOutboxList = new ArrayList<secu.org.message.Mail>();
	private List<Item> mItems = new ArrayList<Item>();
	public LoadInboxTask(InboxActivity activity, String userId,
	String password, Bank bank,
	Resources resources) {
		super(activity, userId,
				password, null,
				resources);
		mInboxActivity = activity;
		mBank = bank;
	}
	/**
	 * Before starting background thread Show Progress Dialog
	 * */
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.INBOX.name(),
				"starting onPreExecute ", this.getClass().getSimpleName());
		pDialog = new ProgressDialog(mInboxActivity);
		pDialog.setMessage("Loading Messages ...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		try {
			pDialog.show();
		} catch (Exception e) {
			pDialog.dismiss();
		}
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.INBOX.name(),
				"ending onPreExecute ", this.getClass().getSimpleName());
	}
	/**
	 * After completing background task Dismiss the progress dialog
	 * **/
	protected void onPostExecute(String file_url) {
		// dismiss the dialog after getting all products
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.INBOX.name(),
				"starting onPostExecute ", this.getClass().getSimpleName());
		try {
			pDialog.dismiss();
			//add to session
			//mInboxList
			secu.org.application.SecureMailSingleton.getSingletonObject().setInBoxMailList(mInboxList);
			secu.org.application.SecureMailSingleton.getSingletonObject().setOutBoxMailList(mOutboxList);
			// add the row click event
			mInboxActivity.buildList();
			mInboxActivity.setRowListener();

			
    		} catch(Exception e) {
    			secu.org.util.LoggerHelper.d(
    					secu.org.application.LoggingTag.INBOX.name(),
    					"user stopped onPostExecute ", this.getClass().getSimpleName());
    		}
	
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.INBOX.name(),
				"ending onPostExecute ", this.getClass().getSimpleName());
	}

	@Override
	protected String doInBackground(Context... param) {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.INBOX.name(),
				"starting doInBackground ", this.getClass().getSimpleName());
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		// mApplication.getBank()
		secu.org.ofx.OFXOperatorGetMessage ofxOperatorGetMessage = new secu.org.ofx.OFXOperatorGetMessage();
		try {
			List<secu.org.message.Mail> mailList = ofxOperatorGetMessage
					.ofxOperation(mBank);

			// the mail list contains inbox and outbox message
			// I am going to remove the outbox messages
			mInboxList = new ArrayList<secu.org.message.Mail>();
			
			String from = mInboxActivity.getString(R.string.mailfrom);
			for (secu.org.message.Mail mail : mailList) {
				if (!mail.getFrom().trim().equalsIgnoreCase(from)) {
					mOutboxList.add(mail);
				} else {
					mInboxList.add(mail);
				}

			}

			

		} catch (Exception e1) {
			secu.org.util.LoggerHelper.e(
					secu.org.application.LoggingTag.INBOX.name(),
					"error doInBackground ", this.getClass().getSimpleName());
		}
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.INBOX.name(),
				"ending doInBackground ", this.getClass().getSimpleName());
		return null;
	}
}
