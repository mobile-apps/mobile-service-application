package secu.org.task;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import secu.org.activity.AccountListActivity;
import secu.org.activity.LoginActivity;
import secu.org.application.BankApplication;
import secu.org.bank.Bank;
import secu.org.ofx.OFXOperator;
import secu.org.ofx.response.OFX;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class DownloadAccounts extends AsyncTask<Context, Void, HttpResponse> {
	int myProgress;
	/** progress dialog to show user that the download is processing. */
    private ProgressDialog dialog;
    private String userId;
    private String password;
    private OFX ofx = null;
    private Bank bank = null;
    private LoginActivity loginActivity = null;
	
    
	public DownloadAccounts(LoginActivity loginActivity, String userId, String password) {
		this.userId = userId;
		this.password = password;
		this.loginActivity = loginActivity;
	}
	@Override
	protected HttpResponse doInBackground(Context... arg0) {
		//HttpClient client = new DefaultHttpClient();
				AndroidHttpClient client = AndroidHttpClient.newInstance("Android");

				//HttpClient client = new AndroidHttpClient(); 
			    HttpPost post = new HttpPost("https://onlineaccess.ncsecu.org/secuofx/secu.ofx");
			    post.setHeader("Content-type", "application/x-ofx");
				secu.org.ofx.request.Statement testStatement = new secu.org.ofx.request.Statement();
				testStatement.setAccountId("4400781");
				testStatement.setAccountType(secu.org.account.AccountType.CHECKING);
				testStatement.setUserID(userId);
				testStatement.setPassword(password);
			  //  post.setHeader("Connection", "close");
				secu.org.ofx.OFXOperator connection = new secu.org.ofx.OFXOperator();
			    String request = "";//connection.createStatementRequest(testStatement);
			    StringEntity input = null;
				try {
					input = new StringEntity(request);
				    post.setEntity(input);
				    return client.execute(post);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			   // input.setContentType("application/x-ofx");
	
		// TODO Auto-generated method stub
		return null;
	}
	@Override
    protected void onPostExecute(HttpResponse result) {
		dialog.dismiss();
        //Do something with result
        if (result != null) {
        	
        }

    }
	// -- gets called just before thread begins
    @Override
    protected void onPreExecute() 
    {
    	dialog = new ProgressDialog(loginActivity);
    	dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    	dialog.setMessage("Down loading accounts. Please wait");
    	dialog.show();
 
    	Log.i( "ofx", "onPreExecute()" );
          
            super.onPreExecute();
            
    }

	

}
