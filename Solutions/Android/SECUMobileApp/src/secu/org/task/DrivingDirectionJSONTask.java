package secu.org.task;

import greendroid.widget.ItemAdapter;
import greendroid.widget.item.DescriptionItem;
import greendroid.widget.item.Item;
import greendroid.widget.item.SeparatorItem;
import greendroid.widget.item.SubtitleItem;
import greendroid.widget.item.ThumbnailItem;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;

import android.content.Context;
import android.os.AsyncTask;
import android.app.ProgressDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import secu.org.activity.R;
import secu.org.activity.LocatorActivity;
import secu.org.application.BankApplication;
import secu.org.locator.DrivingDirectionJSONParser;
import secu.org.locator.DrivingLeg;
import secu.org.locator.DrivingStep;
import secu.org.locator.LocatorJSONParser;
import secu.org.locator.LocatorResult;
import secu.org.ofx.OFXOperator;
import secu.org.session.ActivitySession;
import secu.org.util.MethodNameHelper;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class DrivingDirectionJSONTask extends AsyncTask<Context, Integer, String> {
	int myProgress;
	/** progress dialog to show user that the download is processing. */
	private final static Logger LOGGER = Logger.getLogger(DrivingDirectionJSONTask.class
			.getName());
	private ProgressDialog dialog;
	LocatorActivity mLocatorActivity;
	secu.org.locator.LocatorResult mLocatorResult;
	secu.org.locator.Location mLocation;
    List<DrivingStep> mDrivingStepList = new ArrayList<DrivingStep>();
    DrivingLeg mDrivingLeg;
	// contacts JSONArray
	JSONArray contacts = null;

	public DrivingDirectionJSONTask(LocatorActivity act,
			secu.org.locator.LocatorResult locatorResult, secu.org.locator.Location location) {
		mLocatorActivity = act;
		mLocatorResult = locatorResult;
		mLocation = location;
	}

	@Override
	protected void onPreExecute() {
		String name = new MethodNameHelper().getName();
		LOGGER.info("Starting Method [" + name + "]");
		dialog = new ProgressDialog(mLocatorActivity);
		dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		dialog.setMessage("Down loading driving directions. Please wait");
		dialog.show();

		Log.i("locator", "onPreExecute()");

		super.onPreExecute();

	}

	@Override
	protected String doInBackground(Context... params) {
		// Creating JSON Parser instance
		DrivingDirectionJSONParser drivingDirectionJSONParser = new DrivingDirectionJSONParser(mLocatorResult,mLocation);

		// locatorResult = eval(getJsonObjectPost(LocationSearchURL,
		// 'LocationType=' + 4 + '&Lat=' + 35.7976032 + '&Lng=' +
		// -78.65636030000001 +'&NumOfLocations=' + 5));
		// getting JSON string from URL
		JSONObject json = drivingDirectionJSONParser.getJSONFromUrl();
		try {
			String status = json.getString("status");
			JSONArray list = json.getJSONArray("routes");
			
			
			JSONObject jsonRouteObject =list.getJSONObject(0);
			JSONArray legsArray = jsonRouteObject.getJSONArray("legs");
			JSONObject jsonLegObject =legsArray.getJSONObject(0);
			JSONObject distanceJSONObject = jsonLegObject.getJSONObject(DrivingLeg.DISTANCE);
			
			String distance = distanceJSONObject.getString(DrivingLeg.TEXT);
			JSONObject durationJSONObject = jsonLegObject.getJSONObject(DrivingLeg.DURATION);
			String duration = durationJSONObject.getString(DrivingLeg.TEXT);
			String endAddress = jsonLegObject.getString(DrivingLeg.END_ADDRESS);
			String startAddress =jsonLegObject.getString(DrivingLeg.START_ADDRESS);
			mDrivingLeg = new DrivingLeg(distance, duration, startAddress,endAddress);
			JSONArray stepsArray = jsonLegObject.getJSONArray("steps");
			// loop thru list and create results set
			for (int x = 0; x < stepsArray.length(); x++) {
				JSONObject jsonStepObject =stepsArray.getJSONObject(x);
				JSONObject stepdistanceJSONObject = jsonStepObject.getJSONObject(DrivingStep.DISTANCE);
				String stepdistance = stepdistanceJSONObject.getString(DrivingStep.TEXT);
				
				JSONObject stepdurationJSONObject= jsonStepObject.getJSONObject(DrivingStep.DURATION);
				String stepduration= stepdurationJSONObject.getString(DrivingStep.TEXT);
				String htmlinstructions = jsonStepObject.getString(DrivingStep.HTML_INSTRUCTIONS);
				DrivingStep drivingStep = new DrivingStep(stepdistance, stepduration, htmlinstructions);
				mDrivingStepList.add(drivingStep);
			}
			mDrivingLeg.setmDrivingStepList(mDrivingStepList);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return null;
	}

    @Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		dialog.dismiss();
		//AccountTransactionActivity
        Intent myIntent = new Intent(this.mLocatorActivity, secu.org.activity.DrivingDirectionActivity.class);
		
			myIntent.putExtra(DrivingLeg.SESSION_NAME, mDrivingLeg);
			myIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE,
					"Driving Directions");

			
			this.mLocatorActivity.startActivity(myIntent);
		/*String hoursOpen = mLocatorActivity.getString(R.string.openhours);
		List<Item> items = new ArrayList<Item>();
		//items.add(new SubtitleItem("Starting address ", mLocation.getStartingAddress()));
		 items.add(new ThumbnailItem("Starting address", mLocation.getStartingAddress(), R.drawable.startingaddress));
	       
	       
		// items.add(new SeparatorItem(""));
		for (LocatorResult locatorResult : mLocatorActivity.getResultList()) {
			
			items.add(new SeparatorItem(""));
			int imageNumber = 0;
			if (locatorResult.isATM()) {
				imageNumber = R.drawable.atm;
			}
			if (locatorResult.isBranchWithATM()) {
				imageNumber = R.drawable.branchwithatm;
			}
			else
			{
				imageNumber = R.drawable.branchwithoutatm;
			}
	        items.add(new ThumbnailItem(locatorResult.toString(), locatorResult.getStreetAddress(), imageNumber));
	        items.add(new DescriptionItem(locatorResult.longDescription()));
	        
		}
		final ItemAdapter adapter = new ItemAdapter(mLocatorActivity, items);
		mLocatorActivity.setListAdapter(adapter);*/
		// import greendroid.widget.item.Item;
		// import greendroid.widget.item.SeparatorItem;

	}

}
