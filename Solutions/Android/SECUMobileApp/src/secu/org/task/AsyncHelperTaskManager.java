package secu.org.task;

import secu.org.asyn.IProgressTracker;
import secu.org.asyn.OnTaskCompleteListener;
import secu.org.asyn.Task;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.util.Log;

public final class AsyncHelperTaskManager implements IProgressTracker,
		OnCancelListener {

	private final OnTaskCompleteListener mTaskCompleteListener;
	private final ProgressDialog mProgressDialog;
	private AbstractTask mAsyncTask;


	public AsyncHelperTaskManager(Context context,
			OnTaskCompleteListener taskCompleteListener) {
		// Save reference to complete listener (activity)
		mTaskCompleteListener = taskCompleteListener;
		// Setup progress dialog
		mProgressDialog = new ProgressDialog(context);
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(true);
		mProgressDialog.setOnCancelListener(this);
	}

	public void setupTask(AbstractTask asyncTask) {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.ASYNC.name(),
				"starting setupTask", this.getClass().getSimpleName());
		// Keep task
		mAsyncTask = asyncTask;
		// Wire task to tracker (this)
		mAsyncTask.setProgressTracker(this);
		// Start task
		mAsyncTask.execute();
		// Notify activity about completion
		//mTaskCompleteListener.onTaskComplete(mAsyncTask);
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.ASYNC.name(),
				"ending setupTask", this.getClass().getSimpleName());
	}

	@Override
	public void onProgress(String message) {
		// Show dialog if it wasn't shown yet or was removed on configuration
		// (rotation) change
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.ASYNC.name(),
				"starting onProgress", this.getClass().getSimpleName());
		if (mProgressDialog != null) {
			//if (!mProgressDialog.isShowing()) {
				//mProgressDialog.show();
			//}
			// Show current message in progress dialog
			//mProgressDialog.setMessage(message);
		}
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.ASYNC.name(),
				"ending onProgress", this.getClass().getSimpleName());
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.ASYNC.name(),
				"starting onCancel", this.getClass().getSimpleName());
		// Cancel task
		mAsyncTask.cancel(true);
		// Notify activity about completion
		mTaskCompleteListener.onTaskComplete(mAsyncTask);
		// Reset task
		mAsyncTask = null;
		secu.org.util.LoggerHelper
				.d(secu.org.application.LoggingTag.ASYNC.name(),
						"ending onCancel", this.getClass().getSimpleName());
	}

	@Override
	public void onComplete() {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.ASYNC.name(),
				"starting onComplete", this.getClass().getSimpleName());
		// Close progress dialog
		mProgressDialog.dismiss();
		// Notify activity about completion
		mTaskCompleteListener.onTaskComplete(mAsyncTask);
		// Reset task
		mAsyncTask = null;
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.ASYNC.name(),
				"ending onComplete", this.getClass().getSimpleName());
	}

	public Object retainTask() {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.ASYNC.name(),
				"starting retainTask", this.getClass().getSimpleName());
		// Detach task from tracker (this) before retain
		if (mAsyncTask != null) {
			mAsyncTask.setProgressTracker(null);
		}
		// Retain task
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.ASYNC.name(),
				"ending retainTask", this.getClass().getSimpleName());
		return mAsyncTask;
	}

	public void handleRetainedTask(Object instanceObj) {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.ASYNC.name(),
				"starting handleRetainedTask", this.getClass().getSimpleName());
		if (instanceObj != null) {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.ASYNC.name(),
				"Null Object instance ", this.getClass().getSimpleName());
		} else {
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.ASYNC.name(),
					"Object instance not null [" + instanceObj + "]", this.getClass().getSimpleName());
		}
		// Restore retained task and attach it to tracker (this)
		if (instanceObj instanceof AsyncHelperTaskManager) {
			mAsyncTask = (AbstractTask) instanceObj;
			mAsyncTask.setProgressTracker(this);
		}
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.ASYNC.name(),
				"ending handleRetainedTask", this.getClass().getSimpleName());
	}

	public boolean isWorking() {
		// Track current status
		return mAsyncTask != null;
	}
}