package secu.org.task;

import java.util.logging.Logger;

import secu.org.activity.R;
import secu.org.activity.AccountListActivity;
import secu.org.activity.LoginActivity;
import secu.org.application.BankApplication;
import secu.org.application.BankSingleton;
import secu.org.application.ResourceEnumeration;
import secu.org.asyn.IProgressTracker;
import secu.org.bank.Bank;
import secu.org.bank.LogonStatusEnumeration;
import secu.org.ofx.OFXOperator;
import secu.org.ofx.response.OFX;
import secu.org.util.MethodNameHelper;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class OFXDownloadTask extends AbstractTask {
	protected final Resources mResources;

	private Boolean mResult;
	private String mProgressMessage;
	private IProgressTracker mProgressTracker;

	int myProgress;

	private ProgressDialog dialog;
	private String userId;
	private String password;
	private OFX ofx = null;
	private Bank bank = null;
	private LoginActivity loginActivity = null;
	BankApplication mBankApplication;

	public OFXDownloadTask(LoginActivity loginActivity, String userId,
			String password, BankApplication bankApplication,
			Resources resources) {
		super(loginActivity, userId,
			password, bankApplication,
			resources);
		this.userId = userId;
		this.password = password;
		this.loginActivity = loginActivity;
		this.mBankApplication = bankApplication;
		// Keep reference to resources
		mResources = resources;
		// Initialise initial pre-execute message
		mProgressMessage = resources
				.getString(secu.org.activity.R.string.task_starting);
	}

	@Override
	protected String doInBackground(Context... params) {

		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.LOGON.name(),
				"starting doInBackground ", this.getClass().getSimpleName());
		// TODO Auto-generated method stub
       // android.os.Debug.startMethodTracing("download");
		String returnMessage = "GOOD";
		OFXOperator OFXConnection = new OFXOperator();
		try {
			//bank = OFXConnection.downLoadBankAccountsAndStatements(userId,
				//	password);
			bank = OFXConnection.downLoadBankAccounts(userId, password);

		} catch (Exception ex) {
			bank = null;
			Log.i("bank error[", ex.getMessage() + "]");

		}
       // android.os.Debug.stopMethodTracing();

		secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOGON.name(),
                "ending doInBackground ", this.getClass().getSimpleName());
		return returnMessage;
	}

	// -- gets called just before thread begins
	@Override
	protected void onPreExecute() {

		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.LOGON.name(),
				"starting onPreExecute ", this.getClass().getSimpleName());
        String downloadMessage = this.loginActivity.getResources().getString(R.string.account_downloading);
		dialog = new ProgressDialog(loginActivity);
		dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		dialog.setMessage(downloadMessage);
		dialog.show();

		super.onPreExecute();
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.LOGON.name(),
				"ending onPreExecute ", this.getClass().getSimpleName());

	}

	// -- called as soon as doInBackground method completes
	// -- notice that the third param gets passed to this method
	@Override
	protected void onPostExecute(String result) {

		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.LOGON.name(),
				"starting onPostExecute ", this.getClass().getSimpleName());
		super.onPostExecute(result);
		try {
		dialog.dismiss();
		} catch(Exception e) {
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.LOGON.name(),
					"user stopped onPostExecute ", this.getClass().getSimpleName());
		}
		if (bank == null) {
			// this.loginActivity.setHasInvalidCredentials(true);
			TextView textView = (TextView) this.loginActivity
					.findViewById(R.id.textmessage);
         	textView.setTextColor(Color.RED);
			textView.setText(R.string.invalidcredentials);
			Toast.makeText(this.loginActivity.getApplicationContext(),
                    ResourceEnumeration.INVALID_CREDENTIALS.getValue(), Toast.LENGTH_LONG).show();
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.LOGON.name(),
					"invalid user onPostExecute ", this.getClass().getSimpleName());
			return;
		}
		Intent myIntent = new Intent(this.loginActivity,
				AccountListActivity.class);
		if (bank.getLogonStatus().equals(LogonStatusEnumeration.LogonSuccessful)) {

			myIntent.putExtra(BankApplication.GD_ACTION_BAR_TITLE,
                    ResourceEnumeration.ACCOUNT_SUMMARY_ACTIVITY_TITLE.getValue());
            bank.setUserId(userId);
            bank.setPassword(password);
			BankSingleton.getSingletonObject().setBank(bank);
			this.loginActivity.startActivity(myIntent);
		} else {
			int messageInt = R.string.invalidcredentials;
			switch(bank.getLogonStatus()) 
			{
			case FailedAttempts:
				messageInt = R.string.failedattempts;
				break;
			case InvalidCredentials:
				messageInt = R.string.invalidcredentials;
				break;
			default:
				messageInt = R.string.invalidcredentials;
				break;
			}
			
			
			// this.loginActivity.setHasInvalidCredentials(true);
			TextView textView = (TextView) this.loginActivity
					.findViewById(R.id.textmessage);

			textView.setTextColor(Color.RED);
			textView.setText(messageInt);
			Toast.makeText(this.loginActivity.getApplicationContext(),
					this.loginActivity.getString(messageInt), Toast.LENGTH_LONG).show();
		}

		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.LOGON.name(),
				"ending onPostExecute ", this.getClass().getSimpleName());

	}

	
	/* UI Thread */
	public void setProgressTracker(IProgressTracker progressTracker) {
		// Attach to progress tracker
		mProgressTracker = progressTracker;
		// Initialise progress tracker with current task state
		if (mProgressTracker != null) {
			mProgressTracker.onProgress(mProgressMessage);
			if (mResult != null) {
				mProgressTracker.onComplete();
			}
		}
	}

}
