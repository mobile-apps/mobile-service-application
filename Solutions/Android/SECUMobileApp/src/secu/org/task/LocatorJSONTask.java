package secu.org.task;

import greendroid.widget.ItemAdapter;
import greendroid.widget.item.DescriptionItem;
import greendroid.widget.item.Item;
import greendroid.widget.item.SeparatorItem;
import greendroid.widget.item.SubtitleItem;
import greendroid.widget.item.ThumbnailItem;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.client.*;
import android.location.Address;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;

import android.content.Context;
import android.os.AsyncTask;
import android.app.ProgressDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import secu.org.activity.R;
import secu.org.locator.LocatorJSONParser;
import secu.org.locator.LocatorResult;
import secu.org.ofx.OFXOperator;
import secu.org.util.MethodNameHelper;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import android.widget.Toast;
import secu.org.activity.LocatorActivity;
import secu.org.application.*;

public class LocatorJSONTask extends AbstractTask {
	int myProgress;
	/** progress dialog to show user that the download is processing. */
	private final static Logger LOGGER = Logger.getLogger(LocatorJSONTask.class
			.getName());
	private ProgressDialog dialog;
	LocatorActivity mLocatorActivity;
	secu.org.locator.Location mLocation;
	private boolean mEncounterError = false;

	// contacts JSONArray
	JSONArray contacts = null;

	public LocatorJSONTask(LocatorActivity act, String userId,
			String password, secu.org.locator.Location location, Resources resources) {
		super(act, userId,
				password, null,
				resources);
		mLocatorActivity = act;
		mLocation = location;
	}

	@Override
	protected void onPreExecute() {
		String name = new MethodNameHelper().getName();
		LOGGER.info("Starting Method [" + name + "]");
		dialog = new ProgressDialog(mLocatorActivity);
		dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		dialog.setMessage("Downloading nearest ATM/Branches. Please wait");
		dialog.show();

		Log.i("locator", "onPreExecute()");

		super.onPreExecute();

	}

    /**
     * get address from google because native GeCodeder did not work
     * @return
     */
    private String getAddressFromGoogle() {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "starting getAddressFromGoogle", this.getClass().getSimpleName());
        String latlng = mLocation.getLatitudeString() + "," + mLocation.getLongitudeString();
        String address = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+ latlng + "&sensor=true";
        String fullAddress = "";
        HttpGet httpGet = new HttpGet(address);
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        StringBuilder stringBuilder = new StringBuilder();
        List<android.location.Address> retList = null;

        try {
            response = client.execute(httpGet);
            org.apache.http.HttpEntity entity = response.getEntity();
            java.io.InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
            JSONObject jsonObject = new JSONObject();
            String test = stringBuilder.toString();
            jsonObject = new JSONObject(stringBuilder.toString());
            retList = new ArrayList<Address>();
            if ("OK".equalsIgnoreCase(jsonObject.getString("status"))) {
                JSONArray results = jsonObject.getJSONArray("results");
                for (int i = 0; i < results.length(); i++) {
                    JSONObject result = results.getJSONObject(i);
                    fullAddress  = result.getString("formatted_address");
                   break;
                }
            }



        } catch (Exception e) {
            secu.org.util.LoggerHelper.e(
                    secu.org.application.LoggingTag.LOCATION.name(),
                    "error getAddressFromGoogle " + e.getMessage(), this.getClass().getSimpleName());
            e.printStackTrace();

        }
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "ending getAddressFromGoogle", this.getClass().getSimpleName());
        return fullAddress;
    }

	@Override
	protected String doInBackground(Context... params) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "starting doInBackground", this.getClass().getSimpleName());
		// Creating JSON Parser instance
		LocatorJSONParser locatorJSONParser = new LocatorJSONParser(mLocation);
        if (mLocation.getStartingAddress() == null) {
            String fullAddress =getAddressFromGoogle();
            mLocation.setStartingAddress(fullAddress);
        }

		// locatorResult = eval(getJsonObjectPost(LocationSearchURL,
		// 'LocationType=' + 4 + '&Lat=' + 35.7976032 + '&Lng=' +
		// -78.65636030000001 +'&NumOfLocations=' + 5));
		// getting JSON string from URL
		JSONObject json = null;
		try {
			json = locatorJSONParser.getJSONFromUrl();
			JSONArray ResultIndexNameOrder = json
					.getJSONArray("ResultIndexNameOrder");
			JSONArray list = json.getJSONArray("List");

			// loop thru list and create results set
			for (int x = 0; x < list.length(); x++) {
				JSONObject jsonObject = list.getJSONObject(x);
				String distance = jsonObject.getString(LocatorResult.DISTANCE);
				String atm24hours = jsonObject
						.getString(LocatorResult.ATM24HOURS);
				String email = jsonObject.getString(LocatorResult.EMAIL);
				String latitude = jsonObject.getString(LocatorResult.LATITUDE);
				String locationName = jsonObject
						.getString(LocatorResult.LOCATIONAME);
				String longitude = jsonObject
						.getString(LocatorResult.LONGITUDE);
				JSONObject mailingObject = null;
				StringBuilder mailingAddressBuilder = new StringBuilder();
				try {
					mailingObject = jsonObject
							.getJSONObject(LocatorResult.MAILINGADDRESS);
					if (mailingObject != null) {
						mailingAddressBuilder.append(mailingObject
								.getString(LocatorResult.STREET));
						mailingAddressBuilder.append(" ");
						mailingAddressBuilder.append(mailingObject
								.getString(LocatorResult.CITY));
						mailingAddressBuilder.append(" ");
						mailingAddressBuilder.append(mailingObject
								.getString(LocatorResult.STATE));
						mailingAddressBuilder.append(" ");
						mailingAddressBuilder.append(mailingObject
								.getString(LocatorResult.ZIP));
					}
				} catch (Exception ex) {
					mailingAddressBuilder.append("");
				}

				JSONObject streetAddressObject = jsonObject
						.getJSONObject(LocatorResult.STREETADDRESS);
				StringBuilder streetAddressBuilder = new StringBuilder();
				streetAddressBuilder.append(streetAddressObject
						.getString(LocatorResult.STREET));
				streetAddressBuilder.append(" ");
				streetAddressBuilder.append(streetAddressObject
						.getString(LocatorResult.CITY));
				streetAddressBuilder.append(" ");
				streetAddressBuilder.append(streetAddressObject
						.getString(LocatorResult.STATE));
				streetAddressBuilder.append(" ");
				streetAddressBuilder.append(streetAddressObject
						.getString(LocatorResult.ZIP));

				String telephoneNumbers = jsonObject
						.getString(LocatorResult.TELEPONENUMBERS);
				String type = jsonObject.getString(LocatorResult.TYPE);
				String typeId = jsonObject.getString(LocatorResult.TYPEID);

				LocatorResult locatorResult = new LocatorResult(distance, type,
						atm24hours, mailingAddressBuilder.toString(), typeId,
						email, locationName, "", "", latitude, longitude,
						streetAddressBuilder.toString());
				mLocatorActivity.getResultList().add(locatorResult);

			}
			JSONObject xyz = list.getJSONObject(1);
			String dist = xyz.getString("Distance");
			String LocationName = xyz.getString("LocationName");

			int a = 0;
		} catch (Exception e) {
			// TODO Auto-generated catch block
            secu.org.util.LoggerHelper.e(
                    secu.org.application.LoggingTag.LOCATION.name(),
                    "error doInBackground " + e.getMessage(), this.getClass().getSimpleName());
			mEncounterError = true;
			e.printStackTrace();
		}
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "ending doInBackground", this.getClass().getSimpleName());
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.LOCATION.name(),
                "starting onPostExecute", this.getClass().getSimpleName());
		try {
			dialog.dismiss();
			} catch(Exception e) {
				secu.org.util.LoggerHelper.d(
						secu.org.application.LoggingTag.LOGON.name(),
						"user stopped onPostExecute ", this.getClass().getSimpleName());
			}
		
		if (!mEncounterError) {
			String hoursOpen = mLocatorActivity.getString(R.string.openhours);
			List<Item> items = new ArrayList<Item>();
			// items.add(new SubtitleItem("Starting address ",
			// mLocation.getStartingAddress()));
			items.add(new ThumbnailItem("Starting address", mLocation
					.getStartingAddress(), R.drawable.startingaddress));

			// items.add(new SeparatorItem(""));
			for (LocatorResult locatorResult : mLocatorActivity.getResultList()) {

				items.add(new SeparatorItem(""));
				int imageNumber = 0;
				if (locatorResult.isATM()) {
					imageNumber = R.drawable.atm;
				}
				if (locatorResult.isBranchWithATM()) {
					imageNumber = R.drawable.branchwithatm;
				} else {
					imageNumber = R.drawable.branchwithoutatm;
				}
				items.add(new ThumbnailItem(locatorResult.toString(),
						locatorResult.getStreetAddress(), imageNumber));
				items.add(new DescriptionItem(locatorResult.longDescription()));

			}
			final ItemAdapter adapter = new ItemAdapter(mLocatorActivity, items);
			mLocatorActivity.setListAdapter(adapter);
            secu.org.util.LoggerHelper.d(
                    secu.org.application.LoggingTag.LOCATION.name(),
                    "ending onPostExecute", this.getClass().getSimpleName());
		} else {
			//has error
			Alert alert = new Alert("Error", "Can not connect to the internet");
			AlertManager manager = new AlertManager(mLocatorActivity);
			manager.showAlert(alert);
            secu.org.util.LoggerHelper.d(
                    secu.org.application.LoggingTag.LOCATION.name(),
                    "error onPostExecute", this.getClass().getSimpleName());
		}
		// import greendroid.widget.item.Item;
		// import greendroid.widget.item.SeparatorItem;

	}

}
