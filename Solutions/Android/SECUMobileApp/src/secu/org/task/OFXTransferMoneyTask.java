package secu.org.task;

import java.math.BigDecimal;

import secu.org.activity.R;
import secu.org.account.Account;
import secu.org.account.AccountTransfer;
import secu.org.activity.AccountListActivity;
import secu.org.activity.LoginActivity;
import secu.org.activity.MoveMoneyActivity;
import secu.org.application.BankApplication;
import secu.org.application.BankSingleton;
import secu.org.bank.Bank;
import secu.org.ofx.OFXOperator;
import secu.org.ofx.response.OFX;
import android.view.ViewGroup.LayoutParams;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class OFXTransferMoneyTask extends AbstractTask {
	int myProgress;
	/** progress dialog to show user that the download is processing. */
	private ProgressDialog dialog;
	private AccountTransfer mAccountTransfer;
	protected final Resources mResources;
	private OFX ofx = null;
	private Bank bank = null;
	private MoveMoneyActivity mMoveMoneyActivity = null;
	BankApplication mBankApplication;

	public OFXTransferMoneyTask(MoveMoneyActivity moveMoneyActivity, String userId, String password,
			AccountTransfer accountTransfer, Resources resources) {
		super(moveMoneyActivity, userId,
				password, null,
				resources);
		this.mAccountTransfer = accountTransfer;
		this.mMoveMoneyActivity = moveMoneyActivity;
		// Keep reference to resources
		this.mResources = resources;
		//this.mBankApplication = bankApplication;
		
		
	}

	@Override
	protected String doInBackground(Context... params) {
		// TODO Auto-generated method stub
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.MOVEMONEY.name(),
				"starting doInBackground ", this.getClass().getSimpleName());
		String returnMessage = "GOOD";
		OFXOperator OFXConnection = new OFXOperator();
		try {
			mAccountTransfer = OFXConnection.transferMoney(mAccountTransfer);
			//bank = OFXConnection.downLoadBankAccountsAndStatements(mAccountTransfer.getBank().getUserId(), mAccountTransfer.getBank().getPassword());
			//mBankApplication.setBank(bank);
		} catch (Exception ex) {
			Log.i("transfer error[", ex.getMessage() + "]");

		}
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.MOVEMONEY.name(),
				"ending doInBackground ", this.getClass().getSimpleName());
		return returnMessage;
	}

	// -- gets called just before thread begins
	@Override
	protected void onPreExecute() {
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.MOVEMONEY.name(),
				"starting onPreExecute ", this.getClass().getSimpleName());
		dialog = new ProgressDialog(mMoveMoneyActivity);
		dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		dialog.setMessage("Transferring money. Please wait");
		try {
		dialog.show();
		} catch (Exception e) {
			secu.org.util.LoggerHelper.d(
					secu.org.application.LoggingTag.MOVEMONEY.name(),
					"error onPreExecute ", this.getClass().getSimpleName());
		}

		Log.i("ofx", "onPreExecute()");
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.MOVEMONEY.name(),
				"ending onPreExecute ", this.getClass().getSimpleName());
		super.onPreExecute();

	}

	// -- called as soon as doInBackground method completes
	// -- notice that the third param gets passed to this method
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.MOVEMONEY.name(),
				"starting onPostExecute ", this.getClass().getSimpleName());
		try{
		dialog.dismiss();
		} catch (Exception e) {
			secu.org.util.LoggerHelper.e(
					secu.org.application.LoggingTag.MOVEMONEY.name(),
					"error onPostExecute ", this.getClass().getSimpleName());
		}
		if (mAccountTransfer == null || !mAccountTransfer.hasBeenTransfer()) {
			Toast.makeText(this.mMoveMoneyActivity.getApplicationContext(),
					"Money transfer encountered error"  , Toast.LENGTH_LONG)
					.show();
			TextView textView = (TextView) this.mMoveMoneyActivity.findViewById(R.id.textmessage);
            String message = "Money transfer encountered error " + mAccountTransfer.getMessage() ;
			textView.setTextColor(Color.RED);
			textView.setText(message);
			return;
		}
		final EditText amountEditText = (EditText)mMoveMoneyActivity.findViewById(R.id.EditTextAmount);
		// display popup confirmation
		LayoutInflater layoutInflater = (LayoutInflater) mMoveMoneyActivity
				.getBaseContext().getSystemService(
						mMoveMoneyActivity.LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(R.layout.transferconfirmation,
				null);

		final PopupWindow popupWindow = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

		Button btnDismiss = (Button) popupView.findViewById(R.id.dismiss);
		TextView statusTextView = (TextView)popupView.findViewById(R.id.statustext);
		statusTextView.setText("Confirmation number " + mAccountTransfer.getConfirmationNumber());
		//instead of a server call, perform local calculation of accounts 
		performAccountCalculation();
		
		Button btnOpenPopup = (Button) mMoveMoneyActivity
				.findViewById(R.id.btnSubmit);
		btnDismiss.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				popupWindow.dismiss();
				amountEditText.setText("");
				
			}
		});

		popupWindow.showAsDropDown(btnOpenPopup, 50, -30);

		secu.org.util.LoggerHelper.d(
				secu.org.application.LoggingTag.MOVEMONEY.name(),
				"ending onPostExecute ", this.getClass().getSimpleName());

	}
	/*
	 * round number down and by 2 digit
	 */
	private double round(double unrounded) {
		BigDecimal unRoundedDecimal = new BigDecimal(unrounded);
		BigDecimal roundedDecimal = unRoundedDecimal.setScale(2, BigDecimal.ROUND_DOWN);
		return roundedDecimal.doubleValue();
	}
	/**
	 * perform local calculation to save processing time
	 * and update accounts in session
	 * 
	 */
    private void performAccountCalculation() {
    	double amount = Double.valueOf(mAccountTransfer.getAmount());
    	double fromAccountAmount = Double.valueOf(mAccountTransfer.getFromAccount().getRawAccountBalance());
    	double toAccountAmount = Double.valueOf(mAccountTransfer.getToAccount().getRawAccountBalance());
    	
    	//add amount to toAccount
    	toAccountAmount = round( (toAccountAmount + amount));
    	//substract amount 
    	fromAccountAmount = round ((fromAccountAmount - amount));
    	//update session with new amounts
    	Account fromAccount = BankSingleton.getSingletonObject().getBank().getAccountList().get(mAccountTransfer.getFromAccountIndex());
    	fromAccount.setAccountBalance(Double.toString(fromAccountAmount));
    	BankSingleton.getSingletonObject().getBank().getAccountList().set(mAccountTransfer.getFromAccountIndex(), fromAccount);
    	
       	Account toAccount = BankSingleton.getSingletonObject().getBank().getAccountList().get(mAccountTransfer.getToAccountIndex());
    	toAccount.setAccountBalance(Double.toString(toAccountAmount));
    	BankSingleton.getSingletonObject().getBank().getAccountList().set(mAccountTransfer.getToAccountIndex(), toAccount);

    	//refresh dropdown list
    	this.mMoveMoneyActivity.addItemsOnSpinnerToAccounts();
    	this.mMoveMoneyActivity.addItemsOnSpinnerFromAccounts();
    }
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		// TODO Auto-generated method stub

	}

}
