package secu.org.task;

import secu.org.application.BankApplication;
import secu.org.asyn.IProgressTracker;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;

public abstract class AbstractTask extends AsyncTask<Context, Integer, String> {
	protected final Resources mResources;

	private Boolean mResult;
	private String mProgressMessage;
	private IProgressTracker mProgressTracker;

	int myProgress;
	
	private ProgressDialog dialog;
	

	public AbstractTask(Activity loginActivity, String userId,
			String password, Object bankApplication,
			Resources resources) {

		// Keep reference to resources
		mResources = resources;
		// Initialise initial pre-execute message
		mProgressMessage = resources
				.getString(secu.org.activity.R.string.task_starting);
	}

	@Override
	protected abstract String doInBackground(Context... params) ;

	// -- gets called just before thread begins
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	// -- called as soon as doInBackground method completes
	// -- notice that the third param gets passed to this method
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		// TODO Auto-generated method stub

	}

	/* UI Thread */
	public void setProgressTracker(IProgressTracker progressTracker) {
		// Attach to progress tracker
		mProgressTracker = progressTracker;
		// Initialise progress tracker with current task state
		if (mProgressTracker != null) {
			mProgressTracker.onProgress(mProgressMessage);
			if (mResult != null) {
				mProgressTracker.onComplete();
			}
		}
	}

}
