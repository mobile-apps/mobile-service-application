package secu.org.web;

import android.app.Activity;
import android.net.Uri;
import android.net.http.SslError;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class HTTPsWebView extends WebViewClient {
	
	Activity mActivity;
	public HTTPsWebView(Activity activity) {
		mActivity=activity;
	}
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (Uri.parse(url).getHost().contains("tx234217.ncsecu.org")) {
            // This is your web site, so do not override; let the WebView to load the page
            return false;
        }
        // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
       // Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        
       // mActivity.startActivity(intent);
        return false;
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
    {
    	super.onReceivedError(view, errorCode, description, failingUrl);

    }
    
    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        super.onReceivedSslError(view, handler, error);

        // this will ignore the Ssl error and will go forward to your site
        handler.proceed();
    }
}

