package secu.org.google.cloud.message;

import secu.org.activity.R;
import secu.org.activity.SecurityControllerActivity;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    String TAG = "GcmIntentService";
    String mType;
    MessageTypeEnumeration mMessageTypeEnumeration;
    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "starting onHandleIntent ", this.getClass().getSimpleName());
        Bundle extras = intent.getExtras();

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " +
                        extras.toString());
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // This loop represents the service doing some work.


                // Post notification of received message.
                String message = intent.getStringExtra("message");
                try {
                mType = intent.getStringExtra("type").toUpperCase();
                mMessageTypeEnumeration = MessageTypeEnumeration.valueOf(mType);
                } catch (Exception e) {
                    secu.org.util.LoggerHelper.d(
                            secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                            "onHandleIntent Error  " + e.getMessage(), this.getClass().getSimpleName());
                }
                String title = intent.getStringExtra("title");
                GCMMessage gcmMessage = new GCMMessage();
                gcmMessage.setTitle(title);
                gcmMessage.setMessage(message);
                if (message != null  && mType != null) {
                    handleMessage(gcmMessage);
                } else {
                    sendNotification("Received: " + extras.toString());
                }

                //sendNotification("Received: " + extras.toString());
                Log.d(TAG, "Received: " + extras.toString());
            }
        }
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "ending onHandleIntent ", this.getClass().getSimpleName());
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);

    }

    /**
     * handle the message and record the message
     * @param gcmMessage
     */
    private void handleMessage(GCMMessage gcmMessage) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "starting handleMessage ", this.getClass().getSimpleName());
        secu.org.database.DatabaseActivityHelper databaseHelper = new secu.org.database.DatabaseActivityHelper(this);
        //write message
        databaseHelper.createRecords(gcmMessage.getMessage(),mMessageTypeEnumeration );
        switch (mMessageTypeEnumeration) {
            case NOTIFICATION:
                NotificationMessengerHandler.refresh(this, gcmMessage.getMessage());
                gcmMessage.setMessage(gcmMessage.getTitle());
                sendNotification(gcmMessage);

                break;
            case SECURITY:
                break;
            case RATESFEES:
                sendNotification(gcmMessage);
                RatesAndFeesMessengerHandler ratesAndFeesMessengerHandler = new RatesAndFeesMessengerHandler();
                ratesAndFeesMessengerHandler.refresh(this);

                break;

        }  //end of switch

        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "ending handleMessage ", this.getClass().getSimpleName());
    }


    /*
    Send Notification
     */
    private void sendNotification(GCMMessage gcmMessage) {
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "starting sendNotification ", this.getClass().getSimpleName());
            mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, SecurityControllerActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.icon)
                        .setContentTitle(gcmMessage.getTitle())
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(gcmMessage.getMessage()))
                        .setContentText(gcmMessage.getMessage());

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        secu.org.util.LoggerHelper.d(
                secu.org.application.LoggingTag.GOOGLEMESSAGE.name(),
                "ending sendNotification ", this.getClass().getSimpleName());

    }



    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, SecurityControllerActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.icon)
                        .setContentTitle("Online Access")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        //RatesAndFeesMessengerHandler ratesAndFeesMessengerHandler = new RatesAndFeesMessengerHandler();
        //ratesAndFeesMessengerHandler.refresh(this);
    }



}