package secu.org.google.cloud.message;

import android.net.http.AndroidHttpClient;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.params.HttpConnectionParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import secu.org.application.LoggingTag;
import secu.org.ratesfees.LendingRatesSingleton;

/**
 * Created by s16715d on 11/18/13.
 */
public class NotificationMessengerHandler {

    private static final  String filename = "notification.xml";

    /**
     * Convert the inputStream to string
     * @param is
     * @return
     */
    private static String convertStreamToString(InputStream is) {
        secu.org.util.LoggerHelper.d(
                LoggingTag.GOOGLEMESSAGE.name(),
                "starting convertStreamToString", NotificationMessengerHandler.class.getSimpleName());
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        secu.org.util.LoggerHelper.d(
                LoggingTag.GOOGLEMESSAGE.name(),
                "ending convertStreamToString", NotificationMessengerHandler.class.getSimpleName());
        return sb.toString();
    }


    /**
     * read the notice message from local storage
     * @param context
     * @return
     */
    public static String readFromFile(android.content.Context context) {
        secu.org.util.LoggerHelper.d(
                LoggingTag.GOOGLEMESSAGE.name(),
                "starting readFromFile", NotificationMessengerHandler.class.getSimpleName());
        String message = "";
        java.io.InputStream inputStream = null;
        try {
            inputStream = context.openFileInput(filename);
            message = convertStreamToString(inputStream);

        } catch (java.io.FileNotFoundException e) {
            android.util.Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            android.util.Log.e("login activity", "Can not read file: " + e.toString());
        }
        secu.org.util.LoggerHelper.d(
                LoggingTag.GOOGLEMESSAGE.name(),
                "ending readFromFile", NotificationMessengerHandler.class.getSimpleName());

        return message;
    }


    public static void refresh(GcmIntentService gcmIntentService, String message) {
        secu.org.util.LoggerHelper.d(
                LoggingTag.GOOGLEMESSAGE.name(),
                "starting refresh", NotificationMessengerHandler.class.getSimpleName());
        try {
            java.io.FileOutputStream outputStream;
            outputStream = gcmIntentService.openFileOutput(filename, android.content.Context.MODE_PRIVATE);
            outputStream.write(message.getBytes());
            outputStream.close();
            //write to preference
            secu.org.application.PreferenceManagerSingleton.getSingletonObject().enableNotification(gcmIntentService);


        } catch (IOException e) {
            secu.org.util.LoggerHelper.e(
                    LoggingTag.GOOGLEMESSAGE.name(),
                    "Error " + e.getMessage(), NotificationMessengerHandler.class.getSimpleName());
        }
        secu.org.util.LoggerHelper.d(
                LoggingTag.GOOGLEMESSAGE.name(),
                "ending refresh", NotificationMessengerHandler.class.getSimpleName());


    }
}
