package secu.org.google.cloud.message;

import java.io.IOException;

import secu.org.activity.SecurityControllerActivity;
import secu.org.application.BankSingleton;
import secu.org.service.AlertService;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

public class RegistrationTask extends AsyncTask<String, Void, String> {
	SecurityControllerActivity mSecurityControllerActivity;
	
  public RegistrationTask(SecurityControllerActivity mainActivity) {
	  mSecurityControllerActivity = mainActivity;
  }

	@Override
	protected String doInBackground(String... arg0) {
		 String msg = "";
         try {
             if (mSecurityControllerActivity.gcm == null) {
            	 mSecurityControllerActivity.gcm =
             }
             String regid =mSecurityControllerActivity.gcm.register(mSecurityControllerActivity.SENDER_ID);
             msg = "Device registered, registration ID=" + regid;

             // You should send the registration ID to your server over HTTP,
             // so it can use GCM/HTTP or CCS to send messages to your app.
             // The request to your server should be authenticated if your app
             // is using accounts.
             sendRegistrationIdToBackend(regid);

             // For this demo: we don't need to send it because the device
             // will send upstream messages to a server that echo back the
             // message using the 'from' address in the message.

             // Persist the regID - no need to register again.
             mSecurityControllerActivity.storeRegistrationId(mSecurityControllerActivity, regid);
         } catch (IOException ex) {
             msg = "Error :" + ex.getMessage();
             // If there is an error, don't just keep trying to register.
             // Require the user to click a button again, or perform
             // exponential back-off.
         }
         return msg;

	}
	private void sendRegistrationIdToBackend( String regid ) {

	      // Your implementation here.
	    }

    @Override
    protected void onPostExecute(String msg) {
    	//mMainActivity.mDisplay.append(msg + "\n");
    }


}
