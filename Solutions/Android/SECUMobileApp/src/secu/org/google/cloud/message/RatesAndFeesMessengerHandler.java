package secu.org.google.cloud.message;

import android.app.IntentService;
import android.content.Context;
import android.net.http.AndroidHttpClient;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.params.HttpConnectionParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import secu.org.application.BankApplication;
import secu.org.application.ResourceEnumeration;
import secu.org.ratesfees.LendingRatesSingleton;

/**
 * Created by s16715d on 11/18/13.
 */
public class RatesAndFeesMessengerHandler {






    public void refresh(IntentService gcmIntentService) {

        AndroidHttpClient client = null;
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        client = AndroidHttpClient.newInstance("Android");

        HttpConnectionParams
                .setConnectionTimeout(client.getParams(), 15000);
        HttpConnectionParams.setSoTimeout(client.getParams(), 5000);
        // HttpClient client = new AndroidHttpClient();
        HttpGet get = new HttpGet(ResourceEnumeration.RATES_SERVER_URL.getValue());
        try {
            HttpResponse response = client.execute(get);
            br = new BufferedReader(new InputStreamReader(
                    (response.getEntity().getContent())));
            String output;

            while ((output = br.readLine()) != null) {
                sb.append(output);
            }
            br.close();
            client.getConnectionManager().shutdown();
            String test = sb.toString();
            // client.close();
            client = null;

            //save file
            // java.io.File file = new java.io.File(gcmIntentService.getFilesDir(), "aa.xml");


            java.io.FileOutputStream outputStream;
            outputStream = BankApplication.getAppContext().openFileOutput(LendingRatesSingleton.filename, Context.MODE_PRIVATE);



            outputStream.write(sb.toString().getBytes());
            outputStream.close();









        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
