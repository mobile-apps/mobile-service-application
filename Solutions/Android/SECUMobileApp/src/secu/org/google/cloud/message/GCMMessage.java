package secu.org.google.cloud.message;

/**
 * Created by s16715d on 11/22/13.
 */
public class GCMMessage {
    private String title;
    private String longMessage;

    public String getLongMessage() {
        return longMessage;
    }

    public void setLongMessage(String longMessage) {
        this.longMessage = longMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.longMessage = message;
        this.message = message;
    }

    private String message;
    private String messageType;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
