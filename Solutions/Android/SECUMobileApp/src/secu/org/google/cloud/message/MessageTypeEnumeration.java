package secu.org.google.cloud.message;

/**
 * Created by s16715d on 11/22/13.
 */
public enum MessageTypeEnumeration {
    RATESFEES("RATESFEES"),
    NOTIFICATION("NOTIFICATION"),
    SECURITY("SECURITY"),;

    private final String messageType;

    private MessageTypeEnumeration(String sMessageType) {
        messageType = sMessageType;
    }
    public boolean equalsName(String otherName){
        return (otherName == null)? false:messageType.equals(otherName);
    }
    public String toString(){
        return messageType;
    }
}
