package greendroid.widget.item;
import greendroid.widget.itemview.ItemView;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ViewGroup;

import com.cyrilmottier.android.greendroid.R;
public class LabelValueItem extends TextItem {

	private String mLabelText;
	private String mValueText;
	
	public String getLabelText() {
		return mLabelText;
	}

	public String getValueText() {
		return mValueText;
	}

	public LabelValueItem(String labelText, String valueText) {
		mLabelText = labelText;
		mValueText = valueText;
	}

	@Override
	public ItemView newView(Context context, ViewGroup parent) {

		return createCellFromXml(context, R.layout.gd_label_value_item_view,
				parent);
	}
	
	 @Override
	    public void inflate(Resources r, XmlPullParser parser, AttributeSet attrs) throws XmlPullParserException, IOException {
	        super.inflate(r, parser, attrs);

	        TypedArray a = r.obtainAttributes(attrs, R.styleable.Item);
	        //text = a.getString(R.styleable.TextItem_text);
	        a.recycle();
	    }
	
	
}

