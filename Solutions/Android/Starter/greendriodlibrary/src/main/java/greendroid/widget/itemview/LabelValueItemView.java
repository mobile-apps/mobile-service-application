package greendroid.widget.itemview;
import greendroid.widget.item.LabelValueItem;
import greendroid.widget.item.Item;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import android.widget.LinearLayout;
import com.cyrilmottier.android.greendroid.R;

public class LabelValueItemView extends LinearLayout implements ItemView {
	private TextView mTextViewLabel;
	private TextView mTextViewValue;

	public LabelValueItemView(Context context) {
		this(context, null);
	}

	public LabelValueItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void prepareItemView() {
		mTextViewLabel = (TextView) findViewById(R.id.textViewLabel);
		mTextViewValue = (TextView) findViewById(R.id.textViewValue);

	}

	@Override
	public void setObject(Item object) {
		// TODO Auto-generated method stub
		 final LabelValueItem item = (LabelValueItem) object;
		 mTextViewLabel.setText(item.getLabelText());
		 mTextViewValue.setText(item.getValueText());
	      
	}
}
