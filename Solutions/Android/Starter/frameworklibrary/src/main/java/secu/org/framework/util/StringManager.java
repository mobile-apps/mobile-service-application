package secu.org.framework.util;

import java.util.Arrays;

public class StringManager {
	
	public static String formatRightPad(String content,
			int totalNumberOfCharacters, int totalAdjustments, char padding) {

		if (content.length() >= totalNumberOfCharacters) {
			content = content.substring(0, totalNumberOfCharacters
					- totalAdjustments);
			int paddingLength = totalNumberOfCharacters - content.length();
			char[] exmarks = new char[paddingLength];
			Arrays.fill(exmarks, padding);
			String exmarksString = new String(exmarks);
			// String.format(padding, args)
			String str = String.format(content + "%s", exmarksString);
			return str;
			// System.out.println("[" + str + "]");

		} else {
			int paddingLength = totalNumberOfCharacters - content.length();
			char[] exmarks = new char[paddingLength];
			Arrays.fill(exmarks, ' ');
			String exmarksString = new String(exmarks);
			String str = String.format(content + "%s", exmarksString);
			return str;
			// System.out.println("[" + str + "]");
		}

	}
}
