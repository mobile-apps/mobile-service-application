package secu.org.framework.util;

public class NumberManager {

	public static boolean isValidDecimal(String decimalValue)
	{
		boolean isValid = true;
		try {
			Double.valueOf(decimalValue);
		} catch (Exception e) {
			return false;
		}
		return isValid;
	}
}
