package secu.org.framework.util;

import java.util.*;
import java.text.*;

public class DateManager {

	public static long computeDaysDifference(Date earlierDate, Date currentDate) {
		Calendar calendarEarlierDate = Calendar.getInstance();
		Calendar calendarCurrentDate = Calendar.getInstance();
		calendarEarlierDate.setTime(earlierDate);
		calendarCurrentDate.setTime(currentDate);
		long milliseconds1 = calendarEarlierDate.getTimeInMillis();
		long milliseconds2 = calendarCurrentDate.getTimeInMillis();
		long diff = milliseconds2 - milliseconds1;
		long diffDays = diff / (24 * 60 * 60 * 1000);
		return diffDays;
	}

	public static Date convertToDate(String dateStr) {
		dateStr = dateStr.substring(0, 8);
		DateFormat formatter;
		Date date = null;
		formatter = new SimpleDateFormat("yyyymmdd", Locale.ENGLISH);

		try {
			date = (Date) formatter.parse(dateStr);
			return date;

		} catch (ParseException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public static String displayDate(String dateToDisplay) {
		dateToDisplay = dateToDisplay.substring(0, 8);
		DateFormat formatter;
		Date date = null;
		formatter = new SimpleDateFormat("yyyyMMdd");
		String formattedDate = "";

		try {

			date = (Date) formatter.parse(dateToDisplay);

			formattedDate = DateFormat.getDateInstance(DateFormat.MEDIUM)
					.format(date);

		} catch (ParseException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}

		return formattedDate;
	}

	public static void main(String args[]) {
		displayDate("20120517120000.000[0:GMT]");
	}
}
