//
//  MATViewController.h
//  iPhone
//
//  Created by Angela Wong on 7/14/14.
//  Copyright (c) 2014 Mobile Applications Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MATViewController : UIViewController



@property (weak, nonatomic) IBOutlet UITextField *organizationName;
@property (weak, nonatomic) IBOutlet UIImageView *organizationImage;
@property (weak, nonatomic) IBOutlet UITextField *userID;
@property (weak, nonatomic) IBOutlet UITextField *password;

@end
