//
//  MATAppDelegate.h
//  iPhone
//
//  Created by Angela Wong on 7/14/14.
//  Copyright (c) 2014 Mobile Applications Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MATAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
