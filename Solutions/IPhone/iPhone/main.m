//
//  main.m
//  iPhone
//
//  Created by Angela Wong on 7/14/14.
//  Copyright (c) 2014 Mobile Applications Team. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MATAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MATAppDelegate class]));
    }
}
