//
//  MenuHelper.swift
//  Service
//
//  Created by wtccuser on 11/22/14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

import Foundation
class MenuHelper
{
    var eventMenuItemLabel : String
    //read from the conf setting
    //this is the  Initialization
    init () {
        //read the repsource relative path
        //specify the define property   file and declare the resource type as plist
        //plist is a value par file
        var path = NSBundle.mainBundle().pathForResource("MenuPropertyList", ofType: "plist")
        //call the init and get a return the dictionary
        var dict = NSDictionary(contentsOfFile: path!)
        //set the menu item decription and set the class variable
        eventMenuItemLabel =  dict["Events"] as String
        
    }
}
