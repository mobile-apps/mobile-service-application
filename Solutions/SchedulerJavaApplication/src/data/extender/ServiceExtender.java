/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package data.extender;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wtccuser
 */
public class ServiceExtender extends dataAccess.Service{
    
    ////////////////////////////////////////////////////////////////////////////
    // Attributes
    ////////////////////////////////////////////////////////////////////////////
    private String daysOfWeek;
    
    public ServiceExtender(dataAccess.Service service) {
        
    }

    /**
     * @return the daysOfWeek
     */
    public String getDaysOfWeek() {
    
        ArrayList<String> daysOfWeekList = new ArrayList<>();
        
        if(this.getSundayFlag() != null  && this.getSundayFlag())
        {
            daysOfWeekList.add("Sun");
        }
        if(this.getMondayFlag() != null  && this.getMondayFlag())
        {
            daysOfWeekList.add("Mon");
        }
        if(this.getTuesdayFlag() != null  && this.getTuesdayFlag())
        {
            daysOfWeekList.add("Tues");
        }
        if(this.getWednesdayFlag() != null  && this.getWednesdayFlag())
        {
            daysOfWeekList.add("Wed");
        }
        if(this.getThursdayFlag() != null  && this.getThursdayFlag())
        {
            daysOfWeekList.add("Thurs");
        }
        if(this.getFridayFlag() != null  && this.getFridayFlag())
        {
            daysOfWeekList.add("Fri");
        }
        if(this.getSaturdayFlag() != null  && this.getSaturdayFlag())
        {
            daysOfWeekList.add("Sat");
        }
        
        //convert arraylist of strings into comma-separated string
        java.lang.StringBuilder s = new java.lang.StringBuilder();
        
        String idList = daysOfWeekList.toString();
        
        
   //     daysOfWeek = String.join(",", daysOfWeekList);
        daysOfWeek = idList.substring(1, idList.length() - 1).replace(", ", ",");
        return daysOfWeek;
    }

    
    public ArrayList<Integer> getIntDaysOfWeek() {
    
        ArrayList<Integer> daysOfWeekIntList = new ArrayList<>();
        
        if(this.getSundayFlag() != null  && this.getSundayFlag())
        {
            daysOfWeekIntList.add(1);
        }
        if(this.getMondayFlag() != null  && this.getMondayFlag())
        {
            daysOfWeekIntList.add(2);
        }
        if(this.getTuesdayFlag() != null  && this.getTuesdayFlag())
        {
            daysOfWeekIntList.add(3);
        }
        if(this.getWednesdayFlag() != null  && this.getWednesdayFlag())
        {
            daysOfWeekIntList.add(4);
        }
        if(this.getThursdayFlag() != null  && this.getThursdayFlag())
        {
            daysOfWeekIntList.add(5);
        }
        if(this.getFridayFlag() != null  && this.getFridayFlag())
        {
            daysOfWeekIntList.add(6);
        }
        if(this.getSaturdayFlag() != null  && this.getSaturdayFlag())
        {
            daysOfWeekIntList.add(7);
        }
        
        //convert arraylist of strings into comma-separated string
        //java.lang.StringBuilder s = new java.lang.StringBuilder();
        
        //daysOfWeek = String.join(",", daysOfWeekList);
        return daysOfWeekIntList;
    }
    
    

    /**
     * @param daysOfWeek the daysOfWeek to set
     */
    public void setDaysOfWeek(String daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }
}
