/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.email;

import dataAccess.RegisteredUser;
import dataAccess.Service;
import dataAccess.UserCategory;
import dataAccess.UserService;
import dataHelper.RegisterUserHelper;
import dataHelper.ServiceHelper;
import dataHelper.UserCategoryHelper;
import dataHelper.UserServiceHelper;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.*;
import javax.mail.internet.*;

/**
 *
 * @author wtccuser
 */
public class EmailHelper {

    // use the classname for the logger, this way you can refactor
    private final static Logger mLog = Logger.getLogger(EmailHelper.class
            .getName());

    private String from;
    private String password;

    public EmailHelper(String from, String password) {
        mLog.log(Level.INFO, "EmailHelper(String from, String password)");
        this.from = from;
        this.password = password;
    }

    public EmailHelper() {
    }

    private class SMTPAuthenticator extends javax.mail.Authenticator {

        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            if (from != null && password != null) {
                mLog.log(Level.INFO, "password {0} from {1}", new Object[]{password, from});
                return new PasswordAuthentication(from, password);
            } else {
                mLog.log(Level.INFO, "support@cokcc.org ,batmanZZ00");

                return new PasswordAuthentication("support@cokcc.org", "batmanZZ00");
            }
        }
    }

    public void send(EmailMessage emailMessage) {
        mLog.log(Level.INFO, "begin send ");
        String to = emailMessage.getTo();
        // Sender's email ID needs to be mentioned
        //String from = "support@cokcc.org";
        String from = emailMessage.getFrom();

        // Assuming you are sending email from localhost
        // String host = "mail.cokcc.org";
        String host = emailMessage.getEmailServer();

        // Get system properties
        Properties properties = System.getProperties();
        mLog.log(Level.INFO, "host {0}", host);
        // Setup mail server
        properties.setProperty("mail.smtp.host", host);
        properties.setProperty("mail.smtp.auth", "true");
        SecurityManager security = System.getSecurityManager();
        Authenticator auth = new SMTPAuthenticator();
        // Get the default Session object.
        Session session = Session.getDefaultInstance(properties, auth);

        try {
            mLog.log(Level.INFO, "Create a default MimeMessage object");
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);
            mLog.log(Level.INFO, " from {0}", from);
            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));
            mLog.log(Level.INFO, " to [{0}]", to);
            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(to));
            mLog.log(Level.INFO, " subject {0}", emailMessage.getSubject());
            // Set Subject: header field
            message.setSubject(emailMessage.getSubject());
            mLog.log(Level.INFO, " body {0}", emailMessage.getBody());
            // Now set the actual message
            message.setText(emailMessage.getBody());
            mLog.log(Level.INFO, "Try to send message");
            // Send message
            Transport.send(message);
            mLog.log(Level.INFO, "Sent message successfully....");
            //System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {

            String error = mex.getMessage();
            mLog.log(Level.SEVERE, "error {0}", error);
            throw new java.lang.IllegalStateException(mex.getMessage());
        }// TODO cod
    }

    public void send() {
        // Recipient's email ID needs to be mentioned.

        String to = "java.university@nc.rr.com";
        // Sender's email ID needs to be mentioned
        from = "support@cokcc.org";

        // Assuming you are sending email from localhost
        String host = "mail.cokcc.org";

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.setProperty("mail.smtp.host", host);
        properties.setProperty("mail.smtp.auth", "true");
        SecurityManager security = System.getSecurityManager();
        Authenticator auth = new SMTPAuthenticator();
        // Get the default Session object.
        Session session = Session.getDefaultInstance(properties, auth);

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(to));

            // Set Subject: header field
            message.setSubject("This is the Subject Line!");

            // Now set the actual message
            message.setText("This is actual message");

            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
        }// TODO code application logic here
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        send_new_service_message();
        send_reminder_message();
        
    }
    
    public static void send_new_service_message() {      
        EmailHelper emailHelper = new EmailHelper();
        
        //Timestamp stamp = new Timestamp(Calendar.getInstance().getTimeInMillis());
        //Date now = new Date(stamp.getTime());
        Date now = new Date();
        
        List<Service> services = ServiceHelper.findAll();
            for (Service service : services) {
                Calendar c = Calendar.getInstance(); 
                c.setTime(service.getCreatedDate()); 
                c.add(Calendar.DATE, 1);
                Date cdate = c.getTime();      
        
                if(cdate.compareTo(now)>0){
                    // Send email to all registered users inetested in the same category when a service created
                    UserCategory[] usersByCategory = UserCategoryHelper.findByCategoryId(service.getCategoryId());
                    
                    List<RegisteredUser> users = new ArrayList<>();
                    for(UserCategory userByCategory : usersByCategory) {
                        RegisteredUser registeredUser = RegisterUserHelper.findById(userByCategory.getRegisteredUserId());
                        users.add(registeredUser);
                    }
                    for (RegisteredUser user : users) {
                        EmailMessage emailMessage = new EmailMessage();
                        emailMessage.setTo(user.getEmail());
                        emailMessage.setFrom("support@cokcc.org");
                        emailMessage.setEmailServer("mail.cokcc.org");
                        emailMessage.setSubject("New Service: " + service.getName());
                        String body = "Service " + service.getName() + " has been created. Event starts on "
                                + service.getStartDate() + " General Information: "
                                + service.getGeneralInformation();
                        emailMessage.setBody(body);
                        emailHelper.send(emailMessage);
                    }
                    mLog.log(Level.OFF, "New service notification email has been sent to all users");
                    // end of send email
                }
            }
        
    }
    
    public static void send_reminder_message() {  
         EmailHelper emailHelper = new EmailHelper();
        
        //Timestamp stamp = new Timestamp(Calendar.getInstance().getTimeInMillis());
        //Date now = new Date(stamp.getTime());
        Date now = new Date();
        
        UserService[] userServices = UserServiceHelper.findAll();
            for (UserService userService : userServices) {
                Calendar fr = Calendar.getInstance(); 
                Calendar sr = Calendar.getInstance(); 
                Service service = ServiceHelper.findServiceByServiceId(userService.getServiceId());
                
                fr.setTime(service.getStartDate()); 
                fr.add(Calendar.DAY_OF_MONTH, -5);
                Date firstReminderDate = fr.getTime(); 
               
                sr.setTime(service.getStartDate()); 
                sr.add(Calendar.DAY_OF_MONTH, -1);
                Date secondReminderDate = sr.getTime();   
        
                if ((firstReminderDate.compareTo(now)<0) && (userService.getFirstReminder() == null)) {
                    EmailMessage emailMessage = new EmailMessage();
                    RegisteredUser user = RegisterUserHelper.findById(userService.getRegisteredUserId());
                    emailMessage.setTo(user.getEmail());
                    emailMessage.setFrom("support@cokcc.org");
                    emailMessage.setEmailServer("mail.cokcc.org");
                    emailMessage.setSubject("Service Reminder: " + service.getName());
                    String body = "This is the first reminder of Service " + service.getName() + ". Event starts on "
                            + service.getStartDate() + " General Information: "
                            + service.getGeneralInformation();
                    emailMessage.setBody(body);
                    emailHelper.send(emailMessage);
                    
                    mLog.log(Level.OFF, "Service first reminder notification email has been sent to registered users");
                    // end of send email
                    userService.setFirstReminder("1");
                    UserServiceHelper.update(userService.getUserServiceId(), userService);
                }
                else if ((secondReminderDate.compareTo(now)<0) && (userService.getSecondReminder() == null)) {
                    EmailMessage emailMessage = new EmailMessage();
                    RegisteredUser user = RegisterUserHelper.findById(userService.getRegisteredUserId());
                    emailMessage.setTo(user.getEmail());
                    emailMessage.setFrom("support@cokcc.org");
                    emailMessage.setEmailServer("mail.cokcc.org");
                    emailMessage.setSubject("Service Reminder: " + service.getName());
                    String body = "This is the last reminder of Service " + service.getName() + ". Event starts on "
                            + service.getStartDate() + " General Information: "
                            + service.getGeneralInformation();
                    emailMessage.setBody(body);
                    emailHelper.send(emailMessage);
                    
                    mLog.log(Level.OFF, "Service second reminder notification email has been sent to registered users");
                    // end of send email
                    userService.setSecondReminder("1");
                    UserServiceHelper.update(userService.getUserServiceId(), userService);
                }
                
            }
        
    }   

}
