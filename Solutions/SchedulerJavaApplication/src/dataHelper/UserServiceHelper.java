/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataHelper;

import dataAccess.DaoException;
import dataAccess.DaoFactory;
import dataAccess.UserServiceDao;
import java.util.logging.Logger;
import dataAccess.RegisteredUser;
import dataAccess.UserService;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 *
 * @author HSC314
 */
public class UserServiceHelper {
    // use the classname for the logger, this way you can refactor

    private final static Logger LOGGER = Logger.getLogger(UserServiceHelper.class
            .getName());

    /**
     * find all users in database
     *
     * @return
     */
    public static dataAccess.UserService[] findAll() {
        LOGGER.log(Level.OFF, "start findAll ");
        Connection connection = null;
        try {
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            UserServiceDao dao = DaoFactory.createUserServiceDao(connection);
            dataAccess.UserService[] users = dao.findAll();
            LOGGER.log(Level.OFF, "end findAll ");
            return users;
        } catch (Exception ex) {
            Logger.getLogger(UserServiceHelper.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "end findAll ");
        return null;
    }
    
    public static void delete(dataAccess.RegisteredUser registerUser) {
        LOGGER.log(Level.OFF, "started delet ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (registerUser == null) {
                LOGGER.log(Level.OFF, "Null user");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Delete to database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            UserServiceDao dao = DaoFactory.createUserServiceDao(connection);
            dao.deleteByRegisteredUserId(registerUser.getRegisteredUserId());

            // </editor-fold>
            LOGGER.log(Level.OFF, "end delete ");

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
            throw new java.lang.IllegalStateException(ex.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");

    }

    public static void add(int registerUserId, int serviceId) {
        LOGGER.log(Level.OFF, "started add ");
        Connection connection = null;
        try {

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Add to database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            UserServiceDao dao = DaoFactory.createUserServiceDao(connection);
            //setting the properties
            java.util.Date date = new java.util.Date();
            dataAccess.UserService userService = new dataAccess.UserService();
            userService.setServiceId(serviceId);
            userService.setRegisterDate(date);
            userService.setRegisteredUserId(registerUserId);
            int primaryKey = dao.insert(userService);
            
            if (primaryKey > 0) {
              LOGGER.log(Level.OFF, "added ");  
            } else {
                 LOGGER.log(Level.OFF, "failed ");
            }

            // </editor-fold>
            LOGGER.log(Level.OFF, "end add ");

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");

    }
    
     public static void update( int userServiceId, UserService dto ) {
        LOGGER.log(Level.OFF, "started update ");
        Connection connection = null;
        try {

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Update database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            UserServiceDao dao = DaoFactory.createUserServiceDao(connection);
            //setting the properties
            java.util.Date date = new java.util.Date();
            dataAccess.UserService userService = new dataAccess.UserService();
            userService.setUserServiceId(userServiceId);
            userService.setFirstReminder(dto.getFirstReminder());
            userService.setSecondReminder(dto.getSecondReminder());
            boolean updated = dao.update(userServiceId, userService);
            
            if (updated) {
              LOGGER.log(Level.OFF, "updated ");  
            } else {
                 LOGGER.log(Level.OFF, "failed ");
            }

            // </editor-fold>
            LOGGER.log(Level.OFF, "end update");

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended update ");
     }


}
