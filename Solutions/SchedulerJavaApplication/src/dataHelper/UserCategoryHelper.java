/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataHelper;

import dataAccess.DaoFactory;
import dataAccess.RegisteredUser;
import dataAccess.UserCategory;
import dataAccess.UserCategoryDao;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HSC314
 */
public class UserCategoryHelper {
    // use the classname for the logger, this way you can refactor

    private final static Logger LOGGER = Logger.getLogger(UserCategoryHelper.class
            .getName());

    /**
     * find all users in database
     *
     * @return
     */
    public static dataAccess.UserCategory[] findAll() {
        LOGGER.log(Level.OFF, "start findAll ");
        Connection connection = null;
        try {
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            UserCategoryDao dao = DaoFactory.createUserCategoryDao(connection);
            dataAccess.UserCategory[] users = dao.findAll();
            LOGGER.log(Level.OFF, "end findAll ");
            return users;
        } catch (Exception ex) {
            Logger.getLogger(UserCategoryHelper.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "end findAll ");
        return null;
    }
    
    /**
     * find all users in database
     *
     * @return
     */
    public static UserCategory[] findByCategoryId(int categoryId) {
        LOGGER.log(Level.OFF, "start findbyCategoryId ");
        Connection connection = null;
        try {
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            UserCategoryDao dao = DaoFactory.createUserCategoryDao(connection);
            UserCategory[] users = dao.findByCategoryId(categoryId);
            LOGGER.log(Level.OFF, "end findbyCategoryId ");
            return users;
        } catch (Exception ex) {
            Logger.getLogger(UserCategoryHelper.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "end findbyCategoryId ");
        return null;
    }


}
