/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataHelper;

import dataAccess.DaoFactory;
import dataAccess.ServiceDao;
import dataAccess.Service;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import data.extender.ServiceExtender;
import java.sql.SQLException;

/**
 *
 * @author wtccuser
 */
public class ServiceHelper {

    // use the classname for the logger, this way you can refactor
    private final static Logger LOGGER = Logger.getLogger(ServiceHelper.class
            .getName());

    public static List<ServiceExtender> findAllExtender() {
        List<dataAccess.Service> list = findAll();
        if (list != null) {
            List<ServiceExtender> extList = new ArrayList();
            for (dataAccess.Service service : list) {
                extList.add(new ServiceExtender(service));
            }
            return extList;
        } else {
            return null;
        }
    }

    /**
     * find all services in database
     *
     * @return
     */
    public static List<dataAccess.Service> findAll() {
        LOGGER.log(Level.OFF, "start findAll ");
        try {
            //get a connection to the database
            Connection connection = data.DatabaseManager.getConnection();

            //generated class to manage the table
            ServiceDao dao = DaoFactory.createServiceDao(connection);
            dataAccess.Service[] services = dao.findAll();

            //Convert array to list and return the list
            List<dataAccess.Service> list = Arrays.asList(services);

            LOGGER.log(Level.OFF, "end findAll ");
            return list;
        } catch (Exception ex) {
            Logger.getLogger(ServiceHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.OFF, "end findAll ");
        return null;
    }
    
     public static String findByServiceId(int id) {
        Connection connection = null;
        try {
            connection = data.DatabaseManager.getConnection();
            ServiceDao serviceDao = DaoFactory.createServiceDao(connection);
            dataAccess.Service service = serviceDao.findByPrimaryKey(id);
            return service.getName();
        } catch (Exception ex) {
            Logger.getLogger(ServiceHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

     public static Service findServiceByServiceId(int id) {
        Connection connection = null;
        try {
            connection = data.DatabaseManager.getConnection();
            ServiceDao serviceDao = DaoFactory.createServiceDao(connection);
            dataAccess.Service service = serviceDao.findByPrimaryKey(id);
            return service;
        } catch (Exception ex) {
            Logger.getLogger(ServiceHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

}
