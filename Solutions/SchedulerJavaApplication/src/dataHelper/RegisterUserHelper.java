/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataHelper;

import dataAccess.DaoFactory;
import dataAccess.RegisteredUserDao;
import java.util.logging.Logger;
import dataAccess.RegisteredUser;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 *
 * @author HSC314
 */
public class RegisterUserHelper {
    // use the classname for the logger, this way you can refactor

    private final static Logger LOGGER = Logger.getLogger(RegisterUserHelper.class
            .getName());
    
    /**
     * find user by email
     *
     * @param email
     * @return
     */
    public static RegisteredUser find(String email) {
        LOGGER.log(Level.OFF, "start find");
        // <editor-fold defaultstate="collapsed" desc="Validate parameters">
        if (email == null) {
            LOGGER.log(Level.OFF, "Null email");
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="find in database">
        Connection connection = null;
        try {

            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
            //setting the properties
            RegisteredUser user = dao.findByEmail(email);
            return user;

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended find ");
        return null;
        // </editor-fold>  
    }

    /**
     * find all users in database
     *
     * @return
     */
    public static RegisteredUser[] findAll() {
        LOGGER.log(Level.OFF, "start findAll ");
        try {
            //get a connection to the database
            Connection connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
            RegisteredUser[] users = dao.findAll();
            LOGGER.log(Level.OFF, "end findAll ");
            return users;
        } catch (Exception ex) {
            Logger.getLogger(RegisterUserHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.OFF, "end findAll ");
        return null;
    }

     public static RegisteredUser findById(int registeredUserId) {
        LOGGER.log(Level.OFF, "start find");
        // <editor-fold defaultstate="collapsed" desc="Validate parameters">

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="find in database">
        Connection connection = null;
        try {

            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
            //setting the properties
            RegisteredUser user = dao.findByPrimaryKey(registeredUserId);
            return user;

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended find ");
        return null;
        // </editor-fold>  
    }
}
