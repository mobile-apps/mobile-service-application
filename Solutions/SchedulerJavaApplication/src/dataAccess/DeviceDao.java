/*
 * This file was generated - do not edit it directly !!
 * Generated by AuDAO tool, a product of Spolecne s.r.o.
 * For more information please visit http://www.spoledge.com
 */
package dataAccess;

/**
 * This is the DAO.
 *
 * @author generated
 */
public interface DeviceDao extends AbstractDao {

    /**
     * Finds a record identified by its primary key.
     * @return the record found or null
     */
    public Device findByPrimaryKey( int deviceId );

    /**
     * Finds a record.
     */
    public Device findBySubscriber( String subscriber );

    /**
     * Inserts a new record.
     * @return the generated primary key - deviceId
     */
    public int insert( Device dto ) throws DaoException;

}
