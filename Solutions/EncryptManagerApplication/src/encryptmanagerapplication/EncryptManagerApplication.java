/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encryptmanagerapplication;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author wtccuser
 */
public class EncryptManagerApplication extends Application {

    private TextArea mTextArea = new TextArea();
    private RadioButton mRadionButtonNotification = new RadioButton("Notification");
    private RadioButton mRadioButtonSecurity = new RadioButton("Security");
    private RadioButton mRadionButtonRatesFees = new RadioButton("Rates and Fees");
    private TextField mKeyTextFieldTitle = new TextField();
    private TextField mContentTextFieldTitle = new TextField();
    private TextField mURLTextFieldTitle = new TextField();

    public TextArea getmTextArea() {
        return mTextArea;
    }

    public void setmTextArea(TextArea mTextArea) {
        this.mTextArea = mTextArea;
    }

    public TextField getmKeyTextFieldTitle() {
        return mKeyTextFieldTitle;
    }

    public void setmKeyTextFieldTitle(TextField mKeyTextFieldTitle) {
        this.mKeyTextFieldTitle = mKeyTextFieldTitle;
    }

    public TextField getmContentTextFieldTitle() {
        return mContentTextFieldTitle;
    }

    public void setmContentTextFieldTitle(TextField mContentTextFieldTitle) {
        this.mContentTextFieldTitle = mContentTextFieldTitle;
    }

    public TextField getmURLTextFieldTitle() {
        return mURLTextFieldTitle;
    }

    public void setmURLTextFieldTitle(TextField mURLTextFieldTitle) {
        this.mURLTextFieldTitle = mURLTextFieldTitle;
    }

    @Override
    public void start(Stage primaryStage) {
        this.mKeyTextFieldTitle.setText("Bar12345Bar12345");
        this.mContentTextFieldTitle.setText("johndoe@gmail.com");
        this.mURLTextFieldTitle.setText("http://localhost:8080/WebApplication/rest/getSecureUser");
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setVmax(640);
        scrollPane.setPrefSize(415, 650);
        javafx.scene.control.Tooltip toolTipText = new javafx.scene.control.Tooltip("Your message must be at least 8 characters in length and should content non-html tags");
        mTextArea.setMinWidth(650);
        mTextArea.setTooltip(toolTipText);
        scrollPane.setContent(mTextArea);
        Label titleLabel = new Label("Encrypt Key");
        Label contentLabel = new Label("Content");
        mKeyTextFieldTitle.setMinHeight(20);

        // vb.getChildren().add(scrollPane);
        //     scrollPane.getContent().
        Button btn = new Button();
        ButtonGoogleEventHandler buttonEventHandler = new ButtonGoogleEventHandler(this);
        System.out.println("Assign button handler");
        btn.setOnAction(buttonEventHandler);
        btn.setText("Encyrpt");
        Label messageLabel = new Label("Message");
        Label urlMessageLabel = new Label("URL");

        VBox vbox = new VBox();
        vbox.setPadding(new Insets(10));
        vbox.setSpacing(8);
        vbox.getChildren().add(titleLabel);
        vbox.getChildren().add(mKeyTextFieldTitle);
        vbox.getChildren().add(contentLabel);
        vbox.getChildren().add(mContentTextFieldTitle);
        vbox.getChildren().add(urlMessageLabel);
        vbox.getChildren().add(mURLTextFieldTitle);
        vbox.getChildren().add(messageLabel);
        //
        vbox.getChildren().add(scrollPane);
        ToggleGroup group = new ToggleGroup();

        mRadionButtonNotification.setToggleGroup(group);
        mRadionButtonNotification.setSelected(true);

        mRadioButtonSecurity.setToggleGroup(group);

        mRadionButtonRatesFees.setToggleGroup(group);

        HBox hbox = new HBox();

        vbox.getChildren().add(btn);

        StackPane root = new StackPane();
        //root.setAlignment(Pos.CENTER_RIGHT); 

        //root.getChildren().add(scrollPane);
        //root.getChildren().add(btn);
        Scene scene = new Scene(vbox, 650, 450);

        primaryStage.setTitle("Google Message Generator");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
