/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encryptmanagerapplication;

import java.security.Key;
import java.util.Calendar;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author s16715d
 */
public class ButtonGoogleEventHandler implements EventHandler {

    public ButtonGoogleEventHandler(EncryptManagerApplication mEncryptManagerApplication) {
        this.mEncryptManagerApplication = mEncryptManagerApplication;
    }

    /**
     * class logger
     */
    private static Logger mLog = Logger.getLogger(ButtonGoogleEventHandler.class.getName());
    private String mDisplayMessage;
    private Stage mDialogStage;
    private EncryptManagerApplication mEncryptManagerApplication;

    /**
     * Constructor
     *
     * @param messenger
     */
    /**
     * show application message
     */
    private void showMessage() {
            String not="ddd";
        switch(not) {
            case "ddd":
                break;
        }
        mDialogStage = new Stage();
        Button okButton = new Button("Ok");
        okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                mDialogStage.close();
                //Place the code you want to execute on the button click here.
            }
        });
        mDialogStage.initModality(Modality.WINDOW_MODAL);
        mDialogStage.setScene(new Scene(VBoxBuilder.create().
                children(new Text(mDisplayMessage), okButton).
                alignment(Pos.CENTER).padding(new Insets(5)).build()));
        mDialogStage.show();

    }

    @Override
    public void handle(Event event) {

        String text = mEncryptManagerApplication.getmContentTextFieldTitle().getText();

        String key = mEncryptManagerApplication.getmKeyTextFieldTitle().getText(); // 128 bit key

        try {

            // Create key and cipher
            Key aesKey = new SecretKeySpec(key.getBytes(), "AES");

            Cipher cipher = Cipher.getInstance("AES");

            // encrypt the text
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);

            byte[] encrypted = cipher.doFinal(text.getBytes());
            Calendar currentDate = Calendar.getInstance();
            String currentDateStr = Long.toString(currentDate.getTime().getTime());
            byte[] encryptedDate = cipher.doFinal(currentDateStr.getBytes());
            String encrpytDate = new String(encryptedDate);
            String encrpyt = new String(encrypted);
            StringBuffer buffer = new StringBuffer();
            buffer.append(mEncryptManagerApplication.getmURLTextFieldTitle().getText());
            buffer.append("?paramOne=");
            buffer.append(encrpyt);
            buffer.append("&paramTwo=");
            buffer.append(encrpytDate);
            mEncryptManagerApplication.getmTextArea().setText(buffer.toString());

            // decrypt the text
            cipher.init(Cipher.DECRYPT_MODE, aesKey);

            String decrypted = new String(cipher.doFinal(encrypted));

            System.err.println(decrypted);
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

}
