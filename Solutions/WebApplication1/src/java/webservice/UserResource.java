/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.mobile.dto.Device;
import com.mobile.dto.RegisteredUser;
import data.helper.DeviceHelper;
import data.helper.RegisterUserHelper;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author HSC314
 */
@Path("mobile")
public class UserResource {

    private final static Logger LOGGER = Logger.getLogger(UserResource.class
            .getName());
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of UserResource
     */
    public UserResource() {
    }

    /**
     * Add registration
     *http://localhost:8080/WebApplication/webresources/mobile/getUser?email=andrew@ncsecu.org&subscriber=test
     * @param email
     * @param subscriber
     * @return
     */
    @GET
    @Path("/getUser")
    @Produces(MediaType.APPLICATION_JSON)
    public Response find(@QueryParam("email") String email, @QueryParam("subscriber") String subscriber) {
        LOGGER.log(Level.OFF, "started find ");
        //find or add register users
        RegisteredUser registeredUser = RegisterUserHelper.add(email);
        //add device
        //todo need to check for database error and do not perform the device add
        Device device = DeviceHelper.add(subscriber, registeredUser);
        //did we encounter any application errors
        if (registeredUser != null && device != null) {
            LOGGER.log(Level.OFF, "end find ");
            String result = "User saved : " + registeredUser.getRegisteredUserId();
            return Response.status(201).entity(result).build();
        } else {
            //error encounter
            String result = "database error";
            return Response.status(500).entity(result).build();
        }
    }

}
