/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package web;

import com.mobile.dto.Department;
import com.mobile.dto.Service;
import data.helper.CategoryHelper;
import data.helper.DepartmentHelper;
import data.helper.OrganizationHelper;
import data.helper.ServiceHelper;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author HSC314
 */
@ManagedBean
public class ServiceBean implements Serializable {
    private final static Logger LOGGER = Logger.getLogger(CategoryHelper.class
            .getName());
  
    private List<Service> serviceList;

    public List<Service> getServiceList() {
        serviceList = ServiceHelper.findAll();
        return serviceList;
    }

    public void setServiceList(List<Service> serviceList) {
        this.serviceList = serviceList;
    }
    private com.mobile.dto.Service service= new com.mobile.dto.Service();
    private com.mobile.dto.Service selectedService;

    public Service getSelectedService() {
        selectedService = (Service) FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get("service");
        return selectedService;
    }

    public void setSelectedService(Service selectedService) {
        
        this.selectedService = selectedService;
    }
    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
    
    
    public String add() {
        LOGGER.log(Level.OFF, "start add");
        this.service = ServiceHelper.add(this.service);
        LOGGER.log(Level.OFF, "end add ");
  
        return WebConstants.SERVICES_PAGE;
    }

    /**
     * Navigate to detail page
     * @param event
     * @return 
     */
    public String update(Service service) {
        LOGGER.log(Level.OFF, "start update");
        //ServiceHelper.update(service);
        LOGGER.log(Level.OFF, "end update ");

        return WebConstants.SERVICES_PAGE;
    }
    
    public String delete(Service service) {
        LOGGER.log(Level.OFF, "start delete");
        //ServiceHelper.delete(service);
        LOGGER.log(Level.OFF, "end delete ");

        return WebConstants.DEPARTMENTS_PAGE;
    }



//    public String findByOrganizationId(int id) {
//        LOGGER.log(Level.OFF, "start findByOrganizationId");
//        String org = DepartmentHelper.findByOrganizationId(id);
//        LOGGER.log(Level.OFF, "end findByOrganizationId ");
//        return org;
//    }

    /**
     * Navigate to detail page
     *
     * @param event
     * @return
     */
    public String onRowSelectNavigate(SelectEvent event) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put(WebConstants.SELECTED_CATEGORY, event.getObject());
        return WebConstants.CATEGORY_DETAIL_PAGE;
    }
    public void onRowSelect(SelectEvent event) {
        FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().put("service", selectedService);
        ConfigurableNavigationHandler configurableNavigationHandler = 
                (ConfigurableNavigationHandler) FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        configurableNavigationHandler.performNavigation("editService?faces-redirect=true");
    }
   
}
