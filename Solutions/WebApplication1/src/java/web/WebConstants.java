/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package web;

/**
 *
 * @author wtccuser
 */
public class WebConstants {
    public static final String CATEGORIES_PAGE = "categories";
    public static final String CATEGORY_DETAIL_PAGE = "categoryDetail?faces-redirect=true";
    public static final String SELECTED_CATEGORY = "selectedCategory";
    public static final String ORGANIZATIONS_PAGE = "index";
    public static final String DEPARTMENTS_PAGE = "departments";
    public static final String DEPARTMENT_EDIT_PAGE = "editDepartment?faces-redirect=true";  
    public static final String SELECTED_DEPARTMENT = "selectedDepartment";
    public static final String SERVICES_PAGE = "services";
}
