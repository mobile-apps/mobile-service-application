/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package web;

import data.helper.CategoryHelper;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author deweiss
 */
@ManagedBean
@SessionScoped
public class IconFileBean {

    private final static Logger LOGGER = Logger.getLogger(CategoryHelper.class
            .getName());

    private static UploadedFile iconFile;
    private StreamedContent iconImage;

    public static UploadedFile getIconFile() {
        return iconFile;
    }

    public static void setIconFile(UploadedFile iconFile) {
        IconFileBean.iconFile = iconFile;
    }

    public StreamedContent getIconImage() {
        return iconImage;
    }

    public void setIconImage(StreamedContent iconImage) {
        this.iconImage = iconImage;
    }
    
    /**
     * Creates a new instance of IconFileBean
     */
    public IconFileBean() {
        
    }
        public void handleFileUpload(FileUploadEvent event) {
        LOGGER.log(Level.OFF, "start icon file upload");
        iconFile = event.getFile();
        try {
            setIconImage(new DefaultStreamedContent(iconFile.getInputstream(), iconFile.getContentType()));
        } catch (IOException ex) {
            Logger.getLogger(OrganizationBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.OFF, "end icon file upload");
    }


}
