/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package web;

import com.mobile.dto.Category;
import com.mobile.dto.RegisteredUser;
import data.helper.CategoryHelper;
import data.helper.RegisterUserHelper;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import java.util.List;

/**
 *
 * @author HSC314
 */
@ManagedBean
public class CategoryBean implements Serializable {
    private final static Logger LOGGER = Logger.getLogger(CategoryHelper.class
            .getName());
  
    private List<Category> categoryList;

    public List<Category> getCategoryList() {
        categoryList = CategoryHelper.findAll();
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }
    private com.mobile.dto.Category category= new com.mobile.dto.Category();
    private com.mobile.dto.Category selectedCategory;

    public Category getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(Category selectedCategory) {
        this.selectedCategory = selectedCategory;
    }
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
    
    
    public String add() {
              LOGGER.log(Level.OFF, "start add");
        this.category = CategoryHelper.add(category.getName());
        LOGGER.log(Level.OFF, "end add ");
  
        return WebConstants.CATEGORIES_PAGE;
    }

    /**
     * Navigate to detail page
     * @param event
     * @return 
     */
    public String onRowSelectNavigate(SelectEvent event) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put(WebConstants.SELECTED_CATEGORY, event.getObject());
        return WebConstants.CATEGORY_DETAIL_PAGE;
    }
   
}
