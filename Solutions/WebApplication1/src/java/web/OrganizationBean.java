/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import com.mobile.dto.Organization;
import data.helper.CategoryHelper;
import data.helper.OrganizationHelper;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author HSC314
 */
@ManagedBean
//@SessionScoped
public class OrganizationBean implements Serializable {

    private final static Logger LOGGER = Logger.getLogger(CategoryHelper.class
            .getName());

    private List<Organization> organizationList;

    public List<Organization> getOrganizationList() {
        organizationList = OrganizationHelper.findAll();
        return organizationList;
    }

    public void setOrganizationList(List<Organization> organizationList) {
        this.organizationList = organizationList;
    }

    private com.mobile.dto.Organization organization = new com.mobile.dto.Organization();

    private com.mobile.dto.Organization selectedOrganization;

    public Organization getSelectedOrganization() {
        return selectedOrganization;
    }

    public void setSelectedOrganization(Organization selectedOrganization) {
        this.selectedOrganization = selectedOrganization;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public String add() {
        LOGGER.log(Level.OFF, "start add");
        UploadedFile iconFile = IconFileBean.getIconFile();
        if (iconFile != null) {
            LOGGER.log(Level.OFF, "  start add icon");
            int iconSize = (int) iconFile.getSize();
            byte[] iconBytes = new byte[iconSize];
            try {
                iconFile.getInputstream().read(iconBytes, 0, iconSize);
            } catch (IOException ex) {
                Logger.getLogger(OrganizationBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            organization.setIcon(iconBytes);
            LOGGER.log(Level.OFF, "  end add icon");

        }
        this.organization = OrganizationHelper.add(this.organization);
        LOGGER.log(Level.OFF, "end add ");

        return WebConstants.ORGANIZATIONS_PAGE;
    }

    /**
     * Navigate to detail page
     *
     * @param event
     * @return
     */
    public String onRowSelectNavigate(SelectEvent event) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put(WebConstants.SELECTED_CATEGORY, event.getObject());
        return WebConstants.CATEGORY_DETAIL_PAGE;
    }

        /**
     *
     * @param actionEvent
     */
    public void getDepartmentList(ActionEvent actionEvent){
        int i = 1;
    }

}