/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import com.mobile.dto.Department;
import data.helper.DepartmentHelper;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author HSC314
 */
@ManagedBean
public class DepartmentBean implements Serializable {

    private final static Logger LOGGER = Logger.getLogger(DepartmentHelper.class
            .getName());

    private List<Department> departmentList;

    public List<Department> getDepartmentList() {
        departmentList = DepartmentHelper.findAll();
        return departmentList;
    }

    public void setDepartmentList(List<Department> departmentList) {
        this.departmentList = departmentList;
    }

    private com.mobile.dto.Department department = new com.mobile.dto.Department();

    private com.mobile.dto.Department selectedDepartment;

    public Department getSelectedDepartment() {
        selectedDepartment = (Department) FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get("department");
        return selectedDepartment;
    }

    public void setSelectedDepartment(Department selectedDepartment) {
        this.selectedDepartment = selectedDepartment;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String add() {
        LOGGER.log(Level.OFF, "start add");
        UploadedFile iconFile = IconFileBean.getIconFile();
        if (iconFile != null) {
            LOGGER.log(Level.OFF, "  start add icon");
            int iconSize = (int) iconFile.getSize();
            byte[] iconBytes = new byte[iconSize];
            try {
                iconFile.getInputstream().read(iconBytes, 0, iconSize);
            } catch (IOException ex) {
                Logger.getLogger(OrganizationBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            department.setIcon(iconBytes);
            LOGGER.log(Level.OFF, "  end add icon");

        }
        this.department = DepartmentHelper.add(this.department);
        LOGGER.log(Level.OFF, "end add ");

        return WebConstants.DEPARTMENTS_PAGE;
    }
    
    public String update(Department department) {
        LOGGER.log(Level.OFF, "start update");
        DepartmentHelper.update(department);
        LOGGER.log(Level.OFF, "end update ");

        return WebConstants.DEPARTMENTS_PAGE;
    }
    
    public String delete(Department department) {
        LOGGER.log(Level.OFF, "start delete");
        DepartmentHelper.delete(department);
        LOGGER.log(Level.OFF, "end delete ");

        return WebConstants.DEPARTMENTS_PAGE;
    }



    public String findByOrganizationId(int id) {
        LOGGER.log(Level.OFF, "start findByOrganizationId");
        String org = DepartmentHelper.findByOrganizationId(id);
        LOGGER.log(Level.OFF, "end findByOrganizationId ");
        return org;
    }

    /**
     * Navigate to detail page
     *
     * @param event
     * @return
     */
    public String onRowSelectNavigate(SelectEvent event) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put(WebConstants.SELECTED_CATEGORY, event.getObject());
        return WebConstants.CATEGORY_DETAIL_PAGE;
    }
    public void onRowSelect(SelectEvent event) {
        FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().put("department", selectedDepartment);
        ConfigurableNavigationHandler configurableNavigationHandler = 
                (ConfigurableNavigationHandler) FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        configurableNavigationHandler.performNavigation("editDepartment?faces-redirect=true");
    }

}
