/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import com.mobile.dto.RegisteredUser;
import data.helper.CategoryHelper;
import data.helper.RegisterUserHelper;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author HSC314
 */
@ManagedBean(name = "RegisterUserBean")
@SessionScoped
public class RegisterUserBean {

    private com.mobile.dto.RegisteredUser[] registerUsers;
    private final static Logger LOGGER = Logger.getLogger(CategoryHelper.class
            .getName());

    public RegisteredUser[] getRegisterUsers() {
        LOGGER.log(Level.OFF, "start getRegisterUsers ");
        registerUsers = RegisterUserHelper.findAll();
        LOGGER.log(Level.OFF, "end getRegisterUsers ");
        return registerUsers;
    }

    public void setRegisterUsers(RegisteredUser[] registerUsers) {
        this.registerUsers = registerUsers;
    }
}
