/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

/**
 *
 * @author wtccuser
 */
import data.Step;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.Cookie;

/**
 *
 * @author HSC314
 */
@ManagedBean
public class DrivingDirectionBean {

    private List<Step> steps = new ArrayList<Step>();
    FacesContext ctx = FacesContext.getCurrentInstance();

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }
    String contextAddressValue
            = ctx.getExternalContext().getInitParameter("google.json.address");
    private String startingAddress;

    public String getStartingAddress() {
        Map<String, Object> requestCookieMap = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestCookieMap();
        Cookie cookie = (Cookie) requestCookieMap.get("startingAddress");
        if (cookie != null) {
            startingAddress =cookie.getValue();
        }
        return startingAddress;
    }

    public void setStartingAddress(String startingAddress) {
        this.startingAddress = startingAddress;
    }

    public void findDrivingDirections(ActionEvent actionEvent) {
        try {

            //write cookie
            FacesContext.getCurrentInstance()
                    .getExternalContext()
                    .addResponseCookie("startingAddress", startingAddress, null);
            String destination = "1500 Garner Rd, Raleigh, NC 27610";

            Object[] args = {this.startingAddress.trim(), destination.trim()};
            MessageFormat fmt = new MessageFormat("?origin={0}&destination={1}");
            String replaceSpace = fmt.format(args);
            replaceSpace = replaceSpace.replace(" ", "%20");
            //String decoded = java.net.URLEncoder.encode(fmt.format(args), "UTF-8");
            this.contextAddressValue = this.contextAddressValue + replaceSpace;

            URL url = new URL(contextAddressValue);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            int code = conn.getResponseCode();
            String message = conn.getResponseMessage();
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            StringBuffer buffer = new StringBuffer();
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                //System.out.println(output);
                buffer.append(output);
            }

            org.primefaces.json.JSONObject jsonObject = new org.primefaces.json.JSONObject(buffer.toString());
            org.primefaces.json.JSONArray routes = jsonObject.getJSONArray("routes");
            org.primefaces.json.JSONObject legs = routes.getJSONObject(0);
            org.primefaces.json.JSONArray legsArray = legs.getJSONArray("legs");
            org.primefaces.json.JSONObject oo = legsArray.getJSONObject(0);
            org.primefaces.json.JSONArray stepArray = oo.getJSONArray("steps");
            for (int x = 0; x < stepArray.length(); x++) {
                org.primefaces.json.JSONObject rec = stepArray.getJSONObject(x);
                String loc = rec.getString("html_instructions");
                Step step = new Step();
                step.setLeg(loc);
                steps.add(step);
                //int aa = 9;
            }
//org.primefaces.json.JSONObject setObj = jsonObject.getJSONObject("steps");
            // org.primefaces.json.JSONArray stepArrays =jsonObject.getJSONArray("steps");
            conn.disconnect();
        } catch (Exception ex) {
            Logger.getLogger(DrivingDirectionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
