/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.helper;

import com.mobile.dao.DaoFactory;
import com.mobile.dao.RegisteredUserDao;
import java.util.logging.Logger;
import com.mobile.dto.RegisteredUser;
import com.spoledge.audao.db.dao.DaoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 *
 * @author HSC314
 */
public class RegisterUserHelper {
    // use the classname for the logger, this way you can refactor

    private final static Logger LOGGER = Logger.getLogger(RegisterUserHelper.class
            .getName());

    /**
     * add user to database make sure no exception are thrown
     *
     * @param user
     * @return
     */
    public static RegisteredUser add(String email) {
        LOGGER.log(Level.OFF, "started add ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (email == null) {
                LOGGER.log(Level.OFF, "Null user");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Add to database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
            //setting the properties
            RegisteredUser addUser = dao.findByEmail(email);
            if (addUser == null) {
                addUser = new com.mobile.dto.RegisteredUser();
                addUser.setEmail(email);
                java.util.Date date = new java.util.Date();
                addUser.setCreatedDate(date);
                //perform an insert to the database
                Integer primaryKey = null;

                primaryKey = dao.insert(addUser);

                if (primaryKey > 0) {
                    addUser.setRegisteredUserId(primaryKey);
                    LOGGER.log(Level.OFF, "added to database key [" + primaryKey + "]");
                } else {
                    LOGGER.log(Level.SEVERE, "Can not add to user");
                }
            }
            // </editor-fold>
            LOGGER.log(Level.OFF, "end add ");
            return addUser;
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");
        return null;
    }

    /**
     * find user by email
     *
     * @param email
     * @return
     */
    public static RegisteredUser find(String email) {
        LOGGER.log(Level.OFF, "start find");
        // <editor-fold defaultstate="collapsed" desc="Validate parameters">
        if (email == null) {
            LOGGER.log(Level.OFF, "Null email");
        }

        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="find in database">
        Connection connection = null;
        try {

            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
            //setting the properties
            RegisteredUser user = dao.findByEmail(email);
            return user;

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended find ");
        return null;
        // </editor-fold>  
    }

    /**
     * find all users in database
     *
     * @return
     */
    public static com.mobile.dto.RegisteredUser[] findAll() {
        LOGGER.log(Level.OFF, "start findAll ");
        try {
            //get a connection to the database
            Connection connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            RegisteredUserDao dao = DaoFactory.createRegisteredUserDao(connection);
            com.mobile.dto.RegisteredUser[] users = dao.findAll();
            LOGGER.log(Level.OFF, "end findAll ");
            return users;
        } catch (Exception ex) {
            Logger.getLogger(RegisterUserHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.OFF, "end findAll ");
        return null;
    }

}
