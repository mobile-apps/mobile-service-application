/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package data.helper;

import com.mobile.dao.DaoFactory;
import com.mobile.dao.DepartmentDao;
import com.mobile.dao.OrganizationDao;
import com.mobile.dto.Department;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wtccuser
 */
public class DepartmentHelper {
    
    // use the classname for the logger, this way you can refactor
    private final static Logger LOGGER = Logger.getLogger(DepartmentHelper.class
            .getName());
    
    
    
    /**
     * add user to database make sure no exception are thrown
     *
     * @param department
     * @return
     */
    public static Department add(Department department) {
        LOGGER.log(Level.OFF, "started add ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (department == null) {
                LOGGER.log(Level.OFF, "Null Department");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Add to database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            DepartmentDao dao = DaoFactory.createDepartmentDao(connection);
            //setting the properties
            
            Department existingDepartment=null;
            final String queryCheck = "SELECT * from department WHERE organization_id = ? and name = ?";
            final PreparedStatement ps = connection.prepareStatement(queryCheck);
            ps.setInt(1, department.getOrganizationId());
            ps.setString(2, department.getName());
            final ResultSet resultSet = ps.executeQuery();
            if(resultSet.next()) {
                existingDepartment.setDepartmentId(resultSet.getInt(1));
                existingDepartment.setOrganizationId(resultSet.getInt(2));
                existingDepartment.setName(resultSet.getString(3));
                existingDepartment.setLocation(resultSet.getString(4));
                existingDepartment.setEmail(resultSet.getString(5));
                existingDepartment.setCreatedDate(resultSet.getDate(7));
            }
            
            //Department does not exist
            if (existingDepartment == null) {
                java.util.Date date = new java.util.Date();
                department.setCreatedDate(date);
                department.setOrganizationId(department.getOrganizationId());
                
                //perform an insert to the database
                Integer primaryKey = null;

                primaryKey = dao.insert(department);

                if (primaryKey > 0) {
                    department.setDepartmentId(primaryKey);
                    LOGGER.log(Level.OFF, "added to database key [" + primaryKey + "]");
                    return department;
                } else {
                    LOGGER.log(Level.SEVERE, "Cannot add to Department");
                    return null;
                }
            }
            
            //Department already exists
            // </editor-fold>
            LOGGER.log(Level.OFF, "end add ");
            return existingDepartment;
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");
        return null;
    }
    
    public static Department update(Department department) {
        LOGGER.log(Level.OFF, "started update ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (department == null) {
                LOGGER.log(Level.OFF, "Null Department");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Update database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            DepartmentDao dao = DaoFactory.createDepartmentDao(connection);
            //setting the properties
            
            final String queryUpdate = "UPDATE department SET organization_id= ?, name= ?, location=? , email=? WHERE department_id = ?";
            final PreparedStatement ps = connection.prepareStatement(queryUpdate);
            ps.setInt(1, department.getOrganizationId());
            ps.setString(2, department.getName());
            ps.setString(3, department.getLocation());
            ps.setString(4, department.getEmail());
            ps.setInt(5, department.getDepartmentId());
            ps.executeUpdate();
            LOGGER.log(Level.OFF, "updated department");
            return department;
        }
        catch(Exception e){
            LOGGER.log(Level.SEVERE,e.getMessage());
            return null;
                
        }
    }
    
    public static Department delete(Department department) {
        LOGGER.log(Level.OFF, "started delete ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (department == null) {
                LOGGER.log(Level.OFF, "Null Department");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Delete department">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            DepartmentDao dao = DaoFactory.createDepartmentDao(connection);
            //setting the properties
            
            final String queryDelete = "DELETE from department WHERE department_id = ?";
            final PreparedStatement ps = connection.prepareStatement(queryDelete);
            ps.setInt(1, department.getDepartmentId());
            ps.executeUpdate();
            LOGGER.log(Level.OFF, "deleted department");
            return null;
        }
        catch(Exception e){
            LOGGER.log(Level.SEVERE,e.getMessage());
            return null;
                
        }
    }
    
    /**
     * find all departments in database
     *
     * @return
     */
    public static List<com.mobile.dto.Department> findAll() {
        LOGGER.log(Level.OFF, "start findAll ");
        try {
            //get a connection to the database
            Connection connection = data.DatabaseManager.getConnection();
            
            //generated class to manage the table
            DepartmentDao dao = DaoFactory.createDepartmentDao(connection);
            com.mobile.dto.Department[] departments = dao.findAll();
            
            //Convert array to list and return the list
             List<com.mobile.dto.Department> list = Arrays.asList(departments);
            
            LOGGER.log(Level.OFF, "end findAll ");
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DepartmentHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.OFF, "end findAll ");
        return null;
    }
    public static String findByOrganizationId(int id) {
        try {
        Connection connection = data.DatabaseManager.getConnection();
        OrganizationDao orgDao = DaoFactory.createOrganizationDao(connection);
        com.mobile.dto.Organization organization = orgDao.findByPrimaryKey(id);
        return organization.getName();
        }catch(Exception ex){
           Logger.getLogger(DepartmentHelper.class.getName()).log(Level.SEVERE, null, ex); 
        }
        return null;
    }
    
}
