/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package data.helper;

import com.mobile.dao.DaoFactory;
import com.mobile.dao.ServiceDao;
import com.mobile.dao.DepartmentDao;
import com.mobile.dao.CategoryDao;
import com.mobile.dto.Service;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wtccuser
 */
public class ServiceHelper {
    
    // use the classname for the logger, this way you can refactor
    private final static Logger LOGGER = Logger.getLogger(ServiceHelper.class
            .getName());
    
    
    
    /**
     * add user to database make sure no exception are thrown
     *
     * @param service
     * @return
     */
    public static Service add(Service service) {
        LOGGER.log(Level.OFF, "started add ");
        Connection connection = null;
        try {
            // <editor-fold defaultstate="collapsed" desc="Validate parameters">
            if (service == null) {
                LOGGER.log(Level.OFF, "Null Service");
            }

            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Add to database">
            //get a connection to the database
            connection = data.DatabaseManager.getConnection();
            //generated class to manage the table
            ServiceDao dao = DaoFactory.createServiceDao(connection);
            //setting the properties
            
            Service existingService=null;
            final String queryCheck = "SELECT * from service WHERE department_id = ? and category_id = ? and name = ?";
            final PreparedStatement ps = connection.prepareStatement(queryCheck);
            ps.setInt(1, service.getDepartmentId());
            ps.setInt(2, service.getCategoryId());
            ps.setString(3, service.getName());
            final ResultSet resultSet = ps.executeQuery();
            if(resultSet.next()) {
                existingService.setServiceId(resultSet.getInt(1));
                existingService.setDepartmentId(resultSet.getInt(2));
                existingService.setCategoryId(resultSet.getInt(3));
                existingService.setName(resultSet.getString(4));
                existingService.setCreatedDate(resultSet.getTimestamp(5));
                existingService.setStartDate(resultSet.getDate(6));
                existingService.setEndDate(resultSet.getDate(7));
                existingService.setLocation(resultSet.getString(8));
                existingService.setAllowedParticipant(resultSet.getInt(9));
            }
            
            //Department does not exist
            if (existingService == null) {
                java.util.Date date = new java.util.Date();
                service.setCreatedDate(date);
                service.setDepartmentId(service.getDepartmentId());
                
                //perform an insert to the database
                Integer primaryKey = null;

                primaryKey = dao.insert(service);

                if (primaryKey > 0) {
                    service.setServiceId(primaryKey);
                    LOGGER.log(Level.OFF, "added to database key [" + primaryKey + "]");
                    return service;
                } else {
                    LOGGER.log(Level.SEVERE, "Cannot add to Service");
                    return null;
                }
            }
            
            //Service already exists
            // </editor-fold>
            LOGGER.log(Level.OFF, "end add ");
            return existingService;
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        }
        LOGGER.log(Level.OFF, "ended add ");
        return null;
    }
    
    /**
     * find all services in database
     *
     * @return
     */
    public static List<com.mobile.dto.Service> findAll() {
        LOGGER.log(Level.OFF, "start findAll ");
        try {
            //get a connection to the database
            Connection connection = data.DatabaseManager.getConnection();
            
            //generated class to manage the table
            ServiceDao dao = DaoFactory.createServiceDao(connection);
            com.mobile.dto.Service[] services = dao.findAll()
                    ;
            
            //Convert array to list and return the list
             List<com.mobile.dto.Service> list = Arrays.asList(services);
            
            LOGGER.log(Level.OFF, "end findAll ");
            return list;
        } catch (Exception ex) {
            Logger.getLogger(ServiceHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        LOGGER.log(Level.OFF, "end findAll ");
        return null;
    }
    
    public static String findByServiceId(int id) {
        try {
        Connection connection = data.DatabaseManager.getConnection();
        ServiceDao serviceDao = DaoFactory.createServiceDao(connection);
        com.mobile.dto.Service service = serviceDao.findByPrimaryKey(id);
        return service.getName();
        }catch(Exception ex){
           Logger.getLogger(ServiceHelper.class.getName()).log(Level.SEVERE, null, ex); 
        }
        return null;
    }
    
}
