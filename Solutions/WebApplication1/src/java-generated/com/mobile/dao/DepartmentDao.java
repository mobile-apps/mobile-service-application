/*
 * This file was generated - do not edit it directly !!
 * Generated by AuDAO tool, a product of Spolecne s.r.o.
 * For more information please visit http://www.spoledge.com
 */
package com.mobile.dao;

import java.sql.Date;
import java.sql.Timestamp;

import com.spoledge.audao.db.dao.AbstractDao;
import com.spoledge.audao.db.dao.DaoException;

import com.mobile.dto.Department;


/**
 * This is the DAO.
 *
 * @author generated
 */
public interface DepartmentDao extends AbstractDao {

    /**
     * Finds a record identified by its primary key.
     * @return the record found or null
     */
    public Department findByPrimaryKey( int departmentId );

    /**
     * Finds a record.
     */
    public Department findByOrganizationId( int organizationId );

    /**
     * Finds records ordered by name.
     */
    public Department[] findAll( );

    /**
     * Inserts a new record.
     * @return the generated primary key - departmentId
     */
    public int insert( Department dto ) throws DaoException;

}
