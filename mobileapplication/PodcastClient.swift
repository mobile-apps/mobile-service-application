//
//  PodcastClient.swift
//  mobileapplication
//
//  Created by Hope Benziger on 8/25/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation

//return array of podcasts and error code
typealias FetchPodcastCompletion = ([Podcast]?, NSError?) -> Void

class PodcastClient : NSObject{
    
    let hostName : String //the is restful service host name
    //todo this should be read from the configuration file
    init(hostName: String) {
        self.hostName = hostName
    }
    
    //define the url address
    var urlAddress : NSURL {
        var urlpath : String = "http://jbossewsalex-javauniversity.rhcloud.com/rest/mobile/getAllDepartments"
        let url:NSURL = NSURL(string: urlpath)!
        return url
        //    return NSURL(scheme: "http", host: hostName, path: "/rest/mobile/getAllDepartments")! //todo the path should read from config file
    }
    /*
    Make the restful url call and return array of data
    */
    func fetchData ( completionHandler: FetchPodcastCompletion) {
        var request  = NSURLRequest(URL: urlAddress)
        //alwasys make an async http call
        
        
        
    }//end of fetchdata
    
    func fetchPodcasts(completionHandler: FetchPodcastCompletion) {
        println("starting fetchPodcasts")
        var request = NSURLRequest(URL: urlAddress)
        NSURLConnection.sendAsynchronousRequest(
            request, queue: NSOperationQueue.currentQueue()) {
                (response, data, error) in
                //did we encounter an error
                if let error = error {
                    println("fetchPodcasts Error\(error)")
                    completionHandler(nil, error)
                    return
                }
                //try to parse json
                var err: NSError?
                let jsonResult: Dictionary? = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as? Dictionary<String, AnyObject>
                
                //let results: NSArray = jsonResult["results"] as NSArray?
                // var test = jsonResult["results"]
                let jsonA: AnyObject! = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil)
                
                let podcasts = jsonA["podcasts"]! as! [[String : AnyObject]]
                for podcast in podcasts {
                    let title = podcast[Podcast.TitleProperty]! as! String
                    let description = podcast[Podcast.DescriptionProperty]! as! String
                    let mediaLocation = podcast[Podcast.MediaLocationProperty]! as! String
                    let publishDate = podcast[Podcast.PublishDateProperty]! as! String
                    
                    println("podcast: \(title) \(description) \(mediaLocation) \(publishDate)")
                }
                
                
                
                let json: AnyObject? = NSJSONSerialization.JSONObjectWithData(
                    data,
                    options: NSJSONReadingOptions(0),
                    error: &err)
                if let err = err {
                    println("fetchPodcasts Error\(error)")
                    completionHandler(nil, error)
                    return
                }
                //loop thru json object and parse data
                //  let results: NSArray = json["results"]! as NSArray
                
                if let json: AnyObject = json {
                    let podcasts = json["podcasts"]
                    
                    if let json = json as? [String: AnyObject] {
                        //println("fetchDepartments json\(json)")
                        var myPodcasts: NSArray = json["podcasts"] as! NSArray
                        for element in myPodcasts {
                            // print("\(element) ")
                            
                        }
                        
                        var podcasts: [Podcast] = []
                        
                        
                        
                        for (key, item) in json {
                            if let item = item as? [String: String] {
                                /*                               var department = Department(name: item[Department.NameProperty]!,
                                generalInformation: item[Department.GeneralInformationProperty]!)
                                println("depart \(department)")
                                departments.append(department)*/
                            }
                        }//end of for
                        
                        completionHandler(podcasts, nil)
                        return
                    }
                    completionHandler(nil,
                        NSError(domain: "podcast", code: 100, userInfo: nil))
                    return
                } else {
                    completionHandler(nil,
                        NSError(domain: "podcast", code: 101, userInfo: nil))
                    return
                }
        }
    }

    
    
} // end of PodcastClient Class