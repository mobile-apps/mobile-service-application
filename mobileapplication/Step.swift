//
//  Step.swift
//  mobileapplication
//
//  Created by Chitra Rajamanickam on 8/27/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation

typealias Distance = String
typealias Duration = String
typealias Manuever = String
typealias HtmlInstruction = String


public class Step {
    
    class var RootProperty : String {
        get {
            return "steps"
        }
    }
    
    
    //Define json column names used to parse json object
    class var DistanceProperty : String {
        get {
            return "distance"
        }
    }
    
    
    class var DurationProperty : String {
        get {
            return "duration"
        }
    }
    
    
    class var ManueverProperty : String {
        get {
            return "manuever"
        }
    }
    
    
    class var HtmlInstructionProperty : String {
        get {
            return "htmlInstruction"
        }
    }
    
    
    var _distance : String!
    var distance : String {
        get {
            return _distance
        }
        set {
            self._distance = newValue
        }
    }
    
    
    var _duration : String!
    var duration : String {
        get {
            return _duration
        }
        set {
            self._duration = newValue
        }
    }
    
    
    var _manuever : String!
    var manuever : String {
        get {
            return _manuever
        }
        set {
            self._manuever = newValue
        }
    }
    
    
    var _htmlInstruction : String!
    var htmlInstruction : String {
        get {
            return _htmlInstruction
        }
        set {
            self._htmlInstruction = newValue
        }
    }
    
    
    //Constructor
    init (distance : Distance, duration : Duration, manuever : Manuever, htmlInstruction : HtmlInstruction) {
        
        self._distance = distance
        self._duration = duration
        self._manuever = manuever
        self._htmlInstruction = htmlInstruction
        
    }
    
    
    init (distance : Distance, duration : Duration, htmlInstruction : HtmlInstruction) {
        self._distance = distance
        self._duration = duration
        self._htmlInstruction = htmlInstruction
        
    }
    
} //class
