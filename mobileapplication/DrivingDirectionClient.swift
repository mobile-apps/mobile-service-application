//
//  DrivingDirectionClient.swift
//  mobileapplication
//
//  Created by Chitra Rajamanickam on 8/27/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation

//return directions and error code
typealias findDrivingDirectionsCompletion = ([Step]?, NSError?) -> Void

class DrivingDirectionClient : NSObject {
    
    let hostName : String //the is restful service host name
    
    //todo this should be read from the configuration file
    init(hostName: String) {
        
        self.hostName = hostName
        
    }
    
    
    //Define the url address
    var urlAddress : NSURL {
        
        var urlpath : String = "http://jbossewsalex-javauniversity.rhcloud.com/rest/mobile/getGoogleMap"
        
        let url:NSURL = NSURL(string: urlpath)!
        
        return url
        
        //todo the path should read from config file
        //return NSURL(scheme: "http", host: hostName, path: "/rest/mobile/getGoogleMap")!
        
    }
    
    
    func fetchData ( completionHandler: findDrivingDirectionsCompletion) {
        
        var request  = NSURLRequest(URL: urlAddress)
        
        // always make an async http call
        
    } //fetchdata
    
    
    func findDrivingDirections(completionHandler: findDrivingDirectionsCompletion, fromLatitude : Double, fromLongitude : Double, toLatitude : Double, toLongitude : Double) {
        
        globalLog.info("starting findDrivingDirections")
        
        // ?latitude=35.79449833333333 & longitude=-78.60169833333333
        
        var fullURL : String
        fullURL = String(format:"%f", urlAddress)
        fullURL += "?latitudeStart=" + String(format:"%f", fromLatitude)
        fullURL += "&longitudeStart=" + String(format:"%f", fromLongitude)
        fullURL += "&latitudeEnd=" + String(format:"%f", toLatitude)
        fullURL += "&longitudeEnd=" + String(format:"%f", toLongitude)
        
        globalLog.debug("fullURL : " + fullURL)
        
        var request = NSURLRequest(URL: NSURL(string: fullURL)!)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.currentQueue()) {
            
            (response, data, error) in
            
            //try to parse json
            var err: NSError?
            
            let jsonA: AnyObject! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error: nil)
            
            //If error
            if let error = error {
                
                globalLog.error("findDrivingDirections Error: " + error.localizedDescription)
                
                completionHandler(nil, error)
                
                return
                
            } //if let error = error
            
            var returnSteps : [Step] = [] //return Steps
            
            let startingAddress = jsonA[Direction.StartingAddressProperty]! as! [[String : AnyObject]]
            let endingAddress = jsonA[Direction.EndingAddressProperty]! as! [[String:AnyObject]]
            let steps = jsonA[Direction.StepsProperty]! as! [[String:AnyObject]]
            
            for step in steps {
                
                let distanceJSON = step[Step.DistanceProperty]! as! String
                let durationJSON = step[Step.DurationProperty]! as! String
                let htmlInstructionJSON = step[Step.HtmlInstructionProperty]! as! String
                let manueverJSON = step[Step.ManueverProperty]! as! String
                
                let stepObject = Step(distance: distanceJSON, duration: durationJSON, manuever: manueverJSON, htmlInstruction: htmlInstructionJSON)
                
                returnSteps.append(stepObject)
            
            } //for step in steps
                        
            
            globalLog.info("ending findDrivingDirections")
            
        } //sendAsynchronousRequest
        
        
    } //findDrivingDirections
    
    
} //class
