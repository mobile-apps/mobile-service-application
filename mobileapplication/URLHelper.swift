//
//  URLHelper.swift
//  mobileapplication
//
//  Created by wtccuser on 8/29/15.
//  Copyright (c) 2015 wtccuser. All rights reserved.
//

import Foundation
public enum URLAddress
{
    case GetAllDepartments
    case GetAllPodcasts
    case GetDirections
    
}
/**
*/
public class URLHelper : NSObject {
    /*
    *Returns url web address of the json web service
    * The url name must specify an absolute enumeration.     * <p>
    * This method always returns url address, int on the screen.
    *
    *@parm URLAddress name of the json web service
    *@return the url location of the json web service
    */
    class func getURLAddress(urlAddress: URLAddress) -> String {
        
        var rootURL : String = String()
        
        //get the root url address
        if let path = NSBundle.mainBundle().pathForResource("urls", ofType: "plist") {
            if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, String> {
                rootURL  = dict["rootURL"]!
            }
        }//end of if let path
        
        
        var returnURLAddres : String = String()
        switch urlAddress
        {
        case URLAddress.GetAllDepartments:
            if let path = NSBundle.mainBundle().pathForResource("urls", ofType: "plist") {
                if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, String> {
                    returnURLAddres  = dict["url_getAllDepartments"]!
                    returnURLAddres = rootURL + returnURLAddres
                }
            }//end of if let path
            
        case URLAddress.GetDirections:
            
            if let path = NSBundle.mainBundle().pathForResource("urls", ofType: "plist") {
                
                if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, String> {
                    
                    returnURLAddres  = dict["url_directions"]!
                    returnURLAddres = rootURL + returnURLAddres
                    
                } //if let dict
                
            }//if let path
            
        default:
            returnURLAddres = String()
            
        }//end of switch
        
        return returnURLAddres
        //URLHelper.getURLAddress(URLAddress.GetAllDepartments)
        
    }//end of func
    
    
}//end of class
